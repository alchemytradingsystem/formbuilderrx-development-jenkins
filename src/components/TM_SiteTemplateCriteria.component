<apex:component controller="TM_TemplateCriteriaController">
	<apex:attribute name="siteTemplateConditionCriteria" type="RenderCriteriaConditionWrapper" description="criteria object" assignTo="{!criteria}" />
	<style>
		span.slds-form-element__label.custom-margin {
			margin-bottom: -5px;
		}
		
		img.action-img.slds-input__icon.custom-icon {
			cursor: pointer;
		    width: 50px;
		    height: 36px;
		    margin-top: -18px;
		    right: 0px;
		    background-color: #f5f6f7;
		    border: 1px solid #cfd0d2;
		    border-radius: 4px;
		    padding: 6px 14px;
		}
		
        .slds-lookup-Size-modifier ul li {
            display: inline-block;
            font-size: 13px;
            padding: 0;
            margin-left: 0;
            margin-bottom: 0;
            position: relative !important;
        }
        
        .slds-lookup-Size-modifier ul li:after {
            content: "";
            display: block;
            position: absolute;
            right: 0;
            top: 9px;
            width: 1px;
            height: 17px;
            background: #ccc;
        }
        
        .slds-lookup-Size-modifier ul li:last-child:after {
            display: none;
        }
        
        .slds-pill__container.no-border {
        	border:none;
        }
	</style>
	<script>
		function isBlank(value) {
			return (value == undefined || value == null || value.trim() == '');
		}
		
		function isNotBlank(value) {
			return (value != undefined && value != null && value.trim() != '');
		}
		
		var siteTemplatePicklistLookupNewWin = null;
		
        function siteTemplateCloseLookupPopup(indexToUpdate, selectedValueList) {
       		if (null != siteTemplatePicklistLookupNewWin) {              
				siteTemplatePicklistLookupNewWin.close();
			}
			
			var selectedInstance;
			var tempSelectedList = null;
			var tempSelected;
			for (selectedInstance in selectedValueList) {
    			tempSelected = {};
    			tempSelected['id'] = selectedValueList[selectedInstance].value;
    			tempSelected['name'] = selectedValueList[selectedInstance].label;
    			if(tempSelectedList == null) {
    				tempSelectedList = [];
    			}
    			tempSelectedList.push(tempSelected);
			}
			console.log(tempSelectedList);
           	siteTemplateUpdatePicklistValue(indexToUpdate, JSON.stringify(tempSelectedList));
        }
		
		function siteTemplateOpenLookupPopup(index, objectName, referencedObjectName, fieldAPIName, referencedFieldAPIName) { 
            var prefix = '{!JSENCODE(nameSpacePrefix)}';
            var object = '';
            var field = '';
            if(isBlank(referencedObjectName)) {
            	object = objectName;
				field = fieldAPIName;
            } else {
            	object = referencedObjectName;
            	field = referencedFieldAPIName;
            }
            var value = jQuery('input.siteTemplate-picklist-value-' + index)[0].value;
			var url = "/apex/"+prefix+"PortalPackagePicklistValueLookup?objectName=" + object + "&fieldName=" + field + "&fieldValue=" + value + "&index=" + index + "&type=" + "TM_SiteTemplateCriteria" + "&isMultiSelect=false"+"&isFirstBlank=true";
			siteTemplatePicklistLookupNewWin = window.open(url, 'Popup','height=500,width=600,left=100,top=50,resizable=no,scrollbars=yes,toolbar=no,status=no');
			if (window.focus) {
				siteTemplatePicklistLookupNewWin.focus();
			}
			
			siteTemplatePicklistLookupNewWin.onblur = function() { this.close(); };            
                    
            return false;
        }
	</script>
	<!-- Action function to implement default behaviour (disable active checkbox) -->
	<apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateDisableActive" rerender="siteTemplateActiveBtnPanel, siteTemplateCriteriaExpression, siteTemplateCriteriaPanel, popupErrorMessage" action="{!disableActive}">
    </apex:actionFunction>
    
    <!-- Action select to implement active checkbox -->
    <apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateDisplayCriteria" rerender="siteTemplateCriteriaExpression, siteTemplateCriteriaPanel, popupErrorMessage" action="{!activeSelected}">
    </apex:actionFunction>
    
    <!-- Action function to add or remove blank value to criteria -->
    <apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateUpdateCriteria" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!updateCriteriaType}">
    	<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
    </apex:actionFunction>
    
    <!-- Action function to add or remove blank value to criteria -->
    <apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateFetchFieldList" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!updateCriteriaList}">
    	<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
    </apex:actionFunction>
    
    <!-- Action function to set field type for field -->
    <apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateCriteriaSetFieldType" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!setFieldType}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
	</apex:actionFunction>
	
	<!-- Action to set field type for one level deep -->
	<apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateCriteriaSetReferencedFieldType" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!setReferencedFieldType}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
	</apex:actionFunction>
	
	<!-- Action Function to Remove Criteria -->
	<apex:actionFunction onComplete="hideLoadingImage();" name="siteTemplateRemoveCriteria" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!removeCriteria}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
	</apex:actionFunction>
	
	<!-- Action function to get Records for reference type field value -->
	<apex:actionFunction name="siteTemplateGetRecords" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!getRecords}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
	</apex:actionFunction>
	
	<!-- Action function to select reference type field value -->
	<apex:actionFunction name="siteTemplateSelectReferencedValue" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!selectReferencedValue}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
		<apex:param name="selectedValue" value="" assignTo="{!selectedValue}"/>
		<apex:param name="selectedDisplayValue" value="" assignTo="{!selectedDisplayValue}"/>
	</apex:actionFunction>
	
	<!-- Action function to remove selected reference type field value -->
	<apex:actionFunction name="siteTemplateRemoveReferencedValue" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!removeReferencedValue}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
	</apex:actionFunction>
	
	<apex:actionFunction name="siteTemplateUpdatePicklistValue" rerender="siteTemplateCriteriaPanel, popupErrorMessage" action="{!updatePicklistValue}">
		<apex:param name="changeIndex" value="" assignTo="{!changeIndex}"/>
		<apex:param name="tempSelectedList" value="" assignTo="{!picklistSelectedValueList}"/>
	</apex:actionFunction>
	
	<apex:outputpanel id="siteTemplateConditionPanel" layout="block">
		<section>
			<fieldset class="slds-form--compound">
				<div class="form-element__group">
					<div class="slds-form-element__row">
						<!-- Active Checkbox Block Starts -->
						<apex:outputpanel id="siteTemplateActiveBtnPanel" layout="block" styleClass="slds-form-element__control" >
							<span class="slds-form-element__label custom-margin">Active</span>
							<label class="slds-checkbox erx-checkbox-default" style="padding-right:0">
								<apex:inputCheckbox value="{!criteria.isActive}" disabled="{!criteria.isDefault}" onchange="showLoadingImage();siteTemplateDisplayCriteria();"/>
								<span class="slds-checkbox--faux erx-faux"></span>
							</label>
						</apex:outputpanel>
						<!-- Active Checkbox Block Ends -->
						<!-- Default Checkbox Block Starts -->
						<apex:outputpanel id="siteTemplateDefaultBtnPanel" layout="block" styleClass="slds-form-element__control" >
							<span class="slds-form-element__label custom-margin">Set Default</span>
							<label class="slds-checkbox" style="padding-right:0">
								<apex:inputCheckbox value="{!criteria.isDefault}" onchange="showLoadingImage();siteTemplateDisableActive();"/>
								<span class="slds-checkbox--faux erx-faux"></span>
							</label>
						</apex:outputpanel>
						<!-- Active Checkbox Block Ends -->
						<!-- Rendered Criteria Container Starts -->
						<apex:outputpanel id="siteTemplateCriteriaExpression" styleClass="slds-grid" layout="block" style="margin-top:5px;">
							<!-- Rendered Expression Container -->
							<apex:outputpanel styleClass="slds-grow" rendered="{!NOT(criteria.isDefault) && criteria.isActive}" layout="block">
                                <span class="slds-input-has-icon slds-input-has-icon--right">
                                    <img src="{!URLFOR($Resource.SLDS0121, 'assets/icons/utility/filterList_120.png')}" class="slds-input__icon" />
                                    <apex:inputText html-placeholder="Filter Logic ... e.g. ( 1 AND 2 ) OR 3 , if blank default to all AND" 
                                    	styleClass="slds-input" id="criteriaExpression" 
                                    	value="{!criteria.renderedConditionExpression}" />
                                </span>
                            </apex:outputpanel>
                        </apex:outputpanel>
                        <apex:outputpanel layout="block" id="siteTemplateCriteriaPanel">
	                        <apex:outputpanel styleClass="slds-grid" layout="block" style="margin-top:5px;">
								<!-- Rendered Criteria Container Starts -->
								<apex:variable var="index" value="{!0}" />
								<apex:outputpanel styleClass="slds-scrollable--x" layout="block" rendered="{!NOT(criteria.isDefault) && criteria.isActive}">
			                        <table class="slds-table slds-table--bordered slds-max-medium-table--stacked-horizontal">
			                            <thead>
			                                <tr class="slds-text-heading--label">
			                                    <th scope="col">
			                                        <span class="slds-truncate">Order</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Criteria Type</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Object</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Field</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Lookup Field</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Operator</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Value</span>
			                                    </th>
			                                    <th scope="col">
			                                        <span class="slds-truncate">Action</span>
			                                    </th>
			                                </tr>
			                            </thead>
			                            <tbody>
			                            	<apex:repeat value="{!criteria.renderedCondition}" var="condition">
				                            	<tr>
				                            		<td>
			                            				{!index + 1}
				                            		</td>
				                            		<td>
			                            				<apex:selectList size="1" styleClass="slds-select" value="{!condition.criteriaType}" onchange="showLoadingImage();siteTemplateUpdateCriteria({!index});">
			                            					<apex:selectOption itemLabel="--None--" itemValue=""></apex:selectOption>
			                            					<apex:selectOption itemLabel="Object" itemValue="object"></apex:selectOption>
			                            					<apex:selectOption itemLabel="Param" itemValue="param"></apex:selectOption>
			                            				</apex:selectList>
				                            		</td>
				                            		<td>
			                            				<apex:selectList size="1" styleClass="slds-select" value="{!condition.modelName}" rendered="{!condition.criteriaType == 'object'}" onchange="showLoadingImage();siteTemplateFetchFieldList({!index});">
			                            					<apex:selectOption itemLabel="--None--" itemValue=""></apex:selectOption>
		                            						<apex:selectOptions value="{!criteriaObjectSelectOption}"></apex:selectOptions>
			                            				</apex:selectList>
				                            		</td>
				                            		<td>
				                            			<!-- Fields Panel -->
				                            			<apex:outputPanel layout="block" rendered="{!condition.criteriaType == 'object'}">
					                            			<apex:selectList size="1" styleClass="slds-select" value="{!condition.fieldAPIName}" 
					                            				disabled="true" rendered="{!condition.modelName == ''}">
				                            					<apex:selectOption itemLabel="--None--" itemValue=""></apex:selectOption>
			                            					</apex:selectList>
				                            				<apex:selectList size="1" styleClass="slds-select" value="{!condition.fieldAPIName}" 
				                            					onchange="showLoadingImage();siteTemplateCriteriaSetFieldType({!index});" rendered="{!NOT(condition.modelName == '')}">
				                            					<apex:selectOption itemLabel="--None--" itemValue=""></apex:selectOption>
				                            					<apex:selectOptions value="{!describeResultsOutput[condition.modelName]['selectoption']}"></apex:selectOptions>
				                            				</apex:selectList>
			                            				</apex:outputPanel>
			                            				<apex:outputPanel layout="block" rendered="{!condition.criteriaType == 'param'}">
			                            					<apex:inputText html-placeholder="Param Name" styleClass="slds-input" value="{!condition.fieldAPIName}"/>
		                            					</apex:outputPanel>
				                            		</td>
				                            		<td>
				                            			<!-- One Level Deep down field -->
				                            			<apex:outputPanel layout="block" rendered="{!condition.criteriaType == 'object'}">
					                            			<apex:selectList size="1" styleClass="slds-select" value="{!condition.referencedFieldAPIName}" 
					                            				disabled="true" rendered="{!condition.fieldDisplayType != 'reference'}">
				                            					<apex:selectOption itemLabel="--None--" itemValue=""></apex:selectOption>
			                            					</apex:selectList>
			                            					<apex:selectList size="1" styleClass="slds-select" value="{!condition.referencedFieldAPIName}" 
					                            				onchange="showLoadingImage();siteTemplateCriteriaSetReferencedFieldType({!index});" rendered="{!condition.fieldDisplayType == 'reference'}">
				                            					<apex:selectOption itemLabel="--None--" itemValue=""></apex:selectOption>
				                            					<apex:selectOptions value="{!describeResultsOutput[condition.referencedObjectName]['selectoption']}"></apex:selectOptions>
			                            					</apex:selectList>
		                            					</apex:outputPanel>
				                            		</td>
				                            		<td>
				                            			<!-- Operator Panel -->
				                            			<apex:selectList size="1" styleClass="slds-select" value="{!condition.fieldOperator}" rendered="{!condition.criteriaType != ''}">
				                            				<apex:selectOptions value="{!operatorList}"></apex:selectOptions>
				                            			</apex:selectList>
				                            		</td>
				                            		<td>
				                            			<apex:inputText html-placeholder="Param Value" styleClass="slds-input" value="{!condition.value}" 
				                            				rendered="{!condition.criteriaType == 'param'}"/>
				                            			<!-- Show Input Text if Selected Field is not Reference and Picklist -->
				                            			<apex:outputPanel layout="block" rendered="{!condition.criteriaType == 'object'}">
					                            			<apex:inputText styleClass="slds-input" value="{!condition.value}" 
					                            				rendered="{!condition.fieldDisplayType != 'picklist' && condition.fieldDisplayType != 'reference'}"/>
			                            				
				                            				<apex:inputText styleClass="slds-input" value="{!condition.value}" 
					                            				rendered="{!condition.fieldDisplayType == 'reference' && condition.referencedFieldDisplayType != '' && condition.referencedFieldDisplayType != 'picklist' 
					                            					&& condition.referencedFieldDisplayType != 'reference' && condition.referencedFieldDisplayType != 'id'}"/>
		                            						<!-- Input Box for Reference Type Field Starts -->
		                            						<apex:outputPanel layout="block" styleClass="slds-lookup" 
		                            							rendered="{!(condition.fieldDisplayType == 'reference' && condition.referencedFieldDisplayType == '') 
		                            								|| (condition.referencedFieldDisplayType == 'reference') || (condition.referencedFieldDisplayType == 'id')}">
					                                            <apex:outputPanel layout="block" styleClass="slds-form-element" rendered="{!NOT(condition.value != null && condition.value != '')}">
					                                                <apex:outputPanel layout="block" styleClass="slds-form-element__control slds-input-has-icon slds-input-has-icon--right" > 
					                                                    <img src="{!URLFOR($Resource.SLDS0121, '/assets/icons/utility/search_120.png')}" class="action-img slds-input__icon" />
					                                                    <apex:inputText styleClass="slds-input" value="{!condition.lookupSearchString}" onkeyup="siteTemplateGetRecords({!index}); return false;"/>
					                                                </apex:outputPanel>
					                                            </apex:outputPanel>
					                                            <apex:outputPanel styleClass="slds-lookup__menu slds-lookup-Size-modifier" layout="block" rendered="{!condition.lookupSearchString != null && condition.lookupSearchString != ''}">
					                                            	<apex:outputPanel layout="block" rendered="{!condition.lookupRecords != null && condition.lookupRecords.size > 0}">
				                                            			<ul class="slds-lookup__list">
				                                            				<apex:repeat value="{!condition.lookupRecords}" var="lookupRecord">
			                                                                	<li class="slds-lookup__item">
			                                                                    	<a href="#!" onclick="siteTemplateSelectReferencedValue({!index}, '{!JSENCODE(lookupRecord.id)}', '{!JSENCODE(lookupRecord.name)}')">
			                                                                        	{!lookupRecord.name}
																					</a>
																				</li>
																			</apex:repeat>
																		</ul>
					                                            	</apex:outputPanel>
					                                            	<apex:outputPanel layout="block" rendered="{!condition.lookupRecords == null || condition.lookupRecords.size == 0}">
					                                            		<ul class="slds-lookup__list">
					                                            			<li class="slds-lookup__item">
		                                                                        No Records Found
		                                                                    </li>
					                                            		</ul>
				                                            		</apex:outputPanel>
					                                            </apex:outputPanel>
					                                            <!-- Selected Value Panel Starts -->
																<apex:outputPanel layout="block" styleClass="slds-pill__container no-border" rendered="{!condition.value != null && condition.value != ''}">
																	<a href="#" class="slds-pill">
																		<span class="slds-pill__label">{!condition.displayValue}</span>
																		<button class="slds-button slds-button--icon-bare slds-pill__remove" onclick="siteTemplateRemoveReferencedValue({!index}); return false;">
																			<img src="{!URLFOR($Resource.SLDS0121, 'assets/icons/utility/close_120.png')}" class="action-img slds-button__icon" />
																				<span class="slds-assistive-text">Remove</span>
																		</button>
																	</a>
																</apex:outputPanel>
					                                            <!-- Selected Value Panel Ends -->
				                                            </apex:outputPanel>
				                                            <!-- Input Box for Reference Type Field Ends -->
				                                            <!-- Input Box for Picklist Type Field Starts -->
				                                            <apex:outputPanel layout="block" styleClass="slds-lookup" 
			                            						rendered="{!condition.fieldDisplayType == 'picklist' || condition.referencedFieldDisplayType == 'picklist'}">
					                                            <apex:outputPanel layout="block" styleClass="slds-form-element" >
					                                                <div class="slds-form-element__control slds-input-has-icon slds-input-has-icon--right">
			                                                    		<img src="{!URLFOR($Resource.lookupImage)}" 
			                                                    			onclick="siteTemplateOpenLookupPopup({!index}, '{!JSENCODE(condition.modelName)}', '{!JSENCODE(condition.referencedObjectName)}', '{!JSENCODE(condition.fieldAPIName)}', '{!JSENCODE(condition.referencedFieldAPIName)}')" 
			                                                    			class="action-img slds-input__icon custom-icon" />
					                                                    <apex:inputText styleClass="slds-input siteTemplate-picklist-value-{!index}" value="{!condition.value}"/>
					                                                </div>
					                                            </apex:outputPanel>
				                                            </apex:outputPanel>
			                                            <!-- Input Box for Picklist Type Field Ends -->
			                                            </apex:outputPanel>
				                            		</td>
				                            		<td>
				                            			<apex:outputPanel layout="block" styleClass="slds-dropdown-trigger" rendered="{!index != (criteria.renderedCondition.size - 1)}">
				                                            <button type="button" onclick="showLoadingImage();siteTemplateRemoveCriteria({!index});" class="slds-button slds-button--neutral action-button">
				                                                <img src="{!URLFOR($Resource.SLDS0121, 'assets/icons/utility/delete_120.png')}" class="action-img" />
				                                            </button>
				                                            <div class="slds-dropdown slds-dropdown--bottom slds-nubbin--bottom slds-dropdown--menu erx-popover">
				                                                <ul class="slds-dropdown__list">
				                                                    <li class="slds-dropdown__item">
				                                                        <p class="slds-truncate">Delete</p>
				                                                    </li>
				                                                </ul>
				                                            </div>
				                                        </apex:outputPanel>
				                            		</td>
				                            	</tr>
				                            	<apex:variable var="index" value="{!index+1}" />
			                            	</apex:repeat>
		                            	</tbody>
	                            	</table>
								</apex:outputpanel>
								<!-- Rendered Criteria Container Ends -->
	                        </apex:outputpanel>
	                        <!-- Rendered Criteria Container Ends -->
                        </apex:outputpanel>
					</div>
				</div>
			</fieldset>
		</section>
	</apex:outputpanel>
</apex:component>