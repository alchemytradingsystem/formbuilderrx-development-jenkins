<!-- 
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2017-06-05 
	Test Commit for PD-1812.
-->
<apex:component controller="UserConfigureTextController" > 

	<!--  Meta tags -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<apex:stylesheet value="{!URLFOR($Resource.salesforcelight, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
	<apex:stylesheet value="{!URLFOR($Resource.Erx_Portal_Css, 'jquery-ui.css')}" />
	<!-- 09-08-18 Added by Shubham Re PD-3814 -->
	<apex:outputPanel rendered="{!includeBackwardCompatibilityResource}">
		<apex:stylesheet value="{!URLFOR($Resource.Erx_Portal_Css, 'bootstrap.min.css')}" />
	</apex:outputPanel>
	<apex:stylesheet value="{!URLFOR($Resource.Portal_Font_Awsome, 'css/font-awesome.css')}" />
	<apex:stylesheet value="{!URLFOR($Resource.RichTextEditor, 'angular-wysiwyg-master/dist/style.css')}" />
	
	<!--Scripts -->
	<apex:includeScript value="{!$Resource.standardjquery}" />
	<apex:includeScript value="{!URLFOR($Resource.AngularJS_AngularSanitize, 'AngularJS/1.5.0-angular.min.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.Bootstrap_JS, 'BootstrapJS/3.3.6-bootstrap.min.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.RichTextEditor, 'angular-wysiwyg-master/dist/bootstrap-colorpicker-module.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.RichTextEditor, 'angular-wysiwyg-master/dist/angular-wysiwyg.js')}" />
	<style>
	.minDiv {
		margin: 1.2%;
	}
	.btn-sm{
		padding:5px 10px;
		font-size:12px;
		line-height:1.5;
		border-radius:3px;
	}
	
	.btn-group{
		position:relative;
		display:inline-block;
		vertical-align:middle;
	}
	.form-control {
	    display: block;
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    font-size: 14px;
	    line-height: 1.42857143;
	    color: #555;
	    background-color: #fff;
	    background-image: none;
	    border: 1px solid #ccc;
	    border-radius: 4px;
    }
	</style>
	
	<apex:attribute name="envRecord" type="string"
		description="user configuration information for Env"
		assignTo="{!envRecordJSON}" />
	
	<!-- Main Body Starts -->
	<div class="slds" ng-app="portalRegistrationApp"
		ng-controller="portalRegistrationController">
		<div class="slds-card">
			<header class="slds-card__header slds-grid">
				<div class="slds-media slds-media--center slds-has-flexi-truncate">
					<div class="slds-media__figure"></div>
					<!--               <div class="slds-media__body">
		                <h3 class="slds-text-heading--small slds-truncate">Self Registration Message</h3>
		              </div> -->
				</div>
				<div class="slds-no-flex">
					<div class="slds-button-group">
	
						<button class="slds-button slds-button--brand"
							ng-click="addAddAnotherButton();">Save</button>
						<a href="Portal_RegistrationSetting"
							class="slds-button slds-button--neutral slds-button--small"> <!-- <input type="button" value="Back"  onclick=""> -->
							Back <!-- </input> -->
						</a>
					</div>
				</div>
			</header>
			<div class="slds-card minDiv">
				<section class="slds-card__body">
	
					<div class="slds-form--stacked col-Padding">
						<div class="slds-media slds-media--center slds-has-flexi-truncate">
							<div class="slds-media__body">
								<h3 class="slds-text-heading--small slds-truncate">Login
									Page:</h3>
							</div>
						</div>
						<div class="slds-form-element">
							<label class="slds-form-element__label">Login Button Label</label>
							<div class="slds-form-element__control">
								<input type="text"
									ng-model="Env_Wrapper[namespacePrefix+'Login_Button_Label__c']"
									class="slds-input" id="selectedfieldInstruction"
									name="selectedfieldInstruction" />
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Upper Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Login_User_Configure_Text__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Lower Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Login_User_Configure_Text_Another__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="slds-card minDiv">
				<section class="slds-card__body">
					<div class="slds-form--stacked col-Padding">
						<div class="slds-media slds-media--center slds-has-flexi-truncate">
	
							<div class="slds-media__body">
								<h3 class="slds-text-heading--small slds-truncate">Registration
									Page:</h3>
							</div>
						</div>
						<div class="slds-form-element">
							<label class="slds-form-element__label">Register Button
								Label</label>
							<div class="slds-form-element__control">
								<input type="text"
									ng-model="Env_Wrapper[namespacePrefix+'Registration_button_label__c']"
									class="slds-input" id="selectedfieldInstruction"
									name="selectedfieldInstruction" />
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Upper Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Registration_User_Configure_Text__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Lower Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Registration_User_Configure_Text_Another__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="slds-card minDiv">
				<section class="slds-card__body">
					<div class="slds-form--stacked col-Padding">
						<div class="slds-media slds-media--center slds-has-flexi-truncate">
	
							<div class="slds-media__body">
								<h3 class="slds-text-heading--small slds-truncate">Forgot
									Page:</h3>
							</div>
						</div>
						<div class="slds-form-element">
							<label class="slds-form-element__label">Forgot Button
								Label</label>
							<div class="slds-form-element__control">
								<input type="text"
									ng-model="Env_Wrapper[namespacePrefix+'Forgot_Button_Label__c']"
									class="slds-input" id="selectedfieldInstruction"
									name="selectedfieldInstruction" />
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Upper Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Forget_User_Configure_Text__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Lower Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Forgot_User_Configure_Text_Another__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!--  portal change password message configuration -->
			<div class="slds-card minDiv">
				<section class="slds-card__body">
					<div class="slds-form--stacked col-Padding">
						<div class="slds-media slds-media--center slds-has-flexi-truncate">
							<div class="slds-media__body">
								<h3 class="slds-text-heading--small slds-truncate">Change
									Password Page:</h3>
							</div>
						</div>
						<div class="slds-form-element">
							<label class="slds-form-element__label">Change Password
								Button Label</label>
							<div class="slds-form-element__control">
								<input type="text"
									ng-model="Env_Wrapper[namespacePrefix+'Change_Password_Button_Label__c']"
									class="slds-input" id="selectedfieldInstruction"
									name="selectedfieldInstruction" />
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Upper Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Change_Password_Configure_Text__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
	
	
						<div class="slds-form-element">
							<label class="slds-form-element__label">Lower Text</label>
							<div class="slds-form-element__control">
								<wysiwyg textarea-id="selectedfieldMultipleAppInstruction"
									textarea-class="form-control" textarea-height="180px"
									textarea-name="selectedfieldMultipleAppInstruction"
									ng-model="Env_Wrapper[namespacePrefix+'Change_Password_Configure_Text_Another__c']"
									textarea-menu="[['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'], ['font'], ['font-size'], ['font-color', 'hilite-color'], ['remove-format'], ['left-justify', 'center-justify', 'right-justify'], ['code']]">
								</wysiwyg>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!--  portal change password message configuration -->
		</div>
	</div>
       
	<script>
		var portalRegistrationApp = angular.module("portalRegistrationApp", ['colorpicker.module', 'wysiwyg.module']);
		portalRegistrationApp.controller('portalRegistrationController', function($scope,$timeout, $window) {
			$scope.Env_Wrapper = JSON.parse('{!JSENCODE(envRecordJSON)}');
			$scope.namespacePrefix = '{!JSENCODE(getPrefix)}';
		
			/* 
		                    method to configure add another buttons
			 */
			$scope.addAddAnotherButton = function(){
				// Calling Remote Action to Get Object.
				Visualforce.remoting.Manager.invokeAction(
						'{!$RemoteAction.UserConfigureTextController.saveData}',
						JSON.stringify($scope.Env_Wrapper),
						function(result, event){
							if (event.status) {
								// Update Selected Dropdown Values List.
								alert(result);
							} else if (event.type === 'exception') {
								alert('An error has occurred while saving the page.')
							} else {
								alert('Unable to connect with server. Try again later.')
							}                            
						}, 
						{escape: true}
				);
			}
		});
		
		// To remove scroll bar 
		$( document ).ready(function() {
			$('[id*= "mainBody"]').css('max-height','none');
			$('[id*= "mainBody"]').css('overflow-y','hidden');
		
		});
	</script>

</apex:component>