/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Trigger on Env__c
*/
trigger preventOperationOnEnvironment on Env__c (before insert,before update, after insert, after update,before delete) {
     //stop insert or update or delete of env record from backend
     if(trigger.isInsert || trigger.isUpdate) { 
         if(trigger.isBefore) {
	         for(Env__c env : Trigger.new) {
	              if(!ERx_Portal_EnvPanelController.isFromAdminPanel) {
	                  env.addError('Insertion/Updation not allowed. Please use admin panel');             
	              }
	         }
         }        
     }  else if(trigger.isDelete) {
         for(Env__c env : Trigger.old) {
             if(!ERx_Portal_EnvPanelController.isFromAdminPanel) {
                 env.addError('Deletion not allowed. Please use admin panel');             
             }
         }
     }    
     
}