trigger EnvTrigger on Env__c ( after insert, before update) {
 //Added for PD-2729: make editing configurable in live environment
   FormBuilder_Settings__c editing = FormBuilder_Settings__c.getValues('Admin Settings');
 
   if(editing !=null && !editing.Allow_editing__c && 
    		(Trigger.isUpdate && Trigger.isBefore )) {
     LiveEnv.preventingEditing(Trigger.old,Trigger.new,PortalPackageHelper.getliveEnvIds(new Set<Id>()));
    }
    
    // PD-1966 Portal Registration: rearrange order of all fields | Adding portal login custom fields for the first time when an env is created
    if(trigger.isAfter && trigger.isInsert) {
    	List<Portal_Login_Custom_Field__c> defaultPortalRecordsList = new List<Portal_Login_Custom_Field__c>();
        Map<String,String> portalLoginBasicFields = new Map<String,String>{'firstname' => 'First Name' ,'lastname' => 'Last Name','email' => 'Email','password' => 'Password','confirmpassword' => 'Confirm Password'};
        Integer fieldOrder = 0;
        Portal_Login_Custom_Field__c defaultPortalRecords = new Portal_Login_Custom_Field__c();
        for(Env__c env : Trigger.new) {
         	for(String fieldVal : portalLoginBasicFields.keySet()) {
         		//PD-3036 corrected function parameter
	            defaultPortalRecords = ERx_PortalPackUtil.prepareDefaultCustomLoginFields(env.Id,fieldVal,portalLoginBasicFields.get(fieldVal),fieldOrder++);
	            defaultPortalRecordsList.add(defaultPortalRecords);
         	}
	    }
	    if(defaultPortalRecordsList != null && defaultPortalRecordsList.size() > 0) {
	    	insert defaultPortalRecordsList;
	    }
	}
        
}