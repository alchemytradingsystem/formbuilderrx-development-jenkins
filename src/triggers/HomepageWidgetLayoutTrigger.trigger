trigger HomepageWidgetLayoutTrigger on Homepage_Widget_Layout__c (after insert, before delete, before update) {
  	//Added for PD-2729: make editing configurable in live environment
    FormBuilder_Settings__c editing = FormBuilder_Settings__c.getValues('Admin Settings');
  	// Changed parameters in preventingDeletingEditing Re PD-3347 
    if(Trigger.isDelete && Trigger.isBefore){
        LiveEnv.preventingDeletingEditing(Trigger.old,Trigger.new,PortalPackageHelper.getliveEnvIds(new Set<Id>()));
    }else if((Trigger.isInsert && Trigger.isAfter) && editing !=null && !editing.Allow_editing__c){
   		 	LiveEnv.preventingDeletingEditing(Trigger.new,Trigger.old,PortalPackageHelper.getliveEnvIds(new Set<Id>()));
    } 
    else if((Trigger.isUpdate && Trigger.isBefore) && editing !=null && !editing.Allow_editing__c) {
   		 	LiveEnv.preventingEditing(Trigger.old,Trigger.new,PortalPackageHelper.getliveEnvIds(new Set<Id>()));
    }
  
}