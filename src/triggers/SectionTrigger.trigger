/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Trigger on Erx_Section__c
*/
trigger SectionTrigger on Erx_Section__c (after insert, before delete, before update) {
    Set<Id> liveSectionIds = null;
    List<Erx_Section__c> sectionList = null;
    //PD-3379 New "Allow Editing" custom setting has no effect - To allow editing of pages in live env
    FormBuilder_Settings__c editing = FormBuilder_Settings__c.getValues('Admin Settings');
    //section list and live section ids
    if(Trigger.isDelete && Trigger.isBefore) { 
        liveSectionIds = PortalPackageHelper.getLiveSectionIds(Trigger.old);
        sectionList = Trigger.old;
    } else if (Trigger.isUpdate && Trigger.isBefore) {
        liveSectionIds = PortalPackageHelper.getLiveSectionIds(Trigger.new);
        sectionList = Trigger.new; 
    } else if (Trigger.isInsert && Trigger.isAfter) {
        liveSectionIds = PortalPackageHelper.getLiveSectionIds(Trigger.new); 
        sectionList = Trigger.new;
    }
    //stop edit or update of live section
    for(Erx_Section__c section : sectionList) {
        if(liveSectionIds.contains(section.Id) && !editing.Allow_editing__c) {
            section.addError(PortalPackageConstants.SECTION_UPSERTION_DELETION_ERROR_MESSAGE);
        }
    }
}