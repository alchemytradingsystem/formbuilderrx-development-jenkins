trigger CreatePortalUserTrigger on Contact (before insert,before update,after insert, after update) {
    
    CreatePortalUserTriggerHandler handler = new CreatePortalUserTriggerHandler(Trigger.isExecuting);
    
    if(( Trigger.isInsert || Trigger.isUpdate ) && Trigger.isBefore){
        // After Insert
        handler.onBeforeInsertOrUpdate(Trigger.new);
    }else if(Trigger.isInsert && Trigger.isAfter){
        // After Insert
        handler.onAfterInsert(Trigger.new);
    }else if(Trigger.isUpdate && Trigger.isAfter){
        // After Update
        handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
     
} 