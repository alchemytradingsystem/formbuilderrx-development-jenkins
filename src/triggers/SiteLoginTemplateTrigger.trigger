/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Trigger on SiteLoginTemplate__c
*/
trigger SiteLoginTemplateTrigger on SiteLoginTemplate__c (after insert, before delete, before update) {
   
   //Added for PD-2729: make editing configurable in live environment
    FormBuilder_Settings__c editing = FormBuilder_Settings__c.getValues('Admin Settings');
   // Changed parameters in preventingDeletingEditing Re PD-3347 
   if(Trigger.isDelete && Trigger.isBefore){
        LiveEnv.preventingDeletingEditing(Trigger.old,Trigger.new,PortalPackageHelper.getLiveTemplateIds(Trigger.oldMap.keySet()));
    }else if(editing !=null && !editing.Allow_editing__c && 
    		((Trigger.isInsert && Trigger.isAfter) || (Trigger.isUpdate && Trigger.isBefore))) {
    	LiveEnv.preventingDeletingEditing(Trigger.new,Trigger.old,PortalPackageHelper.getLiveTemplateIds(Trigger.newMap.keySet()));
    	}
   }