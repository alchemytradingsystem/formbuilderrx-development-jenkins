trigger PortalRegistrationMessageTrigger on Portal_Registration_Message__c ( before update) {
    FormBuilder_Settings__c editing = FormBuilder_Settings__c.getValues('Admin Settings');
	// Changed parameters in preventingDeletingEditing Re PD-3347	
     if(editing !=null && !editing.Allow_editing__c && 
    		((Trigger.isUpdate && Trigger.isBefore))) {
    	LiveEnv.preventingDeletingEditing(Trigger.new,Trigger.old,PortalPackageHelper.getliveEnvIds(new Set<Id>()));
    }
}