/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Admin Panel Controller class to create page and assosiate with environment.(Class is global to work in lightning)
*/
 global with sharing class PortalPageConfigureController {
    /****
    * @description GlobalDescribeMap store sobject describe type
    */
    private Map < String, Schema.SObjectType > GlobalDescribeMap = null;
    /****
    * @description store sobject type
    */
    private Schema.SObjectType SObjectTypeObj = null;
    /****
    * @description store sobject type
    */
    private List<Schema.FieldSetMember> fieldSetMember;
    /****
    * @description store filterFieldsDisplayList
    */
    private List<String> filterFieldsDisplayList;
    /****
    * @description map to store filter field 
    */
    private Map<String, String> filterFieldsDisplayMap;

    /****
    * @description store filter field display list
    */
    public String filterFieldsDisplayListString {get;set;}
    /****
    * @description store filter field display map
    */
    public String filterFieldsDisplayMapString {get;set;}
    /****
    * @description store pop up error messages
    */
    public String popupErrorMessage {get;set;}
    /****
    * @description store page error messages
    */
    public String pageErrorMessage {get;set;}
    /****
    * @description store current page name
    */
    public String pageName {get;set;}
    /****
    * @description store env id
    */
    public ID envId {get;set;}
    /****
    * @description active or not
    */
    public Boolean isActive {get;set;}
    /****
    * @description page id that need to be deleted
    */
    public String pageIdToBeDeleted {get;set;}
    /****
    * @description env name that need to be searched
    */
    public String envNameToSearch {get;set;}
    /****
    * @description package prefix
    */
    public String packageName {get;set;}
    /****
    * @description store page json
    */
    public String visualForcePagesJson {get;set;}
    /****
    * @description used whilepage load  thats why public
    */
    public Current_Env__c curEnv {get;set;}
       /****
    * @description To store Permission error message
    */
    public String permissionErrorMessage;
    
    public String pageId {get;set;}
    
    /****
    * @description To store user time zone id
    */
    public String userTimeZoneId {get;set;}
    /****
    * @description map of page id and valid or not
    */
    public String validPageOrNot {get;set;}
    
    private FormBuilder_Settings__c formBuilderSetting;
    /****
    * @description init instance variable
    */
    public PortalPageConfigureController() {
        try{
            // Getting all visualforce pages for custom page linking.
            List<String> visualForcePages = new List<String>();
            List<ApexPage> lstpages = [select name from ApexPage limit: limits.getLimitQueryRows()];
            for(ApexPage apexPage : lstpages) {
                visualForcePages.add(apexPage.name);
            }
            System.debug('@@@@@@@@@@@@@@1');
            visualForcePagesJson = JSON.serialize(visualForcePages);
            // Getting current selected env id
            curEnv = ERx_PortalPackUtil.getCurrentEnvForConfiguration();
            
            
            if(curEnv != null) {
                envId = curEnv.Env__c;
            }
            System.debug('@@@@@@@@@@@@@@2');
            
            // getting package name
            packageName = PortalPackageUtility.getNameSpacePerfix();
            if(packageName  != null && packageName  != ''){
                packageName  =  packageName+'__' ;
            }
            
            GlobalDescribeMap = Schema.getGlobalDescribe();
            SObjectTypeObj = GlobalDescribeMap.get(packageName + 'Erx_Page__c');
            if(SObjectTypeObj == null) {
                throw new PortalPackageException('Invalid Object. Object does not exists.');
            }
            System.debug('@@@@@@@@@@@@@@3');
            //getting the field set fields
            fieldSetMember = PortalPackageUtility.readFieldSetbyFieldSetNameAndObj((packageName + 'displayed_field_columns'), SObjectTypeObj);
            System.debug('@@@@@@@@@@@@@@4');
            // Creating List and Map for Displaying on Page in a Table format.
            filterFieldsDisplayMap = new Map<String, String>();
            filterFieldsDisplayList = new List<String>();
            /*filterFieldsDisplayMap.put((packageName + 'Page_Name__c'), 'Page Name');
            filterFieldsDisplayList.add((packageName + 'Page_Name__c')); */
            if(fieldSetMember != null) {
                for(Schema.FieldSetMember fsm: fieldSetMember){
                    //if(fsm.getFieldPath().trim().toUpperCase() != (packageName + 'Page_Name__c').toUppercase()) {
                        filterFieldsDisplayMap.put(fsm.getFieldPath(), fsm.getLabel());
                        filterFieldsDisplayList.add(fsm.getFieldPath());
                    //}
                }
            }
            System.debug('@@@@@@@@@@@@@@5');
            filterFieldsDisplayListString = JSON.serialize(filterFieldsDisplayList);
            filterFieldsDisplayMapString = JSON.serialize(filterFieldsDisplayMap);
            pageId = '';
            //Getting Permission error message
            formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
            if(formBuilderSetting != null && formBuilderSetting.Permission_Error_Message__c != null) {
              permissionErrorMessage = formBuilderSetting.Permission_Error_Message__c;
            } else {
              permissionErrorMessage = PortalPackageConstants.PERMISSION_ERROR_MESSAGE;
            }
            userTimeZoneId = UserInfo.getTimeZone().getID();
        } catch(Exception e) {
            //System.assert(false,e+'@@@@@@@@@@@'+e.getLineNumber());
        }
    }
     
    /**
     * @description Method is used to check for the paid non paid to 
     *              avoid current environment prob(Method is global to work in lightning)
     * @return Page Reference of Home Page if the user is non paid
     * @added By metacube
     */
    @RemoteAction  
    global static String checkIfPaidUser(String envType){
        String pg = '';
        try{
            PackageSecurity pSecure = new PackageSecurity();
            
            
            if(!pSecure.isPaid){
                if(envType.toUpperCase() == 'COMMUNITY'){
                    pg = 'Portal_Home';
                }
            }
        }catch(Exception e){
            ERx_PortalPackageLoggerHandler.addException(e,'PortalPageConfigureController','checkIfPaidUser','No Page Id');
            ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
        return pg;
    }
    
    
    /**
     * @description Method is used to Create Query to fetch the all the page records
     * @return Query to fetch all the records
     */
    private String createQuery(){
        String query = 'SELECT ID,';
        if(filterFieldsDisplayList != null) {
            for(String field : filterFieldsDisplayList){
                //modify by sandeep
                if(field != packageName + 'is_Custom_Page__c' && field != packageName + 'Is_Desktop_View_In_Mobile__c' && field != packageName + 'Custom_Page_Name__c' && field != packageName + 'is_Custom_Header_Footer__c' && field != packageName + 'IsActive__c' && field != packageName + 'Env__c' && field != packageName + 'Order__c') {
                    if(field == 'CreatedDate' && !PortalPackageUtility.isElementFoundInList(filterFieldsDisplayList, 'CreatedBy.name')) {
						query += 'CreatedBy.Name' + ',';
                    } else if(field == 'LastModifiedDate' && !PortalPackageUtility.isElementFoundInList(filterFieldsDisplayList, 'LastModifiedBy.Name')) {
                    	query += 'LastModifiedBy.Name' + ',';
                    }
                    query += field + ',';
                }
            }
            //Added by sandeep
            // 11-05-18 Added by Shubham Re PD-2198 query page name displayed
            //PD-4017 | Gourav | added custom setting to avoid the page list schema notification and optimization of query
            if(formBuilderSetting != null){
            	if(formBuilderSetting.Avoid_Page_List_Schema_Notification__c){
            		query += 'Is_Desktop_View_In_Mobile__c,is_Custom_Page__c,Page_Name_Displayed__c,ModelList__c,Don_t_Include_In_Checklist__c ,Custom_Page_Name__c,pageParameter__c, Order__c, is_Custom_Header_Footer__c, IsActive__c, Env__c,';
            	}else{
            		query += 'Is_Desktop_View_In_Mobile__c,is_Custom_Page__c,Page_Name_Displayed__c,ModelList__c,Don_t_Include_In_Checklist__c ,Custom_Page_Name__c,pageParameter__c, Order__c, is_Custom_Header_Footer__c, IsActive__c, Env__c,(Select Id, Modal_Data__c,View_Layout__c,View_Layout_1__c,View_Layout_2__c From Erx_Sections__r order by Order__c),';
            	}
            }
            
        }
        
        return query.subString(0,query.length()-1) + ' FROM ' + PortalPackageUtility.escapeQuotes(packageName) + 'Erx_Page__c where Env__c = \'' + PortalPackageUtility.escapeQuotes(envId) + '\' order by Order__c asc';
        
    }
    
    /**
     * @description Method Execute the query and return the result.
     * @return JSON String fetched records.
     */
    public String getRecords() {
    	String returnString = JSON.serialize(new List<Erx_Page__c>());
        try {
        	list<String> listFields = new list<String>{'Is_Desktop_View_In_Mobile__c','is_Custom_Page__c','ModelList__c','Don_t_Include_In_Checklist__c','Custom_Page_Name__c','pageParameter__c','Order__c','is_Custom_Header_Footer__c','IsActive__c','Env__c'};
    	 	if(filterFieldsDisplayList != null) {
        		for(String field : filterFieldsDisplayList){
                	if(field != packageName + 'is_Custom_Page__c' && field != packageName + 'Custom_Page_Name__c' && field != packageName + 'is_Custom_Header_Footer__c' && field != packageName + 'IsActive__c' && field != packageName + 'Env__c' && field != packageName + 'Order__c') {
                   		if(!field.contains('.')) {
                   			listFields.add(field);
                   		}
                	}
            	}
        	}
        	// If User has read permission to page object then we execute query otherwise return blank.
            if(String.isNotBlank(envId)) {
            	if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_ACCESS, packageName)) {
	            	if(SecurityEnforceUtility.checkFieldAccess('Erx_Page__c', listFields, SecurityTypes.IS_ACCESS, packageName)){
	            		List<Erx_Page__c> pageList = Database.query(createQuery());
	            		checkDeprectedField(pageList);
	            		returnString = JSON.serialize(pageList);	
	            	} else {
	            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::Erx_Page__c' +' FieldList :: ' +listFields), 'PortalPageConfigureController', 'getRecords', null);
	            		throw new PortalPackageException(permissionErrorMessage);
	            	} 
            	} else {   
	            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::Erx_Page__c'), 'PortalPageConfigureController', 'getRecords', null);
	            	throw new PortalPackageException(permissionErrorMessage);
	            }
            } else {
            	throw new PortalPackageException('No Env found or selected');
            }
        }catch (QueryException e) {
            ERx_PortalPackageLoggerHandler.addException(e,'PortalPageConfigureController','getRecords','No Page Id');
            ERx_PortalPackageLoggerHandler.saveExceptionLog();
        } 
        return returnString;
    } 
    
    /**
     * @description Method is used check deprected field  
     * @param contain page list 
     */
    public void checkDeprectedField(List<Erx_Page__c> pageList){
    	try{ 
    		Map<String,boolean> validPageOrNotMap = new Map<String,boolean>();
	    	for(Erx_Page__c page : pageList){
	    		//PD-4017 | Gourav | added custom setting to avoid the page list schema notification
	    		if(formBuilderSetting.Avoid_Page_List_Schema_Notification__c){
	    		 	validPageOrNotMap.put(page.id,false);
	    		}else{
	    			validPageOrNotMap.put(page.id,PortalPackageHelper.getCurrentPageStatus(page));
	    		}
	    	}
	    	validPageOrNot = JSON.serialize(validPageOrNotMap);
    	}catch(Exception e){
		 	ERx_PortalPackageLoggerHandler.addException(e,'PortalPageConfigureController','checkDeprectedField','No Page Id');
    	}
    }
    /**
     * @description Method is used to clone new page.(Method is global to work in lightning)
     * @param pageObject json of newly cloned page
     * @return Success Map or Error Map.
     */
    @RemoteAction
    global static Map<String, String> cloneNewPage(String pageObject, String paramList) {
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        //Getting Permission Error message
        String permissionErrorMessage;
        FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
        if(formBuilderSetting != null && formBuilderSetting.Permission_Error_Message__c != null) {
            permissionErrorMessage = formBuilderSetting.Permission_Error_Message__c;
        } else {
            permissionErrorMessage = PortalPackageConstants.PERMISSION_ERROR_MESSAGE;
        }
        Map<String, String> returnString = new Map<String, String>();
        try {           
	        // deserializing page object JSON.
	        PageObjWrapper pageObj = (PageObjWrapper) JSON.deserialize(pageObject, PageObjWrapper.class);
        
            if(String.isNotBlank(pageObj.clonePageFrom)){
            	// Creating New Page Instance
	            String nameSpacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
	            Erx_Page__c new_page = new Erx_Page__c();
	            list<Erx_Page__c> pageList = new list<Erx_Page__c>();
	            //creating new instance for section 
	            list<Erx_Section__c> sectionObjLst = new list<Erx_Section__c>();
	    		//get page and its child sections to copy data from existing page
	    		//list of fields for ERX PAGE
	    		List<String> fieldsListErxPage = new List<String>{'Is_Desktop_View_In_Mobile__c','order__c','IsActive__c','Env__c','Don_t_Include_In_Checklist__c','ModelList__c','Page_Buttons__c',
	    			'Disable_Criteria__c','pageParameter__c','is_Custom_Page__c','Custom_Page_Name__c','is_Custom_Header_Footer__c','Page_Name__c','Page_Name_Displayed__c','Description__c','Id',
	    			'Content_Display_Condition__c','Error_Message_Location__c','Page_Completion__c','Page_Header__c',
	    			'Page_Tab_Indexing__c','Redirect_Criteria__c','Rendered_Criteria__c','Created__c','LastModified__c'};
    			//list of fields for SECTION
    			List<String> fieldsListSection = new List<String>{'Erx_Page__c','Modal_Data__c','Order__c','View_Layout__c','View_Layout_1__c','View_Layout_2__c'};
    			 
    			//check object access for ERX_PAGE and ERX_SECTION
    			if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_ACCESS, nameSpacePrefix) && SecurityEnforceUtility.checkObjectAccess('Erx_Section__c', SecurityTypes.IS_ACCESS, nameSpacePrefix)){
    				if(SecurityEnforceUtility.checkFieldAccess('Erx_Page__c', fieldsListErxPage, SecurityTypes.IS_ACCESS, nameSpacePrefix) && SecurityEnforceUtility.checkFieldAccess('Erx_Section__c', fieldsListSection, SecurityTypes.IS_ACCESS, nameSpacePrefix)){
    					//escape single quotes to avoid SOQL injection
    					String pageId = PortalPackageUtility.escapeQuotes(pageObj.clonePageFrom);
    					pageList = [SELECT id,order__c,IsActive__c,Env__c,Don_t_Include_In_Checklist__c,ModelList__c,Page_Buttons__c,Disable_Criteria__c,pageParameter__c,
    					is_Custom_Page__c,Custom_Page_Name__c,is_Custom_Header_Footer__c,Page_Name__c,Page_Name_Displayed__c,Description__c,Content_Display_Condition__c,Error_Message_Location__c,
    					Page_Completion__c,Page_Header__c,Page_Tab_Indexing__c,Redirect_Criteria__c,Rendered_Criteria__c,Is_Desktop_View_In_Mobile__c,LastModified__c,Created__c,
    					(SELECT Erx_Page__c,Modal_Data__c,Order__c,View_Layout__c,View_Layout_1__c,View_Layout_2__c FROM Erx_Sections__r) FROM Erx_Page__c WHERE id =: pageId];
    				
    				}else{
	    				throw new PortalPackageException(permissionErrorMessage);
	    			}
    			}else{
    				throw new PortalPackageException(permissionErrorMessage);
    			}
    			
    			//copy existing data from page to be cloned
    			if(pageList.size() > 0){
					  	new_page = pageList[0].clone(false,false,false,false);
    			}
    			
    			new_page.order__c = pageObj.pageOrder;
	            new_page.IsActive__c = pageObj.isActive;
	            new_page.Env__c = pageObj.envId;
	            new_page.Don_t_Include_In_Checklist__c   = pageObj.includeinCheckListOrNot;
	            // setting default value
	            new_page.is_Custom_Page__c = false;
	            new_page.Custom_Page_Name__c = '';
	            new_page.is_Custom_Header_Footer__c = false;
	            new_page.Page_Name__c = pageObj.pageName;
	            new_page.Page_Name_Displayed__c = pageObj.pageNameDisplayed;
	            new_page.Description__c = pageObj.description;
	            new_page.Is_Desktop_View_In_Mobile__c = pageObj.isDesktopViewInMobile;
    			
	            //Permission Check before insert Erx Page
	        	if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_INSERT, nameSpacePrefix) 
	        		&& SecurityEnforceUtility.checkObjectAccess('Erx_Section__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
		        		if(SecurityEnforceUtility.checkFieldAccess('Erx_Page__c',fieldsListErxPage, SecurityTypes.IS_INSERT, nameSpacePrefix)
		        			&& SecurityEnforceUtility.checkFieldAccess('Erx_Section__c',fieldsListSection, SecurityTypes.IS_INSERT, nameSpacePrefix)) {
		        			// 04-10-18 Added by Shubham Re PD-3390
		        			new_page.Created__c = UserInfo.getName() + ' ' + getLocalTime();
            				new_page.LastModified__c = new_page.Created__c;
		        			insert new_page;
		        		} else {
		                    throw new PortalPackageException(permissionErrorMessage);
		                }
	        	} else {
	                throw new PortalPackageException(permissionErrorMessage);
	            }
	            //CREATE SECTION
	            for(Erx_Section__c sectionObj : pageList[0].Erx_Sections__r){
	            	Erx_Section__c section = sectionObj.clone();
	            	section.Erx_Page__c = new_page.id;
	            	sectionObjLst.add(section);
	            }
	            
	            //Permission Check before insert Section
    			insert sectionObjLst;
	            returnString.put('Success', new_page.Id);
            } else {
                throw new PortalPackageException( 'No Page Id Found');
            }
        } catch(Exception e) {
            ERx_PortalPackageLoggerHandler.addException(e,'PortalPageConfigureController','createNewPage','No Page Id');
            ERx_PortalPackageLoggerHandler.saveExceptionLog();
            if(!e.getMessage().contains(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE) && !e.getMessage().contains('PageTrigger')) {
            	returnString.put(e.getMessage(), '');
            } else {
            	returnString.put(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE, '');
            }
        }
        
        return returnString;
    }
    
    /**
     * @description Method is used to create new page.(Method is global to work in lightning)
     * @param pageObject json of newly created page
     * @param page updatedor edit
     * @return Success Map or Error Map.
     */
    @RemoteAction
    global static Map<String, String> createNewPage(String pageObject, Boolean isUpdatePage, String paramList) {
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;

        //Getting Permission Error message
        String permissionErrorMessage;
        FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
        if(formBuilderSetting != null && formBuilderSetting.Permission_Error_Message__c != null) {
            permissionErrorMessage = formBuilderSetting.Permission_Error_Message__c;
        } else {
            permissionErrorMessage = PortalPackageConstants.PERMISSION_ERROR_MESSAGE;
        }
                    
        // deserializing page object JSON.
        PageObjWrapper pageObj = (PageObjWrapper) JSON.deserialize(pageObject, PageObjWrapper.class);
        // Fetching max Page Order
      /*  AggregateResult[] groupedResults = [select max(Order__c)maxOrder from Erx_Page__c where Env__c = :pageObj.envId];
        Integer maxOrder;
        if(groupedResults != null && groupedResults.size() > 0) {
            maxOrder = Integer.valueOf(groupedResults[0].get('maxOrder'));
        }
        if(maxOrder == null) {
            maxOrder = 0;
        }*/
        Map<String, String> returnString = new Map<String, String>();
        try {
            // Creating New Page Instance
            String nameSpacePrefix = PortalPackageUtility.getNameSpacePerfix();
            if(!(String.isBlank(nameSpacePrefix))) {
                nameSpacePrefix = nameSpacePrefix + '__';
            }
            Erx_Page__c new_page = new Erx_Page__c();
            new_page.order__c = pageObj.pageOrder;
            new_page.IsActive__c = pageObj.isActive;
            new_page.Env__c = pageObj.envId;
            new_page.Don_t_Include_In_Checklist__c   = pageObj.includeinCheckListOrNot;
            new_page.Is_Desktop_View_In_Mobile__c = pageObj.isDesktopViewInMobile;
            // setting default value
            new_page.is_Custom_Page__c = false;
            new_page.Custom_Page_Name__c = '';
            new_page.is_Custom_Header_Footer__c = false;
            // Removed ModelList__c, Page_Buttons__c, Disable_Criteria__c Properties Its a Bug whenever page is updated these properties also updated.
            if(pageObj.pageType == 'Custom') {
                new_page.is_Custom_Page__c = true;
                new_page.Is_Desktop_View_In_Mobile__c = false;
                new_page.Custom_Page_Name__c = pageObj.customPageName;
                new_page.is_Custom_Header_Footer__c = pageObj.isCustomHeaderFooter;
                new_page.Page_Name__c = pageObj.customPageName;
                new_page.Page_Name_Displayed__c = pageObj.pageNameDisplayed;
                //Added By Shubham Re PD-3766 description field for pages  
                new_page.Description__c = pageObj.description;
                // If Page Type is custom then updating page parameter list.
                if(paramList != null && paramList != '') {
	                new_page.pageParameter__c = JSON.serialize((List<String>) JSON.deserialize(paramList, List<String>.class));
	            } else {
	                new_page.pageParameter__c = '[]';
	            }
            } else {
                new_page.Page_Name__c = pageObj.pageName;
                new_page.Page_Name_Displayed__c = pageObj.pageNameDisplayed;
                //Added By Shubham Re PD-3766 description field for pages
                new_page.Description__c = pageObj.description;
            }
            if(isUpdatePage) {
                //if request is update request.
                new_page.Id = pageObj.pageId;
            } else {
            	// If Page is newly created then assigning default values.
            	new_page.ModelList__c = '[]';
            	new_page.Page_Buttons__c = '{}';
            	new_page.Disable_Criteria__c = '[]';
            	// If Page is Not custom populated page parameter by default value if page cuustom then its populate in pageObj.pageType == 'Custom' block. 
            	if(pageObj.pageType != 'Custom') {
            		new_page.pageParameter__c = '[]';
            	}
            }
            
            // No Need for Null Check (Removed by Avish)
            // Inserting New Sections Data.
            //Code updated to enforce the security to upsert records, first check object level update & insert access then field level update & insert access for fields to be updated
            /*if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_UPDATE, nameSpacePrefix) && SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
                List<String> fieldsList = new List<String>{'order__c','IsActive__c','Env__c','Don_t_Include_In_Checklist__c','ModelList__c','Page_Buttons__c','Disable_Criteria__c','pageParameter__c'
                ,'is_Custom_Page__c','Custom_Page_Name__c','is_Custom_Header_Footer__c','Page_Name__c','Id'};
                if(SecurityEnforceUtility.checkFieldAccess('Erx_Page__c', fieldsList, SecurityTypes.IS_UPDATE, nameSpacePrefix) && SecurityEnforceUtility.checkFieldAccess('Erx_Page__c',fieldsList, SecurityTypes.IS_INSERT, nameSpacePrefix)) {   
                    upsert new_page;
                } else {
                    throw new PortalPackageException(permissionErrorMessage);
                }
            } else {
                throw new PortalPackageException(permissionErrorMessage);
            }*/
            // Removed upsert opration and divide into insert and update to solve salesforce permission issue.
            List<String> fieldsList = new List<String>{'Is_Desktop_View_In_Mobile__c','order__c','IsActive__c','Env__c','Don_t_Include_In_Checklist__c','ModelList__c','Page_Buttons__c','Disable_Criteria__c','pageParameter__c'
                ,'is_Custom_Page__c','Custom_Page_Name__c','is_Custom_Header_Footer__c','Page_Name__c','Page_Name_Displayed__c','Description__c','Id','LastModified__c','Created__c'};
            if(String.isBlank(new_page.Id)) {
            	if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
            		if(SecurityEnforceUtility.checkFieldAccess('Erx_Page__c',fieldsList, SecurityTypes.IS_INSERT, nameSpacePrefix)) {
            			// 04-10-18 Added by Shubham Re PD-3390
            			new_page.Created__c = UserInfo.getName() + ' ' + getLocalTime();
            			new_page.LastModified__c = new_page.Created__c;
            			insert new_page;
            		} else {
	                    throw new PortalPackageException(permissionErrorMessage);
	                }
            	} else {
                    throw new PortalPackageException(permissionErrorMessage);
                }
            } else {
            	if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_UPDATE, nameSpacePrefix)) {
            		if(SecurityEnforceUtility.checkFieldAccess('Erx_Page__c', fieldsList, SecurityTypes.IS_UPDATE, nameSpacePrefix)) {
            			// 04-10-18 Added by Shubham Re PD-3390
            			new_page.LastModified__c = UserInfo.getName() + ' ' + getLocalTime();
            			update new_page;
            		} else {
	                    throw new PortalPackageException(permissionErrorMessage);
	                }
            	} else {
                    throw new PortalPackageException(permissionErrorMessage);
                }
            }
            returnString.put('Success', new_page.Id);
        } catch(Exception e) {
            ERx_PortalPackageLoggerHandler.addException(e,'PortalPageConfigureController','createNewPage','No Page Id');
            ERx_PortalPackageLoggerHandler.saveExceptionLog();
            if(!e.getMessage().contains(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE) && !e.getMessage().contains('PageTrigger')) {
            	returnString.put(e.getMessage(), '');
            } else {
            	returnString.put(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE, '');
            }
        }
        
        return returnString;
    }
    
    /*
    	04-10-18 Added by Shubham Re PD-3390 get local time
    */
    public static String getLocalTime(){
    	Datetime dateGMT = System.now();
		Datetime dateLocal = Datetime.valueOf(dateGMT);
		return dateLocal.format();
    }
    
    /**
     * @description Method is used to delete the page.(Method is global to work in lightning)
     * @param pageId deleted page id
     * @return isSuccess page deleted successfully or not
     */
    @RemoteAction
    global static Boolean deletePage(String pageId) {
        boolean isSuccess;
        //Getting Permission Error message
        String permissionErrorMessage;
        FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
        if(formBuilderSetting != null && formBuilderSetting.Permission_Error_Message__c != null) {
            permissionErrorMessage = formBuilderSetting.Permission_Error_Message__c;
        } else {
            permissionErrorMessage = PortalPackageConstants.PERMISSION_ERROR_MESSAGE;
        }
        try {
            isSuccess = true;
            // Creating New Page Instance
            String nameSpacePrefix = PortalPackageUtility.getNameSpacePerfix();
            if(!(String.isBlank(nameSpacePrefix))) {
                nameSpacePrefix = nameSpacePrefix + '__';
            }
            Erx_Page__c deletePage;
           
           // If User has read permission to page object then we execute query otherwise return blank.
            if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_ACCESS, nameSpacePrefix) ){
            	deletePage = [Select Id from Erx_Page__c where ID = :pageId];
            } else {
            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::Erx_Section__c and Erx_Page__c'), 'PortalPageConfigureController', 'deletePage', pageId);
   				throw new PortalPackageException(permissionErrorMessage);            
   			}
             
            // Deleting the page..
            if(deletePage != null) {
                //Code updated to enforce the security to delete records, check object level delete access
                if(SecurityEnforceUtility.checkObjectAccess('Erx_Page__c', SecurityTypes.IS_DELETE, nameSpacePrefix)) {
                    delete deletePage;
                } else {
	            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::Erx_Section__c and Erx_Page__c'), 'PortalPageConfigureController', 'deletePage', pageId);
	   				throw new PortalPackageException(permissionErrorMessage);            
	   			}
            }
            
        } catch(Exception e) {
            ERx_PortalPackageLoggerHandler.addException(e,'PortalPageConfigureController','deletePage','No Page Id');
            ERx_PortalPackageLoggerHandler.saveExceptionLog();
            if(!e.getMessage().contains(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE) && !e.getMessage().contains('PageTrigger')) {
            	throw new PortalPackageException(e.getMessage());
            } else {
            	isSuccess = false;
            }
        }
        return isSuccess;
    }
    
    /**
    * FormBuilderRx
    * @company : EnrollmentRx, LLC. - Copyright 2016 All rights reserved.
    * @website : http://www.enrollmentrx.com/
    * @author  Metacube
    * @version 1.0
    * @date   2016-05-13 
    * @description Page Object Wrapper Class
    */
    public class PageObjWrapper {
        public String pageName;
        // 11-05-18 Added by Shubham Re PD-2198
        public String pageNameDisplayed;
        public String description;
        public boolean isActive;
        public String envId;
        public String customPageName;
        public String pageType;
        public Boolean isCustomHeaderFooter;
        public Integer pageOrder;
        public Id pageId;
        public Boolean includeinCheckListOrNot;
        public String pageParameterValue;
        public String clonePageFrom;
        public boolean isDesktopViewInMobile;
    }
    
      /*******************************************************************************************************
    * @description forces DML to insert log messages in database
    */
    public void initPageException() {
        try {
         ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
         
        } catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }
    
    

}