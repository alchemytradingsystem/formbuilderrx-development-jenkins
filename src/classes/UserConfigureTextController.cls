global with sharing class UserConfigureTextController {
    /*
    	@description: to store envjson
    */ 
    public String envRecordJSON {get;set;} 
     /*
    	@description: property stores package prefix i.e. ERx_Forms
    */ 
   	public String getPrefix {get;set;}
   	
   	public Boolean includeBackwardCompatibilityResource{get;set;}
    public UserConfigureTextController(){
    	/*<!-- 09-08-18 Added by Shubham Re PD-3814 -->*/
    	FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
		if(adminSettings != null){ 
			if(adminSettings.Include_backward_compatibility_Resource__c){
				includeBackwardCompatibilityResource = true;
			}
			else{
				includeBackwardCompatibilityResource = false;
			}
		}
    	getPrefix = PortalPackageUtility.getFinalNameSpacePerfix();
    }
    
    /**
     * @description Method is used to save and update  new Configuration.
     * @param pageObject json of newly configuration
     * @return Success String or Error String.
     */
    @RemoteAction	
	global static String saveData(String envData){
		Env__c env;	
		String returnString;
		String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();		 
		try{ //  Added PD-2729 editing configurable through custom setting in live env
   			 Env__c curEnvData =(env__c)JSON.deserialize(envData, Sobject.class);
   			 
   			 if(SecurityEnforceUtility.checkObjectAccess('Env__c', SecurityTypes.IS_ACCESS, PortalPackageUtility.getFinalNameSpacePerfix())){
				if(SecurityEnforceUtility.checkFieldAccess('Env__c', 'Environment_Status__c', SecurityTypes.IS_ACCESS, PortalPackageUtility.getFinalNameSpacePerfix())){
   		   			env = [SELECT id,Environment_Status__c
   		   				   FROM Env__c 
   		   				   WHERE id=:curEnvData.id LIMIT 1];
		     			FormBuilder_Settings__c fs= FormBuilder_Settings__c.getValues('Admin Settings');
		     
			       	     if(fs!=null && env!=null && !fs.Allow_editing__c && env.Environment_Status__c == 'Live'){
			       	     	returnString = PortalPackageConstants.ENV_UPDATION_ERROR_MESSAGE;
			       	     }
			       	     else{
			       	     	upsert curEnvData;
			       	     	returnString = 'Page has been saved successfully.';
					     }
				}	     
       	     	else {
            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c  FieldList :: Environment_Status__c'), 'UserConfigureTextController', 'saveData', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
   			 }	
			else {
	           	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c '), 'UserConfigureTextController', 'saveData', null);
	           	throw new PortalPackageException(permissionErrorMessage); 
   		    	}
		 
		}
		catch(Exception ex){
		      ERx_PortalPackageLoggerHandler.addException(ex,'UserConfigureText','saveData',null); 
    	}
		return returnString;
		
	}  

}