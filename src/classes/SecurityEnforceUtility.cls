/**
    EventRx
    @company : Copyright © 2016, Enrollment Rx, LLC
    All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    @website : http://www.enrollmentrx.com/
    @author EnrollmentRx
    @version 1.0
    @date 2015-02-10
    @description Class for FLS enforcement
*/
public with sharing class SecurityEnforceUtility {
	
	public static Map<String, Security_Check_Bypass_Setting__c> byPassObjectFieldDetails {
		get {
			byPassObjectFieldDetails = PortalPackageUtility.getByPassObjectandFieldDetails();
			return byPassObjectFieldDetails;
		}
		set;
	}
	
	public static Map<String, Set<String>> byPassObjectFieldList {
		get {
			byPassObjectFieldList = PortalPackageUtility.getByPassFieldlist();
			return byPassObjectFieldList;
		}
		set;
	}
    /**
     * @description checks the object level accessibility(insert/update/delete/read) for passed object
     * @param objectName stores the object name which needs to be check for security
     * @param accessType stores the enum type(IS_UPDATE/IS_DELETE/IS_INSERT/IS_ACCESS) based on which objectName will be checked for that level of security accessibility
     * @param nameSpace stores the package namespace
     * @return true if objectName has security accessibility for passed accessType otherwise false
    */
    public static boolean checkObjectAccess(String objectName, SecurityTypes accessType, String nameSpace) {
        Boolean result = true;
        objectName = objectName.toLowerCase();
        DescribeSobjectResult objeDesc = getObjectDescribe(objectName.toLowerCase(),nameSpace.toLowerCase());
        if(objeDesc != null) {
            if(!objeDesc.isUpdateable() && accessType == SecurityTypes.IS_UPDATE && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_UPDATE__c)){
                result = false;
            }else if(!objeDesc.isDeletable() && accessType == SecurityTypes.IS_DELETE && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_DELETE__c)){
                result = false; 
            }else if(!objeDesc.isCreateable() && accessType == SecurityTypes.IS_INSERT && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_INSERT__c)){
                result = false; 
            }else if(!objeDesc.isAccessible() && accessType == SecurityTypes.IS_ACCESS && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_ACCESS__c)){
                result = false; 
            }
        } else {
            result = false;
        }
        return result;
    }
    
    /**
     * @description checks the object level accessibility(insert/update/delete/read) for passed object
     * @param objectName stores the object name which needs to be check for security
     * @param accessType stores the enum type(IS_UPDATE/IS_DELETE/IS_INSERT/IS_ACCESS) based on which objectName will be checked for that level of security accessibility
     * @param nameSpace stores the package namespace
     * @return true if objectName has security accessibility for passed accessType otherwise false
    */
    public static boolean checkObjectAccess(List<String> objectList, SecurityTypes accessType, String nameSpace) {
        Boolean result = true;
        DescribeSobjectResult objeDesc = null;
        if(objectList != null && objectList.size() > 0) {
            for(String objectName : objectList) {
            	objectName = objectName.toLowerCase();
                objeDesc = getObjectDescribe(objectName,nameSpace.toLowerCase());
                if(objeDesc != null) {
                    if(!objeDesc.isUpdateable() && accessType == SecurityTypes.IS_UPDATE && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_UPDATE__c)){
                        result = false;
                        break;
                    }else if(!objeDesc.isDeletable() && accessType == SecurityTypes.IS_DELETE && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_DELETE__c)){
                        result = false; 
                        break;
                    }else if(!objeDesc.isCreateable() && accessType == SecurityTypes.IS_INSERT && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_INSERT__c)){
                        result = false;
                        break; 
                    }else if(!objeDesc.isAccessible() && accessType == SecurityTypes.IS_ACCESS && !(byPassObjectFieldDetails.containsKey(objectName) && byPassObjectFieldDetails.get(objectName).ByPass_ACCESS__c)){
                        result = false;
                        break; 
                    }
                } else {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }
    
    
    /**
     * @description checks the field level accessibility(insert/update/read) for passed single field
     * @param objectName stores the object name whose field needs to be check for security
     * @param fieldName stores the field name which needs to be check for security
     * @param accessType stores the enum type(IS_UPDATE/IS_DELETE/IS_INSERT/IS_ACCESS) based on which fieldName will be checked for that level of security accessibility
     * @param nameSpace stores the package namespace
     * @return true if fieldName has security accessibility for passed accessType otherwise false
    */
    public static boolean checkFieldAccess(String objectName, String fieldName, SecurityTypes accessType, String nameSpace){
        Boolean result = true;
        nameSpace = nameSpace.toLowerCase();
        objectName = objectName.toLowerCase();
        DescribeSobjectResult objeDesc = getObjectDescribe(objectName, nameSpace);
        if(objeDesc != null) {
            Map <String, Schema.SObjectField> fieldMap = objeDesc.fields.getMap();
            DescribeFieldResult selectedField = getFieldDescribe(objectName, fieldName.toLowerCase(), nameSpace, fieldMap);
            if(selectedField != null) {
            	if(!(byPassObjectFieldList.containsKey(objectName) && byPassObjectFieldList.get(objectName).contains(fieldName.toLowerCase()))) {
	                if((!selectedField.isPermissionable() || selectedField.isAutoNumber() || selectedField.isCalculated()) && (accessType == SecurityTypes.IS_UPDATE || accessType == SecurityTypes.IS_INSERT || accessType == SecurityTypes.IS_ACCESS)){
	                    result = true;
	                } else if((!(selectedField.isPermissionable() && selectedField.isUpdateable())) && accessType == SecurityTypes.IS_UPDATE){
	                    result = false;
	                }else if((!(selectedField.isPermissionable() && selectedField.isCreateable())) && accessType == SecurityTypes.IS_INSERT){
	                    result = false; 
	                }else if(!selectedField.isAccessible() && accessType == SecurityTypes.IS_ACCESS){
	                    result = false; 
	                }
            	}
            } else {
                result = false; 
            } 
        } else {
            result = false; 
        }
        return result;
    }

    /**
     * @description checks the field level accessibility(insert/update/read) for passed fieldlist
     * @param objectName stores the object name whose fields need to be check for security
     * @param fieldList stores the fields List which needs to be check for security
     * @param accessType stores the enum type(IS_UPDATE/IS_DELETE/IS_INSERT/IS_ACCESS) based on which fields will be checked for that level of security accessibility
     * @param nameSpace stores the package namespace
     * @return true if all fields of fieldList has security accessibility for passed accessType otherwise false
    */
    public static boolean checkFieldAccess(String objectName, List<String> fieldList, SecurityTypes accessType, String nameSpace){
        Boolean result = true;
        nameSpace = nameSpace.toLowerCase();
        objectName = objectName.toLowerCase();
        DescribeSobjectResult objeDesc = getObjectDescribe(objectName, nameSpace);
        if(objeDesc != null) {
            Map <String, Schema.SObjectField> fieldMap = objeDesc.fields.getMap();
            for(String fieldName : fieldList) {
                DescribeFieldResult selectedField = getFieldDescribe(objectName, fieldName.toLowerCase(), nameSpace, fieldMap );
                if(selectedField != null) {
                	if(!(byPassObjectFieldList.containsKey(objectName) && byPassObjectFieldList.get(objectName).contains(fieldName.toLowerCase()))) {
	                    if((!selectedField.isPermissionable() || selectedField.isAutoNumber() || selectedField.isCalculated()) && (accessType == SecurityTypes.IS_UPDATE || accessType == SecurityTypes.IS_INSERT || accessType == SecurityTypes.IS_ACCESS)){
	                        continue;
	                    } else if((!(selectedField.isPermissionable() && selectedField.isUpdateable())) && accessType == SecurityTypes.IS_UPDATE){
	                        result = false;
	                        break;
	                    } else if((!(selectedField.isPermissionable() && selectedField.isCreateable())) && accessType == SecurityTypes.IS_INSERT){
	                        result = false; 
	                        break;
	                    } else if(!selectedField.isAccessible() && accessType == SecurityTypes.IS_ACCESS){
	                        result = false; 
	                        break;
	                    }
                	}
                } else {
                    result = false;
                    break; 
                }
            }
        } else {
            result = false; 
        }
        return result;
    } 
    
    /**
     * @description checks the field level accessibility(insert/update/read) for passed fieldlist
     * @param objectName stores the object name whose fields need to be check for security
     * @param fieldList stores the fields List which needs to be check for security
     * @param accessType stores the enum type(IS_UPDATE/IS_DELETE/IS_INSERT/IS_ACCESS) based on which fields will be checked for that level of security accessibility
     * @param nameSpace stores the package namespace
     * @return true if all fields of fieldList has security accessibility for passed accessType otherwise false
    */ 
    public static List<String> checkFieldListAccess(String objectName, List<String> fieldList, SecurityTypes accessType, String nameSpace){
        List<String> result = new List<String>();
        nameSpace = nameSpace.toLowerCase();
        objectName = objectName.toLowerCase();
        DescribeSobjectResult objeDesc = getObjectDescribe(objectName, nameSpace);
        if(objeDesc != null) {
            Map <String, Schema.SObjectField> fieldMap = objeDesc.fields.getMap();
            for(String fieldName : fieldList) {
                DescribeFieldResult selectedField = getFieldDescribe(objectName, fieldName.toLowerCase(), nameSpace, fieldMap );
                if(selectedField != null) {
                	if(!(byPassObjectFieldList.containsKey(objectName) && byPassObjectFieldList.get(objectName).contains(fieldName.toLowerCase()))) {
	                    if((!selectedField.isPermissionable() || selectedField.isAutoNumber() || selectedField.isCalculated()) && (accessType == SecurityTypes.IS_UPDATE || accessType == SecurityTypes.IS_INSERT || accessType == SecurityTypes.IS_ACCESS)){
	                        continue;
	                    } else if((!(selectedField.isPermissionable() && selectedField.isUpdateable())) && accessType == SecurityTypes.IS_UPDATE){
	                        result.add(fieldName);
	                    } else if((!(selectedField.isPermissionable() && selectedField.isCreateable())) && accessType == SecurityTypes.IS_INSERT){
	                        result.add(fieldName);
	                    } else if(!selectedField.isAccessible() && accessType == SecurityTypes.IS_ACCESS){
	                        result.add(fieldName);
	                    }
                	}
                } 
            }
        } else {
            result.add('[Method]: getObjectDescribe, [args]: ' + objectName + ',' + nameSpace + ', [Return]: null'); 
        }
        return result;
    } 
    
    /**
     * @description This method describe fields for given object
     * @param fieldName name of field on which describe applied.
     * @param nameSpace package namespace
     * @param fieldMap map of field which contain fields with their describe result
     * @return DescribeFieldResult contain field meta info
    */
    private static DescribeFieldResult getFieldDescribe(String objectName, String fieldName, String nameSpace,Map <String, Schema.SObjectField> fieldMap ){
        DescribeFieldResult fieldDesc;
        Schema.SObjectField fieldObj;
        //Other package field can be used in different package 
        //hence replace self package to get appropriate API object
     	if(String.isNotBlank(fieldName) && String.isNotBlank(nameSpace) && (fieldName.startsWith(nameSpace))) {
            fieldName = fieldName.replace(nameSpace,'');
        }
        if(String.isNotBlank(fieldName)) {
            fieldName = fieldName.trim();
        }
        fieldObj = fieldMap.get(nameSpace+fieldName);      
        if(fieldObj == null) {
            fieldObj = fieldMap.get(fieldName);
        }
        
        //set appropriate return 
        if(fieldObj != null){
            fieldDesc = fieldObj.getDescribe();
        } else {
            fieldDesc = null;
            ERx_PortalPackageLoggerHandler.logDebugMessage(fieldName + ' field not found on :: ' + objectName);
            throw new PortalPackageException(fieldName + ' field not found on :: ' + objectName);
        }
        return  fieldDesc;
    }
    
    /**
     * @description This method describe given object
     * @param objectName name of object on which describe will applied.
     * @param nameSpace package namespace
     * @return DescribeSobjectResult contain object meta info
    */
    private static DescribeSobjectResult getObjectDescribe(String objectName, String namespace){
        DescribeSobjectResult objDesc;
        //Other package field can be used in different package 
        //hence replace self package to get appropriate API object
        if(String.isNotBlank(nameSpace) && (objectName.startsWith(nameSpace))) {
            objectName = objectName.replace(nameSpace,'');
        }
        if(String.isNotBlank(objectName)) {
            objectName = objectName.trim();
        }
        Schema.SObjectType sobjectDesc = Schema.getGlobalDescribe().get(namespace+objectName);
        if(sobjectDesc == null){
            sobjectDesc = Schema.getGlobalDescribe().get(objectName);
        }
        //set appropriate return 
        if(sobjectDesc != null){
            objDesc = sobjectDesc.getDescribe();
        }else {
            objDesc = null;
            ERx_PortalPackageLoggerHandler.logDebugMessage('Object not found :: ' + objectName);
            throw new PortalPackageException('Object not found :: ' + objectName);
        }
        return objDesc;
    }
    
}