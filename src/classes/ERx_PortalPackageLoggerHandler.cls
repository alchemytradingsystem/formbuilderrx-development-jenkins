/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description The PageBuildDirector program generate final page with the  help of HtmlPage class and contain remote action method for reterieve, update, insert record.
*/
public with sharing class ERx_PortalPackageLoggerHandler { 
	
    private static list<Portal_Package_Logger__c> lstException = new list<Portal_Package_Logger__c>();
    private static String nameSpace;
    
    public static void addException (Exception ex, String className, String methodName, String pageId) {
    	String message = ex.getMessage();
    	
    	//Re:PD-2018 Include object name in Portal Package Loggers to which user has insufficient access	
    	if(ex instanceof System.DmlException) {
    		String objName; 
	        System.DmlException e = (System.DmlException)ex;
	        for(Integer affectedRow = 0; affectedRow < e.getNumDml(); affectedRow++){
	        	//Get ID of the failed record that caused the error described by the failed row.
	        	Id recId = e.getDmlId(affectedRow);
	        	if(recId != null) {
		            //Get object name of ID of the failed record
		            objName = recId.getSObjectType().getDescribe().getName();
		            break;
	        	}
	        }
	        if(string.isNotBlank(objName)) {
	        	message = message.replace(PortalPackageConstants.LOGGER_OBJECT,objName);
	        }
	    }
	 	
    	Portal_Package_Logger__c log = new Portal_Package_Logger__c(
    		LineNumber__c = ex.getLineNumber(), 
    		Message__c = ('PageId - '+pageId+' :: Class Name - ' + className + ' :: Method Name - ' + methodName + ' :: Message - ' + message), 
    		StackTraceString__c = ex.getStackTraceString(),
        	TypeName__c = 'Error'
    	);
    	lstException.add(log); 
	}
	
	public static void addInformation (String infoMessage, String className, String methodName, String pageId) {
		//Getting InfoLoggerInsert custom Setting value 
        Boolean isInfoLoggerInsert = PortalPackageUtility.checkInfoLoggerInsertSetting();
        if(isInfoLoggerInsert) {
	    	Portal_Package_Logger__c log = new Portal_Package_Logger__c(
	    		Message__c = ('PageId - '+pageId+' :: Class Name - ' + className + ' :: Method Name - ' + methodName + ' :: Message - ' + infoMessage), 
	        	TypeName__c = 'Info'
	    	);
	    	lstException.add(log); 
        }
	}
	
	public static void addWarning (String warningMessage, String className, String methodName, String pageId) {
    	Portal_Package_Logger__c log = new Portal_Package_Logger__c(
    		Message__c = ('PageId - '+pageId+' :: Class Name - ' + className + ' :: Method Name - ' + methodName + ' :: Message - ' + warningMessage), 
        	TypeName__c = 'Warning'
    	);
    	lstException.add(log); 
	}
	
	public static void addPermissionError (String permissionError, String className, String methodName, String pageId) {
    	Portal_Package_Logger__c log = new Portal_Package_Logger__c(
    		Message__c = 'Please give Permission to Object and Fields'+('PageId - '+pageId+' :: Class Name - ' + className + ' :: Method Name - ' + methodName + ' :: Message - ' + permissionError), 
        	TypeName__c = 'permissionError'
    	);
    	lstException.add(log); 
	}
	public static void saveExceptionLog() {
    	if(!lstException.isEmpty()) {
    		//Code changes to enforce the security to insert records, first check object level insert access then field level insert access for fields to be inserted
			if(nameSpace == null) {
				nameSpace = PortalPackageUtility.getNameSpacePerfix(); 
		        if(String.isNotBlank(nameSpace)){
		        	nameSpace = nameSpace + '__';
		        }
			}
    		if(SecurityEnforceUtility.checkObjectAccess('Portal_Package_Logger__c', SecurityTypes.IS_INSERT, nameSpace)) {
    			List<String> fieldsList = new List<String>{'LineNumber__c', 'Message__c','StackTraceString__c', 'TypeName__c'};
    			if(SecurityEnforceUtility.checkFieldAccess('Portal_Package_Logger__c', fieldsList, SecurityTypes.IS_INSERT, nameSpace)) {
    				if(lstException.size() > 0) {
						try {
							insert lstException;
							lstException = new list<Portal_Package_Logger__c>();
						} catch(Exception e) {
							Erx_EmailLoggerCleanUp.sendMail('Error While Inserting Logs in orgid = ' + UserInfo.getOrganizationId(), 'Review the following error while inserting logs : ' + e.getMessage(), 'developer1@enrollmentrx.com');
						}
    				}
				}	
    		}
    	}
  	}
  	
  	public static void logDebugMessage(String debugMessage) {
  		System.debug('Erx Form Builder Debug Log Starts :: ' + debugMessage);
  	}
}