/********
FormBuilderRx
@company : Copyright © 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
 ********/
public with sharing class TM_TemplateController {
	//the Id of current Site, store on Env__c.Env_Id__c
	public String entityId{get;set;}
	//temlate record Id
	public String templateId {get;set;}
	//env Id
	public String envId {get;set;}
	//if this is template review or site reivew or live env
	public String previewMode {get;set;}
	//template for site login only
	public String siteLoginTemplate {get;set;}
	public String templateHeader {get;set;}
	public String templateFooter {get;set;}
	public String templateResource {get;set;}
	public SiteLoginTemplate__c currentTemplate {get;set;}
	public String packageName{get;set;}
	/****
	 * @description To store Permission error message
	 */
	private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
	//public string testENVID {get{return ERx_PortalPackUtil.getCurrentEnv().Id;}set;}
	public string testENVID {get{return Site.getSiteId();}set;}
	public string testUserInfo {get{return UserInfo.getName();}set;}
	// Unused code commented by formbuilder team
	//public string testNetworkId {get{return Network.getNetworkId();}set;}

	public TM_TemplateController(){
		packageName = PortalPackageUtility.getNameSpacePerfix();
		if(packageName != null && packageName != '') {
			packageName = packageName + '__';
		}
		templateHeader = ''; 
		templateFooter = ''; 
		templateResource = ''; 
		siteLoginTemplate = packageName+'Test_TM_Template'; 
		//page param for preview mode
		previewMode = ApexPages.currentPage().getParameters().get('previewMode');

		if(previewMode == 'tmpReview'){
			//template content preview
			templateId = ApexPages.currentPage().getParameters().get('tmpId');

		}else{

			list<Env__c> envList = null;
			Env__c liveTestENV = null;
			List<String> listFields = new List<String>{'TP_Manage_Active_Template__c', 'Template_for_Site_Login__c', 'VF_Template_for_Site_Login__c',
				'Main_Template_Criteria__c', 'Site_Template_Criteria__c'};
				string queryStr = 'SELECT '+packageName+'Main_Template_Criteria__c, '+packageName+'Site_Template_Criteria__c, '
						+packageName+'TP_Manage_Active_Template__c, '+packageName+'Template_for_Site_Login__c, '
						+packageName+'VF_Template_for_Site_Login__c '
						+' FROM '+packageName+'Env__c';

				if(previewMode == 'siteReview'){
					//site login preview
					envId = ApexPages.currentPage().getParameters().get('envId');
					queryStr += ' WHERE Id =: envId';
				}else if(previewMode == 'widgetReview'){
					//site login preview
					Current_Env__c ce = ERx_PortalPackUtil.getCurrentEnvForConfiguration();
					if(ce != null){
						envId = ce.Env__c;
					}
					queryStr += ' WHERE Id =: envId';
				}else{
					//live portal/site env
					/*
                entityId = Site.getSiteId();   
                string eMode = (ERx_PortalPackUtil.getEnvModeForCurrentUser() == EnvMode.Live)? 'Live' : 'Test' ;
                queryStr += ' WHERE '+packageName+'Environment_Status__c = \'' + eMode +'\' AND '+packageName+'Env_Site_Id__c =: entityId limit 1';
					 */

					//Manish: We already have method/API to pick desired ENV based on test/live in context with USER
					// Kindly call that method everywhere for fetching right ENV
					liveTestENV = ERx_PortalPackUtil.getEnvDetailsForCurrentUser();
					if(liveTestENV != null){
						envList = new list<Env__c>{liveTestENV};
					}
					//system.assert(false, liveTestENV);
				}


				// Manish: In case, no need of live/test ENV
				if(liveTestENV == null){
					system.debug('+++++++++'+queryStr);
					if(SecurityEnforceUtility.checkObjectAccess('Env__c', SecurityTypes.IS_ACCESS, packageName)){
						if(SecurityEnforceUtility.checkFieldAccess('Env__c', listFields, SecurityTypes.IS_ACCESS, packageName)){
							envList = Database.query(queryStr);	
						} else {
							ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c  FieldList :: ' 
									+ PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'TM_TemplateController', 'TM_TemplateController', null);
							throw new PortalPackageException(permissionErrorMessage); 
						}
					} else {
						ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c'), 'TM_TemplateController', 'TM_TemplateController', null);
						throw new PortalPackageException(permissionErrorMessage); 
					}
				}
				// get and set template fields with data fetched from SF objects
				if(envList.size()>0){
					Boolean isDefaultTemplateShown = true;
					if(String.isNotBlank(envList[0].Main_Template_Criteria__c)) {
						// Fetching Template based on Criteria
						String criteriaTemplateId = ERx_PortalPackUtil.fetchTemplateId(envList[0].Main_Template_Criteria__c, 
								ApexPages.currentPage().getParameters(), envList[0].Id);
						if(String.isNotBlank(criteriaTemplateId)) {
							templateId = criteriaTemplateId;
							isDefaultTemplateShown = false;
						}
					}
					if(isDefaultTemplateShown) {
						// assign the main template default
						templateId = envList[0].TP_Manage_Active_Template__c;
					}

					//check if it is site login page now.
					String pageNameX = ApexPages.CurrentPage().getUrl();
					if(pageNameX!=null && (pageNameX.containsIgnoreCase('Portal_Login') ||pageNameX.containsIgnoreCase('Portal_Register') 
							|| pageNameX.containsIgnoreCase('Portal_ForgotPassword') || pageNameX.containsIgnoreCase('Portal_ChangePassword')  )){
						//use VF page as site login template
						if(envList[0].VF_Template_for_Site_Login__c != null && envList[0].VF_Template_for_Site_Login__c != ''){
							siteLoginTemplate = envList[0].VF_Template_for_Site_Login__c;
						}
						//if separate template defined, then use new defined template
						isDefaultTemplateShown = true;
						if(String.isNotBlank(envList[0].Site_Template_Criteria__c)) {
							// Fetching template for site based on criteria
							String criteriaTemplateId = ERx_PortalPackUtil.fetchTemplateId(envList[0].Site_Template_Criteria__c, 
									ApexPages.currentPage().getParameters(), envList[0].Id);
							if(String.isNotBlank(criteriaTemplateId)) {
								templateId = criteriaTemplateId;
								isDefaultTemplateShown = false;
							}
						}
						if(isDefaultTemplateShown && envList[0].Template_for_Site_Login__c != null){
							// Assigning default site template
							templateId = envList[0].Template_for_Site_Login__c;
						}
					}
				}
		} 

		// System should always has template Id here
		if(templateId != null){  
			List<String> listFields = new List<String>{'Static_Resource_List__c','SiteTemplateHeader__c','SiteTemplateFooter__c', 'isJqueryInHeaderandFooter__c',
				'jQueryHandledHeader__c', 'jQueryHandledFooter__c','ERxFBTemplate__c','ERxFB_Template_Style__c','ERxFB_Logo_Main_Heading__c','ERxFB_Logo_Sub_Heading__c','ERxFB_Logo_Image_Url__c'};
				if(SecurityEnforceUtility.checkObjectAccess('SiteLoginTemplate__c', SecurityTypes.IS_ACCESS, packageName)){
					if(SecurityEnforceUtility.checkFieldAccess('SiteLoginTemplate__c', listFields, SecurityTypes.IS_ACCESS, packageName)){
						currentTemplate = [SELECT Static_Resource_List__c, SiteTemplateHeader__c, SiteTemplateFooter__c, isJqueryInHeaderandFooter__c,
						                   jQueryHandledHeader__c, jQueryHandledFooter__c,ERxFBTemplate__c,ERxFB_Template_Style__c,ERxFB_Logo_Main_Heading__c,ERxFB_Logo_Sub_Heading__c,ERxFB_Logo_Image_Url__c   
						                   FROM SiteLoginTemplate__c 
						                   WHERE Id =: templateId];
						//populate site header and footer
						// KB : Condition added to handle dup jquery in header and footer PD-2628
						if(currentTemplate.isJqueryInHeaderandFooter__c) {
							// Case : When Handle JQuery checked for template assigning changed template header and footer. 
							templateHeader = currentTemplate.jQueryHandledHeader__c;
							templateFooter = currentTemplate.jQueryHandledFooter__c;		            	
						} else {
							// Case : Otherwise assigning the original template header and footer 
							templateHeader = currentTemplate.SiteTemplateHeader__c; 
							templateFooter = currentTemplate.SiteTemplateFooter__c;
						}
						//System.debug('head::::::::::::::::::::::::'+templateHeader);
						//process resource list
						//to compose URL need to know what is current site path
						string siteURL = Site.getPathPrefix();
						if(currentTemplate.Static_Resource_List__c!=null && currentTemplate.Static_Resource_List__c != ''){
							list<TM_TemplateContentController.ResourceItem> resourceList = new list<TM_TemplateContentController.ResourceItem>();
							resourceList = (List<TM_TemplateContentController.ResourceItem>)JSON.deserialize(currentTemplate.Static_Resource_List__c, 
									List<TM_TemplateContentController.ResourceItem>.class);

							
							Integer staticResourceIndex = 1;
							for(TM_TemplateContentController.ResourceItem item : resourceList){
								//System.debug('=======>1'+item.type);
								//System.debug('=======>2'+item.subPath);
								// handling null & single quote by FormBuilder Team - Avish
								if(String.isNotBlank(item.name)) {
									string reString = '';
									if(item.type == 'CSS'){
										reString = '<link  rel="stylesheet" type="text/css" href="' + PortalPackageUtility.escapeQuotes(siteURL) 
												+ '/resource/'+ PortalPackageUtility.escapeQuotes(item.name) + PortalPackageUtility.escapeQuotes(item.subPath) +'" ></link>';
										//System.debug('st1rrrrrrrrrrr::'+reString);
										templateResource += reString;
									}else if(item.type == 'JavaScript'){
										reString = '<script type="text/javascript" src="' + PortalPackageUtility.escapeQuotes(siteURL) + '/resource/' 
												+ PortalPackageUtility.escapeQuotes(item.name) + PortalPackageUtility.escapeQuotes(item.subPath) + '" ></script>';
										//System.debug('st2rrrrrrrrrrr::'+reString);
										templateResource += reString;
									}else if(item.type == 'JavaScript/JQuery'){
										// Handle duplicate jquery in template PD-2628.
										reString = '<script type="text/javascript" src="' + PortalPackageUtility.escapeQuotes(siteURL) + '/resource/' 
												+ PortalPackageUtility.escapeQuotes(item.name) + PortalPackageUtility.escapeQuotes(item.subPath) + '" ></script>';
										//System.debug('st2rrrrrrrrrrr::'+reString);
										if(String.isNotBlank(item.alias)) {
											reString += '<script> var ' + item.alias + ' = jQuery.noConflict(true); </script>';
										} else {
											reString += '<script> var staticResourceJQuery_' + staticResourceIndex + ' = jQuery.noConflict(true); </script>';
										}
										templateResource += reString;
									}else if(item.type == 'Image'){
										//System.debug('st3rrrrrrrrrrr::'+reString);
										reString = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(item.name) + PortalPackageUtility.escapeQuotes(item.subPath);
										// applied alias blank check before replacing the header and footer, it cause to change the blank by url by FormBuilder Team - Avish
										if(templateHeader != null && String.isNotBlank(item.alias)){
											templateHeader = templateHeader.replace(item.alias, reString);
										}
										if(templateFooter != null && String.isNotBlank(item.alias)){
											templateFooter = templateFooter.replace(item.alias, reString);
										}
									}else if(item.type == 'Open Text'){
										reString = PortalPackageUtility.escapeQuotes(item.subPath);
										//System.debug('=======>3'+reString);
										//System.debug('=======>4'+templateHeader+templateFooter);
										// applied alias blank check before replacing the header and footer, it cause to change the blank by url by FormBuilder Team - Avish
										if(templateHeader != null && String.isNotBlank(item.alias)){
											templateHeader = templateHeader.replace(item.alias, reString);
										}

										if(templateFooter != null && String.isNotBlank(item.alias)){
											templateFooter = templateFooter.replace(item.alias, reString);
										}
									}
								}
								staticResourceIndex++;
							}
							
								
							
						}
						
						//PD-3816 | Gourav | template code dynamic
							if(currentTemplate.ERxFBTemplate__c){
								String dynamicStyle= ':root{';
								JsonToObjectWrapper wrapperObj= new JsonToObjectWrapper();
								List<JsonToObjectWrapper> listofStyle = new List<JsonToObjectWrapper>();
								String styleJson = currentTemplate.ERxFB_Template_Style__c;
								if(! String.isEmpty(styleJson) ){
									listofStyle = wrapperObj.parse(styleJson);
								}
								if(listofStyle!= null && listofStyle.size()>0){
									for(JsonToObjectWrapper currObj : listofStyle ){
										 dynamicStyle += currObj.sAPI + ':' + currObj.sVal + ';';
									}
									
									
								}
								dynamicStyle += '}';
								if(templateHeader.contains('##ERxTemplateStyle##')){
									templateHeader = templateHeader.replace('##ERxTemplateStyle##' ,dynamicStyle);
									
								}
								if(templateHeader.contains('##UrlPathPrefix##')){
									templateHeader = templateHeader.replaceAll('##UrlPathPrefix##' ,siteURL);
									
								}
								if(templateFooter.contains('##UrlPathPrefix##')){
									templateFooter = templateFooter.replaceAll('##UrlPathPrefix##' ,siteURL);
								}
								
								if(templateHeader.containsIgnoreCase('##LogoMainHeading##')){
									if(String.isNotBlank(currentTemplate.ERxFB_Logo_Main_Heading__c)){
										templateHeader=templateHeader.replace('##LogoMainHeading##',currentTemplate.ERxFB_Logo_Main_Heading__c);
									}else{
										templateHeader=templateHeader.replace('##LogoMainHeading##','');
									}
								}
								if(templateHeader.containsIgnoreCase('##LogoSubHeading##')){
									if(String.isNotBlank(currentTemplate.ERxFB_Logo_Sub_Heading__c)){
										templateHeader=templateHeader.replace('##LogoSubHeading##',currentTemplate.ERxFB_Logo_Sub_Heading__c);
									}else{
										templateHeader=templateHeader.replace('##LogoSubHeading##','');
									}
								}
								if(templateHeader.contains('##NameSpacePrefix##')){
									templateHeader = templateHeader.replaceAll('##NameSpacePrefix##' ,packageName);
									
								}
								if(templateFooter.contains('##NameSpacePrefix##')){
									templateFooter = templateFooter.replaceAll('##NameSpacePrefix##' ,packageName);
								}
								
								//<i class="fa fa-university"></i>
								//##LogoImage##
								if(templateHeader.containsIgnoreCase('##LogoImage##')){
									if(String.isNotBlank(currentTemplate.ERxFB_Logo_Image_Url__c) && !currentTemplate.ERxFB_Logo_Image_Url__c.contains('null')){
										List<String> imageUrls = currentTemplate.ERxFB_Logo_Image_Url__c.split('##');
										if(imageUrls != null && imageUrls.size()>0){
											String logoImgLink='';
											if(imageUrls.size()==1){
											 	logoImgLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(imageUrls[0]);
											}else if(imageUrls.size()==2){
												logoImgLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(imageUrls[0]) + PortalPackageUtility.escapeQuotes(imageUrls[1]) ;
											}
											
											String htmlImageLink = '<img src=" '+ logoImgLink  +'"/>';
											templateHeader=templateHeader.replace('##LogoImage##',htmlImageLink);
										}
									}else{
										templateHeader=templateHeader.replace('##LogoImage##','<i class="fa fa-university"></i>');
									}
								}
								
								String styleLinks = '<link rel="stylesheet" type="text/css" href="'+ siteURL +'/resource/'+packageName+'template_1/css/style.css" media="all" /><link rel="stylesheet" type="text/css" href="'+ siteURL +'/resource/'+packageName+'template_1/css/responsive.css" media="all" />';
								//system.assert(false,ApexPages.currentPage());
								if(templateHeader.containsIgnoreCase('##StyleLinks##')){
									if(String.isNotBlank(ApexPages.currentPage().getUrl()) && ApexPages.currentPage().getUrl().contains('HomepageBuilder_ManageLayout') ){
										templateHeader=templateHeader.replace('##StyleLinks##','');
									} else{
										templateHeader = templateHeader.replace('##StyleLinks##',styleLinks);	
									}
									
										
								}
								
							}
						
						
					} else {
						ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: SiteLoginTemplate__c  FieldList :: ' 
								+ PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'TM_TemplateController', 'TM_TemplateController', null);
						throw new PortalPackageException(permissionErrorMessage); 
					}
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: SiteLoginTemplate__c'), 'TM_TemplateController', 'TM_TemplateController', null);
					throw new PortalPackageException(permissionErrorMessage); 
				}
		}
	}

	/*******************************************************************************************************
	 * @description forces DML to insert log messages in database
	 */
	public void initPageException() {
		try {
			ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
			ERx_PortalPackageLoggerHandler.saveExceptionLog();

		} catch (Exception e) {
			ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
			ERx_PortalPackageLoggerHandler.saveExceptionLog();
		}
	}
	
	public class JsonToObjectWrapper {

	public String sLbl;
	public String sAPI;
	public String sType;
	public String sVal;

	
	public List<JsonToObjectWrapper> parse(String json) {
		return (List<JsonToObjectWrapper>) System.JSON.deserialize(json, List<JsonToObjectWrapper>.class);
	}
}


	/************************************************************
        Static Method for portal package
        API : TM_TemplateController.getHeaderFooterMap().get('head');
              TM_TemplateController.getHeaderFooterMap().get('foot');
	 ************************************************************/
	//public static Map<String,String> getHeaderFooterMap(){
	//    Map<String, String> headfoot = new Map<String, String>();
	//    String header,footer;
	//    String activeEntityId = Site.getSiteId();
	//    List<Live_Env__c> liveEnvs = [Select l.Env__r.TP_Manage_Active_Template__r.SiteTemplateHeader__c, l.Env__r.TP_Manage_Active_Template__r.SiteTemplateFooter__c, l.Env__r.Name, l.Env__r.Env_Id__c,l.Env__r.Env_Site_Id__c, l.Env__c From Live_Env__c l where l.Env__r.Env_Site_Id__c =: activeEntityId];
	//    if(liveEnvs == null || liveEnvs.size() <= 0 || liveEnvs[0].Env__c == null){
	//        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Live Environment is founded.'));
	//        return null;
	//    }
	//    else if(liveEnvs.size() > 1){
	//        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'More than one Live Environment is founded.'));
	//        return null;
	//    }
	//    else{
	//        header = liveEnvs[0].Env__r.TP_Manage_Active_Template__r.SiteTemplateHeader__c;
	//        footer = liveEnvs[0].Env__r.TP_Manage_Active_Template__r.SiteTemplateFooter__c;
	//        if( header != null && footer != null){
	//            headfoot.put('header',header);
	//            headfoot.put('footer',footer);
	//        }

	//        return headfoot;
	//    }


	//    return headfoot;
	//}

	//public static Map<String,String> getHeaderFooterMap(String liveId){
	//    Map<String, String> headfoot = new Map<String, String>();
	//    String header,footer;
	//    String activeEntityId = liveId;
	//    List<Live_Env__c> liveEnvs = [Select l.Env__r.TP_Manage_Active_Template__r.SiteTemplateHeader__c, l.Env__r.TP_Manage_Active_Template__r.SiteTemplateFooter__c, l.Env__r.Name, l.Env__r.Env_Id__c,l.Env__r.Env_Site_Id__c, l.Env__c From Live_Env__c l where l.Env__r.Env_Site_Id__c =: activeEntityId];
	//    if(liveEnvs == null || liveEnvs.size() <= 0 || liveEnvs[0].Env__c == null){
	//        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Live Environment is founded.'));
	//        return null;
	//    }
	//    else if(liveEnvs.size() > 1){
	//        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'More than one Live Environment is founded.'));
	//        return null;
	//    }
	//    else{
	//        header = liveEnvs[0].Env__r.TP_Manage_Active_Template__r.SiteTemplateHeader__c;
	//        footer = liveEnvs[0].Env__r.TP_Manage_Active_Template__r.SiteTemplateFooter__c;
	//        if( header != null && footer != null){
	//            headfoot.put('header',header);
	//            headfoot.put('footer',footer);
	//        }

	//        return headfoot;
	//    }

	//    return headfoot;
	//}

}