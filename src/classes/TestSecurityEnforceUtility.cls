@IsTest(SeeAllData=true)
public class TestSecurityEnforceUtility {
	
    static testMethod void testcheckObjectAccess() {
		Test.startTest();
        List<String> objectList = new List<String>();
        objectList.add('Contact');
        objectList.add('Account');
		SecurityEnforceUtility.checkObjectAccess(objectList,SecurityTypes.IS_UPDATE,'')	;
		Test.stopTest();
		
	}
    
    static testMethod void testcheckFieldAccess() {
		Test.startTest();
		SecurityEnforceUtility.checkFieldAccess('Contact','firstName',SecurityTypes.IS_UPDATE,'');
		Test.stopTest();
	}
    
    static testMethod void testcheckFieldAccess2() {
		Test.startTest();
        List<String> fieldList = new List<String>();
        fieldList.add('firstName');
        fieldList.add('LastName');
		SecurityEnforceUtility.checkFieldAccess('Contact',fieldList,SecurityTypes.IS_UPDATE,'')	;
		Test.stopTest();
		
	}
    
    
}