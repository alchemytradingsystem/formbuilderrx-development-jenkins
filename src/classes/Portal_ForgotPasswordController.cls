/********
FormBuilderRx
@company : Copyright © 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
********/
public with sharing class Portal_ForgotPasswordController {
	public Portal_UserWrapper uWrap {get;set;}
	public Portal_Settings setting {get;set;}
	public User cu {get;set;}
	public Env__c cEnv{get; set;}
	public String urlParamMapString {get;set;}
    private Map<String, String> urlParamMap;
	
	//PD-4253 | Shubham
    public String FaviconIcon{
    	get {
    		Env__c cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String imageLink = '';
    		String siteURL = Site.getPathPrefix();
	        if(cEnv != null){
	            if(String.isNotBlank(cEnv.Favicon_Icon__c) && !cEnv.Favicon_Icon__c.contains('null')){
	            	List<String> pathValues = cEnv.Favicon_Icon__c.split('##');
					if(pathValues != null && pathValues.size() > 0){
						if(pathValues.size() == 1){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]);	
						}
						else if(pathValues.size() == 2){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]) + PortalPackageUtility.escapeQuotes(pathValues[1]) ;
						}
					}
	            }
	        }
	        return imageLink;
    	}
    	set;
    }
	
	public String gtmScript{get;set;}  
	
	public Portal_ForgotPasswordController() {
	 	cEnv = ERx_PortalPackUtil.getCurrentEnv();
	 	gtmScript ='';
	 	if(cEnv != null){
        	gtmScript = cEnv.GTM_Account_Id__c;
        }
		System.debug('crrrEnv---------------------'+cEnv);
		if(cEnv != null){
			uWrap = new Portal_UserWrapper(cEnv.Id);
			setting = new Portal_Settings(cEnv.Id);
		}
		if(String.isNotBlank(cEnv.Forget_User_Configure_Text__c) && cEnv.Forget_User_Configure_Text__c.contains('<pre>')){
        	cEnv.Forget_User_Configure_Text__c= changeToHTML(cEnv.Forget_User_Configure_Text__c);
        }
        if(String.isNotBlank(cEnv.Forgot_User_Configure_Text_Another__c) && cEnv.Forgot_User_Configure_Text_Another__c.contains('<pre>')){
        	cEnv.Forgot_User_Configure_Text_Another__c= changeToHTML(cEnv.Forgot_User_Configure_Text_Another__c);
        }
        
        urlParamMap = ApexPages.currentPage().getParameters();
        urlParamMapString = POrtalPackageHelper.createParamString(urlParamMap);
        
	}
	
	
	//Function to change string into html for rich text field
    //PD-1812 changes.
    public String changeToHTML(String str){
    	str = str.replace('&quot;','\"').replace('&lt;','<').replace('&gt;','>');
    	return str;
    }

	public PageReference submit(){
		//field missing input on page
		urlParamMap = ApexPages.currentPage().getParameters();
        urlParamMapString = POrtalPackageHelper.createParamString(urlParamMap);
		if(!uWrap.isValidInputPassword()){
			return Portal_LoginUtil.addErrorMessage(setting.Password_Field_Missing_Message);
		}

		//user not found
		if(uWrap.existingUser() != null){
			//if user exists but not portal user
			this.cu = uWrap.existingUser();
			if(cu != null && uWrap.existingUser().ProfileId != setting.Student_Portal_User_Profile){
            	// 29-1-2018 Changed by Shubham Re PD-3197 Use appropriate Login configuration messages
            	return Portal_LoginUtil.addErrorMessage(setting.Password_Non_Portal_User_Exists_Message);
			}
			/*
			else{
				//portal user already exisitng
				return Portal_LoginUtil.addErrorMessage(setting.Reg_Portal_User_Exists_Message);
			}
			*/
			else{
			//user found, but non-portal user
			//if(uWrap.existingUser().ProfileId != setting.Student_Portal_User_Profile){
            //	return Portal_LoginUtil.addErrorMessage(setting.Password_Non_Portal_User_Exists_Message);
			//}
			
			//else{
				//found real potal user and start reseting password
				if(Site.forgotPassword(uWrap.email)){
					PageReference pr = Page.Portal_ForgotPasswordConfirm;
					pr = PortalPackageHelper.putParamInPageReference(pr, urlParamMap);
	  				pr.setRedirect(true);
	  				return pr;
				}
			//}
			}
        }
        
        return Portal_LoginUtil.addErrorMessage(setting.Password_No_Portal_User_Message);
	}
}