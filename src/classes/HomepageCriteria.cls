public class HomepageCriteria {
    public String order;
    public String objectName;
    public String fieldName;
    public String referenceObjectName;
	public String referenceFieldName;
    public String operator;
    public String value;
}