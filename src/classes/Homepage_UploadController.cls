/**
	FormBuilderRx 
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Class is used upload home page
 */
public with sharing class Homepage_UploadController {
	public List<Attachment> Attachments;
	public List<ContentVersion> fileList;
	public Blob document {get; set;}
	public String contentType {get; set;}
	public String fileName {get; set;}
	public sObject itemObj {get;set;}
	public Id attachmentId {get;set;}
	public Id fileId {get;set;}
	public Id cId;//checklist item Id
	public String cName{get;set;}//checklist item name
	public boolean hasConfigurationObject{get;set;}//checklist item name
	//contains configured file size of upload
	public String fileSize {get;set;}
	//contains configured file type of upload
	public String fileType {get;set;}
	public String uploadType{get;set;}
	public Boolean isApplicationFieldExist{get;set;}
	public Env__c cEnv {get;set;}
    
    //PD-4253 | Shubham
    public String FaviconIcon{get;set;}
    public String gtmScript{get;set;}
	
    /*Below parameters are used to hold the value of sobject field value on 5-10-2018*/
    Public Boolean isAllowUploadValue{get;set;}
    Public Boolean isDisplayInstructiveTextValue{get;set;}
    public String instructiveTextValue {get;set;}
	
    public Homepage_UploadController () { 
		String query = '';
		if(ngForceController.hasApplicationObject && !Test.isRunningTest()){
			//reterive current env
			Env__c env = ERx_PortalPackUtil.getEnvDetailsForCurrentUser();
			
			/*PD-4103*/
			// for gtm
			String script = '';
	        if(env != null){
	            script = env.GTM_Account_Id__c;
	        }
	        gtmScript = script;
	        // for favicon icon
	        String imageLink = '';
    		String siteURL = Site.getPathPrefix();
	        if(env != null){
	            if(String.isNotBlank(env.Favicon_Icon__c) && !env.Favicon_Icon__c.contains('null')){
	            	List<String> pathValues = env.Favicon_Icon__c.split('##');
					if(pathValues != null && pathValues.size() > 0){
						if(pathValues.size() == 1){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]);	
						}
						else if(pathValues.size() == 2){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]) + PortalPackageUtility.escapeQuotes(pathValues[1]) ;
						}
					}
	            }
	        }
	        FaviconIcon = imageLink;
			/*ends PD-4103*/
			isApplicationFieldExist = false;
			//set file size and file type from env
			fileSize = env.File_Size__c;
			fileType = env.File_Type__c;
			uploadType = env.Upload_Type__c;
			//PD-3532 || by vaishali : check added to handle previous condition
			if(uploadType == '' || uploadType == null){
				uploadType = 'Attachment';
			}
			//for existing functionality allow allow file types
			if(String.isBlank(fileType)){
				fileType = 'pdf,doc,jpg,gif,png,txt';
			}else {
				//PD-3143 Ui Error message on checklist upload and instruction message
				fileType = fileType.toLowerCase();
			}
			//if doc file allow then also add docx
			if(fileType.containsIgnoreCase('doc')){
				fileType = fileType + ',docx';
			}
			//if jpg file allow then also add jpeg
			if(fileType.containsIgnoreCase('jpg')){
				fileType = fileType + ',jpeg';
			}
			//for existing functionality file size is empty then default size to 4 MB
			if(String.isBlank(fileSize)){
				fileSize = String.valueOf(4*1024*1024);
			}
			cId= PortalPackageUtility.escapeQuotes(ApexPages.currentPage().getParameters().get('id'));
			cName= PortalPackageUtility.escapeQuotes(ApexPages.currentPage().getParameters().get('name'));
			//if id comming blank no need to proceed code for PD-1579
			if(String.isNotBlank(cId)){
				//PD-1579 changes 
				//Get Sbject from id
				Schema.SObjectType sobjectType = cId.getSObjectType();
				String sobjectName = sobjectType.getDescribe().getName();
				String packageName = PortalPackageUtility.getFinalNameSpacePerfix();
				list<String> listFields;
				hasConfigurationObject = ngForceController.hasConfigurationObject;
				if(hasConfigurationObject) {
					Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap();
					/*Set <Schema.SObjectField> objectSet = objectFields.values();
					if(objectSet.contains('EnrollmentrxRx__Instructive_Text__c')) {
						isApplicationFieldExist = true;
					} else {
						isApplicationFieldExist = false
					}*/
					for(Schema.SObjectField f : objectFields.values()) {
            			string fieldName = f.getDescribe().getName();
	            		if (fieldName == 'EnrollmentrxRx__Instructive_Text__c' || fieldName == 'EnrollmentrxRx__Allow_Upload__c') {
	            			isApplicationFieldExist = true;
	            			break;
	            		}
        			}
        			if(isApplicationFieldExist) {
						listFields  = new list<String>{'EnrollmentrxRx__Allow_Upload__c','EnrollmentrxRx__Instructive_Text__c'};
        			} else {
        				listFields  = new list<String>{'Allow_Upload__c','Instructive_Text__c'};
        			}
				} else {
					listFields  = new list<String>{'Allow_Upload__c','Display_Instructive_Text__c'};
				}
				//Query allow Upload and Instruction Text From Sobject
				//Security Check 
				if(String.isNotBlank(sobjectName)){
					if(SecurityEnforceUtility.checkObjectAccess(sobjectName, SecurityTypes.IS_ACCESS, packageName)){
						if(SecurityEnforceUtility.checkFieldAccess(sobjectName, listFields, SecurityTypes.IS_ACCESS, packageName)){
							if(hasConfigurationObject) {
								if(isApplicationFieldExist) {
									query = 'SELECT id,EnrollmentrxRx__Allow_Upload__c,EnrollmentrxRx__Instructive_Text__c'
											+' FROM ' + sobjectName 
											+' WHERE Id =\'' + cId + '\'';
								} else {
									query = 'SELECT id,Allow_Upload__c,Instructive_Text__c'
											+' FROM ' + sobjectName 
											+' WHERE Id =\'' + cId + '\'';			       				
									
								}
							} else {
								query = 'SELECT id, Allow_Upload__c, Instructive_Text__c, Display_Instructive_Text__c '
										+' FROM ' + sobjectName 
										+' WHERE Id =\'' + cId + '\'';
							}
							itemObj = Database.query(query);
									
						} else{
							ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'objectName ::' + sobjectName  
									+' FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')));
						}
					} else {
						ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'objectName ::' 
								+ sobjectName  +' FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')));
					}
					if(itemObj != null){ 
						/// if instructive text contains pre tag than alter html
						if(hasConfigurationObject) {
							if(isApplicationFieldExist) {
								if(String.isNotBlank(String.valueOf(itemObj.get('EnrollmentrxRx__Instructive_Text__c'))) 
										&& String.valueOf(itemObj.get('EnrollmentrxRx__Instructive_Text__c')).containsIgnoreCase('pre')){
									itemObj.put('EnrollmentrxRx__Instructive_Text__c', changeToHTML(String.valueOf(itemObj.get('EnrollmentrxRx__Instructive_Text__c'))));
                                    instructiveTextValue =  changeToHTML(String.valueOf(itemObj.get('EnrollmentrxRx__Instructive_Text__c')));        
                                } else{
                                	   instructiveTextValue =  String.valueOf(itemObj.get('EnrollmentrxRx__Instructive_Text__c'));         
                                }
                                isAllowUploadValue = Boolean.valueOf(itemObj.get('EnrollmentrxRx__Allow_Upload__c'));
							} else {
								if(String.isNotBlank(String.valueOf(itemObj.get('Instructive_Text__c'))) 
										&& String.valueOf(itemObj.get('Instructive_Text__c')).containsIgnoreCase('pre')){
									itemObj.put('Instructive_Text__c', changeToHTML(String.valueOf(itemObj.get('Instructive_Text__c'))));
                                    instructiveTextValue =  changeToHTML(String.valueOf(itemObj.get('Instructive_Text__c')));
                                } else{
                                        instructiveTextValue =  String.valueOf(itemObj.get('Instructive_Text__c'));      
                                }
                                isAllowUploadValue = Boolean.valueOf(itemObj.get('Allow_Upload__c'));
                                
							}
						} else {
							if(String.isNotBlank(String.valueOf(itemObj.get('Instructive_Text__c'))) 
									&& String.valueOf(itemObj.get('Instructive_Text__c')).containsIgnoreCase('pre')){
								itemObj.put('Instructive_Text__c', changeToHTML(String.valueOf(itemObj.get('Instructive_Text__c'))));
								instructiveTextValue =  changeToHTML(String.valueOf(itemObj.get('Instructive_Text__c')));     
                            } else{
                            	 instructiveTextValue =  String.valueOf(itemObj.get('Instructive_Text__c'));          
                            }
                            isDisplayInstructiveTextValue = Boolean.valueOf(itemObj.get('Display_Instructive_Text__c'));
                            isAllowUploadValue = Boolean.valueOf(itemObj.get('Allow_Upload__c'));
						}
					}
				}
			} 
		}else{
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Checklist functionality is not supported.'));
		}
		//ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Checklist functionality is not supported.'+query+'**'+'**'+hasConfigurationObject));
	}

	//Function to change string into html for rich text field
	public String changeToHTML(String str){
		str = str.replace('&quot;','\"').replace('&lt;','<').replace('&gt;','>');
		return str;
	}


	public List<Attachment>  getAttachments(){
		Attachments=[SELECT Id,Name,CreatedDate, parentId 
		             FROM Attachment 
		             WHERE ParentId=: cId ORDER BY CreatedDate DESC ];
		return Attachments;
	}
	
	public List<ContentVersion> getFileList(){
		List<ContentDocumentLink> junctionFileList = [SELECT Id, LinkedEntityId, ContentDocumentId 
	            												FROM ContentDocumentLink 
	            												WHERE LinkedEntityId =: cId];
	           Set<Id> documentId = new Set<Id>();
	            for(ContentDocumentLink cdl : junctionFileList) {
	            	documentId.add(cdl.ContentDocumentId);
	            }
	             fileList = [SELECT Id, title, ContentDocumentId, description, createdDate 
	            									FROM ContentVersion 
	            									WHERE ContentDocumentId in: documentId];
	    return fileList;
	}
	
	

	public PageReference deleteAttachment(){
		if(attachmentId != null){
			try{
				Attachment delAtt = [SELECT id 
				                     FROM Attachment 
				                     WHERE id = : attachmentId ];
				if(delAtt != null){
					if(Schema.sObjectType.Attachment.isDeletable())
						delete delAtt;
				}
				return redirect();
			}catch(Exception e) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
				return null;
			}
		}
		return null;
	}
	
	//PD-3532
	public PageReference deleteFile(){
		if(fileId != null){
			try {
				List<ContentVersion> fileList = [SELECT Id, ContentDocumentId 
												FROM ContentVersion 
												WHERE id =: fileId];
				List<ContentDocument> docList = [SELECT Id 
												FROM ContentDocument 
												WHERE id =: fileList[0].ContentDocumentId];
				delete docList;
				return redirect();
			}
			catch(Exception e) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
				return null;
			}
		}
		return null;
	}
	
	//PD-3532 || method name from uploadFile changed to uploadAttachment
	public PageReference uploadAttachment() {
		Attachment attach = new Attachment();
		contentType = contentType;
		if (Schema.sObjectType.Attachment.fields.Body.isCreateable())
			attach.Body = document;
		if (Schema.sObjectType.Attachment.fields.Name.isCreateable())
			attach.Name = fileName;
		if (Schema.sObjectType.Attachment.fields.ParentId.isCreateable())
			attach.ParentId = cId;
		try {
			insert(attach);
			return redirect();
		} catch(Exception e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
	}
	
	//PD-3532 || method added to upload document as file
	public PageReference uploadFile(){
		ContentVersion file = new ContentVersion();
		file.Title=fileName;
		file.PathOnClient = '/'+fileName;
		file.VersionData = document;
		try{
			insert file;
			ContentVersion file1 = [Select Id,title,ContentDocumentId from ContentVersion where Title = :fileName LIMIT 1];
			ContentDocumentLink cDe = new ContentDocumentLink();
			cDe.ContentDocumentId = file1.ContentDocumentId;
			cDe.LinkedEntityId = cId; // you can use objectId,GroupId etc
			//PD-3532 | Need to check this before moving code
			cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
			insert cDe;
			return redirect();
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
	}
	
	public PageReference back(){
		PageReference p = new PageReference('/');
		p.setRedirect(true);
		return p;
	}

	private PageReference redirect(){
		String url='/' + ngForceController.namespacePrefix + 'Homepage_Upload?' + '&id=' + cId+'&name=' + cName;
		PageReference p = new PageReference(url);
		p.setRedirect(true);
		return p;
	}
}