/**
	FormBuilderRx
  	@company : Copyright © 2016, Enrollment Rx, LLC
  	All rights reserved.
  	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
  	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
  	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  	@website : http://www.enrollmentrx.com/
  	@author EnrollmentRx
  	@version 1.28
  	@date 2017-09-14 
  	@Created By Kavita Beniwal
  	@description LC_RenderCriteriaConditionWrapper Class is used to hold criteria field Order By Wrapper variables
 **/
public with sharing class LC_OrderByWrapper {
	
	/****
	 * @description stores order By Field expression
	 */
	public String orderByField{get;set;}
	
	/****
	 * @description stores order By Reference Object expression
	 */
	public String orderByReferenceObject{get;set;}
	
	/****
	 * @description stores order By Reference Field expression
	 */
	public String orderByReferenceField{get;set;}
	
	/****
	 * @description stores sort Order clause expression
	 */
	public String sortOrder{get;set;}
    
}