@istest
public class Portal_RegisterControllerTest {
    static testmethod void testMethodRegister(){
        dataReady();

        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Portal_RegisterController pr = new Portal_RegisterController();
        pr.uWrap.firstName = 'testtesttest';
        pr.uWrap.lastName = 'testtesttest';
        pr.uWrap.email = 'testtestNewuser11211@test.com';
        pr.uWrap.password = '443322334';
        pr.uWrap.confirmPassword = '443322334';
        pr.register();
        pr.basicFieldsValidation();
        pr.extraFieldsValidation();


        pr.uWrap.confirmPassword = '';
        pr.register();

        pr.uWrap.confirmPassword = '443322334';
        pr.register();

        System.assertNotEquals(pr, null);
    }
    //Pd-2689 | Gourav | to test socialRegister
    static testmethod void testMethodRegisterSocial(){
        dataReady();

        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Portal_RegisterController pr = new Portal_RegisterController();
        pr.uWrap.firstName = 'testnew';
        pr.uWrap.lastName = 'newTest';
        pr.uWrap.email = 'testNewuser112@test.com';
        pr.isSocialLogin = true;
        pr.register();
        System.assertNotEquals(pr, null);
    }
    //Pd-2689 | Gourav | to test the error message
    static testmethod void testMethodRegisterSocialError(){
        dataReady();
        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        ApexPages.currentPage().getParameters().put('ErrorDescription','requiredFields##@##gourav##@##gandhi##@##a@b.com');

        Portal_RegisterController pr = new Portal_RegisterController();
        ApexPages.currentPage().getParameters().put('ErrorDescription','missingEmail');
        ApexPages.currentPage().getParameters().put('ProviderType','google');
        Portal_RegisterController pr1 = new Portal_RegisterController();
        ApexPages.currentPage().getParameters().put('ErrorDescription','unknownEror');
        Portal_RegisterController pr2 = new Portal_RegisterController();

    }

    private static void dataReady(){
        Lead l=new Lead();
        l.Email='TesetRui12312222@test.com';
        l.LastName='test';
        l.Company='test';
        //insert l;

        Contact c=new Contact();
        c.LastName='c';
        c.Email='TesetssRui12312222@test.com';
        insert c;

        Contact c1=new Contact();
        c1.LastName='c';
        c1.Email='xyz123asd@test.com';
        insert c1;

        Test_Erx_Data_Preparation.preparation();
    }

    static testmethod void testMethodRegister9(){
        dataReady();
        Contact c4=new Contact();
        c4.LastName='c';
        c4.Email='xyz1221asd@test.com';
        insert c4;
        Test.setCurrentPage(Page.Portal_Register);
        //User uas = [select Id, FIrstName from User where Email = 'TesetssRui12312222@test.com'];
        //Contact conco = [select Id, FIrstName from Contact where Email = 'TesetssRui12312222@test.com'];
        //system.debug('Anydatatype_msg++++++++'+uas);
        //system.debug('Anydatatype_msg++++++++'+conco);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Test_Erx_Data_Preparation.customFIeldsList[0].Is_Field_Required__c = true;
        update Test_Erx_Data_Preparation.customFIeldsList[0];

        Portal_RegisterController pr = new Portal_RegisterController();

        pr.uWrap.firstName = 'testtesttest';
        pr.uWrap.lastName = 'testtesttest';
        pr.uWrap.email = 'xyz1221asd@test.com';
        //pr.uWrap.password = '443322334';
        //pr.uWrap.existingContact() = c4;
        pr.register();
        pr.uWrap.password = '44334';
        //pr.existingCu = null;
        //pr.existingCt = c4;
        pr.register();
        System.assertNotEquals(pr, null);

    }


    static testmethod void testMethodRegister2(){
        dataReady();
        Test.setCurrentPage(Page.Portal_Register);
        //User uas = [select Id, FIrstName from User where Email = 'TesetssRui12312222@test.com'];
        //Contact conco = [select Id, FIrstName from Contact where Email = 'TesetssRui12312222@test.com'];
        //system.debug('Anydatatype_msg++++++++'+uas);
        //system.debug('Anydatatype_msg++++++++'+conco);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Test_Erx_Data_Preparation.customFIeldsList[0].Is_Field_Required__c = true;
        update Test_Erx_Data_Preparation.customFIeldsList[0];

        Portal_RegisterController pr = new Portal_RegisterController();

        pr.uWrap.firstName = 'testtesttest';
        pr.uWrap.lastName = 'testtesttest';
        pr.uWrap.email = 'TesetssRui12312222@test.com';
        pr.uWrap.password = '443322334';
        pr.register();
        pr.basicFieldsValidation();
        pr.extraFieldsValidation();


        pr.uWrap.confirmPassword = '';
        pr.register();

        pr.uWrap.confirmPassword = '443322334';
        pr.register();

        pr.setting.Account_Name = null;
        pr.retrieveAccountId();

        pr.setting.Account_Name = 'testAccount';
        pr.retrieveAccountId();

        System.assertNotEquals(pr, null);

    }

    static testmethod void testCHangeToHtml(){
        dataReady();
        Portal_RegisterController pr = new Portal_RegisterController();
        Test.startTest();
        String actual = pr.changeToHTML('');
        String expected = '';
        System.assertEquals(expected, actual);
        Test.stopTest();
    }
    
    static testmethod void testMethodRegister3(){
        dataReady();

        Portal_Registration_Message__c msg = new Portal_Registration_Message__c();
        msg.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        msg.Account_Name__c = 'testAcc';
        msg.Incorrect_Username_Password_Message__c='testUnamePsd';
        msg.Reg_Field_Missing_Message__c='msing1';
        msg.Reg_Password_No_Match_Message__c='msing1';
        msg.Reg_Email_Format_Error_Message__c='msing1';
        msg.Reg_Username_Too_Long_Message__c='msing1';
        msg.Login_Field_Missing_Message__c='msing1';
        msg.Login_Non_Portal_User_Message__c='test';
        msg.Reg_Portal_User_Exists_Message__c='test2';
        msg.Password_Field_Missing_Message__c = 'testpasswd';
        msg.Password_Non_Portal_User_Exists_Message__c = 'nonportaluser';
        msg.Password_Reset_Confirm_Message__c = 'testconfirm';
        msg.Student_Portal_User_Profile__c= Test_Erx_Data_Preparation.testAdminUser.ProfileId;
        insert msg;
        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);


        Portal_RegisterController pr = new Portal_RegisterController();

        pr.uWrap.firstName = 'testtesttest';
        pr.uWrap.lastName = 'testtesttest';
        pr.uWrap.email = 'TesetssRui12312222@test.com';
        pr.uWrap.password = '443322334';
        pr.setting = new Portal_Settings(pr.cEnv.Id);
        //pr.register();
        //pr.basicFieldsValidation();
        //pr.extraFieldsValidation();


        pr.uWrap.confirmPassword = '';
        pr.register();

        pr.uWrap.confirmPassword = '4433223345';
        pr.register();

        pr.uWrap.confirmPassword = '443322334';
        pr.uWrap.email = 'Tst.com';
        pr.register();

        pr.uWrap.email = 'TesetssRuiTesetssRuiTesetssRuiTesetssRuiTesetssRui12312222@test.com';
        pr.register();

        pr.uWrap.email = 'Tesetssx2@test.com';
        pr.register();

        pr.uWrap.email = Test_Erx_Data_Preparation.testAdminUser.email;
        pr.register();


        pr.register();
        String favIcon = pr.FaviconIcon;
        String gtm = pr.gtmScript;
		pr.initPageException();
        System.assertNotEquals(pr, null);
    }
    
}