/** 
    FormBuilderRx
    @company : Copyright © 2016, Enrollment Rx, LLC
    All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    @website : http://www.enrollmentrx.com/
    @author EnrollmentRx
    @version 1.0
    @date 2016-08-23
    @description Test Class is used for Erx_PageEntities
*/ 
@isTest
public with sharing class TestERx_Portal_EnvPanelController {
    private static Env__c env;
    private static User testUser; 
    
    static testMethod void testException () {
        Test.startTest();
            ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
            portalEnvObject.isPaidUsrCon = true;
            portalEnvObject.getStatus();
            portalEnvObject.configureEnv();
            portalEnvObject.configureEditEnv();
            portalEnvObject.configureCurrentCommunityId();
            portalEnvObject.configureCurrentSiteId();
            portalEnvObject.configureCloneEnv();
            portalEnvObject.userAll();
            portalEnvObject.userEnv();
            portalEnvObject.setSiteEnv();
            portalEnvObject.SiteConfigure();
            portalEnvObject.checkLicense();
            portalEnvObject.deleteEnv();
            portalEnvObject.cloneEnv();
            portalEnvObject.save();
            portalEnvObject.isPaidUsrCon = false;
            portalEnvObject.configureCurrentCommunityId();
            portalEnvObject.configureCurrentSiteId();
            System.assertEquals(true, (portalEnvObject != null));
        Test.stopTest();
    }
    
     static testMethod void testObjectPermission() {
        preparetestUser();
        prepareDataForClone();
        prepareDataForPortalCustomLoginField();
        prepareDataForHomePageWidget();
        prepareDataForPortalRedistrationMessage();
        prepareDataForHomePageWidgetLayout();
        prepareDataForCustomSetting();
        system.runAs(testUser){
        Test.startTest();
            ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
            portalEnvObject.isPaidUsrCon = true;
            portalEnvObject.configureEnv();
            portalEnvObject.configureCurrentCommunityId();
            portalEnvObject.configureCurrentSiteId();
            portalEnvObject.configureCloneEnv();
            portalEnvObject.userAll();
            portalEnvObject.userEnv();
            portalEnvObject.getStatus();
            portalEnvObject.configPopup();
            //portalEnvObject.saveUser();
            portalEnvObject.setSiteEnv();
            portalEnvObject.SiteConfigure();
            portalEnvObject.checkLicense();
            portalEnvObject.deleteEnv();
            portalEnvObject.cloneEnv();
            portalEnvObject.save();
            portalEnvObject.isPaidUsrCon = false;
            portalEnvObject.configureCurrentCommunityId();
            portalEnvObject.configureCurrentSiteId();
            System.assertEquals(true, (portalEnvObject != null));
        Test.stopTest();
        }
    }
    
     static testMethod void testConfigureCheckBox(){
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.configureCheckBox();
        System.assertEquals(true,portalEnvObject.checkBox3);
        portalEnvObject.configureDisableCheckBox();
        System.assertEquals(false,portalEnvObject.checkBox3);
        portalEnvObject.cancel();
        portalEnvObject.configureDefaultValue();
        Test.stopTest();
    }
    
     static testMethod void testClone(){
        prepareDataForClone();
        prepareDataForPortalCustomLoginField();
        prepareDataForHomePageWidget();
        prepareDataForPortalRedistrationMessage();
        prepareDataForHomePageWidgetLayout();
        prepareDataForCustomSetting();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.isPaidUsrCon = true;
        portalEnvObject.configureCheckBox();
        portalEnvObject.resetEnv();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.configureCloneEnv();
        portalEnvObject.cloneEnv();
        portalEnvObject.newEnv = null;
        portalEnvObject.configureCloneEnv();
        portalEnvObject.cloneEnv();
        portalEnvObject.configureCurrentCommunityId();
        portalEnvObject.configureCurrentSiteId();
        System.assertEquals(true, portalEnvObject != null);
        Test.stopTest();
    }
    
    
     static testMethod void testDeleteEnv(){
        prepareDataForLiveEnv();
        Test.startTest();
            ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
            portalEnvObject.editEnvId = env.id;
            portalEnvObject.deleteEnv();
            portalEnvObject.editEnvId = null;
            portalEnvObject.deleteEnv();
            system.assertEquals(true,env.id != null);
        Test.stopTest();
    }
    
     static testMethod void testConfigureIdleEnv(){
        prepareDataForIdleEnv();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.editEnvName = 'Test';
        portalEnvObject.configureEnv();
        System.assertEquals(true,ERx_Portal_EnvPanelController.isFromAdminPanel );
        Test.stopTest();
    }
    
     static testMethod void testConfigureLiveEnv(){
        prepareDataForLiveEnv();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.editEnvName = 'Test';
        portalEnvObject.configureEnv();
        System.assertEquals(true,ERx_Portal_EnvPanelController.isFromAdminPanel );
        Test.stopTest();
    }
    
    static testMethod void testConfigureTestEnv(){
        prepareDataForClone();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.configureEnv();
        portalEnvObject.editEnvId = null;
        portalEnvObject.configureEnv();
        System.assertEquals(true,ERx_Portal_EnvPanelController.isFromAdminPanel );
        Test.stopTest();
    }
    
      static testMethod void testConfigureEditEnv(){
        prepareDataForClone();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.configureEditEnv();
        System.assertEquals(true,portalEnvObject.baseUrl != null);
        portalEnvObject.editEnvId = 'abc';
        portalEnvObject.configureEditEnv();
        Test.stopTest();
    }
    
     static testMethod void testSave(){
        prepareDataForClone();
        Test.startTest();
            ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
            portalEnvObject.getTemplateIds();
            portalEnvObject.newEnv.TP_Manage_Property__c = 'Community';
            portalEnvObject.newEnv.Env_Id__c = env.Env_Id__c;
            portalEnvObject.save();
            portalEnvObject.editEnvId = env.id;
            portalEnvObject.save();
            System.assertNotEquals(true,portalEnvObject.enableCondition);
            portalEnvObject.enableCondition = true;
            portalEnvObject.newEnv.TP_Manage_Property__c = 'Community';
            portalEnvObject.newEnv.Env_Id__c = env.Env_Id__c;
            portalEnvObject.save();
            portalEnvObject.resetEnv();
            portalEnvObject.enableCondition = true;
            portalEnvObject.newEnv.TP_Manage_Property__c = 'Site';
            portalEnvObject.newEnv.Env_Id__c = env.Env_Id__c;
            portalEnvObject.save();
        Test.stopTest();
    }
    
     static testMethod void testSiteEnv(){
        prepareDataForClone();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.setSiteEnv();
        portalEnvObject.editEnvId = null;
        portalEnvObject.setSiteEnv();
        System.assertNotEquals(true,ERx_Portal_EnvPanelController.isFromAdminPanel );
        Test.stopTest();
    }
      static testMethod void testConfigureSite(){
        prepareDataForClone();
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.editEnvId = env.id;
        portalEnvObject.SiteConfigure();
        portalEnvObject.editEnvId = null;
        portalEnvObject.SiteConfigure();
        portalEnvObject.cancelSite();
        System.assertNotEquals(true,portalEnvObject.enableSiteConfigure );
        Test.stopTest();
    }
    
    static testMethod void testUserAll(){
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        portalEnvObject.userAll();   
        portalEnvObject.selectUserName = 'Test';
        portalEnvObject.userAll(); 
		portalEnvObject.testUser();
        System.assertEquals(true, portalEnvObject.userList != null);
        
        Test.stopTest();
    }
    
     static testMethod void testUserEnv(){
        preparetestUser();
        prepareDataForClone();
        upsert new Test_Records__c(
            UserId__c = testUser.id,
            Env_Id__c = env.id,
            Name = 'Test'
        );
        Test.startTest();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        System.assertEquals(null,portalEnvObject.userEnv());
        portalEnvObject.editEnvId = env.id;
        System.assertEquals(null,portalEnvObject.userEnv());
        portalEnvObject.userEnv();
        Test.stopTest();
    }
     static testMethod void testforPageReference () {
        prepareDataForLiveEnv();
        Test.startTest();
            ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
            Test.setCurrentPage(new PageReference('TM_TemplateAssignment'));
            System.assertEquals(true, portalEnvObject.setupCurrEntity() != null);
            Test.setCurrentPage(new PageReference('TM_TemplateContent'));
            System.assertEquals(true, portalEnvObject.setupCurrEntity() != null);
            Test.setCurrentPage(new PageReference('Portal_RegistrationSetting'));
            System.assertEquals(true, portalEnvObject.setupCurrEntity() != null);
            Test.setCurrentPage(new PageReference('Portal_Page_Configure'));
            System.assertEquals(true, portalEnvObject.setupCurrEntity() != null);
            Test.setCurrentPage(new PageReference('HomepageBuilder_ng'));
            System.assertEquals(true, portalEnvObject.setupCurrEntity() != null);
            Test.setCurrentPage(new PageReference(''));
            System.assertEquals(true, portalEnvObject.setupCurrEntity() != null);
        Test.stopTest();
    }
    
    static testMethod void testSaveUser(){
        preparetestUser();
        Test.startTest();
        prepareDataForLiveEnv();
        ERx_Portal_EnvPanelController portalEnvObject = new ERx_Portal_EnvPanelController();
        //portalEnvObject.saveUser();
        portalEnvObject.selectUserId = testUser.id;
        portalEnvObject.selectUserName = 'Testing';
        //portalEnvObject.testUser = 'Testing';
        portalEnvObject.editEnvId =env.Id;
       	// portalEnvObject.saveUser();
        System.assertEquals(true,[select id from env__c].size() > 0);
        Test.stopTest();
    }
    public static void preparetestUser(){
        Account a; 
        Contact student;  
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
    }
    
    public static void prepareDataForClone() {
        //List<Network> networkList = [Select Id,Name from Network Limit 3];
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        List<Env__c> listEnv = new List<Env__c>();
            
        system.debug(networkList);
        for(Integer i=0;i<networkList.size() ;i++) {
            Env__c env1 = new Env__c(Name = 'Test', Env_Id__c = String.valueOf(networkList[i].get('Id')) ,Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Test' );
            listEnv.add(env1);
        }
         
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert listEnv;
        env = listEnv[0];
        
        Erx_Page__c page1 = new Erx_Page__c ( Env__c = env.Id, 
            IsActive__c = true,
            Page_Header__c = '{"title":"Test"}', 
            Page_Name__c = ('Test Page ' + (1)), 
            Order__c = 1,
            Page_Buttons__c = '{"Buttons":[]}',
            Disable_Criteria__c = '[]',
            ModelList__c = '[]',
            Rendered_Criteria__c = '[]'
            );
        
        insert page1;   
        Erx_Section__c sectionObj = new Erx_Section__c( Erx_Page__c = page1.id,
        Modal_Data__c = '[{"objectName":"Account","objectAPIName":"Account","modelName":"account","field":[{"fieldName":"Account ID","fieldAPIName":"Id"},{"fieldName":"Account Name","fieldAPIName":"Name"}],"conditionExpression":null,"condition":[{"valueType":"STATIC","value":"test","referencedObjectName":"","pageId":null,"modelName":null,"isFormulaField":false,"fieldOperator":"equals","fieldDisplayType":"STRING","fieldAPIName":"Name","displayValue":""}]}]',
        Order__c = 1,
        View_Layout__c = '{"title":"test","sectionTabIndexing":"","sectionRenderCriteriaList":[],"sectionOrder":1,"sectionLayout":"ONECOLUMN","sectionCollapsible":"false","sectionBorder":"0px","renderedExpressionFinal":null,"renderedExpression":"","rendered":true,"removeButtonLabel":null,"modelType":"FIELDMODEL","modelName":"","isAddAnother":false,"fontWeight":"normal","fontStyle":"normal","fontSize":"8px","fontFamily":"Arial","fontColor":"#000000","field":[{"usedInFormulaFieldList":null,"type":"STRING","titleType":null,"title":null,"targetObjectRecord":null,"targetobjectAutoSearch":null,"targetObjectAPIName":null,"targetObjectActiveFieldAPIName":null,"targetObject":null,"tabIndexValField":null,"subSectionTabIndexing":null,"startDate":null,"sourceObjectFieldsAPIName":null,"sourceObjectFieldLabels":null,"sourceObjectCriteriaField":null,"sourceObjectClickableField":null,"sourceObjectAPIName":null,"sourceObject":null,"selectSearchLookupFieldName":null,"selectOptionList":null,"selectedValues":null,"searchingobject":null,"searchingfield":null,"searchFieldList":null,"requiredSymbol":null,"requiredExpressionFinal":null,"requiredExpression":null,"requiredErrorMessage":null,"requiredCriteriaList":null,"required":"false","rendredExpression":null,"renderedExpression":null,"rendered":true,"renderCriteriaList":null,"removeButtonLabel":null,"regexValidationPattern":null,"regexValidationErrorMessage":null,"referenceFieldName":null,"placeholderText":"","picklistDependencyMap":null,"parentIdSource":null,"parentId":null,"paragraphText":null,"pageCompleteDiscriptiveText":null,"pageCompleteDisabledSettings":null,"notFoundFieldsList":null,"multiple":null,"multipAppButtonLabel":null,"multiApplicationDescriptiveText":"","modelNameWithFieldAPIName":null,"modelName":"account","maximumsize":null,"mappings":null,"lookupValueList":null,"lookupRecordCriteria":null,"lookupNameObject":null,"label":"Account Name","isUpdatable":null,"isInsertble":null,"isFormulaField":false,"isFirstValueBlank":null,"isdependentField":null,"isControllingField":null,"isBlank":false,"isBackendDependentField":false,"isAutoPopulated":null,"isAddAnother":null,"isAccesible":null,"instructionTextPosition":"","instructionText":"","hoverText":"","fieldSortOrderMap":null,"fieldName":"Account Name","fieldDisplayType":"STRING","fieldAPIName":"Name","endDate":null,"displayValueAPIName":null,"displayFieldList":null,"disableExpressionFinal":null,"disableExpression":null,"disableCriteriaList":null,"disable":"false","dependentCriteriaMap":null,"customPicklistValues":null,"createNewAppDirectURL":null,"createNewAppButtonURL":null,"columnLayout":null,"checkBoxFieldMultiPickListMap":null,"checkBoxFieldConfigurationString":null,"checkBoxFieldConfigurationMap":null,"buttonrRenderedExpressionFinal":null,"buttonrRenderedExpression":null,"buttonrRenderCriteriaList":null,"buttonRendered":null,"bgColorTitle":null,"bgColorCode":null,"backendControllingFieldName":null,"availableValues":null,"autoPopulateRenderCriteriaList":null,"autoPopulateFieldWrapperList":null,"autoPopulateEnableExpressionFinal":null,"autoPopulatedFormula":null,"autoPopulatedFinalConditionExpression":null,"autoPopulatedFieldList":null,"autoPopulatedExpression":null,"autoPopulatedConditionCriteriaList":null,"autoPopulate_Id":null,"autoLookuprendered":null,"autoLookupRenderCriteriaList":null,"allowedContentType":null,"addAnotherLimit":null,"addAnotherButtonLabel":null}],"description":"","bgColorTitle":"#34495E","bgColorDesc":"#FFFFFF","backgroundColor":"#ffffff","addAnotherLimit":null,"addAnotherButtonLabel":null}');
        insert sectionObj;
       
    }
    
    public static void prepareDataForPortalCustomLoginField(){
        Portal_Login_Custom_Field__c plcf =  new Portal_Login_Custom_Field__c();
        plcf.Env__c = env.id;
        plcf.Name = 'Test Portal Login Custom Field';
        insert plcf;
    }
    
    public static void prepareDataForHomePageWidgetLayout(){
        Homepage_Widget_Layout__c hwl =  new Homepage_Widget_Layout__c();
        hwl.Env__c = env.id;
        hwl.Name = 'Test Homepage Widget Layout';
        insert hwl;
    }
    
     public static void prepareDataForPortalRedistrationMessage(){
        Portal_Registration_Message__c prm =  new Portal_Registration_Message__c();
        prm.Env__c = env.id;
        insert prm;
    }
    
     public static void prepareDataForHomePageWidget(){
        Homepage_Widget__c hw =  new Homepage_Widget__c();
        hw.Env__c = env.id;
        insert hw;
    }
    
    public static void prepareDataForCustomSetting(){
        FormBuilder_Settings__c setting  =  new FormBuilder_Settings__c();
        setting.Name = 'Admin Settings';
        setting.Permission_Error_Message__c = 'Permission_Error_Message__c';
        setting.Admin_Email__c = 'abx@email.com';
        setting.Lookup_Max_Limit__c = 5;
        setting.Lookup_Admin_Max_Limit__c =6;
        insert setting;
    }
    
     public static void prepareDataForLiveEnv() {
        //List<Network> networkList = [Select Id,Name from Network Limit 3];
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        List<Env__c> listEnv = new List<Env__c>();
        for(Integer i=0;i<networkList.size() ;i++) {
            Env__c env1 = new Env__c(Name = 'Test', Env_Id__c = String.valueOf(networkList[i].get('Id')) ,Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Live' );
            listEnv.add(env1);
        }
        Env__c env1 = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        listEnv.add(env1);
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert listEnv;
        env = listEnv[0];
    }
    
     public static void prepareDataForIdleEnv() {
        //List<Network> networkList = [Select Id,Name from Network Limit 3];
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        List<Env__c> listEnv = new List<Env__c>();
        for(Integer i=0;i<networkList.size() ;i++) {
            Env__c env1 = new Env__c(Name = 'Test', Env_Id__c = String.valueOf(networkList[i].get('Id')) ,Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Idle' );
            listEnv.add(env1);
        }
        Env__c env1 = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Idle' );
        listEnv.add(env1);
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert listEnv;
        env = listEnv[0];
    }
    
    
    
}