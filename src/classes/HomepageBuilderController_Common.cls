public with sharing class HomepageBuilderController_Common {
	//sobject
  public String hwlJSON{get;set;}
  	/****
    * @description To store PackageName;
    */
    private static String namespacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();

    /****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
	//get all existing hw id in current org
	public String hwIdMapJSON{get;set;}
	/****
    * @description list of all document in Widget Library folder
    */
    public String widgetLibraryDocumentList{get;set;}
    
    public HomepageBuilderController_Common (ngForceController ngcon){
        Id hwlId=ApexPages.currentPage().getParameters().get('id');
        if(!String.isBlank(hwlId)){
        	Homepage_Widget_Layout__c hwl;
        	List<String> listFields =  new List<String> {'Grid_Data__c','Grid_Width__c'};
        	if(SecurityEnforceUtility.checkObjectAccess('Homepage_Widget_Layout__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('Homepage_Widget_Layout__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
             		hwl=[select Id,Name,Grid_Data__c,Grid_Width__c from Homepage_Widget_Layout__c where Id=:hwlId];
            		hwlJSON=String.escapeSingleQuotes(JSON.serialize(hwl));
					hwlJSON=hwlJSON.replace('</script>','<\\/script>');
				} else {
            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Homepage_Widget_Layout__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'HomepageBuilderController_Common', 'HomepageBuilderController_Common', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
        	} else {
            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Homepage_Widget_Layout__c ' ), 'HomepageBuilderController_Common', 'HomepageBuilderController_Common', null);
				throw new PortalPackageException(permissionErrorMessage);
        	}
			List<Homepage_Widget__c> hwList;
				if(SecurityEnforceUtility.checkObjectAccess('Homepage_Widget_Layout__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
					hwList=[select Id from Homepage_Widget__c limit: limits.getLimitQueryRows()];
				} else {
            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Homepage_Widget_Layout__c ' ), 'HomepageBuilderController_Common', 'HomepageBuilderController_Common', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
			if(hwList.size()!=0){
				Map<String,Boolean> hwIdMap=new Map<String,Boolean>();
				for(Homepage_Widget__c hw:hwList)
				hwIdMap.put(hw.Id,true);
				hwIdMapJSON=JSON.serialize(hwIdMap);
			}
			
			widgetLibraryDocumentList = JSON.serialize(getWidgetLibraryList());
        }
    }

    /*******************************************************************************************************
    * @description Reterieve all document contains in widget library folder.
    * @return return list of document from folder widget library folder.
    */
    public List<Document> getWidgetLibraryList(){
    	List<Document> documents = [SELECT Id,Name FROM Document WHERE Folder.Name = 'Widget Library' LIMIT :limits.getLimitQueryRows()];
    	return documents;
    }
    
    /*******************************************************************************************************
    * @description forces DML to insert log messages in database
    */
    public void initPageException() {
        try {
         ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();

        } catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }

 }