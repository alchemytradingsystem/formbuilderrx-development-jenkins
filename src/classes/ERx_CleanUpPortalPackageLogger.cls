/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-12-28 
	@description ERx_CleanUpPortalPackageLogger is a batch class to delete PortalPackageLogger records
*/
	global with sharing class ERx_CleanUpPortalPackageLogger implements Database.Batchable<sObject>, Database.Stateful {

		/****
        * @description Stores query string
        */
		private final String query;
		
	    /*
	    	@description: Main Constructor
	    */
	    global ERx_CleanUpPortalPackageLogger(String q) {
	    	query = q;
	   	}
	
		/*
	    	@description: Start Method returns the data on which the Batch job will be operating
	    */
	   	global Database.QueryLocator start(Database.BatchableContext BC) {
	        return Database.getQueryLocator(query);
	   	} 
	   
	    /*
	    	@description: Execute method performs all the processing required for Batch Job i.e. delete the logger records
	    */
	   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	   		try {
				//PD-4402 || Delete Security check
				if(SecurityEnforceUtility.checkObjectAccess('Portal_Package_Logger__c', SecurityTypes.IS_DELETE, PortalPackageUtility.getNameSpacePerfix())){
					List<Database.DeleteResult> drList = Database.Delete(scope, false);
					// Iterate through each returned result
					for(Database.DeleteResult dr : drList) {
					    if (!dr.isSuccess()) {
					        // Operation failed, so get all errors
					        for(Database.Error err : dr.getErrors()) {
					        	ERx_PortalPackageLoggerHandler.addException(new PortalPackageException('The following error has occurred.'+err.getStatusCode() + ': ' + err.getMessage()+'Record fields that affected this error: ' + err.getFields()), 'Daily CleanUp PortalPackageLogger','ERx_CleanUpPortalPackageLogger execute',dr.getId());                 
					        }
					    }
					}
					Database.emptyRecycleBin(scope);
				} else {
		        	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Package_Logger__c'), 'ERx_CleanUpPortalPackageLogger', 'execute', null);
					throw new PortalPackageException(PortalPackageUtility.getPermissionErrorMessage()); 
				}
	   		} catch(Exception e) {
	   			ERx_PortalPackageLoggerHandler.addException(e, 'Daily CleanUp PortalPackageLogger','ERx_CleanUpPortalPackageLogger execute',null);     
	   		}
	   		ERx_PortalPackageLoggerHandler.saveExceptionLog();
	   	}
	
		/*
	    	@description: Finish Method performs finishing activities like sending an email with information 
	    				  about the batch job records processed and status
	    */
	   	global void finish(Database.BatchableContext BC) {
	    	// Get the ID of the AsyncApexJob representing this batch job
		    // from Database.BatchableContext.
		    // Query the AsyncApexJob object to retrieve the current job's information.
	    	AsyncApexJob a = 
	            [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	            TotalJobItems, CreatedBy.Email
	            FROM AsyncApexJob WHERE Id =
	            :BC.getJobId()];
	                          
	        // Send an email to the Apex job's submitter 
	        //   notifying of job Failed or Aborted. 
	        if(a.Status == 'Failed' || a.Status == 'Aborted') {	    	
		        Erx_EmailLoggerCleanUp.sendMail('Record Clean Up Status: ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.', a.CreatedBy.Email);
	        }
	    }
}