/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description this class is used to preventing deleting and editing  on admin panel in live Environment 
*/
public with sharing class LiveEnv {
	
	public static void preventingDeletingEditing(List<Sobject> objList, List<Sobject> newObjList , Set<Id> liveIds ){
		for(Sobject obj : objList) {
		   if(obj.getSObjectType()==Schema.Portal_Registration_Message__c.getSObjectType()){
		        if(liveIds.contains(String.valueOf(obj.get('Env__c')))) {
		            obj.addError(PortalPackageConstants.REGISTRATION_MESSAGE_UPSERTION_DELETION_ERROR_MESSAGE);
		        }
		   } 
		   if(obj.getSObjectType() == Schema.SiteLoginTemplate__c.getSObjectType()){
		   		if(liveIds.contains(String.valueOf(obj.get('Id')))) {
		   			// 13-03-18 Added By Shubham Re PD-3347 criteria conditions for another environment can be edited when allow editing false 
		        	if(newObjList != null && newObjList.size() > 0){
		        		Sobject newObj = null;
			        	for(Sobject objct : newObjList){
			        		if(obj.id == objct.Id){
			        			newObj = objct;
			        			break;
			        		}
			        	}
		        		if(newObj != null){
				        	if(obj.get('Active__c')!= newObj.get('Active__c') ||
				  	 		obj.get('Description__c')!= newObj.get('Description__c') ||
				  	 		obj.get('jQueryHandledFooter__c')!= newObj.get('jQueryHandledFooter__c') ||
				  	 		obj.get('isJqueryInHeaderandFooter__c')!= newObj.get('isJqueryInHeaderandFooter__c') ||
				  	 		obj.get('SiteTemplateFooter__c')!= newObj.get('SiteTemplateFooter__c') ||
				  	 		obj.get('SiteTemplateHeader__c')!= newObj.get('SiteTemplateHeader__c') ||
				  	 		obj.get('SiteTemplateName__c')!= newObj.get('SiteTemplateName__c') ||
				  	 		obj.get('Static_Resource_List__c')!= newObj.get('Static_Resource_List__c') ||
				  	 		obj.get('jQueryHandledHeader__c')!= newObj.get('jQueryHandledHeader__c') ){
				            	obj.addError(PortalPackageConstants.TEMPLATE_UPSERTION_DELETION_ERROR_MESSAGE);
				        	}
		        		}
		        	}
		        	else{
		        		obj.addError(PortalPackageConstants.TEMPLATE_UPSERTION_DELETION_ERROR_MESSAGE);
		        	}
		        }
		   }
		   if(obj.getSObjectType()==Schema.Portal_Login_Custom_Field__c.getSObjectType()){
		        if(liveIds.contains(String.valueOf(obj.get('Env__c'))) && Boolean.valueOf(obj.get('Active__c'))) {
       		   		obj.addError(PortalPackageConstants.PORTAL_LOGIN_CUSTOM_FIELD_UPSERTION_DELETION_ERROR_MESSAGE);
      			  }
		   }
		   if(obj.getSObjectType()==Schema.Erx_Page__c.getSObjectType()){
      			 if(liveIds.contains(String.valueOf(obj.get('Id'))) && Boolean.valueOf(obj.get('IsActive__c'))) {
           			 obj.addError(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE);
        		}
		   } 
		  if(obj.getSObjectType()==Schema.Homepage_Widget_Layout__c.getSObjectType()){
      			  if(liveIds.contains(String.valueOf(obj.get('Env__c'))) && Boolean.valueOf(obj.get('Is_Active__c'))) {
       		   		obj.addError(PortalPackageConstants.HOMEPAGE_WIDGET_LAYOUT_UPSERTION_DELETION_ERROR_MESSAGE);
      			  }
		   }
		   if(obj.getSObjectType()==Schema.Homepage_Widget__c.getSObjectType()){
      			  if(liveIds.contains(String.valueOf(obj.get('Env__c'))) && Boolean.valueOf(obj.get('Is_Active__c'))) {
       		   		obj.addError(PortalPackageConstants.HOMEPAGE_WIDGET_UPSERTION_DELETION_ERROR_MESSAGE);
      			  }
		   }
	    }
	
	}
	public static void preventingEditing(List<Sobject> beforeUpdateObjList, List<Sobject> afterUpdateObjList,Set<Id> liveIds){
	  for(Integer obj=0;obj<beforeUpdateObjList.size();obj++){
	  	if(beforeUpdateObjList.get(obj).getSObjectType()==Schema.Env__c.getSObjectType()){
		  	 if(liveIds.contains(beforeUpdateObjList.get(obj).id)){
		  	 	if(beforeUpdateObjList.get(obj).get('TP_Manage_Active_Template__c')!= afterUpdateObjList.get(obj).get('TP_Manage_Active_Template__c') ||
		  	 		beforeUpdateObjList.get(obj).get('Site_Template_Criteria__c')!=afterUpdateObjList.get(obj).get('Site_Template_Criteria__c') ||
		  	 		beforeUpdateObjList.get(obj).get('Template_for_Site_Login__c')!=afterUpdateObjList.get(obj).get('Template_for_Site_Login__c') ||
		  	 		beforeUpdateObjList.get(obj).get('Main_Template_Criteria__c')!=afterUpdateObjList.get(obj).get('Main_Template_Criteria__c'))
		  	  		beforeUpdateObjList.get(obj).addError(PortalPackageConstants.TEMPLATE_UPSERTION_DELETION_ERROR_MESSAGE);
		  	 }
		  	 //PD-2689 | Gourav | new fields on envs
		  	 if(liveIds.contains(beforeUpdateObjList.get(obj).id)){
		  	 	if(beforeUpdateObjList.get(obj).get('Social_Login__c')!=afterUpdateObjList.get(obj).get('Social_Login__c') ||
		  	 	 	beforeUpdateObjList.get(obj).get('By_Pass_required_fields__c')!=afterUpdateObjList.get(obj).get('By_Pass_required_fields__c')) {
		  	  		beforeUpdateObjList.get(obj).addError(PortalPackageConstants.PORTAL_SOCIAL_LOGIN_UPSERTION_DELETION_ERROR_MESSAGE);
		  	 	}
		  	 }
		  	 
	  	}
	  	 if(beforeUpdateObjList.get(obj).getSObjectType() == Schema.Portal_Login_Custom_Field__c.getSObjectType()){
		        if(liveIds.contains(String.valueOf(beforeUpdateObjList.get(obj).get('Env__c')))
		        	 && (Boolean.valueOf(beforeUpdateObjList.get(obj).get('Active__c')!=afterUpdateObjList.get(obj).get('Active__c')) || Boolean.valueOf(beforeUpdateObjList.get(obj).get('Active__c')))) {
       		   		beforeUpdateObjList.get(obj).addError(PortalPackageConstants.PORTAL_LOGIN_CUSTOM_FIELD_UPSERTION_DELETION_ERROR_MESSAGE);
      			  }
		   }
		   if(beforeUpdateObjList.get(obj).getSObjectType() == Schema.Erx_Page__c.getSObjectType()){
      			 if(liveIds.contains(String.valueOf(beforeUpdateObjList.get(obj).get('Id')))
      			 	 && ( Boolean.valueOf(beforeUpdateObjList.get(obj).get('IsActive__c')!=afterUpdateObjList.get(obj).get('IsActive__c')) || Boolean.valueOf(beforeUpdateObjList.get(obj).get('IsActive__c')))) {
           			 beforeUpdateObjList.get(obj).addError(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE);
        		}
		   } 
		  if(beforeUpdateObjList.get(obj).getSObjectType() == Schema.Homepage_Widget_Layout__c.getSObjectType()){
      			  if(liveIds.contains(String.valueOf(beforeUpdateObjList.get(obj).get('Env__c'))) 
      			  	&& (Boolean.valueOf(beforeUpdateObjList.get(obj).get('Is_Active__c')!=afterUpdateObjList.get(obj).get('Is_Active__c')) || Boolean.valueOf(beforeUpdateObjList.get(obj).get('Is_Active__c')))) {
       		   		afterUpdateObjList.get(obj).addError(PortalPackageConstants.HOMEPAGE_WIDGET_LAYOUT_UPSERTION_DELETION_ERROR_MESSAGE);
      			  }
		   }
		   if(beforeUpdateObjList.get(obj).getSObjectType() == Schema.Homepage_Widget__c.getSObjectType()){
      			  if(liveIds.contains(String.valueOf(beforeUpdateObjList.get(obj).get('Env__c'))) 
      			  		&& (Boolean.valueOf(beforeUpdateObjList.get(obj).get('Is_Active__c')!=afterUpdateObjList.get(obj).get('Is_Active__c')) || Boolean.valueOf(beforeUpdateObjList.get(obj).get('Is_Active__c')))) {
       		   		afterUpdateObjList.get(obj).addError(PortalPackageConstants.HOMEPAGE_WIDGET_UPSERTION_DELETION_ERROR_MESSAGE);
      			  }
		   }
	  }
	}
}