/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved. 
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description The PackageSecurityVerificationController class is used for calling the rest service which links to the LMA Org.
*/
public with sharing class PackageSecurity {
    /****
    * @description contains value whether User has licence
    */ 
    public Boolean isPaid {get;set;}
    //public Boolean payProceed {get;set;}
    /****
    * @description contains value which pages to be allowed for non paid
    */ 
    public set<String> setPageNames;
    /****
    * @description contains value of all Package Security Custom Setting
    */
    public map<String,Package_Security__c> mapPackageCustomSetting {get;set;}
    /****
    * @description contains value for the pageName
    */
    public String pageName{get;set;}
    /****
    * @description contains value when expiry current date not used currently
    */
    public boolean onVerge {get;set;}
    /****
    * @description contains value for SUbscriber Id
    */
    public String subOrgId;
    /****
    * @description contains value for packageName
    */
    public String packageName;
    /****
    * @description contains value for Expiration Date
    */
    public Date Expiration_Date ;
    /****
    * @description contains custom setting obj
    */
    public Package_Security__c psCustomSetting ;
    /****
    * @description contains value for which pages to be allowed for non paid
    */
    public String stringPages = 'Portal_Page_Configure;TM_TemplateContent;';
    /*
    * @description package namespace
    */
    public String nameSpacePrefix {get;set;}
    
    /* To check Bootstrap files */
    public boolean includeBackwardCompatibilityResource {get;set;}
    
    //condition to check if need to redirect to or not
    public Boolean isPageRedirect {get;set;}
    	
    public PackageSecurity(){
    	/*<!-- 09-08-18 Added by Shubham Re PD-3814 -->*/
    	FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
		if(adminSettings != null){ 
			if(adminSettings.Include_backward_compatibility_Resource__c){
				includeBackwardCompatibilityResource = true;
			}
			else{
				includeBackwardCompatibilityResource = false;
			}
		}
    	// Remove declaration of variable because it is class level variable
    	nameSpacePrefix = PortalPackageUtility.getNameSpacePerfix();
        if(!(String.isBlank(nameSpacePrefix))) {
        	nameSpacePrefix = nameSpacePrefix + '__';
        }
        mapPackageCustomSetting = new map<String,Package_Security__c>();
        mapPackageCustomSetting = Package_Security__c.getAll();
        psCustomSetting = new Package_Security__c();
        packageName = 'Enrollment Rx: FormBuilder Rx';
        if(!Test.isRunningTest()){
            subOrgId = UserInfo.getOrganizationId(); 
        }else{
            subOrgId = '00D61000000cuCs';
        }
        
        //Package_Security__c psCustomSetting1 = mapPackageCustomSetting.get(subOrgId);
        //system.assert(false,psCustomSetting1.LastModifiedDate.Date());
        if(mapPackageCustomSetting.containsKey(subOrgId)){
            psCustomSetting = mapPackageCustomSetting.get(subOrgId);
            if(psCustomSetting.isPaid__c){
                Date dateExpiry = psCustomSetting.Expiration_Date__c;
                if(((Date.Today()).daysBetween(dateExpiry)) > 0){
                    isPaid = true;
                    onVerge = false;
                    
                }else if(((Date.Today()).daysBetween(dateExpiry)) < 0){
                    isPaid = false;
                    onVerge = true;
                }else{
                    isPaid = true;
                    onVerge = true;
                }
            }else{
                isPaid = false;
                onVerge = false;
            }
        }else{
            isPaid = false;
            onVerge = false;
        }
        
        checkValidity();
        setPageNames = new set<String> ();
        for(String fileName : stringPages.split(';')){
            setPageNames.add(fileName);
        }
        
        //PD-4060 | Gourav | checking conditons for site and community
        Current_Env__c currentEnvObj = ERx_PortalPackUtil.getCurrentEnvForConfiguration();
        if(currentEnvObj != null){
        	isPageRedirect=false;
        	Map<Id,Env__c> envList = new Map<Id,Env__c>([Select id,TP_Manage_Property__c from Env__c LIMIT :limits.getLimitQueryRows()]);
        	if(envList.containsKey(currentEnvObj.Env__c)){
        		if((currentEnvObj.Type__c == 'Community' && isPaid) || currentEnvObj.Type__c == 'Site'  ){
	        		isPageRedirect = true;
        		}
        	}else{
        		isPageRedirect = false;
        	}
        } else{
        	isPageRedirect = false;
        }
    }
    
    /**
     * @description Method to call API
     */
    public void checkValidity(){ 
        boolean flag;
        if(!Test.isRunningTest()){
            
            if(psCustomSetting != null && psCustomSetting.LastModifiedDate != null){
                  flag = (psCustomSetting.LastModifiedDate.Date() != Date.Today());  
            }else{
                
                flag = true;
            }      
        }else{
            flag = true;
        }
        
        if(flag){
        
            PackageSecurityVerificationController controllerObj = new PackageSecurityVerificationController();
            //system.assert(false,subOrgId);
            String result = controllerObj.getCalloutResponseContents(String.valueOf(subOrgId.substring(0, 15)),packageName);
            if(!String.isBlank(result)){
            	try{
            		Expiration_Date = convertStringToDate(result);
            	}catch(Exception e){
            		Expiration_Date = null;
            	}
            	
            }else{
            	Expiration_Date = null;
            }
            
             
            //jsonParser = jsonParserLicence.parse(result);
            if((Expiration_Date != Date.newInstance(1900, 1, 1)) && (((Date.Today()).daysBetween(convertStringToDate(result))) >= 0)){
                isPaid = true;
            }else{
                isPaid = false;
            }
        }
        
    }
    
    /**
     * @description Method to insert custom setting
     */
    public PageReference insertCustomSetting(){
        boolean flag;
        if(psCustomSetting != null && psCustomSetting.LastModifiedDate != null){
              flag = (psCustomSetting.LastModifiedDate.Date() != Date.Today());  
        }else{
            
            flag = true;
        } 
        
        if(flag){
            if(mapPackageCustomSetting.containsKey(subOrgId)){
                Package_Security__c psCustomSetting = mapPackageCustomSetting.get(subOrgId);
                psCustomSetting.Expiration_Date__c = Expiration_Date;
                psCustomSetting.isPaid__c = isPaid;
                
                //Code updated to enforce the security to update records, check object level update access
                if(SecurityEnforceUtility.checkObjectAccess('Package_Security__c', SecurityTypes.IS_UPDATE, nameSpacePrefix)) {
					// updating the package security record.
					if(psCustomSetting != null) {
						update psCustomSetting;
					}
	        	}
                
            }else if(isPaid){
                /** INSERT VALUES IN CUSTOM SETTING **/
                Package_Security__c cs = new Package_Security__c();
                cs.Name=subOrgId;
                cs.Expiration_Date__c = Expiration_Date;
                cs.isPaid__c = isPaid;
                //Code updated to enforce the security to insert records, check object level insert access
                if(SecurityEnforceUtility.checkObjectAccess('Package_Security__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
					// Inserting the package security record.
					if(cs != null) {
						insert cs;
					}
	        	}
                
            }else{
                /** INSERT  VALUES IN CUSTOM SETTING **/
                Package_Security__c cs = new Package_Security__c();
                cs.Name=subOrgId;
                cs.Expiration_Date__c = null;
                cs.isPaid__c = false;
                //Code updated to enforce the security to insert records, check object level insert access
                if(SecurityEnforceUtility.checkObjectAccess('Package_Security__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
					// Inserting the package security record.
					if(cs != null) {
						insert cs;
					}
	        	}
            } 
        }
        
        //PD-3601 || Gourav | check implemented to see if this is landing page or not.
        FormBuilder_Settings__c fbsetting = new FormBuilder_Settings__c();
		fbsetting = FormBuilder_Settings__c.getValues('Admin Settings');
		PageReference p;
		if(ApexPages.currentPage() != null && ApexPages.currentPage().getUrl() != null){
			if(ApexPages.currentPage().getUrl().contains('Portal_Home')){
				 if(fbsetting != null && !fbsetting.Getting_Started__c){
				 	p= new PageReference('/apex/GettingStarted');	
				 } 
			}
		}
		return p;
        
    }
    
    /**
     * @description Method to convert string to date
     */
    public Date convertStringToDate(String dateStr){
        if(!String.isBlank(dateStr)){
            dateStr = dateStr.replace('\"','');
            if(dateStr.contains('-')){
                list<String> dateStrLst = dateStr.split('-');
                Date dtExp = Date.newInstance(Integer.valueOf(dateStrLst[0]),Integer.valueOf(dateStrLst[1]),Integer.valueOf(dateStrLst[2]));
                return dtExp;
            }else if(dateStr.contains('/')){
                list<String> dateStrLst = dateStr.split('/');
                Date dtExp = Date.newInstance(Integer.valueOf(dateStrLst[0]),Integer.valueOf(dateStrLst[1]),Integer.valueOf(dateStrLst[2]));
                return dtExp;
            }
        }
        return null;
    }
    
    /**
     * @description Method to verify the pages
     */
    public Boolean checkProceed(String pageName){
        if(isPaid){
            //since we dont want to show pay procceed section
            return false;
        }else{
            boolean flag = true;
            if(setPageNames.contains(pageName)){
                flag = false;
            }
            return flag;
        }
    }
    
    /**
     * @description Method to forcefully update custom setting with API Data
     */
    public void forceUpdateCustomSetting(){
    	//API CALL
    	PackageSecurityVerificationController controllerObj = new PackageSecurityVerificationController();
        String result = controllerObj.getCalloutResponseContents(String.valueOf(subOrgId.substring(0, 15)),packageName);
        //if result not blank set expiry date as null
        if(!String.isBlank(result)){
        	try{
        		Expiration_Date = convertStringToDate(result);
        	}catch(Exception e){
        		Expiration_Date = null;
        	}
        }else{
        	Expiration_Date = null;
        }
        
        //if expiry date is null or hard coded or less than today set value as false
        if((Expiration_Date != Date.newInstance(1900, 1, 1)) && (((Date.Today()).daysBetween(convertStringToDate(result))) >= 0)){
            isPaid = true;
        }else{
            isPaid = false;
        }
        // insert/update value in custom setting
        if(mapPackageCustomSetting.containsKey(subOrgId)){
	        Package_Security__c psCustomSetting = mapPackageCustomSetting.get(subOrgId);
	        psCustomSetting.Expiration_Date__c = Expiration_Date;
	        psCustomSetting.isPaid__c = isPaid;
	        
	        //Code updated to enforce the security to update records, check object level update access
	        if(SecurityEnforceUtility.checkObjectAccess('Package_Security__c', SecurityTypes.IS_UPDATE, nameSpacePrefix)) {
				// Updating the package security record.
				if(psCustomSetting != null) {
					update psCustomSetting;
				}
        	}
	    }else if(isPaid){
	        /** INSERT VALUES IN CUSTOM SETTING **/
	        Package_Security__c cs = new Package_Security__c();
	        cs.Name=subOrgId;
	        cs.Expiration_Date__c = Expiration_Date;
	        cs.isPaid__c = isPaid;
	        
	        //Code updated to enforce the security to insert records, check object level insert access
         	if(SecurityEnforceUtility.checkObjectAccess('Package_Security__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
				// Inserting the package security record.
				if(cs != null) {
					insert cs;
				}
        	}
	    }else{ 
	        /** INSERT VALUES IN CUSTOM SETTING **/
	        Package_Security__c cs = new Package_Security__c();
	        cs.Name=subOrgId;
	        cs.Expiration_Date__c = null;
	        cs.isPaid__c = false;
	        
	        //Code updated to enforce the security to insert records, check object level insert access
	        if(SecurityEnforceUtility.checkObjectAccess('Package_Security__c', SecurityTypes.IS_INSERT, nameSpacePrefix)) {
				// Inserting the package security record.
				if(cs != null) {
					insert cs;
				}
        	}
	    } 
    }
       
}