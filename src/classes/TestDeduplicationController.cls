/*FormBuilderRx
    @company : Copyright © 2016, Enrollment Rx, LLC
    All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    @website : http://www.enrollmentrx.com/
    @author EnrollmentRx
    @date 2018-06-14 
    @description Test Class for DeduplicationController
*/
@isTest
public class TestDeduplicationController {
    
    public static void createDataForDeduplication(){
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        Env__c env = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        env.deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and (2 or 3) or 4"},{"message":"Yes Contact Match","filterOption":"Message","filter":"1 and 2 "},{"message":"Another Match","filterOption":"Message","filter":"3 or 4"}]';
        env.deduplication_condition__c = '[{"targetValue":"AccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"MyAccount","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"FirstName","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"STRING","compareWith":"Field"},{"targetValue":"EnrollmentrxRx__Gender__c","sourceField":"EnrollmentrxRx__Gender__c","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"PICKLIST","compareWith":"Field"},{"targetValue":"Birthdate","sourceField":"birthdate","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"DATE","compareWith":"Field"},{"targetValue":"Id","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"123456789123456","filterFieldName":null,"fieldType":"ID","compareWith":"Field"}]';
        env.lead_deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and 2"},{"message":"Yes Lead Match","filterOption":"Message","filter":"1 or 2"}]';
        env.lead_deduplication_condition__c = '[{"targetValue":"PartnerAccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"true","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"Email","sourceField":"email","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"EMAIL","compareWith":"Field"}]';
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert env;
        Current_Env__c current_Env = new Current_Env__c();
		current_Env.Name = UserInfo.getUserId();
		current_Env.Env__c = env.Id;
		current_Env.Type__c = 'Community';
		insert current_Env;
        Portal_Login_Custom_Field__c plcf1 =  new Portal_Login_Custom_Field__c();
        plcf1.Env__c = env.id;
        plcf1.Name = 'Test Portal Login Custom Field1';
        plcf1.Field_API_Name__c = 'Test Portal Login Custom Field1';
        Portal_Login_Custom_Field__c plcf2 =  new Portal_Login_Custom_Field__c();
        plcf2.Env__c = env.id;
        plcf2.Name = 'Test Portal Login Custom Field2';
        plcf2.Field_API_Name__c = 'Test Portal Login Custom Field2';
        List<Portal_Login_Custom_Field__c> portalFields = new List<Portal_Login_Custom_Field__c>();
        portalFields.add(plcf1);
        portalFields.add(plcf2);
        insert portalFields;
    }
    
    public static void createDataForDeduplication1(){
        System.debug('1');
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        Env__c env = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        env.deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"(1 and ((12 or 3) or 4))"},{"message":"Yes Contact Match","filterOption":"Message","filter":"1 and 2 "},{"message":"Another Match","filterOption":"Message","filter":"3 or 4"}]';
        env.deduplication_condition__c = '[{"targetValue":"AccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"MyAccount","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"FirstName","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"07/02/1994","filterFieldName":null,"fieldType":"DATE","compareWith":"Value"},{"targetValue":"EnrollmentrxRx__Gender__c","sourceField":"EnrollmentrxRx__Gender__c","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"PICKLIST","compareWith":"Field"},{"targetValue":"Birthdate","sourceField":"birthdate","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"DATE","compareWith":"Field"},{"targetValue":"Id","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"123456789123456","filterFieldName":null,"fieldType":"ID","compareWith":"Field"}]';
        env.lead_deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":""},{"message":"Yes Lead Match","filterOption":"Message","filter":"1 or 2"}]';
        env.lead_deduplication_condition__c = '[{"targetValue":"PartnerAccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"true","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"Email","sourceField":"email","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"EMAIL","compareWith":"Field"}]';
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert env;
        Current_Env__c current_Env = new Current_Env__c();
		current_Env.Name = UserInfo.getUserId();
		current_Env.Env__c = env.Id;
		current_Env.Type__c = 'Community';
		insert current_Env;
        Portal_Login_Custom_Field__c plcf1 =  new Portal_Login_Custom_Field__c();
        plcf1.Env__c = env.id;
        plcf1.Name = 'Test Portal Login Custom Field1';
        plcf1.Field_API_Name__c = 'Test Portal Login Custom Field1';
        Portal_Login_Custom_Field__c plcf2 =  new Portal_Login_Custom_Field__c();
        plcf2.Env__c = env.id;
        plcf2.Name = 'Test Portal Login Custom Field2';
        plcf2.Field_API_Name__c = 'Test Portal Login Custom Field2';
        List<Portal_Login_Custom_Field__c> portalFields = new List<Portal_Login_Custom_Field__c>();
        portalFields.add(plcf1);
        portalFields.add(plcf2);
        insert portalFields;
    }
    
    public static void createDataForDeduplication2(){
        System.debug('2');
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        Env__c env = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        env.deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1"},{"message":"Yes Contact Match","filterOption":"Message","filter":"2"},{"message":"Another Match","filterOption":"Message","filter":"3"}]';
        env.deduplication_condition__c = '[{"targetValue":"AccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"MyAccount","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"FirstName","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"03/04/199012","filterFieldName":null,"fieldType":"DATE","compareWith":"Value"},{"targetValue":"EnrollmentrxRx__Gender__c","sourceField":"EnrollmentrxRx__Gender__c","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"BOOLEAN","compareWith":"Value"},{"targetValue":"Birthdate","sourceField":"birthdate","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"02/08/19901","filterFieldName":null,"fieldType":"DATE","compareWith":"Value"},{"targetValue":"Id","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"123456789123456","filterFieldName":null,"fieldType":"ID","compareWith":"Field"}]';
        env.lead_deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and 2"},{"message":"Yes Lead Match","filterOption":"Message","filter":"1 or 2"}]';
        env.lead_deduplication_condition__c = '[{"targetValue":"PartnerAccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"true","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"Email","sourceField":"email","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"EMAIL","compareWith":"Field"}]';
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert env;
        Current_Env__c current_Env = new Current_Env__c();
		current_Env.Name = UserInfo.getUserId();
		current_Env.Env__c = env.Id;
		current_Env.Type__c = 'Community';
		insert current_Env;
        Portal_Login_Custom_Field__c plcf1 =  new Portal_Login_Custom_Field__c();
        plcf1.Env__c = env.id;
        plcf1.Name = 'Test Portal Login Custom Field1';
        plcf1.Field_API_Name__c = 'Test Portal Login Custom Field1';
        Portal_Login_Custom_Field__c plcf2 =  new Portal_Login_Custom_Field__c();
        plcf2.Env__c = env.id;
        plcf2.Name = 'Test Portal Login Custom Field2';
        plcf2.Field_API_Name__c = 'Test Portal Login Custom Field2';
        List<Portal_Login_Custom_Field__c> portalFields = new List<Portal_Login_Custom_Field__c>();
        portalFields.add(plcf1);
        portalFields.add(plcf2);
        insert portalFields;
    }
    
    public static void createDataForDeduplication3(){
        System.debug('3');
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        Env__c env = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        env.deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and (2 or 3) or 4"},{"message":"Yes Contact Match","filterOption":"Message","filter":"1 and 2 "},{"message":"Another Match","filterOption":"Message","filter":"3 or 4"}]';
        env.deduplication_condition__c = '[{"targetValue":"AccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"MyAccount","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"FirstName","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"STRING","compareWith":"Field"},{"targetValue":"EnrollmentrxRx__Gender__c","sourceField":"EnrollmentrxRx__Gender__c","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"PICKLIST","compareWith":"Field"},{"targetValue":"DoNotCall","sourceField":"","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"BOOLEAN","compareWith":"VALUE"},{"targetValue":"Id","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"123456789123456","filterFieldName":null,"fieldType":"ID","compareWith":"Field"}]';
        env.lead_deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and 2"},{"message":"Yes Lead Match","filterOption":"Message","filter":"1 or 2"}]';
        env.lead_deduplication_condition__c = '[{"targetValue":"PartnerAccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"true","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"Email","sourceField":"email","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"EMAIL","compareWith":"Field"}]';
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert env;
        System.debug('3 '+env);
        Current_Env__c current_Env = new Current_Env__c();
		current_Env.Name = UserInfo.getUserId();
		current_Env.Env__c = env.Id;
		current_Env.Type__c = 'Community';
		insert current_Env;
        Portal_Login_Custom_Field__c plcf1 =  new Portal_Login_Custom_Field__c();
        plcf1.Env__c = env.id;
        plcf1.Name = 'Test Portal Login Custom Field1';
        plcf1.Field_API_Name__c = 'Test Portal Login Custom Field1';
        Portal_Login_Custom_Field__c plcf2 =  new Portal_Login_Custom_Field__c();
        plcf2.Env__c = env.id;
        plcf2.Name = 'Test Portal Login Custom Field2';
        plcf2.Field_API_Name__c = 'Test Portal Login Custom Field2';
        List<Portal_Login_Custom_Field__c> portalFields = new List<Portal_Login_Custom_Field__c>();
        portalFields.add(plcf1);
        portalFields.add(plcf2);
        insert portalFields;
    }
    
    public static void createInvalidDataForDeduplication(){
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        Env__c env = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        env.deduplication_criteria__c = '';
        env.deduplication_condition__c = '';
        env.lead_deduplication_criteria__c = '';
        env.lead_deduplication_condition__c = '';
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert env;
        Current_Env__c current_Env = new Current_Env__c();
		current_Env.Name = UserInfo.getUserId();
		current_Env.Env__c = env.Id;
		current_Env.Type__c = 'Community';
		insert current_Env;
        Portal_Login_Custom_Field__c plcf1 =  new Portal_Login_Custom_Field__c();
        plcf1.Env__c = env.id;
        plcf1.Name = 'Test Portal Login Custom Field1';
        plcf1.Field_API_Name__c = 'Test Portal Login Custom Field1';
        Portal_Login_Custom_Field__c plcf2 =  new Portal_Login_Custom_Field__c();
        plcf2.Env__c = env.id;
        plcf2.Name = 'Test Portal Login Custom Field2';
        plcf2.Field_API_Name__c = 'Test Portal Login Custom Field2';
        List<Portal_Login_Custom_Field__c> portalFields = new List<Portal_Login_Custom_Field__c>();
        portalFields.add(plcf1);
        portalFields.add(plcf2);
        insert portalFields;
    }
    
    public static testmethod void testInvalidList(){
        createInvalidDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        List<SelectOption> actualList = dc.getSourceList();
        System.assertEquals(5,actualList.size());
        Test.stopTest();
    }
    
    public static testmethod void testGetSourceList(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        List<SelectOption> actualList = dc.getSourceList();
        System.assertEquals(5,actualList.size());
        Test.stopTest();
    }
    
    public static testmethod void testAddLogic(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.addLogic();
        System.assertEquals(4,dc.filterLogicList.size());
        Test.stopTest();
    }
    
    public static testmethod void testDeleteLogic(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.selectedIndex = 2;
        dc.deleteLogic();
        dc.messageBlank();
        System.assertEquals(2,dc.filterLogicList.size());
        Test.stopTest();
    }
    
    public static testmethod void testAddFields(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.addFields();
        System.assertEquals(6,dc.contactFieldList.size());
        Test.stopTest();
    }
    
    public static testmethod void testDeleteRow(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.selectedIndex = 4;
        dc.deleteRow();
        System.assertEquals(4,dc.contactFieldList.size());
        Test.stopTest();
    }
    
    public static testmethod void testAddLogicLead(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.addLogicLead();
        System.assertEquals(3,dc.leadFilterLogicList.size());
        Test.stopTest();
    }
    
    public static testmethod void testDeleteLogicLead(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.selectedIndex = 1;
        dc.deleteLogicLead();
        dc.messageBlankLead();
        System.assertEquals(1,dc.leadFilterLogicList.size());
        Test.stopTest();
    }
    
    public static testmethod void testAddFieldsLead(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.addFieldsLead();
        System.assertEquals(3,dc.leadFieldList.size());
        Test.stopTest();
    }
    
    public static testmethod void testDeleteRowLead(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.selectedIndex = 1;
        dc.deleteRowLead();
        System.assertEquals(1,dc.leadFieldList.size());
        Test.stopTest();
    }
    
    public static testmethod void testCheckFieldType(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.selectedIndex = 1;
        dc.checkFieldType();
        dc.selectedIndex = 2;
        dc.checkFieldType();
        System.assertEquals(2,dc.leadFieldList.size());
        Test.stopTest();
    }
    
    public static testmethod void testCheckFieldTypeLead(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        dc.selectedIndex = 1;
        dc.checkFieldTypeLead();
        dc.selectedIndex = 2;
        dc.checkFieldTypeLead();
        System.assertEquals(2,dc.leadFieldList.size());
        Test.stopTest();
    }
    
    public static testmethod void testValidateCriteria(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        String actual = dc.validateCriteria(dc.filterLogicList,'Contact',dc.contactFieldList);
        String expected = '';
        System.assertEquals(expected,actual);
        Test.stopTest();
    }
    
    public static testmethod void testValidateCriteria1(){
    	createDataForDeduplication1();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        String actual = dc.validateCriteria(dc.filterLogicList,'Contact',dc.contactFieldList);
        String expected = 'Contact Duplication Criteria 1 : Invalid syntax: Number immediately followed by opening number';
        System.assertEquals(expected,actual);
        Test.stopTest();
    }
    
    public static testmethod void testValidateCriteria2(){
    	createDataForDeduplication2();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        String actual = dc.validateCriteria(dc.filterLogicList,'Contact',dc.contactFieldList);
        String expected = 'Contact Duplication Condition 4 : Date should be in format MM/DD/YYYY';
        System.assertEquals(expected,actual);
        Test.stopTest();
    }
    
    public static testmethod void testValidateCriteria3(){
    	createDataForDeduplication3();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        System.debug(dc.contactFieldList);
        String actual = dc.validateCriteria(dc.filterLogicList,'Contact',dc.contactFieldList);
        String expected = 'Contact Duplication Condition 4 : Invalid Boolean value';
        System.assertEquals(expected,actual);
        Test.stopTest();
    }
    
    public static testmethod void testSaveFilterLogic(){
        createDataForDeduplication();
        Test.startTest();
        DeduplicationController dc = new DeduplicationController();
        PageReference actual = dc.saveFilterLogic();
        System.assertEquals(null,actual);
        Test.stopTest();
    }
}