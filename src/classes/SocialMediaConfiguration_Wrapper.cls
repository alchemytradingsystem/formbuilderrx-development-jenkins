/********
FormBuilderRx
@company : Copyright � 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
PD-2689 | Gourav | Social media wrapper buttons
********/
public class SocialMediaConfiguration_Wrapper {
    
    public string redirectLink{get;set;}
    public string buttonStyle{get;set;}
    public string buttonLabel{get;set;}
    public string provider{get;set;}
    private string fixedUrl = '/services/auth/sso/';
    
    
    public SocialMediaConfiguration_Wrapper(string label){
    	
    	if(label == 'Facebook'){
 			this.buttonStyle = 'fab fa-facebook-f';   		
    	}else if(label == 'Google'){
    		this.buttonStyle = 'fab fa-google';
    	}else if(label == 'LinkedIn'){
    		this.buttonStyle = 'fab fa-linkedin-in';
    	}
   	
     	this.buttonLabel = label;
     	this.provider = label;
     	this.redirectLink = fixedUrl + label;
    }
    
    
    
}