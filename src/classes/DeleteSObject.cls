/**
    FormBuilderRx
	@company : Copyright © 2018, Enrollment Rx, LLC
	All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    @website : http://www.enrollmentrx.com/
    @author EnrollmentRx
    @version 1.0
    @date 7-9-2018
    @description Class for Delete SObject when we need to bypass security check.
*/
public without sharing class DeleteSObject {
	
	//PD-3555 || Without sharing class will allow to delete the Object record without any permission issue
    public void deleteObjectRecord( List<sObject> sObjectList ){
    	try{
			if(sObjectList !=null && sObjectList.size()>0){
				delete sObjectList;
			}
    	}catch(Exception e){
    		throw new PortalPackageException(e.getMessage());
    	}
    }
}