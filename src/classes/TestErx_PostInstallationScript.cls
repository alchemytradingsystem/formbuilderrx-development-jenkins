/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.Post
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-01-31
	@description TestErx_PostInstallationScript to cover test coverage for Erx_PostInstallationScript
*/
@isTest
private class TestErx_PostInstallationScript {

	//Test Case 1: No previous version is defined
    static testMethod void testPostInstallScript() {
    	Test.startTest();
        	List<Erx_Page__c> pageList = new List<Erx_Page__c>();    
        	FormBuilder_Settings__c adminSettings = new FormBuilder_Settings__c();
			adminSettings.Name = 'Admin Settings';
        	insert adminSettings;
        	Env__c env = new Env__c(Version__c=1);
        	insert env;
        	for(integer i = 0; i < 2;i++){
                Erx_Page__c page = new Erx_Page__c();
                page.Page_Name__c = 'Page'+i;
                page.Page_Name_Displayed__c = 'Page'+i;
                page.Env__c = env.Id;
            	pageList.add(page);
            }
        	insert pageList;
	        Erx_PostInstallationScript postInstallScriptTest = new Erx_PostInstallationScript();   
	        List<CronJobDetail> beforeInstallJobLst = [SELECT Id, Name FROM CronJobDetail WHERE Name = 'Formbuilder ERx_LoggerCleanUpScheduler' LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(0, beforeInstallJobLst.size());   
	      	Test.testInstall(postInstallScriptTest, null);
	      	List<CronJobDetail> afterInstallJobLst = [SELECT Id, Name FROM CronJobDetail WHERE Name = 'Formbuilder ERx_LoggerCleanUpScheduler' LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(1, afterInstallJobLst.size());
	    Test.stopTest();
    }
    
    //Test Case 2 : A previous version exists
    static testMethod void testPostInstallScriptPushUpgrade() {
    	Test.startTest();
	        Erx_PostInstallationScript postInstallScriptTest = new Erx_PostInstallationScript();     
	        List<CronJobDetail> beforeInstallJobLst = [SELECT Id, Name FROM CronJobDetail WHERE Name = 'Formbuilder ERx_LoggerCleanUpScheduler' LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(0, beforeInstallJobLst.size());  
	      	Test.testInstall(postInstallScriptTest, new Version(1,0), true);
	      	List<CronJobDetail> afterInstallJobLst = [SELECT Id, Name FROM CronJobDetail WHERE Name = 'Formbuilder ERx_LoggerCleanUpScheduler' LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(1, afterInstallJobLst.size());
	    Test.stopTest();
    }
}