/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-12-29 
	@description Scheduler for ERx_CleanUpPortalPackageLogger batch class to delete PortalPackageLogger records
*/
global with sharing class ERx_LoggerCleanUpScheduler implements Schedulable {
	
	/****
    * @description:  Store count for running Job to check if there are 5 active batch jobs
    */
    @TestVisible private static Integer runningJobCount{
	    get{
	        if(runningJobCount == null)
	            return [SELECT COUNT() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Processing', 'Preparing', 'Queued') LIMIT :limits.getLimitQueryRows()];
	        else
	            return runningJobCount;
	    }
	    set{
	        runningJobCount = value;
	    }
	}
	
	/*
    	@description:  Method to execute the scheduler
    */
	public static String scheduleIt(String schedulerName, String CRON_EXP) {
        ERx_LoggerCleanUpScheduler sd = new ERx_LoggerCleanUpScheduler();
        return System.schedule(PortalPackageUtility.escapeQuotes(schedulerName) , PortalPackageUtility.escapeQuotes(CRON_EXP), sd);
    }
    
     /*
    	@description: Scheduler Method to schedule the class
    */
     global void execute(SchedulableContext sc) {
     	//Getting  segregate days of logger record to be deleted
        FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
        if(formBuilderSetting != null && formBuilderSetting.DeleteLoggerBeforeDays__c != null) {
			
			//Stores Permission error message
		 	String permissionErrorMessage;
	    
        	//Getting Permission error message
            if(formBuilderSetting.Permission_Error_Message__c != null) {
            	permissionErrorMessage = PortalPackageUtility.escapeQuotes(formBuilderSetting.Permission_Error_Message__c);
            } else {
            	permissionErrorMessage = PortalPackageConstants.PERMISSION_ERROR_MESSAGE;
            }
            
			//Stores package prefix i.e. ERx_Forms
			String nameSpacePrefix = PortalPackageUtility.getNameSpacePerfix();
            if(!(String.isBlank(nameSpacePrefix))) {
            	nameSpacePrefix = nameSpacePrefix + '__';
            }
        	
        	//Enforce the security to delete records, check object level delete access
			//security review changes
			if(SecurityEnforceUtility.checkObjectAccess('Portal_Package_Logger__c', SecurityTypes.IS_DELETE, nameSpacePrefix) 
				&& SecurityEnforceUtility.checkObjectAccess('Portal_Package_Logger__c', SecurityTypes.IS_ACCESS, nameSpacePrefix)) {
        		if (runningJobCount < 5) {
        			Integer segregateDays = 0;
        			segregateDays = Integer.valueOf(formBuilderSetting.DeleteLoggerBeforeDays__c);
		        	DateTime segregateDate = System.Today().addDays(-segregateDays);
			        String query = 'select id,CreatedDate from Portal_Package_Logger__c where CreatedDate < ' + segregateDate.format('yyyy-MM-dd\'T\'00:00:00\'Z\'') + ' order by CreatedDate desc';
					ERx_CleanUpPortalPackageLogger delBatch = new ERx_CleanUpPortalPackageLogger(query);
		    		Id BatchProcessId = Database.ExecuteBatch(delBatch, 2000);
        		} else {
        			Datetime dt = Datetime.now().addMinutes(15);
            		String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
            		ERx_LoggerCleanUpScheduler.scheduleIt('Retry : Formbuilder ERx_LoggerCleanUpScheduler'+timeForScheduler, timeForScheduler);	
        		}
        	} else {
				ERx_PortalPackageLoggerHandler.addException(new PortalPackageException(permissionErrorMessage), 'Daily CleanUp PortalPackageLogger','ERx_LoggerCleanUpScheduler execute',null);  
				ERx_PortalPackageLoggerHandler.saveExceptionLog();
			}
        }
    }
}