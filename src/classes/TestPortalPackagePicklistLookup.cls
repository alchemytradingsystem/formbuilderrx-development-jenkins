/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Test Class for PortalPackagePicklistLookup
*/
@IsTest
public class TestPortalPackagePicklistLookup {
    public static Env__c env;
    public static List<Erx_Page__c> erxPagesList;
    public static List<Erx_Section__c> erxSectionList;
    public static Id pageId;
    public static Erx_Page__c pageSave;
    public static Erx_Section__c pageSection;
    
    private static testMethod void testPortalPackagePicklistLookupController () {
        //prepareData();
        test.startTest();
        ApexPages.currentPage().getParameters().put('index','1');
        ApexPages.currentPage().getParameters().put('isMultiSelect','false');
        ApexPages.currentPage().getParameters().put('isFirstBlank','false');
        ApexPages.currentPage().getParameters().put('objectName','Contact');
        
        ApexPages.currentPage().getParameters().put('fieldValue','abc,bcd');
        ApexPages.currentPage().getParameters().put('objectName','Contact');
        ApexPages.currentPage().getParameters().put('fieldName','LeadSource');
        ApexPages.currentPage().getParameters().put('fieldValue','abc,bcd');
        
        ApexPages.currentPage().getParameters().put('type','contact');
        ApexPages.currentPage().getParameters().put('isMultiSelect','false');
        ApexPages.currentPage().getParameters().put('isFirstBlank','false');
        PortalPackagePicklistLookupController portalPackagePicklistObj = new PortalPackagePicklistLookupController();
        System.assertEquals(false,portalPackagePicklistObj.isMultiSelect );
        System.assertEquals(true,portalPackagePicklistObj.type != null);
        portalPackagePicklistObj.getDescribeSObjectResultList('Contact');
        portalPackagePicklistObj.getDescribeSObjectResultList('exception');
        ApexPages.currentPage().getParameters().put('isFirstBlank','true');
        portalPackagePicklistObj = new PortalPackagePicklistLookupController();
        ApexPages.currentPage().getParameters().put('objectName','RecordType');
        ApexPages.currentPage().getParameters().put('fieldName','SobjectType');
        portalPackagePicklistObj = new PortalPackagePicklistLookupController();
        HtmlPage htmlPageObj = new HtmlPage();
        htmlPageObj.environmentErrorMessage = '<h1>Test</h1>';
        htmlPageObj.isPageCompletion = true;
        htmlPageObj.redirectURL = '<h1>Test</h1>';
        htmlPageObj.isSubmitShow = true;
        htmlPageObj.modelNameFieldList = '<h1>Test</h1>';
        htmlPageObj.nextPageName = '<h1>Test</h1>';
        htmlPageObj.previousPageName = '<h1>Test</h1>';
        htmlPageObj.pageTabIndexing = '"Across"';
        htmlPageObj.errorMessageLocation = '"ScrollToTop"';
        
        Boolean val1 = htmlPageObj.isAuthorized;
        String val2 = htmlPageObj.errorMessage;
		String val3 = htmlPageObj.currentPageId;
        String val4 = htmlPageObj.nextPageId;
        String val5 = htmlPageObj.prevPageId;
        String val6 = htmlPageObj.nextSectionId;
        String val7 = htmlPageObj.prevSectionId;
        Boolean val8 = htmlPageObj.isNextSection;
        Boolean val9 = htmlPageObj.isPrevSection;
        Boolean val10 = htmlPageObj.isNextPage;
        Boolean val11 = htmlPageObj.isPrevPage;
        String val12 = htmlPageObj.pageListJSON;
        String val13 = htmlPageObj.helperIcon;
        String val14 = htmlPageObj.errorMessageFields;
        String val15 = htmlPageObj.browserTabTitleEnv;
        Boolean val16 = htmlPageObj.allowForceUpdate;
        String val17 = htmlPageObj.pageHeaderAdminPanel;
        String val18 = htmlPageObj.sectionErrorMessage;
        String val19 = htmlPageObj.pageName;
        Boolean val20 = htmlPageObj.isCheckListShow;
        String val21 = htmlPageObj.pageId;
        String val22 = htmlPageObj.pageDependentMessage;
        boolean val23 = htmlPageObj.isPageRedirect;
        boolean val24 = htmlPageObj.isPageDependent;
        String val25 = htmlPageObj.currentEnvStatus;
        String val26 = htmlPageObj.currentEnvName;
        String val27 = htmlPageObj.currentEnvId;
        String val28 = htmlPageObj.envType;
        boolean val29 = htmlPageObj.isHideNavigation;
        boolean val30 = htmlPageObj.isPageDisable;
        boolean val31 = htmlPageObj.isCustomHeaderFooter;
        String val32 = htmlPageObj.customPageName;
        boolean val33 = htmlPageObj.isCustomPage;
        String val34 = htmlPageObj.modelFieldsMap;
        String val35 = htmlPageObj.modelConditionMap;
        List<PageWrapperModel> val36 = htmlPageObj.pageList;
        Map<String, String> val37 = htmlPageObj.modelObjectMap;
        
        ApexPages.currentPage().getParameters().put('objectName','Test_Object');
        ApexPages.currentPage().getParameters().put('fieldName','Field_Name');
        ApexPages.currentPage().getParameters().put('index','a');
        portalPackagePicklistObj = new PortalPackagePicklistLookupController();
		//Portal Logger COverage
		ERx_PortalPackageLoggerHandler.addException(new PortalPackageException('Test Exception'),'','','');
		ERx_PortalPackageLoggerHandler.addWarning('','','','');
		ERx_PortalPackageLoggerHandler.addInformation('','','','');
		ERx_PortalPackageLoggerHandler.saveExceptionLog();
        FormBuilder_Settings__c fs = new FormBuilder_Settings__c();
        fs.Name='Admin Settings';
        insert fs;
        //SideBarComponent Coverage
        SideBarComponent sideBarObj = new SideBarComponent();
        sideBarObj.pages= new list<PageWrapperModel>();
        sideBarObj.currentEnvId='';
        sideBarObj.currentEnyType='';
        sideBarObj.prefix='';
        sideBarObj.currentPageId ='';
        Boolean values = sideBarObj.debugging;
        //Security enforce Coverage
        Account a;
        Contact student;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
        system.runAs(testUser){
        	try{
        		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Test',SecurityTypes.IS_UPDATE,''));	
        	}Catch(Exception e){
        		system.assertEquals('Object not found :: test', e.getMessage());
        	}
        	try{
        		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Test',SecurityTypes.IS_UPDATE,''));	
        	}Catch(Exception e){
        		system.assertEquals('Object not found :: test', e.getMessage());
        	}
    		
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c',SecurityTypes.IS_UPDATE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c',SecurityTypes.IS_UPDATE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c',SecurityTypes.IS_DELETE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c',SecurityTypes.IS_INSERT,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c',SecurityTypes.IS_ACCESS,PortalPackageUtility.getFinalNameSpacePerfix()));
        	list<string> objNames = new list<string>();
        	objNames.add('Portal_Registration_Message__c');
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess(objNames, SecurityTypes.IS_UPDATE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess(objNames,SecurityTypes.IS_UPDATE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess(objNames,SecurityTypes.IS_DELETE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess(objNames,SecurityTypes.IS_INSERT,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkObjectAccess(objNames,SecurityTypes.IS_ACCESS,PortalPackageUtility.getFinalNameSpacePerfix()));
    		try{
        		system.assertEquals(false,SecurityEnforceUtility.checkFieldAccess('Test','',SecurityTypes.IS_ACCESS,PortalPackageUtility.getFinalNameSpacePerfix()));
        	}Catch(Exception e){
        	}   		
    		system.assertEquals(false,SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c','Student_Portal_User_Profile__c',SecurityTypes.IS_ACCESS,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c','Student_Portal_User_Profile__c',SecurityTypes.IS_UPDATE,PortalPackageUtility.getFinalNameSpacePerfix()));
    		system.assertEquals(false,SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c','Student_Portal_User_Profile__c',SecurityTypes.IS_INSERT,PortalPackageUtility.getFinalNameSpacePerfix()));
        	FieldLayoutController fieldobj = new FieldLayoutController();
        	fieldobj.componentList = '';
            ERxPageBodyController bodyObj = new ERxPageBodyController();
        	bodyObj.components = '';
            Boolean val = bodyObj.debugging;
            String url = bodyObj.baseURl;
        	FormBulderAdminPanelBodyController adminBodyObj = new FormBulderAdminPanelBodyController();
        	adminBodyObj.errorMessage = '';
            String vall1 = adminBodyObj.getCurrencyCode();
            String vall2 = adminBodyObj.getPackageNamespaceName();
        }
        
        test.stopTest();
    } 
    
}