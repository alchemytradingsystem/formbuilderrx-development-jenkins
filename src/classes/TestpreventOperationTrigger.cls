/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Test Class for preventOperationTrigger
*/
@IsTest
public class TestpreventOperationTrigger {
    public static testMethod void testpreventOperationInsertTriggerOperation(){
        Env__c env = new Env__c(Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Test', Name='Applicant Portal' );
        Test.startTest();
        Database.SaveResult insertEnv = database.insert(env, False);
       	System.assertEquals(true, env != null);
        Test.stopTest();
        //System.assertEquals(1, insertEnv.getErrors().size());
    }
    public static testMethod void testpreventOperationDeleteTriggerOperation(){
        Env__c env = new Env__c(Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Live', Name='Applicant Portal' );
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert env;
        System.assertEquals(true, env.Id != null);
        ERx_Portal_EnvPanelController.isFromAdminPanel = false;
        Test.startTest();
         Erx_Page__c page1 = new Erx_Page__c ( Env__c = env.Id, 
            IsActive__c = true,
            Page_Header__c = '{"title":"Test"}', 
            Page_Name__c = ('Test Page ' + (1)), 
            Order__c = 1,
            Page_Buttons__c = '{"Buttons":[]}',
            Disable_Criteria__c = '[]',
            ModelList__c = '[]',
            Rendered_Criteria__c = '[]'
            );
        database.insert(page1, False);
//        delete  page1;
        
        Database.DeleteResult  deleteEnv = database.delete(env, False);
        Test.stopTest();
        //System.assertEquals(1, deleteEnv.getErrors().size());
    }
    
    // Unused Code commented
    /*public static testMethod void testTestPageTrigger(){
         Test.startTest();
         
         Test.stopTest();
    }*/
    
    public static testMethod void testPageEntities() {
        Contact con = new Contact();
        Test.startTest();
        ERx_PageEntities.SelectOpWrap selectOp = new ERx_PageEntities.SelectOpWrap(con.id, 'test');
        ERx_PageEntities.AttachmentWrapper attach = new ERx_PageEntities.AttachmentWrapper();
        ERx_PageEntities.AttachmentWrapper attachOb = new ERx_PageEntities.AttachmentWrapper(null,null,null,null,null);
        ERx_PageEntities.SectionModelWrapper section = new ERx_PageEntities.SectionModelWrapper();
        section.title ='';
        section.modelName ='';
        section.modelType ='';  
        section.field = null;
        section.sectionRenderCriteriaList = null;
        section.sectionLayout ='';
        section.isAddAnother = false;
        section.addAnotherButtonLabel ='';
        section.removeButtonLabel ='';
        section.addAnotherLimit = 0;
        section.sectionOrder = 0;
        section.sectionCollapsible ='';
        section.rendered = false;
        section.renderedExpression ='';
        section.renderedExpressionFinal ='';
        ERx_PageEntities.FieldModelWrapper fieldObj = new ERx_PageEntities.FieldModelWrapper();
        fieldObj.required = 'Test';
        fieldObj.disable = 'Test';
        fieldObj.isBlank = false;
        fieldObj.fieldDisplayType = 'Test';
        fieldObj.type = 'Test';
        fieldObj.label = 'Test';
        fieldObj.rendered = false;
        fieldObj.rendredExpression = 'Test';
        fieldObj.requiredExpressionFinal = 'Test';
        fieldObj.disableExpressionFinal = 'Test';
        fieldObj.renderCriteriaList = null;
        fieldObj.requiredCriteriaList = null;
        fieldObj.disableCriteriaList = null;
        fieldObj.renderedExpression = 'Test';
        fieldObj.requiredExpression = 'Test';
        fieldObj.disableExpression = 'Test';
        fieldObj.requiredSymbol = 'Test';
        fieldObj.requiredErrorMessage = 'Test';
        fieldObj.fieldName = 'Test';
        fieldObj.fieldAPIName = 'Test';
        fieldObj.instructionText = 'Test';
        fieldObj.instructionTextPosition = 'Test';
        fieldObj.placeholderText = 'Test';
        fieldObj.modelName = 'Test';
        fieldObj.hoverText = 'Test';
        fieldObj.isBackendDependentField = false;
        fieldObj.backendControllingFieldName = 'Test';
        fieldObj.checkBoxFieldConfigurationString = 'Test';
        fieldObj.checkBoxFieldConfigurationMap = null;
        fieldObj.selectOptionList = null;
        fieldObj.displayValueAPIName = null;
        fieldObj.lookupValueList = null;
        fieldObj.availableValues = null;
        fieldObj.selectedValues = null;
        fieldObj.parentIdSource = 'Test';
        fieldObj.parentId = 'Test';
        fieldObj.multiple = false;
        fieldObj.allowedContentType = 'Test';
        fieldObj.maximumsize = 0;
        fieldObj.isControllingField = false;
        fieldObj.dependentCriteriaMap = null;
        fieldObj.isdependentField = false;
         fieldObj.lookupRecordCriteria = null;
         fieldObj.isFirstValueBlank = false;
         fieldObj.picklistDependencyMap = null;
         fieldObj.paragraphText = 'Test';
         fieldObj.multiApplicationDescriptiveText = 'Test';
         fieldObj.targetObject = 'Test';
        fieldObj.sourceObject = 'Test';
        fieldObj.sourceObjectFieldsAPIName= null;
        fieldObj.sourceObjectFieldLabels=  null;
        fieldObj.targetObjectActiveFieldAPIName = 'Test';
        fieldObj.sourceObjectClickableField = 'Test';
        fieldObj.sourceObjectCriteriaField  = 'Test';
        fieldObj.targetObjectRecord = null;
        fieldObj.multipAppButtonLabel = 'Test';
        fieldObj.sourceObjectAPIName = 'Test';
        fieldObj.targetObjectAPIName = 'Test';
        fieldObj.createNewAppButtonURL = 'Test';
        fieldObj.buttonrRenderCriteriaList = null;
        fieldObj.buttonrRenderedExpression = 'Test';
        fieldObj.buttonRendered = false;
        fieldObj.buttonrRenderedExpressionFinal = 'Test';
        fieldObj.autoPopulateFieldWrapperList =null;
        fieldObj.isAddAnother = false;
        fieldObj.addAnotherButtonLabel   = 'Test';
        fieldObj.removeButtonLabel   = 'Test';
        fieldObj.addAnotherLimit =0;
        fieldObj.title   = 'Test';
        fieldObj.titleType   = 'Test';
        ERx_PageEntities.AutoPopulateMappingWrapper autoPopulate = new ERx_PageEntities.AutoPopulateMappingWrapper();
        autoPopulate.sourcefield = 'Test';
        autoPopulate.destinationfields = 'Test';
        ERx_PageEntities.WrapperModel wrapperModel = new ERx_PageEntities.WrapperModel(); 
        wrapperModel.modelName = 'Test';
        wrapperModel.objectName = 'Test';
        wrapperModel.objectAPIName= 'Test';
       wrapperModel.field = null;
        wrapperModel.condition = null;
        wrapperModel.conditionExpression = 'Test';
        ERx_PageEntities.PageButtonWrapper pageButtonWrapper = new ERx_PageEntities.PageButtonWrapper();
        pageButtonWrapper.modelList = null;   
        pageButtonWrapper.buttonaNavigation= 'Test';
        pageButtonWrapper.buttonLabel= 'Test';
        pageButtonWrapper.gridPosition= 0;
        ERx_PageEntities.ModelFieldObjectWrapper modelFieldObjectWrapper = new ERx_PageEntities.ModelFieldObjectWrapper();
        modelFieldObjectWrapper.fieldName = 'Test';
        modelFieldObjectWrapper.fieldAPIName= 'Test';
         ERx_PageEntities.ConditionFieldObjectWrapper conditionFieldObjectWrapper = new ERx_PageEntities.ConditionFieldObjectWrapper();
        conditionFieldObjectWrapper.fieldName= 'Test';
        conditionFieldObjectWrapper.fieldAPIName= 'Test';
        conditionFieldObjectWrapper.fieldDisplayType= 'Test';
         ERx_PageEntities.PageWrapper pageWrapper = new ERx_PageEntities.PageWrapper();
         pageWrapper.pageName= 'Test';
        pageWrapper.sectionList= null;
        ERx_PageEntities.PageHeaderWrapper pageHeaderWrapper = new ERx_PageEntities.PageHeaderWrapper();
        pageHeaderWrapper.title= 'Test';
        pageHeaderWrapper.description= 'Test';
        ERx_PageEntities.ConditionWrapper conditionWrapper = new ERx_PageEntities.ConditionWrapper();
        conditionWrapper.fieldAPIName= 'Test';
        conditionWrapper.fieldDisplayType = 'Test';
        conditionWrapper.fieldOperator= 'Test';
        conditionWrapper.valueType = 'Test';
        conditionWrapper.value = 'Test';
        conditionWrapper.modelName = 'Test';
        conditionWrapper.referencedObjectName = 'Test';
        conditionWrapper.displayValue = 'Test';
        ERx_PageEntities.RenderedRequiredConditionWrapper renderedRequiredConditionWrapper = new ERx_PageEntities.RenderedRequiredConditionWrapper();
        renderedRequiredConditionWrapper.modelName = 'Test';
        renderedRequiredConditionWrapper.fieldAPIName  = 'Test';
        renderedRequiredConditionWrapper.fieldDisplayType = 'Test';
       renderedRequiredConditionWrapper.displayValue = 'Test';
        renderedRequiredConditionWrapper.value = 'Test';
        renderedRequiredConditionWrapper.fieldOperator = 'Test';
        renderedRequiredConditionWrapper.referencedObjectName  = 'Test';
        ERx_PageEntities.ModelCriteriaConditionWrapper modelCriteriaConditionWrapper = new ERx_PageEntities.ModelCriteriaConditionWrapper();
        modelCriteriaConditionWrapper.fieldAPIName  = 'Test';
        modelCriteriaConditionWrapper.fieldDisplayType  = 'Test';
        modelCriteriaConditionWrapper.fieldOperator  = 'Test';
        modelCriteriaConditionWrapper.valueType  = 'Test';
        modelCriteriaConditionWrapper.value  = 'Test';
        modelCriteriaConditionWrapper.modelName   = 'Test';
        modelCriteriaConditionWrapper.referencedObjectName  = 'Test';
        modelCriteriaConditionWrapper.displayValue  = 'Test';
        modelCriteriaConditionWrapper.isFormulaField= false;
        ERx_PageEntities.ModelRecordWrapper modelRecordWrapper = new ERx_PageEntities.ModelRecordWrapper();
        System.assertEquals(true, (modelRecordWrapper != null));
        modelRecordWrapper.modelAPI= null;
        modelRecordWrapper.pageModelList = null;
        modelRecordWrapper.conditionWrpperList= null;
        System.assertEquals(true, (modelRecordWrapper.conditionWrpperList == null));
        Test.stopTest();
    }
}