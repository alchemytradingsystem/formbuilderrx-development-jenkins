public with sharing class Portal_ConfigurationSettingController {
	/****
	 * @description store envjson
	 */
	public string curEnvjson{get; set;}
	/****
	 * @description store current env record
	 */
	public Env__c curEnv {get;set;}
	/****
	 * @description store current pagename
	 */
	private final String CURRENT_PAGE = 'Portal_ConfigurationSetting';   
	/****
	 * @description To store Permission error message
	 */
	private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
	/****
	 * @description To store payProceed
	 */
	public Boolean payProceed {get;set;}
	/****
	 * @description To store namespacePrefix
	*/
	public String namespacePrefix{
		get{
			namespacePrefix=PortalPackageUtility.getNameSpacePerfix();
			if(String.isBlank(namespacePrefix))
				namespacePrefix ='';
			else
				namespacePrefix=namespacePrefix+'__';

			return namespacePrefix;
		}
		set;
	}

	public Portal_ConfigurationSettingController(){
		curEnv = new Env__c();
		PackageSecurity pSecure = new PackageSecurity();
		payProceed = pSecure.checkProceed(CURRENT_PAGE);
		if(!payProceed){
			try{
				List<String> listFields = new List<String>{'Change_Password_Configure_Text_Another__c',
					'Change_Password_Configure_Text__c','Change_Password_Button_Label__c','Template_for_Site_Login__c',
					'Self_Register_URL__c','TP_Manage_Property__c','Env_Status__c','VF_Template_for_Site_Login__c',
					'Forget_User_Configure_Text__c','Forgot_Button_Label__c','Forgot_User_Configure_Text_Another__c',
					'Login_Button_Label__c','Login_User_Configure_Text__c' , 'Login_User_Configure_Text_Another__c',
					'Registration_button_label__c','Registration_User_Configure_Text__c','Registration_User_Configure_Text_Another__c'};
				if(SecurityEnforceUtility.checkObjectAccess('Env__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
					if(SecurityEnforceUtility.checkFieldAccess('Env__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
						curEnv = [SELECT Change_Password_Button_Label__c,Change_Password_Configure_Text__c,
									Change_Password_Configure_Text_Another__c,Template_for_Site_Login__c, Self_Register_URL__c,
									TP_Manage_Property__c,Env_Status__c,VF_Template_for_Site_Login__c, Forget_User_Configure_Text__c,
									Forgot_Button_Label__c, Forgot_User_Configure_Text_Another__c,Login_Button_Label__c, 
									Login_User_Configure_Text__c, Login_User_Configure_Text_Another__c,Registration_button_label__c,
									Registration_User_Configure_Text__c,Registration_User_Configure_Text_Another__c  
								FROM Env__c 
								WHERE Id =: getCurrentEnv().Env__c];
						curEnvjson = JSON.serialize(curEnv);
					} else {
						ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'Portal_SiteFieldsMainController', 'Portal_SiteFieldsMainController', null);
						throw new PortalPackageException(permissionErrorMessage); 
					}
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c ' ), 'Portal_SiteFieldsMainController', 'Portal_SiteFieldsMainController', null);
					throw new PortalPackageException(permissionErrorMessage); 
				}

			}catch(Exception e){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Environment Error: '+e.getMessage() ));
			}
		}
	}

	/*******************************************************************************************************
	 * @description to get current Env Id
	 */
	public Current_Env__c getCurrentEnv(){
		return ERx_PortalPackUtil.getCurrentEnvForConfiguration();
	}

	/*******************************************************************************************************
	 * @description forces DML to insert log messages in database
	 */
	public void initPageException() {
		try {
			ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
			ERx_PortalPackageLoggerHandler.saveExceptionLog();

		} catch (Exception e) {
			ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
			ERx_PortalPackageLoggerHandler.saveExceptionLog();
		}
	}
}