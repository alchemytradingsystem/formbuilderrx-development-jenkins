/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description An apex page controller that exposes the change password functionality
 */ 
public with sharing class Portal_ChangePasswordController {
	public String oldPassword {get; set;}
	public String newPassword {get; set;}
	public String verifyNewPassword {get; set;}        
	public Env__c cEnv{get; set;}
	//PD-4253 | Shubham
    public String FaviconIcon{
    	get {
    		Env__c cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String imageLink = '';
    		String siteURL = Site.getPathPrefix();
	        if(cEnv != null){
	            if(String.isNotBlank(cEnv.Favicon_Icon__c) && !cEnv.Favicon_Icon__c.contains('null')){
	            	List<String> pathValues = cEnv.Favicon_Icon__c.split('##');
					if(pathValues != null && pathValues.size() > 0){
						if(pathValues.size() == 1){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]);	
						}
						else if(pathValues.size() == 2){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]) + PortalPackageUtility.escapeQuotes(pathValues[1]) ;
						}
					}
	            }
	        }
	        return imageLink;
    	}
    	set;
    }
	
	//PD-4103 | Priyamvada
    public String gtmScript{
    	get {
    		cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String script = '';
	        if(cEnv != null){
	            script = cEnv.GTM_Account_Id__c;
	        }
	        return script;
    	}
    	set;
    }

	public Portal_ChangePasswordController() {
		//reterive current env
		cEnv = ERx_PortalPackUtil.getCurrentEnv();
		//if change password upper text contains pre tag then convert &quot;&gt;,&lt into html
		if(String.isNotBlank(cEnv.Change_Password_Configure_Text__c) && cEnv.Change_Password_Configure_Text__c.contains('<pre>')){
			cEnv.Change_Password_Configure_Text__c= PortalPackageUtility.changeToHTML(cEnv.Change_Password_Configure_Text__c);
		}
		//if change password lower text contains pre tag then convert &quot;&gt;,&lt into html
		if(String.isNotBlank(cEnv.Change_Password_Configure_Text_Another__c) && cEnv.Change_Password_Configure_Text_Another__c.contains('<pre>')){
			cEnv.Change_Password_Configure_Text_Another__c= PortalPackageUtility.changeToHTML(cEnv.Change_Password_Configure_Text_Another__c);
		}
		//if change password button label is blank then set  label to change password.
		if(String.isBlank(cEnv.Change_Password_Button_Label__c)){
			cEnv.Change_Password_Button_Label__c = 'Change Password';
		}
	}

	public PageReference changePassword() {
		return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
	}
}