@isTest
public class ngForceControllerTest {

	public static sObject contact;
	public static sObject app;
	public static Env__c env;
	public static User adminUser;
	//Sample Widget Library Folder
	private static Document sampleWidgetLibrary;
	

	private static String tooLongAccName = 'LOTS OF ' +
	                                       'CHARACTERS XXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' +
	                                       'XXXXXXXXXXXXXXXX';

	static private void assertError(String jsonResult, String expectedError, String method) {
		system.debug('##################' + jsonResult);
		List<Object> errorArray = (List<Object>)JSON.deserializeUntyped(jsonResult);

		System.assertNotEquals(null, errorArray,
		                       'error array missing from ' + method + ' result');
		System.assertNotEquals(0, errorArray.size(),
		                       'error array is empty in ' + method + ' result');

		Map<String, Object> error = (Map<String, Object>)errorArray[0];
		String errorCode = (String)error.get('errorCode');
		System.assertNotEquals(null, errorCode,
		                       'errorCode property missing from ' + method + ' result');
		System.assertEquals(expectedError, errorCode,
		                    'errorCode should be ' + expectedError + ' in ' + method + ' result');
	}

	static testMethod void testDescribe() {
		// Assume we have accounts
		String jsonResult = ngForceController.describe('Account');

		System.assertNotEquals(null, jsonResult,
		                       'ngForceController.describe returned null');

		Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);

		System.assertNotEquals(null, result.get('fields'),
		                       'fields property missing from ngForceController.describe result');

		// TODO - more assertions on describe results

		// Invalid object type
		// Hope there isn't a QXZXQZXZQXZQ object type!
		jsonResult = ngForceController.describe('QXZXQZXZQXZQ');
		assertError(jsonResult, 'NOT_FOUND', 'ngForceController.describe');
	}

	static private void assertRecord(Map<String, Object> record, String accName, String accNumber, String method) {
		Map<String, Object> attributes = (Map<String, Object>)record.get('attributes');
		System.assertNotEquals(null, attributes,
		                       'attributes property missing from ' + method + ' result');
		System.assertNotEquals(0, attributes.keySet().size(),
		                       'empty attributes object in ' + method + ' result');

		String type = (String)attributes.get('type');
		System.assertNotEquals(null, type,
		                       'type property missing from ' + method + ' result');
		System.assertEquals('Account', type,
		                    'Wrong type in ' + method + ' result');

		String url = (String)attributes.get('url');
		System.assertNotEquals(null, url,
		                       'url property missing from ' + method + ' result');

		Id id = (Id)record.get('Id');
		System.assertNotEquals(null, id,
		                       'Id property missing from ' + method + ' result');
		Account account = [SELECT Id, Name FROM Account WHERE Id = :id LIMIT 1];
		System.assertNotEquals(null, account,
		                       'Couldn\'t find account record identified by ' + method + ' result');
		System.assertEquals(accName, account.Name,
		                    'Account name doesn\'t match in ' + method + ' result');

		String name = (String)record.get('Name');
		System.assertNotEquals(null, name,
		                       'Name property missing from ' + method + ' result');
		System.assertEquals(accName, name,
		                    'Wrong account name in ' + method + ' result');

		String accountNumber = (String)record.get('AccountNumber');
		System.assertNotEquals(null, name,
		                       'AccountNumber property missing from ' + method + ' result');
		System.assertEquals(accNumber, accountNumber,
		                    'Wrong account number in ' + method + ' result');
	}


	static private void testDelete(Id id) {
		String jsonResult = ngForceController.del('QXZXQZXZQXZQ', id);
		assertError(jsonResult, 'NOT_FOUND', 'ngForceController.del');

		jsonResult = ngForceController.del('Account', id);
		system.debug('$$$$$$$$$$$$' + jsonResult);
		System.assertEquals(null, jsonResult,
		                    'Non-null result from ngForceController.del');
		List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id = :id];
		System.assertEquals(0, accounts.size(),
		                    'Account record was not deleted by ngForceController.del');

		jsonResult = ngForceController.del('Account', id);
		assertError(jsonResult, 'ENTITY_IS_DELETED', 'ngForceController.del');
	}

	static testMethod void testCRUD() {
		prepareData();

		Account acc=new Account();
		acc.Name='testDante';
		insert acc;
		testDelete(acc.id);

		Account acc1=new Account();
		acc1.Name='testDante123';
		acc1.accountNumber='testDante123';
		insert acc1;

		Account acc2=new Account();
		acc2.Name='testDante1234';
		acc1.accountNumber='testDante1234';
		insert acc2;

		list<Account> accList=new list<Account>();
		accList.add(acc2);

		ngForceController.query('Account','','','Name=\'testDante123\'');
		ngForceController.query('Account','Name','CreatedById','Name=\'testDante123\'');


		Contact c=new Contact();
		c.LastName='testDante';
		insert c;

		Homepage_Widget__c hw=new Homepage_Widget__c();
		hw.Name='Test Widget';
		hw.Env__c=env.Id;
		insert hw;

		ngForceController.sObjectKlone(c.Id);
		ngForceController.sObjectKlone('fakeId');
		ngForceController.sObjectKlone(acc1.Id);
		ngForceController.sObjectKlone(hw.Id);

		String accJSON=String.escapeSingleQuotes(JSON.serialize(acc1));
		ngForceController.upsertData('Account',accJSON);

		String accListJSON=String.escapeSingleQuotes(JSON.serialize(accList));
		ngForceController.upsertDataBulk('Account',accListJSON);
	}

	static testMethod void testMisc() {
		prepareData();

		System.runAs(adminUser){
			String namespacePrefixClass=ngForceController.namespacePrefixClass;
			String namespacePrefix=ngForceController.namespacePrefix;
			String envJSON=ngForceController.envJSON;
			String criteriaObjectNamesAPEX=ngForceController.criteriaObjectNamesAPEX;
			String describeResultsJSON=ngForceController.describeResultsJSON;


			List<String> objTypes=new List<String>();
			objTypes.add('Fake_Object');
			ngForceController.describes(objTypes);

			if(ngForceController.hasApplicationObject){
				ngForceController.parseHomepageWidgetCriteria('W1',env.Id,(String)contact.get('Id'),(String)app.get('Id'));
				prepareData1();
				ngForceController.parseHomepageWidgetCriteria('W1',env.Id,(String)contact.get('Id'),(String)app.get('Id'));
				prepareData2();
				ngForceController.parseHomepageWidgetCriteria('W1',env.Id,(String)contact.get('Id'),(String)app.get('Id'));
				prepareData3();
				ngForceController.parseHomepageWidgetCriteria('W1',env.Id,(String)contact.get('Id'),(String)app.get('Id'));

				ngForceController.processChecklistCotents((String)app.get('Id'));
			}
			else{
					ngForceController.parseHomepageWidgetCriteria('W1',env.Id,(String)contact.get('Id'),null);
					ngForceController.processChecklistCotents(null);
			}

			System.assertNotEquals(objTypes, null);
		}
	}
	
	public static void prepareData() {
		ERx_Portal_EnvPanelController.isFromAdminPanel = true;
		env=new Env__c();
		env.Version__c=1;
		env.Env_Site_Id__c='123';
		env.Environment_Status__c = 'Live';
		insert env;


		Account a = new Account(Name='Test Account Name');
		insert a;

		contact=Schema.getGlobalDescribe().get('Contact').getDescribe().getSobjectType().newSObject();
		contact.put('FirstName','testDante');
		contact.put('LastName','testDante');
		contact.put('Email','testDante@testDante.com');
		contact.put('AccountId',a.Id);
		insert contact;

		Profile p=[select id from Profile where Name='System Administrator'];

		adminUser = new User();
    adminUser.Username ='testDanteadminUser@testDante.com';
    adminUser.LastName = 'testDanteadminUser';
    adminUser.Email = 'testDanteadminUser@testDante.com';
    adminUser.alias = 'tadmin';
    adminUser.TimeZoneSidKey = 'America/Chicago';
    adminUser.LocaleSidKey = 'en_US';
    adminUser.ProfileId=p.id;
    adminUser.EmailEncodingKey = 'ISO-8859-1';
    adminUser.LanguageLocaleKey = 'en_US';
    insert adminUser;

		Current_Env__c ce=new Current_Env__c();
		ce.Env__c=env.Id;
		ce.Name=adminUser.Id;
		ce.Type__c='Community';
		insert ce;

		if(ngForceController.hasApplicationObject){
			app=Schema.getGlobalDescribe().get('EnrollmentrxRx__Enrollment_Opportunity__c').getDescribe().getSobjectType().newSObject();
			app.put('EnrollmentrxRx__Applicant__c',contact.Id);
			app.put('EnrollmentrxRx__Admissions_Status__c','Inquiry');
			insert app;

			Homepage_Widget_Layout__c hwl=new Homepage_Widget_Layout__c();
			hwl.Name='testLayoutDante';
			hwl.Displaying_Application_Status__c='Inquiry';
			hwl.Env__c=env.Id;
			hwl.Is_Active__c=true;
			hwl.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
			insert hwl;
			contact.put('EnrollmentrxRx__Active_Enrollment_Opportunity__c',app.Id);
			update contact;


			Homepage_Widget_Layout__c hwlDefault=new Homepage_Widget_Layout__c();
			hwlDefault.Name='Default';
			hwlDefault.Default__c=true;
			hwlDefault.Displaying_Application_Status__c='Inquiry';
			hwlDefault.Env__c=env.Id;
			hwlDefault.Is_Active__c=true;
			hwlDefault.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
			insert hwlDefault;

			Homepage_Widget_Layout__c hwl1=new Homepage_Widget_Layout__c();
			hwl1.Name='Test1';
			hwl1.Displaying_Application_Status__c='Inquiry';
			hwl1.Env__c=env.Id;
			hwl1.Is_Active__c=true;
			hwl1.Criteria_Filter_Logic__c='( 1 AND 2 AND 3 ) or 4';
			hwl1.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
			hwl1.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"equals","value":"Inquiry"},{"order":2,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"FirstName","operator":"contains","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"LastName","operator":"contains","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"not equal to","value":""}]';
			insert hwl1;

			Homepage_Widget_Layout__c hwl2=new Homepage_Widget_Layout__c();
			hwl2.Name='Test2';
			hwl2.Displaying_Application_Status__c='Inquiry';
			hwl2.Env__c=env.Id;
			hwl2.Is_Active__c=true;
			hwl2.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
			hwl2.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"starts with","value":"Inquiry"},{"order":2,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"FirstName","operator":"ends with","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"LastName","operator":"does not contain","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"not equal to","value":""}]';
			insert hwl2;
		}
	}

	public static void prepareData1() {
		Homepage_Widget__c hw1=new Homepage_Widget__c();
		hw1.Name='test HW1';
		hw1.Number__c='W1';
		hw1.Is_Active__c=true;
		hw1.Env__c=env.Id;
		insert hw1;
	}

	public static void prepareData2() {
		Homepage_Widget__c hw=new Homepage_Widget__c();
		hw.Name='test Default';
		hw.Default__c=true;
		hw.Number__c='W1';
		hw.Is_Active__c=true;
		hw.Env__c=env.Id;
		insert hw;
	}

	public static void prepareData3() {
		if(ngForceController.hasApplicationObject){
			Homepage_Widget__c hw2=new Homepage_Widget__c();
			hw2.Name='test HW2';
			hw2.Number__c='W1';
			hw2.Is_Active__c=true;
			hw2.Env__c=env.Id;
			hw2.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Program_of_Interest__c","referenceFieldName":"Name","operator":"not equal to","value":"","referenceObjectName":"EnrollmentrxRx__Program_Offered__c"}]';
			insert hw2;
		}
	}
	
	/*
		@description Test method To verify If pre exist criteria should work if in case lookup field is selected as Reference Type	
		Added for PD-2051 - test case 5
	*/
	static testMethod void testFetchRecords() {
		//Prepare Data
		Account a = new Account(Name='Test');
		insert a;
		Test.startTest();
			String strJSON = '{"Account":["\''+a.id+'\'"]}';
			Map<String,String> actual = ngForceController.fetchRecords(strJSON);
			System.assertEquals(true,actual != null);
		Test.stopTest();
	}

	/*
		@description test method to reterive content of data from widget library
	*/
	//PD-1977 - test case 4,7, PD-1978 test case 3,6
	private static testmethod void getSelectedWidgetLibraryContentTest(){
		prepareDataGetSelectedWidgetLibraryContentTest();
		Test.startTest();
		//check reterive content is not blank.
		System.assertEquals(true,ngForceController.getSelectedWidgetLibraryContent(sampleWidgetLibrary.id) != '');
		//check reterive content is equal to SampleWidgetLibraryContent.
		System.assertEquals('SampleWidgetLibraryContent',ngForceController.getSelectedWidgetLibraryContent(sampleWidgetLibrary.id).trim());
		Test.stopTest();
	}
	
	/*
		@description method to prepared data to reterive content of data from widget library
	*/
	private static void prepareDataGetSelectedWidgetLibraryContentTest(){
		Folder widgetLibraryFolder = [Select Id From Folder Where Name = 'Widget Library' LIMIT 1 ];
		sampleWidgetLibrary = new Document(); 
		sampleWidgetLibrary.Name = 'SampleWidgetLibrary'; 
		String widgetLibraryContent = 'SampleWidgetLibraryContent'; 
		sampleWidgetLibrary.Body = Blob.valueOf(widgetLibraryContent);
		sampleWidgetLibrary.ContentType = 'text/plain';
		sampleWidgetLibrary.Type = 'txt';
		sampleWidgetLibrary.folderId = widgetLibraryFolder.id;
		insert sampleWidgetLibrary;
	}
	
	/* 
		@description testMethod to reterive html of eventlisting for widget populate in form builder
	*/
	//PD-1987 test case number 3
	//PD-1989, test case 1,2,3
	private static testMethod void getEventListingContentTest(){
		Test.startTest();
		if(ngForceController.hasEventPackage){
			//when filter is set and when event package is not installed.
			System.assertEquals('',ngForceController.getEventListingContent('Planned','Campus Visit','10'));
			//when filter is not set and when event package is not installed.
			System.assertEquals('',ngForceController.getEventListingContent('','','10'));
		}else{
			//when event package is not installed.
			System.assertEquals(PortalPackageConstants.EVENT_NOT_SUPPORTED,ngForceController.getEventListingContent('','','10'));
		}
		Test.stopTest();
	}
	
	
	//Test method to check PD-2029
	//Check access on fields which are being used in criteria not all
	private static testMethod void queryTest(){
		Account a;
        Contact student;
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testCommunityUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testCommunityUser;
		Test.startTest();
			//when we will query each field
			system.assertEquals(true,String.isNotBlank(ngForceController.query('Contact','','','')));
			//when to query some fields which are being used in criteria
			system.assertEquals(true,String.isNotBlank(ngForceController.query('Contact','','',''))); 
			//when lookupfields starts with,
			system.assertEquals(true,String.isNotBlank(ngForceController.query('Contact','-PD2029-FirstName,Account.Name-fieldName-{"Contact":["FirstName","FirstName"],"Account":["Name"]}\"',',Account.LastModifiedDate','')));
			//when lookupfields starts with,
			system.assertEquals(true,String.isNotBlank(ngForceController.query('Contact','-PD2029-FirstName,Account.Name-fieldName-{"Contact":["FirstName","FirstName"],"Account":["Name"]}\"','Account.LastModifiedDate','')));
			
			//check exception for different user logged in
			System.runAs(testCommunityUser){
				try{
					//when to query some fields which are being used in criteria
					system.assertEquals(true,String.isNotBlank(ngForceController.query('Contact','','','')));
				}catch(Exception e){
					system.assertEquals(true,e != null);
				}
			}
			
			//check exception for different user logged in
			System.runAs(testCommunityUser){
				//when lookupfields starts with,
				system.assertEquals(true,String.isNotBlank(ngForceController.query('Contact','-PD2029-FirstName,Account.Name-fieldName-{"Contact":["FirstName","FirstName"],"Account":["Name"]}\"',',Account.LastModifiedDate','')));
			}
		Test.stopTest();
	}
	
	//ticket number PD-2799 
	public static testMethod void testSaveCheckList(){
		Env__c envRecord = new Env__c(Version__c = 1);
		insert envRecord;
		Test.startTest();
		ngForceController.saveCheckLists(envRecord.id, String.valueOf(4*1024*1024),'File', 'png,doc');
		System.assertEquals('png,doc',[SELECT File_Type__c
										FROM Env__c 
										WHERE Id = :envRecord.Id].File_Type__c);
		Test.stopTest();
	}
	
	//ticket number PD-2799
	public static testMethod void testEditCheckList(){
		Env__c envRecord = new Env__c(Version__c = 1);
		insert envRecord;
		Test.startTest();
		System.assertEquals(envRecord.id, ngForceController.editCheckList(envRecord.id).id);
		Test.stopTest();
	}
	
}