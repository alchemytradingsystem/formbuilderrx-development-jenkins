/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-2-1
	@description Erx_EmailLoggerCleanUp is a class to send email when ERx_CleanUpPortalPackageLogger batch finish
*/
public with sharing class Erx_EmailLoggerCleanUp {
	
	/*
    	@description: Send mail when AsyncApexJob Aborted, Failed
    */
	public static void sendMail(String subject, String textBody, String email) {
		FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
	    String emailAddr = '';
	    if(formBuilderSetting != null && formBuilderSetting.Admin_Email__c != null) {
	    	emailAddr = PortalPackageUtility.escapeQuotes(formBuilderSetting.Admin_Email__c);
	    } else {
	    	emailAddr = email;
	    }
	    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    String[] toAddresses = new String[] {emailAddr};
	    mail.setToAddresses(toAddresses);
	    mail.setSubject(subject);
	    mail.setPlainTextBody(textBody);
	    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	} 
}