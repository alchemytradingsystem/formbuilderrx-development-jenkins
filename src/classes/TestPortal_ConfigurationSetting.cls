/**
    FormBuilderRx
    @company : Copyright ÃÂ© 2016, Enrollment Rx, LLC
    All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    @website : http://www.enrollmentrx.com/
    @author EnrollmentRx
    @version 1.0
    @date 2017-06-05 
    @description Test Class for Portal_ConfigurationSettingController
    Test Commit for PD-1812.
*/
@isTest

public with sharing class TestPortal_ConfigurationSetting {
	public static Env__c testEnv;	
	private static User testCommunityUser;

	 public static testMethod void testPortal_ConfigurationSetting() {
    	 test.startTest();
    		Portal_ConfigurationSettingController pConf = new Portal_ConfigurationSettingController();
    		pConf.initPageException();
         	System.assertEquals(true, pConf != null);
    	 Test.stopTest(); 
    }
    
	
	
	static testMethod void runAsAnotherUser() {
    	Test_Erx_Data_Preparation.preparation();
    	createUser();
    	System.runAs(testCommunityUser){
    		try{
    			Portal_ConfigurationSettingController pConf = new Portal_ConfigurationSettingController();
    		}Catch(Exception e){
    			system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
    		}
    	}
    }
	
	
	private static void createUser(){
	  	Account a;
        Contact student;
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        testCommunityUser = new User(alias = 'u7', email='u7@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u7@testorg.com',contactId=student.Id);
        insert testCommunityUser;
        
         List<Env__c> listEnv = new List<Env__c>();
        for(Integer i=0;i<3 ;i++) {
            Env__c env1 = new Env__c(Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Test' );
            listEnv.add(env1);
        }
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert listEnv;  
        
        testEnv = listEnv[0];
    }
    
}