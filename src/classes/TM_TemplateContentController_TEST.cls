@isTest
private class TM_TemplateContentController_TEST
{ private static User testCommunityUser;
 private static Document sampleWidgetLibrary;
 @isTest
 static void itShould()
 {
        TM_TemplateContentController template = new TM_TemplateContentController();
     
        //template.quickSave();
  Test_Erx_Data_Preparation.preparation();
  System.runAs(Test_Erx_Data_Preparation.testAdminUser){
   TM_TemplateContentController tcc = new TM_TemplateContentController();
            tcc.frontRecordId = null;
            tcc.templateFooter = '';
            tcc.templateName = '';
            tcc.newTemplate = true;
            tcc.frontEndObjectId = 'xyz';   
            tcc.deleteField();
            tcc.quickSave();
   			tcc.getCurrentEnv();
            String assignedTemp = tcc.assignedMainTemplate;
   			tcc.getCurrentEntity();
   			tcc.frontRecordId = Test_Erx_Data_Preparation.siteTemplate[0].Id;
   			tcc.frontEndObjectId = Test_Erx_Data_Preparation.siteTemplate[0].Id;
            //system.assert(false,tcc.currEnv);
            tcc.mainTemplateCriteria = new RenderCriteriaConditionWrapper();
            tcc.mainTemplateCriteria.isDefault = false;
            tcc.mainTemplateCriteria.isActive = true;
      		String s = template.assignedLoginTemplate;
      		List<SelectOption> options = template.getERxFbTemplatesDocuments();
      		template.fbTemplatesDocumentName = '01536000004MNXoAAO';
      		template.addNewTmp();
      		template.editRecord();
      		Folder widgetLibraryFolder = [Select Id From Folder Where Name = 'ERx Formbuilder Templates' LIMIT 1 ];
            sampleWidgetLibrary = new Document(); 
            sampleWidgetLibrary.Name = 'ERx Formbuilder Templates'; 
            String widgetLibraryContent = '<ERx_FB_Template><header><![CDATA[<!doctype html>]]></header><footer><![CDATA[<footer class="footer"></footer></div><script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script src="##UrlPathPrefix##/resource/template_1/js/custom.js"></script></body></html>]]></footer><stylejson><![CDATA[[{"sLbl" : "Theme Body Background", "sAPI" : "--theme-body-bg", "sType" : "1", "sVal" : "#e6e6e6"}]]]></stylejson></ERx_FB_Template>'; 
            sampleWidgetLibrary.Body = Blob.valueOf(widgetLibraryContent);
            sampleWidgetLibrary.ContentType = 'text/plain';
            sampleWidgetLibrary.Type = 'txt';
            sampleWidgetLibrary.folderId = widgetLibraryFolder.id;
            insert sampleWidgetLibrary;
             tcc.fbTemplatesDocumentName = String.valueof(sampleWidgetLibrary.Id);
      		tcc.addNewTmp();
             tcc.proceed();
            tcc.quickSave();
   			tcc.editRecord();
            tcc.quickSave();
   			tcc.validateRec();
   			tcc.tempAssignment();
   			tcc.initiateData();
   			tcc.getTemplateList();
   			tcc.templateOptions();
   			tcc.DefaultTemplate();
			tcc.cloneRecord();
   			tcc.reviewTemplate();
            //System.assert(false,tcc.currEnv);
   			tcc.quickSave();
            tcc.getStaticResources();
            tcc.addNewResourceItem();
            tcc.deleteResourceItem();
            tcc.addNewTmp();
            tcc.save();
            TM_TemplateContentController.ResourceItem it = new TM_TemplateContentController.ResourceItem('s','s','s','s','s');
            tcc.cancel();
            tcc.addNewTmp();
            tcc.deleteField();
            tcc.initPageException();
            tcc.getReadyToIsertNew();
            System.assertNotEquals(null,tcc.pageURL);
            tcc.checkIfPaidUser();
            tcc.tmpList = null;
            tcc.DefaultTemplate();
   		ApexPages.currentPage().getParameters().put('templateId','invalid id');
   		try{
    	//exception not handled in class
    	tcc = new TM_TemplateContentController();
   		}catch(Exception e){}
   			System.assertNotEquals(tcc, null);
  		}
  
  	createUser();
  	System.runAs(testCommunityUser){
   	try{
    TM_TemplateContentController tcc = new TM_TemplateContentController();
    tcc.getCurrentEnv();
    tcc.getCurrentEntity();
    tcc.frontRecordId = Test_Erx_Data_Preparation.siteTemplate[0].Id;
    tcc.frontEndObjectId = Test_Erx_Data_Preparation.siteTemplate[0].Id;
       tcc.getCurrentEntity();
    }Catch(Exception e){
       system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
      }
  }
 }
 
  private static void createUser(){
    Account a;
        Contact student;
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        testCommunityUser = new User(alias = 'ui', email='ui@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='ui@testorg.com',contactId=student.Id);
        insert testCommunityUser;
    }
 
 	static testmethod void testgetERxFbTemplates(){
         TM_TemplateContentController tcc = new TM_TemplateContentController();
         String val1 = tcc.templateCss;
         String val2 = tcc.useERxFBTemplate;
         String val3 = tcc.fbTemplatesDocumentName;
         List<Document> val4 = tcc.documents;
         List<SelectOption> actual = tcc.getERxFbTemplates();
         System.assertEquals(true, actual.size() == 2);
     }
 	
 
 	 static testmethod void testgetERxFbTemplatesDocuments(){
         TM_TemplateContentController tcc = new TM_TemplateContentController();
         List<SelectOption> actual = tcc.getERxFbTemplatesDocuments();
         tcc.assignFbTemplate();
         String val1 = tcc.getFbTemplate();
         tcc.setFbTemplate('');
         tcc.redirectToNewTemplate();
         tcc.proceed();
         System.assertEquals(true, actual.size() == 0);
     }
 
     static testmethod void testgetStaticResources(){
          TM_TemplateContentController tcc = new TM_TemplateContentController();
          list<SelectOption> actual = tcc.getStaticResources();
         tcc.addNewResourceItem();
         list<SelectOption> actual2 = tcc.getStaticResourcesForImage();
     }	
 
 	 static testMethod void testCSSPanelModelWrapper(){
         TM_TemplateContentController tcc = new TM_TemplateContentController();
         TM_TemplateContentController.CSSPanelModelWrapper  css = new TM_TemplateContentController.CSSPanelModelWrapper();
         css.sLbl='';
         css.sAPI='';
         css.sType='';
         css.sVal='';
     }
 
 	static testMethod void testgetReadyToIsertNew1(){
             TM_TemplateContentController tcc = new TM_TemplateContentController();
         List<TM_TemplateContentController.ResourceItem> abc = new List<TM_TemplateContentController.ResourceItem>();
         abc.add(new TM_TemplateContentController.ResourceItem());
         tcc.resourceList = abc;
         tcc.getReadyToIsertNew();
     }
 
     static testMethod void testwithoutPermission(){
         try{
         	Profile testProfile = [Select Id,name From Profile where name='Test' Limit 1];
         	Account a;
            Contact student;
            //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
            a = new Account(name = 'TEST ACCOUNT');
            Database.insert(a);
            //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
            student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
            insert student;
         	User testCommunityUser1 = new User(alias = 'ui1', email='ui1@testorg.com', emailencodingkey='UTF-8', lastname='Testing1', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='ui1@testorg.com',contactId=student.Id);
        	insert testCommunityUser1;
  			System.runAs(testCommunityUser1){
                TM_TemplateContentController tcc = new TM_TemplateContentController();
            }
         }catch(Exception e){
             
         }
     }
 	
}