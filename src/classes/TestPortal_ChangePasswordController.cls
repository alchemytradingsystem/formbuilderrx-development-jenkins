/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description An apex page controller that exposes the change password functionality
 */  
@IsTest
public with sharing class TestPortal_ChangePasswordController {
	//test site register functionality PD-2844
	public static testmethod void testChangePasswordController() {
		// Instantiate a new controller with all parameters in the page
		Test.startTest();
		Account a;
		a = new Account(name = 'TEST ACCOUNT');
		Database.insert(a);
		Contact student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
		insert student;
		Profile testProfile = [select id, name from profile where name='Applicant Portal User'];
		User testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
		insert testUser;
		system.runAs(testUser){
			Env__c envRecord = new Env__c(Version__c = 1, Change_Password_Configure_Text__c = 'test<pre>', Change_Password_Configure_Text_Another__c = '<pre>test', 
					Favicon_Icon__c='Val1##Val2',Environment_Status__c = 'Live',Env_Site_Id__c='123');
			insert envRecord;
			Portal_ChangePasswordController controller = new Portal_ChangePasswordController();
			controller.oldPassword = '123456';
			controller.newPassword = 'qwerty1'; 
			controller.verifyNewPassword = 'qwerty1';  
            String fav = controller.FaviconIcon;
            String gtm = controller.gtmScript;
			System.assertEquals(controller.changePassword(),null); 
		}
		Test.stopTest();                           
	}    
}