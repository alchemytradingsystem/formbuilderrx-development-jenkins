@isTest 
private class Homepage_UploadControllerTest {
	public static sObject ead;
    public static Contact c;

    static testMethod void myUnitTest() {
        // TO DO: implement unit test

        prepareData();
		test.startTest();
        ApexPages.currentPage().getParameters().put('id',(String)c.get('Id'));
        ApexPages.currentPage().getParameters().put('name','Essay');
        ApexPages.currentPage().getParameters().put('err','Error');

        Homepage_UploadController con =new Homepage_UploadController();
		con.cId = c.Id;
    	con.document=Blob.valueOf('Unit Test Attachment Body');
    	con.contentType='application/pdf';
    	con.fileName='test';
		
        con.uploadAttachment();

        con.attachmentId=con.getAttachments()[0].Id;
        con.deleteAttachment();
        con.back();

        //
        con.attachmentId=null;
        con.deleteAttachment();
        con.attachmentId='01p610000086XXX';
        con.deleteAttachment();

        System.assertNotEquals(con, null);
        Test.stopTest();  
    }

    static testMethod void myUnitTest1() {
    	prepareData();
    	Test.startTest();  
    	ApexPages.currentPage().getParameters().put('id','01p610000086XXX');
        ApexPages.currentPage().getParameters().put('name','Essay');
        ApexPages.currentPage().getParameters().put('err','Error');

        Homepage_UploadController con =new Homepage_UploadController();
		con.cId = c.Id;
    	con.document=Blob.valueOf('Unit Test Attachment Body');
    	con.contentType='application/pdf';
    	con.fileName='test';

        con.uploadFile();

        con.contentType='text/plain';
        con.uploadFile();

        con.document=null;
        con.uploadFile();

        System.assertNotEquals(con, null);
        Test.stopTest();
    }
    
    public static testmethod void testFile(){
        prepareData();
		test.startTest();
        ApexPages.currentPage().getParameters().put('id',(String)c.get('Id'));
        ApexPages.currentPage().getParameters().put('name','Essay');
        ApexPages.currentPage().getParameters().put('err','Error');

        Homepage_UploadController con =new Homepage_UploadController();
		con.cId = c.Id;
    	con.document=Blob.valueOf('Unit Test Attachment Body');
    	con.contentType='application/pdf';
    	con.fileName='test';
        con.uploadFile();
        List<ContentVersion> actual = con.getFileList();
        System.assert(true,actual.size()==1);
        con.fileId = actual[0].ContentDocumentId;
        con.deleteFile();
    }

    public static testmethod void testBack(){
        prepareData();
        Test.startTest();  
    	ApexPages.currentPage().getParameters().put('id','01p610000086XXX');
        ApexPages.currentPage().getParameters().put('name','Essay');
        ApexPages.currentPage().getParameters().put('err','Error');
		ngForceController.hasApplicationObject = true;
        Homepage_UploadController con =new Homepage_UploadController();
        con.hasConfigurationObject = true;
        con.isApplicationFieldExist = true;
        con.fileSize ='';
        con.fileType = '';
        con.itemObj = null;
        PageReference actual = con.back();
        PageReference expected = new PageReference('/');
        System.assert(true,String.valueOf(expected).equals(String.valueOf(actual)));
        Test.stopTest();  
    }
    	
    public static void prepareData() {
		ERx_Portal_EnvPanelController.isFromAdminPanel = true;
		Env__c env=new Env__c();
		env.Version__c=1;
		env.Env_Site_Id__c='123';
		env.Environment_Status__c = 'Live';
		insert env;

		Account a = new Account(Name='Test Account Name');
		insert a;

		c=new Contact();
		c.firstName='testDante';
		c.LastName='testDante';
		c.Email='testDante@testDante.com';
		c.AccountId=a.Id;
		insert c;

		Profile p=[select id from Profile where Name='Customer Community Login User'];

        User user = new User();
	    user.Username ='testDante@testDante.com';
	    user.LastName = c.LastName;
	    user.Email = c.Email;
	    user.alias = 'testaa';
	    user.TimeZoneSidKey = 'America/Chicago';
	    user.LocaleSidKey = 'en_US';
	    user.ProfileId=p.id;
	    user.EmailEncodingKey = 'ISO-8859-1';
	   	user.LanguageLocaleKey = 'en_US';
	    user.ContactId = c.id;
        //insert user;

        // if(ngForceController.hasApplicationObject){
    	// 	sObject app=Schema.getGlobalDescribe().get('EnrollmentrxRx__Enrollment_Opportunity__c').getDescribe().getSobjectType().newSObject();
    	// 	app.put('EnrollmentrxRx__Applicant__c',c.Id);
    	// 	app.put('EnrollmentrxRx__Admissions_Status__c','Inquiry');
    	// 	insert app;
        //
    	// 	ead=Schema.getGlobalDescribe().get('EnrollmentrxRx__Admissions_Document__c').getDescribe().getSobjectType().newSObject();
     //   		ead.put('EnrollmentrxRx__Document_Name__c','Essay');
     //   		ead.put('EnrollmentrxRx__Document_Status__c','Not Received');
     //   		ead.put('EnrollmentrxRx__Document_Name__c',app.Id);
     //   		ead.put('EnrollmentrxRx__Date_Requested__c',Date.today());
        //
        // 	insert ead;
        // }
    }
}