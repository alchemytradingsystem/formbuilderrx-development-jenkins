/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description The purpose of this class is to reduce the Describe limits and increasing performance of your app
*/
public with sharing class SchemaCache {
	/****
    * @description contains field that has been deleted.
    */
	public static String errorMessageFields = '';
	/****
    * @description contains object api name and object description
    */
    private static Map<String, SObjectType> gdMap {get;set;}
    /****
    * @description contains object api name and object description
    */
    private static Map<String, DescribeSobjectResult> dsrMap = new Map<String, DescribeSobjectResult>();
    /****
    * @description contains field api name and field description
    */
    private static Map<String, DescribeFieldResult> dfrMap = new Map<String, DescribeFieldResult>();
    /****
    * @description contains object api name and their fields
    */
    private static Map<String, Map<String, Schema.SObjectField>> sObjFldMap = new Map<String, Map<String, Schema.SObjectField>>();
    
    private static Set<String> FieldNotFoundMailSend = new Set<String>();
    
    private static String namespaceFinal = PortalPackageUtility.getFinalNameSpacePerfix();
     
 	/*******************************************************************************************************
    * @description only retrive map of sobjects if not already retrieved
    * @return gdMap, contains object api name and object description
    */
    public static Map<String, SObjectType> getGlobalDescribeMap() { 
        if(gdMap == null) {
            gdMap = Schema.getGlobalDescribe();
        }
        return gdMap;
    }
     
    /*******************************************************************************************************
    * @description Describe an object only once otherwise returns already described
    * @param obj contain object name
    * @return dsrMap, contain object description
    */
    public static DescribeSobjectResult getSobjectDescribe(String obj) { 
        try {
            if(!dsrMap.containsKey(obj)) { 
                dsrMap.put(obj, getGlobalDescribeMap().get(obj).getDescribe());
            }
        } catch (Exception e) {
            ERx_PortalPackageLoggerHandler.addException(e, 'SchemaCache', 'getSobjectDescribe', ApexPages.currentPage().getParameters().get('pageId'));
        }
        return dsrMap.get(obj);
    } 
     
 	/*******************************************************************************************************
    * @description Return map of sobject obj's field using efficient approach
    * @param obj contain object name
    * @return return list of fiels for given object
    */
    public static Map<String, Schema.SObjectField> getSobjectFieldsMap(String obj) {
        try {
          if(!sObjFldMap.containsKey(obj)) {
                sObjFldMap.put(obj, getSobjectDescribe(obj).fields.getMap());
            }
        } catch (Exception e) {
            ERx_PortalPackageLoggerHandler.addException(e, 'SchemaCache', 'getSobjectFieldsMap', ApexPages.currentPage().getParameters().get('pageId'));
        }
        return sObjFldMap.get(obj); 
    }	
     
    /*******************************************************************************************************
    * @description Describe a field of an object only once otherwise returns field already described
    * @param obj contain object name
    * @param fld contain field name
    * @return return field description
    */
    public static DescribeFieldResult getFieldDescribe(String obj, String fld) {
      //PD-4402
      obj = PortalPackageUtility.escapeQuotes(obj);
      fld = PortalPackageUtility.escapeQuotes(fld);
      String key = obj+'___'+fld;
      String errorFields = '';
        try {
          	if(!dfrMap.containsKey(key)) {
          	  errorFields = fld.replace(namespaceFinal,'');
          	  //add deprected and deleted field
          	  DescribeFieldResult dfr = null;
          	  if(getSobjectFieldsMap(obj).get(fld.replace(namespaceFinal,'')) != null){
      	  			dfr = getSobjectFieldsMap(obj).get(fld.replace(namespaceFinal,'')).getDescribe();
          	  }
          	  if(dfr != null && !String.valueOf(dfr.getSobjectField()).contains('INVALID FIELD:')){
          	  	dfrMap.put(key, getSobjectFieldsMap(obj).get(fld.replace(namespaceFinal,'')).getDescribe());
          	  }else{
          	  	if(!errorMessageFields.containsIgnoreCase(fld.replace(namespaceFinal,''))){
          	  		errorMessageFields = errorMessageFields + '[objectName - '+obj+' ---- fieldName - '+fld.replace(namespaceFinal,'') + '],';
          	  	}
          	  	throw new PortalPackageException();
          	  }           
            }
        } catch (Exception e) {
        	if(errorFields != ''){
        		errorFields = 'Fields That need to be check: '+errorFields;
        	}
        	if(!FieldNotFoundMailSend.contains(obj + '----' + fld)) {
        		//ERx_PortalPackUtil.sendEmailToAdmin(' Object Name : ' + obj + '<br/>' + ' Field API Name : ' + fld, ' Fields is missing : ');
        		FieldNotFoundMailSend.add(obj + '----' + fld);
        	}
        	if(ApexPages.currentPage() != null){
        		ERx_PortalPackageLoggerHandler.addException(e, 'SchemaCache', 'getFieldDescribe', ApexPages.currentPage().getParameters().get('pageId')+'   '+errorFields);
        	}else{
        		ERx_PortalPackageLoggerHandler.addException(e, 'SchemaCache', 'getFieldDescribe', errorFields);
        	}
            
            return null;
        }
        return dfrMap.get(key);
    }
}