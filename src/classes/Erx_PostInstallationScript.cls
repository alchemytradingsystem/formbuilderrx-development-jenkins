/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-12-28 
	@description Erx_PostInstallationScript Class first run when managed packaged will be installed 
*/
global class Erx_PostInstallationScript implements InstallHandler {
	
	global void onInstall(InstallContext context) {
		if (context.previousVersion() == null) {
            scheduleClasses();
        } else {
            if (context.isUpgrade()) { 
                scheduleClasses();
            }
        }
        copyCreatedDateAndLastModifiedDate();
        copyPageNameAndPageNameDisplayed();
        createAdminSettingsIfNotExist();
	}
	/*
	04-10-18 Added by Shubham Re PD-3390 method to copy older values of created date and last modified date to new fields used
	*/
	public void copyCreatedDateAndLastModifiedDate(){
		List<Erx_Page__c> pageList = [SELECT Id,Created__c,Created_date__c,LastModified__c,Last_Modified_By__c FROM Erx_Page__c LIMIT :limits.getLimitQueryRows()];
		if(pageList != null && pageList.size() > 0){
			for(Erx_Page__c page : pageList){
				if(String.isBlank(page.Created__c)){
					page.Created__c = page.Created_date__c;
				}
				if(String.isBlank(page.LastModified__c)){
					page.LastModified__c = page.Last_Modified_By__c;
				}
			}
			FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
			if(adminSettings != null){
				if(adminSettings.Allow_editing__c){
					update pageList;
				}
				else{
					adminSettings.Allow_editing__c = true;
					update adminSettings;
					update pageList;
					adminSettings.Allow_editing__c = false;
					update adminSettings;
				}
			}
			else{
				update pageList;
			}
		}
	}
	
	/*
	 	11-05-18 Added by Shubham Re PD-2198 copy Page API Name to page name displayed
	*/
	public void copyPageNameAndPageNameDisplayed(){
		List<Erx_Page__c> pageList = [SELECT Id,Page_Name__c,Page_Name_Displayed__c FROM Erx_Page__c LIMIT :limits.getLimitQueryRows()];
		if(pageList != null && pageList.size() > 0){
			for(Erx_Page__c page : pageList){
				if(String.isBlank(page.Page_Name_Displayed__c)){
					page.Page_Name_Displayed__c = page.Page_Name__c;
				}
			}
			FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
			if(adminSettings != null){
				if(adminSettings.Allow_editing__c){
					update pageList;
				}
				else{
					adminSettings.Allow_editing__c = true;
					update adminSettings;
					update pageList;
					adminSettings.Allow_editing__c = false;
					update adminSettings;
				}
				//Pd-2689 | Gourav | Social media required field error msg setting
	            if(String.isBlank(adminSettings.Social_Media_Required_Field_Message__c)){
	              adminSettings.Social_Media_Required_Field_Message__c='Some more information required';
	            }
			}
			else{
				update pageList;
			}
    	}
	}
	/**
	* 27-07-18 Added By Shubham Re PD-3734 Add Custom setting if not exist
	*/
	public void createAdminSettingsIfNotExist(){
		if(FormBuilder_Settings__c.getValues('Admin Settings') == null){
			FormBuilder_Settings__c adminSettings = new FormBuilder_Settings__c();
			adminSettings.Name = 'Admin Settings';
            adminSettings.Lookup_Max_Limit__c = 20;
            adminSettings.Lookup_Admin_Max_Limit__c = 200;
            adminSettings.Permission_Error_Message__c = 'Permission Error. Please contact administrator';
            adminSettings.Admin_Email__c = 'admin@enrollmentrx.com';
            adminSettings.DeleteLoggerBeforeDays__c = 5;
            adminSettings.Include_backward_compatibility_Resource__c = true;
            insert adminSettings;	
		}
		else{
			FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
			adminSettings.Include_backward_compatibility_Resource__c = true;
            update adminSettings;
		}
	}
	
	/**
     * Method to Schedule classes
     */
    private void scheduleClasses() {
    	Set<String> setApexClassId = new Set<String>();
        
    	for (AsyncApexJob aaj : [SELECT Status, ApexClassId FROM AsyncApexJob
								WHERE JobType = 'ScheduledApex'
    							AND Status NOT IN ('Aborted', 'Completed', 'Failed')
    							LIMIT :limits.getLimitQueryRows()]) {
        	setApexClassId.add(aaj.ApexClassId);
    	}
    
    	Set<String> apexClasses = new Set<String>();
    
    	for (ApexClass ac : [SELECT Name, Id FROM ApexClass WHERE Id IN :setApexClassId
    						LIMIT :limits.getLimitQueryRows()]) {
        	apexClasses.add(ac.Name.toLowerCase().trim());
    	}
    	
    	if (!apexClasses.contains('ERx_LoggerCleanUpScheduler')) {
        	try {
        		ERx_LoggerCleanUpScheduler.scheduleIt('Formbuilder ERx_LoggerCleanUpScheduler', '0 0 0 * * ?');
    		} catch (Exception e) {        		
    			if (!e.getMessage().contains('The Apex job named "Formbuilder ERx_LoggerCleanUpScheduler" is already scheduled for execution.')) {
    				ERx_LoggerCleanUpScheduler.scheduleIt('Formbuilder ERx_LoggerCleanUpScheduler', '0 0 0 * * ?');
    			}
    		}
    	}	    	
    }

}