/********
FormBuilderRx
@company : Copyright � 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author : Gourav Gandhi
@date : 9/5/18
PD-2689
********/
global class SocialRegistrationHandler implements Auth.RegistrationHandler{

		//Class to handle the exception
   		class RegHandlerException extends Exception {}
    
		/**
		 * Let anyone register as long as the required fields are supplied
		 * We require email, lastName, firstName
		 * @author - Gourav 
		 * @data - the user's info from the Auth Provider
		 * @date - 9/5/18
		 **/ 
		global boolean canCreateUser(Auth.UserData data) {
		    System.debug('canCreateUser was called for ' + (data != null ? data.email : 'null'));
		    Boolean retVal = (data != null 
		            && data.email != null
		            && data.lastName != null
		            && data.firstName != null);
		    
		    System.debug('data.username='+data.username);
		    System.debug('data.email='+data.email);
		    System.debug('data.lastName='+data.lastName);
		    System.debug('data.firstName='+data.firstName);
		    
		    return retVal;
		}
		
		/**
		 * Create the User - A required method to implement the Handler Interface
		 * It is called when any user with the email is not associalted with the email Id in the third party link of the user
		 * @param portalId  - Id of the Community
		 * @param data - Auth Provider user data describing the User to create
		 * @return User that has been initialized
		 * @author - Gourav
		**/ 
		global User createUser(Id portalId, Auth.UserData data){
            Portal_RegisterController pRcon = new Portal_RegisterController();
		    if(!canCreateUser(data)) {
		    	system.debug('missing field or email validation thrown');
		    	string missingEmailException =  EncodingUtil.urlEncode('missingEmail##@##Env','UTF-8');
		        throw new RegHandlerException(missingEmailException + pRcon.environmentId);
		    }
		    
		    // Is this a Community Context?
		    if(data.attributeMap.containsKey('sfdc_networkid')) {
		        System.debug('Registering Community user: ' + data.email);
		         String encodedData='';
		        //if user is found with the email Id as of registering user then the previous user will be returned
		        //irrespective of its seervice provider
		        List<User> existedUser = [Select Id from User where email =: data.email];
		        if(existedUser != null && existedUser.size()>0){
		        	System.debug('socialRegistration handler : number of user ' + existedUser);
		        	return existedUser[0];
		        }
		        
		      try{  
			        pRcon.uWrap.email = data.email;
			        pRcon.uWrap.firstname = data.firstname;
			        pRcon.uWrap.lastname=data.lastname;
			        pRcon.isSocialLogin = true;
			        pRcon.register();
			       
			        if(pRcon.isExtraFieldsRequired){
			        	encodedData = dataEncoding(data);
			        	system.debug('required field validation thrown');
			        	throw new RegHandlerException('requiredFields'+ encodedData + pRcon.environmentId);
			        }
			        system.debug('succesfully user registered');
			        return pRcon.uWrap.u;
		      	}catch(Exception e){
		      		system.debug('unknown exception found ' + e.getmessage());
		      		throw new RegHandlerException(e.getmessage());
		      	} 
		    }else{
		    	return null;
		    }
		}
		
		/**
		 * Update the user
		 * It is called when any user with the email is associalted with the email Id in the third party link of the user
		 * @param portalId  - Id of the Community
		 * @param data - Auth Provider user data describing the User to create
		 **/     
		global void updateUser(Id userId, Id portalId, Auth.UserData data){
		   system.debug('In update  User with email : ' + data.email);
		}
		
		public string dataEncoding( Auth.UserData data){
			return EncodingUtil.urlEncode('##@##' + data.firstname + '##@##' + data.lastname + '##@##' + data.email + '##@##Env','UTF-8');
		}
	
	
    
}