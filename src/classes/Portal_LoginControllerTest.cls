/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Portal_LoginControllerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
         /***********************************************
        //start add Error MSG record
        ***********************************************/
        Portal_Registration_Message__c msg = new Portal_Registration_Message__c();
        msg.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        msg.Account_Name__c = 'testAcc';
        msg.Incorrect_Username_Password_Message__c='testUnamePsd';
        msg.Login_Field_Missing_Message__c='msing1';
        msg.Login_Non_Portal_User_Message__c='test';
        msg.Reg_Portal_User_Exists_Message__c='test2';
        msg.Password_Field_Missing_Message__c = 'testpasswd';
        msg.Password_Non_Portal_User_Exists_Message__c = 'nonportaluser';
        msg.Password_Reset_Confirm_Message__c = 'testconfirm';
        insert msg;
		Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);


        Portal_LoginController plc = new Portal_LoginController();
		String fav = plc.FaviconIcon;
        String gtm = plc.gtmScript;
        
        plc.login();

        plc.uWrap.email = 'thisisxx2@eaf.com';
        plc.uWrap.password = '09!28@sdDD';
        plc.login();

        plc.uWrap.email = Test_Erx_Data_Preparation.testAdminUser.email;
        plc.login();

        msg.Student_Portal_User_Profile__c='testprofiletestprofiletestprofile';
        update msg;
        plc.login();

        System.assertNotEquals(msg, null);
    }
    
}