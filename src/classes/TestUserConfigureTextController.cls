/**
    FormBuilderRx
    @company : Copyright ÃÂ© 2016, Enrollment Rx, LLC
    All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
    IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    @website : http://www.enrollmentrx.com/
    @author EnrollmentRx
    @version 1.0
    @date 2017-06-05 
    @description Test Class for UserConfigureTextController
    Test Commit for PD-1812.
*/
@isTest
public with sharing class TestUserConfigureTextController {
	public static string errorMessage =  'Page has been saved successfully.';
	public static String testEnv;
	private static User testCommunityUser;
    public static testMethod void testUserConfigureTextControllerSaveData() {
    	createUser();
    	System.runAs(testCommunityUser){
    	test.startTest();
    	System.assertEquals(errorMessage,UserConfigureTextController.saveData(testEnv));
    	Test.stopTest(); 
    	}
    }
     public static testMethod void testUserConfigureTextControllerSaveData1() {
        try{
        	Profile testProfile = [Select Id,name From Profile where name='Test' Limit 1];
         	Account a;
            Contact student;
            //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
            a = new Account(name = 'TEST ACCOUNT');
            Database.insert(a);
            //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
            student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
            insert student;
         	User testCommunityUser1 = new User(alias = 'ui1', email='ui1@testorg.com', emailencodingkey='UTF-8', lastname='Testing1', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='ui1@testorg.com',contactId=student.Id);
        	insert testCommunityUser1;
            System.runAs(testCommunityUser1){
                test.startTest();
                String actual = UserConfigureTextController.saveData(testEnv);
                System.assertEquals(true, actual != null);
                Test.stopTest(); 
            }
        }catch(Exception e){
            
        }
    }
    
     public static testMethod void testUserConfigureTextController() {
    	 test.startTest();
    	 	FormBuilder_Settings__c adminSettings = new FormBuilder_Settings__c();
         	adminSettings.Name = 'Admin Settings';
         	adminSettings.Include_backward_compatibility_Resource__c = true;
         	insert adminSettings;
         	UserConfigureTextController userConf = new UserConfigureTextController();
         	adminSettings.Include_backward_compatibility_Resource__c = false;
         	update adminSettings;
         	userConf = new UserConfigureTextController();
         	userConf.envRecordJSON = '';
         	userConf.includeBackwardCompatibilityResource = true;
         	System.assertEquals(true, userConf != null);
    	 Test.stopTest(); 
    }
    
    private static void createUser(){
	  	Account a;
        Contact student;
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Student Portal User'];
        testCommunityUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testCommunityUser;
        
         List<Env__c> listEnv = new List<Env__c>();
        for(Integer i=0;i<3 ;i++) {
            Env__c env1 = new Env__c(Description__c = 'Test', Version__c = 1 , TP_Manage_Property__c = 'Community',  Environment_Status__c = 'Test' );
            listEnv.add(env1);
        }
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        insert listEnv;  
        testEnv = JSON.serialize(listEnv[0]);
    }
}