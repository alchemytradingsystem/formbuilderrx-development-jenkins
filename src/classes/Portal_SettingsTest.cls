/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@istest
private class Portal_SettingsTest {
	private static User testCommunityUser;
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        Env__c cEnv = Test_Erx_Data_Preparation.listEnv[0];
        Portal_Settings pt = new Portal_Settings(cEnv.Id);

        System.assertNotEquals(pt, null);
        
        pt.curEnvId = null;
        createUser();
    	System.runAs(testCommunityUser){
    		try{
    			pt = new Portal_Settings(cEnv.Id);
    		}Catch(Exception e){
    			system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
    		}
    	}
    }

    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        Env__c cEnv = Test_Erx_Data_Preparation.listEnv[0];
        Portal_Settings pt = new Portal_Settings(cEnv.Id);

        System.assertNotEquals(pt, null);
    }

     static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
         /***********************************************
        //start add Error MSG record
        ***********************************************/
        Portal_Registration_Message__c msg = new Portal_Registration_Message__c();
        msg.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        msg.Account_Name__c = 'testAcc';
        msg.Incorrect_Username_Password_Message__c='testUnamePsd';
        msg.Login_Field_Missing_Message__c='msing1';
        msg.Login_Non_Portal_User_Message__c='test';
        msg.Reg_Portal_User_Exists_Message__c='test2';
        msg.Password_Field_Missing_Message__c = 'testpasswd';
        msg.Password_Non_Portal_User_Exists_Message__c = 'nonportaluser';
        msg.Password_Reset_Confirm_Message__c = 'testconfirm';
        msg.Student_Portal_User_Profile__c='testproile';
        insert msg;
		/***********************************************
        //end add Error MSG record
        ***********************************************/
        Env__c cEnv = Test_Erx_Data_Preparation.listEnv[0];
        Portal_Settings pt = new Portal_Settings(cEnv.Id);
        pt.setting = null;
		pt.initPageException();
        System.assertNotEquals(pt, null);
    	
    }
    
    private static void createUser(){
	  	Account a;
        Contact student;
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        testCommunityUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testCommunityUser;
    }
}