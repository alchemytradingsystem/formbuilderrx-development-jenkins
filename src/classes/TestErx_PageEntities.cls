/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-08-23
	@description Test Class is used for Erx_PageEntities
*/
@IsTest
public class TestErx_PageEntities {
    private static testMethod void testPageEntities() {
        Test.startTest();
        Contact con = new Contact(lastName='test');
        ERx_PageEntities pageEntities = new ERx_PageEntities();
        ERx_PageEntities.AttachmentWrapper attachmentObj = new ERx_PageEntities.AttachmentWrapper();
        ERx_PageEntities.AttachmentWrapper attachmentObj1 = new ERx_PageEntities.AttachmentWrapper(null,null,null,null,null);
        ERx_PageEntities.ConditionWrapper conditionObj = new ERx_PageEntities.ConditionWrapper();
        ERx_PageEntities.SectionModelWrapper sectionObj = new ERx_PageEntities.SectionModelWrapper();
        ERx_PageEntities.SelectOpWrap selectObj = new ERx_PageEntities.SelectOpWrap(con.id,'NameTest');
        ERx_PageEntities.AutoPopulateMappingWrapper autoPopulateObj = new ERx_PageEntities.AutoPopulateMappingWrapper();
        ERx_PageEntities.ConditionFieldObjectWrapper conditionFieldObj = new ERx_PageEntities.ConditionFieldObjectWrapper();
        ERx_PageEntities.ModelCriteriaConditionWrapper modelCriteriaConditionObj = new ERx_PageEntities.ModelCriteriaConditionWrapper();
        modelCriteriaConditionObj.modelDateConfigType='';
        ERx_PageEntities.TemplateConditionWrapper TemplateConditionWrapper = new ERx_PageEntities.TemplateConditionWrapper(new List<ERx_PageEntities.TemplateRenderedConditionWrapper>(),'');
        System.assertEquals('test', con.lastName);
        Test.stopTest();
    }
    
    static testMethod void testERx_PageEntities(){
        ERx_PageEntities.SectionModelWrapper section = new ERx_PageEntities.SectionModelWrapper();
        section.sectionTabIndexing = '';
        section.bgColorTitle='';
        section.bgColorDesc='';
        section.adminDescription='';
        section.sectionDateConfigModelList=new List<String>();
        ERx_PageEntities.PageWrapper PageWrapper = new ERx_PageEntities.PageWrapper();
        PageWrapper.pageNameDisplayed = '';
        ERx_PageEntities.PageHeaderWrapper PageHeaderWrapper = new ERx_PageEntities.PageHeaderWrapper();
        PageHeaderWrapper.browserTabTitle = '';
        ERx_PageEntities.FieldModelWrapper fieldWrapper = new ERx_PageEntities.FieldModelWrapper();
        fieldWrapper.uploadFileType='';
        fieldWrapper.sourceObjectFieldsDisplayType=new List<String>();
        fieldWrapper.createNewAppDirectURL='';
        fieldWrapper.columnLayout='';
        fieldWrapper.startDate='';
        fieldWrapper.endDate='';
        fieldWrapper.pageCompleteDisabledSettings=true;
        fieldWrapper.pageCompleteDiscriptiveText='';
        fieldWrapper.usedInFormulaFieldList=new List<String>();
        fieldWrapper.autoPopulatedExpression='';
        fieldWrapper.autoPopulatedFormula='';
        fieldWrapper.bgColorCode='';
        fieldWrapper.addAnotherButtonrRenderedExpression='';
        fieldWrapper.addAnotherButtonCountRenderedExpression='';
        fieldWrapper.isAddAnotherCountRendered=true;
        fieldWrapper.subSectionTabIndexing='';
        fieldWrapper.tabIndexValField=2;
        fieldWrapper.bgColorTitle='';
        fieldWrapper.multiappActionType='';
        fieldWrapper.visibleLines=4;
        fieldWrapper.fieldDateConfigModelList=new List<String>();
        fieldWrapper.dynamicColumnSize='';
        fieldWrapper.addAnotherCountCriteriaList=new List<ERx_PageEntities.RenderedRequiredConditionWrapper>();
        ERx_PageEntities.DependentModelWrapper dependentModelWrapperInstance = new ERx_PageEntities.DependentModelWrapper();
        dependentModelWrapperInstance.conditionExpression='';
        ERx_PageEntities.AutoPopulateExpressionWrapper autoPopulateExpressionWrapperInstance = new ERx_PageEntities.AutoPopulateExpressionWrapper();
        autoPopulateExpressionWrapperInstance.autoPopulatedExpression='';
        autoPopulateExpressionWrapperInstance.autoPopulatedFinalConditionExpression='';
        autoPopulateExpressionWrapperInstance.autoPopulatedFormula='';
        autoPopulateExpressionWrapperInstance.dateAutoPopulateType='';
        ERx_PageEntities.AddAnotherButtonWrapper addAnotherButtonWrapperInstance = new ERx_PageEntities.AddAnotherButtonWrapper();
        addAnotherButtonWrapperInstance.buttonLabel='';
        addAnotherButtonWrapperInstance.buttonPosition=1;
        ERx_PageEntities.PageErrorMessageWrapper pageErrorMessageWrapperInstance = new ERx_PageEntities.PageErrorMessageWrapper();
        pageErrorMessageWrapperInstance.errorMessage='';
        ERx_PageEntities.NotFoundFieldWrapper notFoundFieldWrapperInstance = new ERx_PageEntities.NotFoundFieldWrapper();
        notFoundFieldWrapperInstance.fieldAPIName='';
        notFoundFieldWrapperInstance.modelName='';
        ERx_PageEntities.PageCompleteWrapper pageCompleteWrapperInstance = new ERx_PageEntities.PageCompleteWrapper();
        pageCompleteWrapperInstance.pageCompleteDiscriptiveText='';
        pageCompleteWrapperInstance.errorMessage='';
        pageCompleteWrapperInstance.required=true;
        pageCompleteWrapperInstance.requiredSymbol='';
        ERx_PageEntities.PageButtonWrapper pageButtonWrapperInstance = new ERx_PageEntities.PageButtonWrapper();
        pageButtonWrapperInstance.errorMessage='';
        pageButtonWrapperInstance.enforcePageName=true;
        pageButtonWrapperInstance.droppingPosition=2;
        System.assertEquals(2,pageButtonWrapperInstance.droppingPosition);
    }
}