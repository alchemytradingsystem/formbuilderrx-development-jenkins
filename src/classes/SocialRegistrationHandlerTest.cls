/**
  
  @Author - Gourav Gandhi
  @date -29/5/18
**/
 
@isTest
public class SocialRegistrationHandlerTest {
    
    @testSetup
    static void datapreparation(){
    	Test_Erx_Data_Preparation.preparation();
    } 
    /**
     * Positive test for canCreateUser where all the required information is provided
     **/ 
    testmethod public static void testCanCreateUser() {
        SocialRegistrationHandler handler = new SocialRegistrationHandler();
        Auth.UserData data = createUser('test@example.com','John','Adams');
        
        System.Assert(handler.canCreateUser(data),'Handler should be able to create this user');
    }
    
    /**
     * Negative test for canCreateUser because insufficient detail is available
     **/     
    testmethod public static void testCanCreateUserNegative() {
        SocialRegistrationHandler handler = new SocialRegistrationHandler();
        Auth.UserData data = createUser(null,'Thomas','Jones-Drew');
        
        System.Assert(!handler.canCreateUser(data),'Handler should not be able to create user with missing email');
    }
    

    
    /**
     * Simple scenario to create a Community user
     **/ 
    testmethod public static void testCreateCommunityUser() {
        SocialRegistrationHandler handler = new SocialRegistrationHandler();
        
        Auth.UserData data = createUser('tjones@example.com','Thomas','Jones-Drew');
                
        Test.startTest();
        String theCommunityId = '00000001';
        data.attributeMap.put('sfdc_networkid',theCommunityId);
        
        User theUser = handler.createUser(null, data);
        
        Test.stopTest();
        
        validate(theUser,data);
        
        // Additional validations for Community User
        System.Assert(theUser.ContactId!=null,'Contact must be set for user');
        
    }
    
     testmethod public static void testCreateCommunityUserMissingEmail() {
        SocialRegistrationHandler handler = new SocialRegistrationHandler();
        
        Auth.UserData data = createUser(null,'Thomas','Jones-Drew');
                
        Test.startTest();
        String theCommunityId = '00000001';
        data.attributeMap.put('sfdc_networkid',theCommunityId);
        try{
        	User theUser = handler.createUser(null, data);
        }
        catch(exception e){
        	System.assert(e.getmessage().contains('missingEmail'));
        }
        Test.stopTest(); 
    }
    
    testmethod public static void testdataencoding() {
        SocialRegistrationHandler handler = new SocialRegistrationHandler();
        
        Auth.UserData data = createUser('tjones@example.com','Thomas','Jones-Drew');
                
        Test.startTest();
        handler.updateUser(null,null,data);
        String encodedstring = handler.dataEncoding(data);
        
        Test.stopTest();
        system.assert(encodedstring.contains('Thomas'));
    }
    
    /**
     * Helper method to Validate the the User we've created
     * 
     * @param theUser - the User that we created
     * @param data - the original AuthData 
     **/ 
    private static void validate(User theUser, Auth.UserData data) {
        System.Assert(theUser!=null,'User must not be null');
        System.AssertEquals(theUser.email,data.email,'Email address must be the same');
        System.AssertEquals(theUser.FirstName,data.FirstName,'First name must match');
        System.AssertEquals(theUser.LastName,data.LastName,'Last name must match');
    }

    
    /**
     * Helper method to instantiate the handler UserData
     * 
     * @param email
     * @param lastName
     * @param firstName
     * @return Auth.UserData that looks like what we expect 
     **/ 
    private static Auth.UserData createUser(String email,String lastName, String firstName) {
         Map<String, String> attributeMap = new Map<String,String>();
         String identifier = lastName+System.currentTimeMillis();
         String locale = 'en_US';
         return new Auth.UserData( identifier,
               firstName,
               lastName,
              '', // fullname
               email,
               '', // link
               '', // userName
               locale,
               '', // provider
               '', // siteLoginUrl
               attributeMap);       
    }
}