/********
FormBuilderRx
@company : Copyright © 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
********/
public with sharing class Portal_LoginController {

    public Portal_UserWrapper uWrap {get;set;}
    public Portal_Settings setting {get;set;}
    public Env__c cEnv {get;set;}
    public String urlParamMapString {get;set;}
    private Map<String, String> urlParamMap;
    //PD-2689
	public SocialMediaConfiguration_Wrapper[] socialWrapperList{get;set;}
	
	//PD-4253 | Shubham
    public String FaviconIcon{
    	get {
    		Env__c cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String imageLink = '';
    		String siteURL = Site.getPathPrefix();
	        if(cEnv != null){
	            if(String.isNotBlank(cEnv.Favicon_Icon__c) && !cEnv.Favicon_Icon__c.contains('null')){
	            	List<String> pathValues = cEnv.Favicon_Icon__c.split('##');
					if(pathValues != null && pathValues.size() > 0){
						if(pathValues.size() == 1){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]);	
						}
						else if(pathValues.size() == 2){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]) + PortalPackageUtility.escapeQuotes(pathValues[1]) ;
						}
					}
	            }
	        }
	        return imageLink;
    	}
    	set;
    }
	
	//PD-4103 | Priyamvada
    public String gtmScript{
    	get {
    		cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String script = '';
	        if(cEnv != null){
	            script = cEnv.GTM_Account_Id__c;
	        }
	        return script;
    	}
    	set;
    }
    
    public Portal_LoginController() {
    	socialWrapperList = new SocialMediaConfiguration_Wrapper[]{};
        cEnv = ERx_PortalPackUtil.getCurrentEnv();
        if(cEnv != null){
            uWrap = new Portal_UserWrapper(cEnv.Id);
            setting = new Portal_Settings(cEnv.Id);
            socialWrapperList = ERx_PortalPackUtil.socialMediaButtonConfiguration(cEnv);
        }
        if(String.isNotBlank(cEnv.Login_User_Configure_Text__c) && cEnv.Login_User_Configure_Text__c.contains('<pre>')){
        	cEnv.Login_User_Configure_Text__c= changeToHTML(cEnv.Login_User_Configure_Text__c);
        }
        if(String.isNotBlank(cEnv.Login_User_Configure_Text_Another__c) && cEnv.Login_User_Configure_Text_Another__c.contains('<pre>')){
        	cEnv.Login_User_Configure_Text_Another__c= changeToHTML(cEnv.Login_User_Configure_Text_Another__c);
        }
        urlParamMap = ApexPages.currentPage().getParameters();
        urlParamMapString = POrtalPackageHelper.createParamString(urlParamMap);
    }
    
    //Function to change string into html for rich text field
    //PD-1812 changes.
    public String changeToHTML(String str){
    	str = str.replace('&quot;','\"').replace('&lt;','<').replace('&gt;','>');
    	return str;
    }

    public PageReference login(){
    	urlParamMap = ApexPages.currentPage().getParameters();
        urlParamMapString = POrtalPackageHelper.createParamString(urlParamMap);
        if(uWrap != null && !uWrap.isValidInputLogin()){
            //field missing.
            return Portal_LoginUtil.addErrorMessage(setting.Login_Field_Missing_Message);
            
        }

        if(uWrap != null && uWrap.existingUser() == null){
            //if the user does not exist
            return Portal_LoginUtil.addErrorMessage(setting.Login_Incorrect_Username_Password_Messag);
        }else{
            if(setting!=null && (setting.Student_Portal_User_Profile == null || setting.Student_Portal_User_Profile == '')){
                return Portal_LoginUtil.addErrorMessage('No Student Portal User Id, Please Check Custom Setting.'+cEnv.Id);
            } 
            
            // Manish: Execute below code for real user only not for test class
            // Because it gives null reference error
            if(!Test.isRunningTest()){
            	//if user exists but not portal user
           		if(String.valueOf(uWrap.existingUser().ProfileId).substring(0,15) != String.valueOf(setting.Student_Portal_User_Profile).substring(0,15)){
               		return Portal_LoginUtil.addErrorMessage(setting.Login_Non_Portal_User_Message);
           		}	
            }
        }

        //reach here, no error, ready to login.
        //String startUrl = System.currentPageReference().getParameters().get('startURL');
        PageReference pr = Site.login(uWrap.email, uWrap.password,'');
        return pr;
    }
}