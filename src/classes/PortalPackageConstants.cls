/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Portal Package Constants
*/ 
public with sharing class PortalPackageConstants {
    /****
    * @description page upsert deletion error message
    */
    public final static string PAGE_UPSERTION_DELETION_ERROR_MESSAGE = 'Page can not be inserted or deleted from the live Env';
    /****
    * @description section upsert deletion error message
    */
    public final static string SECTION_UPSERTION_DELETION_ERROR_MESSAGE = 'Section can not be inserted or deleted from the live Env';
    /****
    * @description template upsert deletion error message
    */
    public final static string TEMPLATE_UPSERTION_DELETION_ERROR_MESSAGE = 'Template can not be inserted or deleted from the live Env';
    /****
    * @description homepage widget layout upsert deletion error message
    */
    public final static string HOMEPAGE_WIDGET_LAYOUT_UPSERTION_DELETION_ERROR_MESSAGE = 'Homepage Widget Layout can not be inserted or deleted from the live Env';
     /****
    * @description homepage widget upsert deletion error message
    */
    public final static string CHECKLIST_UPLOAD_CONFIGURATION_UPSERTION_ERROR_MESSAGE = 'Checklist upload configuration can not be updated from the live Env';
     /****
    * @description homepage widget upsert deletion error message
    */
    public final static string HOMEPAGE_WIDGET_UPSERTION_DELETION_ERROR_MESSAGE = 'Homepage Widget can not be inserted or deleted from the live Env';
    /****
    * @description homepage widget upsert deletion error message
    */
    public final static string PORTAL_LOGIN_CUSTOM_FIELD_UPSERTION_DELETION_ERROR_MESSAGE = 'Portal Login Custom Field can not be inserted or deleted from the live Env';
     /****
    * @description Social upsert deletion error message
    * PD-2689
    */
    public final static string PORTAL_SOCIAL_LOGIN_UPSERTION_DELETION_ERROR_MESSAGE = 'Portal Social Media Configuration can not be inserted or deleted from the live Env';
    /****
    * @description template upsert deletion error message
    */
     public final static string REGISTRATION_MESSAGE_UPSERTION_DELETION_ERROR_MESSAGE = 'Registration Messages can not be Updated in live Env';
    /****
    * @description template upsert deletion error message
    */
    public final static string TEMPLATE_ASSIGNMENT_ERROR_MESSAGE = 'Template can not be assigned to the live Env';
    /****
    * @description section env deletion error message
    */
    public final static string ENV_DELETION_ERROR_MESSAGE = 'You can not delete live Env';
    
    /****
    * @description section env deletion error message
    */
    public final static string ENV_UPDATION_ERROR_MESSAGE = 'You can not change in live Env';
    
    /****
    * @description equals operator constant
    */
    public final static String EQUALS = ' = ';
    
    /****
    * @description Operators that are used in Condition.
    */
    public static final List<String> OPERATOR_LIST = new List<String>{'=', '!=', '>', '>=', '<', '<=', 'Contains'}; // 
    /****
    * @description renderd operator list
    */
    public static final List<String> RENDERED_OPERATOR_LIST = new List<String>{'=', '!=', '>', '>=', '<', '<='};
    /****
    * @description invalid page id error message
    */
    public static final String INVALID_PAGE_ID_ERROR_MESSAGE = 'Invalid Page Id.'; // Invalid Page Id Error Message.
    /****
    * @description invalid page error message
    */
    public static final String INVALID_PAGE_ERROR_MESSAGE = 'Invalid Page.'; // Invalid Page Error Message.
    /****
    * @description some thing went wrong error message
    */
    public static final String SOME_THING_WENT_WRONG_ERROR_MESSAGE = 'Some Thing Went Wrong. Please Try After Some Time.'; // Exception Message while saving the data.
    
    /****
    * @description Field instruction position dropdown values.
    */
    public static final List<String> FIELD_INSTRUCTION_POSITIONS = new List<String>{'Above Field', 'Above Label', 'Below Field', 'Below Label'};
    
    /****
    * @description model type section
    */
    public static final String SECTION_MODEL = 'SECTIONMODEL'; //model type of section
    /****
    * @description model type field
    */
    public static final String FIELD_MODEL = 'FIELDMODEL'; 
    
    /****
    * @description Upload source dropdown values.
    */
    public static final String GLOBAL_VARIABLE = 'Global Variable';
    /****
    * @description Upload source dropdown values.
    */
    public static final String MODEL = 'Model';
    /****
    * @description Upload source dropdown values.
    */
    public static final String DATASET = 'Data Set';
    /****
    * @description Upload source dropdown values.
    */
    public static final String PARAM = 'Param';
    /****
    * @description Upload source dropdown values.
    */
    public static final String STATIC_CONS = 'Static';
     /****
    * @description Dynamic date configuration
    */
    public static final String DYNAMIC_CONS = 'Dynamic Date';
    
    /****
    * @description Upload source list
    */
    public static final List<String> UPLOAD_SOURCE_LIST = new List<String>{MODEL, PARAM, STATIC_CONS, DYNAMIC_CONS};
    
    /****
    * @description Upload source map
    */
    public static final Map<String, String> UPLOAD_SOURCE_MAP = new Map<String, String>{MODEL=> DATASET, PARAM=> PARAM, STATIC_CONS=> STATIC_CONS, DYNAMIC_CONS=> DYNAMIC_CONS};
    
    /****
    * @description criteria condition source list
    */
    public static final List<String> CRITERIA_CONDITION_SOURCE_LIST = new List<String>{MODEL, PARAM, STATIC_CONS, GLOBAL_VARIABLE, DYNAMIC_CONS};
    
    /****
    * @description criteria condition source map
    */ 
    public static final Map<String, String> CRITERIA_CONDITION_SOURCE_MAP = new Map<String, String>{MODEL=> DATASET, PARAM=> PARAM, STATIC_CONS=> STATIC_CONS, GLOBAL_VARIABLE=> GLOBAL_VARIABLE, DYNAMIC_CONS=> DYNAMIC_CONS};
    
    /****
     *  @description File Size Data Units Constants
     *  public static final Map<String, Long> FILE_SIZE_DATA_LIMITS = new Map<String, Long>{'1MB'=> (1*1024*1024), '2MB'=> (2*1024*1024), '3MB'=> (3*1024*1024), '4MB'=> (4*1024*1024), '5MB'=> (5*1024*1024)};
     *  Note: Creating Map in Reverse order due to known issue of JSON.serialize with Map for Reference https://success.salesforce.com/issues_view?id=a1p300000008Z1fAAE 
    */
    public static final Map<String, Long> FILE_SIZE_DATA_LIMITS = new Map<String, Long>{'4MB'=> (4*1024*1024), '3MB'=> (3*1024*1024), '2MB'=> (2*1024*1024), '1MB'=> (1*1024*1024)};
    
    /****
    * @description File Supported Types in Upload Constants
    */
    public static final List<String> UPLOAD_SUPPORTED_FILES = new List<String>{'Pdf', 'Doc', 'Jpg', 'Gif', 'Png', 'Txt'};
    
    /****
    * @description File Upload Types in Upload Constants
    */
    
    public static final List<String> UPLOAD_TYPES = new List<String>{'Attachment', 'File'};
    
    /****
    * @description Field Type (Change Reference Field Type)
    */
    public static final List<String> REFERENCE_FIELD_TYPE = new List<String>{'Reference', 'Type Ahead', 'Picklist'};
    
    /****
    * @description Field Type (Change Reference Field Type)
    */
    public static final Map<String, String> REFERENCE_FIELD_TYPE_MAP = new Map<String, String>{'Reference' => 'Reference / Type Ahead', 'Picklist' => 'Picklist'};
    
    /****
    * @description Field Type (Change Reference Field Type)
    */
    public static final List<String> BUTTON_NAVIGATION_TYPE = new List<String>{'Previous page', 'Next page', 'Save only','Submit'};
    
    /****
    * @description Field Type (Change Reference Field Type)
    */
    
    /* 17-5-18 PD-3404 : Picklist as Radio Button STARTS */
    public static final List<String> CHECKBOX_FIELD_TYPE = new List<String>{'Boolean', 'Picklist', 'Radio Button'};
    
    public static final List<String> PICKLIST_FIELD_TYPE = new List<String>{'Picklist', 'Radio Button'};
    /* 17-5-18 PD-3404 : Picklist as Radio Button ENDS */
      
    /**-- added by Sumitra */
    /****
    * @description Field Type (Change Multi-pickList Field Type)
    */
    public static final List<String> MULTIPICKLIST_FIELD_TYPE = new List<String>{'MultiPicklist', 'Checkbox'};
    /****
    * @description Sort Order Map
    */
    public static final Map<String, String> SORT_ORDER_MAP = new Map<String, String>{'desc'=> 'Descending', 'asc'=> 'Ascending'};
    /****
    * @description RICH TEXT AREA TYPE
    */
    public static final String RICH_TEXT_AREA = 'RICHTEXT';
    
    /****
    * @description Required symbol list
    */
    public static final List<String> REQUIRED_SYMBOL_LIST = new List<String>{'*', '|'};
    
    
    //PD-3532 || Upload type Constants added
    public static final Map<String, String> UPLOAD_TYPES_HOMEPAGE = new Map<String, String>{'Attachment'=>'Attachment', 'File'=>'File'};
    
    /****
    * @description operator used for rendered condition
    */
    public static final String EQUALS_TO = 'equals';
    /****
    * @description operator used for rendered condition
    */
    public static final String NOT_EQUALS_TO = 'not equal to';
    /****
    * @description operator used for rendered condition
    */
    public static final String CONTAINS = 'contains';
    /****
    * @description operator used for rendered condition
    */
    public static final String DOES_NOT_CONTAINS = 'does not contain';
    /****
    * @description operator used for rendered condition
    */
    public static final String STARTS_WITH = 'starts with';
    /****
    * @description operator used for rendered condition
    */
    public static final String ENDS_WITH = 'ends with';
    /****
    * @description operator used for rendered condition
    */
    public static final String INCLUDES = 'includes'; 
    /****
    * @description operator used for rendered condition
    */
    public static final String EXCLUDES = 'excludes'; 
    /****
    * @description operator used for rendered condition
    */
    public static final String LESS_THAN = 'less than';
    /****
    * @description operator used for rendered condition
    */
    public static final String GREATER_THAN = 'greater than';
    /****
    * @description operator used for rendered condition
    */
    public static final String LESS_THAN_EQUALS = 'less or equal';
    /****
    * @description operator used for rendered condition
    */
    public static final String GREATER_THAN_EQUALS = 'greater or equal';
    
    
    /****
    * @description type ahead type constant
    */
    public static final String TYPE_AHEAD_FLD = 'TYPE AHEAD';
    /****
    * @description LOOKUP type constant
    */
    public static final String LOOKUP_FLD = 'LOOKUP';
    /****
    * @description ID type constant
    */
    public static final String IDFIELD = 'ID';
    /****
    * @description Picklist type constant
    */
    public static final String PICKLISTFLD = 'PICKLIST';
    /****
    * @description date type constant
    */
    public static final String DATEFLD = 'DATE';
    /****
    * @description datetime type constant
    */
    public static final String DATETIMEFLD = 'DATETIME';
    /****
    * @description reference type constant
    */
    public static final String REFERENCEFLD = 'REFERENCE';
    /****
    * @description id type constant
    */
    public static final String IDFLD = 'ID';
    /****
    * @description string type constant
    */
    public static final String TEXTFLD = 'STRING';
    /****
    * @description textarea type constant
    */
    public static final String TEXTAREAFLD = 'TEXTAREA';
    /****
    * @description double type constant
    */
    public static final String DOUBLEFLD = 'DOUBLE';
    /****
    * @description number type constant
    */
    public static final String NUMBERFLD = 'NUMBER';
    /****
    * @description currency type constant
    */
    public static final String CURRENCYFLD = 'CURRENCY';
    /****
    * @description checkbox type constant
    */
    public static final String CHECKBOXFLD = 'BOOLEAN'; 
    /****
    * @description email type constant
    */
    public static final String EMAILFLD = 'EMAIL'; 
    /****
    * @description multipicklist type constant
    */
    public static final String MULTIPICKLISTFLD = 'MULTIPICKLIST'; 
    /****
    * @description percent type constant
    */
    public static final String PERCENTFLD = 'PERCENT'; 
    /****
    * @description time type constant
    */
    public static final String TIMEFLD = 'TIME'; 
    /****
    * @description url type constant
    */
    public static final String URLFLD = 'URL'; 
    /****
    * @description phone type constant
    */
    public static final String PHONEFLD = 'PHONE';
    /****
    * @description encrypted type constant
    */
    public static final String ENCRYPTEDSTRINGFLD = 'ENCRYPTEDSTRING';
    /****
    * @description upload type constant
    */
    public static final String UPLOADFLD = 'UPLOAD';
    /****
    * @description integer type constant
    */
    public static final String INTEGERFLD = 'INTEGER';
    
    /****
    * @description map of type and operator list
    */
    public static final Map<String, List<String>> TYPE_OPERATOR_LIST_MAP = new Map<String, List<String>> {IDFIELD => new List<String>{EQUALS_TO, NOT_EQUALS_TO,STARTS_WITH},
    TEXTFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, CONTAINS, DOES_NOT_CONTAINS, STARTS_WITH, ENDS_WITH}, 
    PICKLISTFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, CONTAINS, DOES_NOT_CONTAINS, STARTS_WITH, ENDS_WITH,LESS_THAN}, 
    MULTIPICKLISTFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO,INCLUDES, EXCLUDES}, 
    DATEFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS, GREATER_THAN_EQUALS},
    DATETIMEFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS,GREATER_THAN_EQUALS}, 
    CHECKBOXFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO},
    REFERENCEFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO}, 
    NUMBERFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, CONTAINS, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS,GREATER_THAN_EQUALS}, 
    INTEGERFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, CONTAINS, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS,GREATER_THAN_EQUALS},
    EMAILFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, CONTAINS, DOES_NOT_CONTAINS, STARTS_WITH}, 
    PHONEFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO,CONTAINS, DOES_NOT_CONTAINS, STARTS_WITH,LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS, GREATER_THAN_EQUALS}, 
    DOUBLEFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS, GREATER_THAN_EQUALS}, 
    CURRENCYFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS, GREATER_THAN_EQUALS},
    URLFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, CONTAINS, DOES_NOT_CONTAINS, STARTS_WITH},
    PERCENTFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO, LESS_THAN, GREATER_THAN, LESS_THAN_EQUALS,GREATER_THAN_EQUALS},
    UPLOADFLD => new List<String>{EQUALS_TO, NOT_EQUALS_TO}};
    /****
    * @description string type list
    */
    public static final Set<String> STRING_TYPE_FIELDS = new Set<String>{PortalPackageConstants.PICKLISTFLD, PortalPackageConstants.REFERENCEFLD, PortalPackageConstants.IDFLD, PortalPackageConstants.TEXTFLD, PortalPackageConstants.TEXTAREAFLD, PortalPackageConstants.EMAILFLD, PortalPackageConstants.URLFLD, PortalPackageConstants.PHONEFLD, PortalPackageConstants.ENCRYPTEDSTRINGFLD};
    /****
    * @description number type list
    */
    public static final Set<String> NUMBER_TYPE_FIELDS = new Set<String>{PortalPackageConstants.DATEFLD, PortalPackageConstants.DATETIMEFLD, PortalPackageConstants.DOUBLEFLD, PortalPackageConstants.NUMBERFLD, PortalPackageConstants.CURRENCYFLD, PortalPackageConstants.EMAILFLD, PortalPackageConstants.PERCENTFLD, PortalPackageConstants.TIMEFLD};
    /****
    * @description number operators
    */
    public static final Set<String> NUMBER_OPERATORS = new Set<String>{PortalPackageConstants.EQUALS_TO, PortalPackageConstants.NOT_EQUALS_TO, PortalPackageConstants.LESS_THAN, PortalPackageConstants.GREATER_THAN, PortalPackageConstants.LESS_THAN_EQUALS, PortalPackageConstants.GREATER_THAN_EQUALS};
    /****
    * @description number operators map
    */
    public static final Map<String, String> NUMBER_OPERATOR_MAP = new Map<String, String>{PortalPackageConstants.EQUALS_TO => ' = ', PortalPackageConstants.NOT_EQUALS_TO => ' != ', PortalPackageConstants.LESS_THAN => ' < ', PortalPackageConstants.GREATER_THAN => ' > ', PortalPackageConstants.LESS_THAN_EQUALS => ' <= ',PortalPackageConstants.GREATER_THAN_EQUALS => ' >= ', PortalPackageConstants.INCLUDES => ' INCLUDES ', PortalPackageConstants.EXCLUDES => ' EXCLUDES '};    
    //public static final Set<String> STRING_OPERATORS = new Set<String>{PortalPackageConstants.CONTAINS, PortalPackageConstants.DOES_NOT_CONTAINS, PortalPackageConstants.STARTS_WITH, PortalPackageConstants.ENDS_WITH};
    /****
    * @description LIKE operator constant
    */
    public static final String LIKE_OPERATOR = ' LIKE ';
    /****
    * @description NOT operator constant
    */
    public static final string NOT_OPERATOR = ' NOT ';
    /****
    * @description AND operator constant
    */
    public static final string AND_OPERATOR = ' AND ';
    /****
    * @description OR operator constant
    */
    public static final string OR_OPERATOR = ' OR ';
    
    /****
    * @description CURRENTUSERID constant
    */
    public static final String CURRENTUSERID = 'CurrentUserId'; 
    /****
    * @description CURRENTCONTACTID constant
    */
    public static final String CURRENTCONTACTID = 'CurrentContactId';   
    /****
    * @description CURRENTPROFILEID constant
    */
    public static final String CURRENTPROFILEID = 'CurrentProfileId';   
    /****
    * @description CURRENTCOMMUNITYID constant
    */
    public static final String CURRENTCOMMUNITYID = 'CurrentCommunityId';
    /****
    * @description CONTACTOBJ constant
    */
    public static final String CONTACTOBJ = 'Contact';
    /****
    * @description USEROBJ constant
    */
    public static final String USEROBJ = 'User';
    /****
    * @description PROFILEOBJCONSTANT constant
    */
    public static final String PROFILEOBJ = 'Profile';
    /****
    * @description NETWORKOBJ constant
    */
    public static final String NETWORKOBJ = 'Network';  
    /****
    * @description ERROR MSG FOR NON PAID USER
    */
    public static final String NON_PAID_USER_RESTRICTED = 'Please contact EnrollmentRX to become a paid user.'; 
    /****
    * @description global variable list
    */
    public static final List<String> GLOBAL_VAR = new List<String>{CURRENTUSERID, CURRENTCONTACTID, CURRENTPROFILEID, CURRENTCOMMUNITYID};
    /****
    * @description global variable map
    */
    public static final Map<String, String> GLOBAL_VAR_MAP = new Map<String, String>{CURRENTUSERID => USEROBJ, CURRENTCONTACTID => CONTACTOBJ, CURRENTPROFILEID => PROFILEOBJ, CURRENTCOMMUNITYID => NETWORKOBJ};
    
    // Auto Populate Field Property #689 Starts
    /**********
    * @description Auto Populate Plus Operator
    */
    public static final String PLUS = '+';
    
    /**********
    * @description Auto Populate Minus Operator
    */
    public static final String MINUS = '-';
    
    /**********
    * @description Auto Populate Multiply Operator
    */
    public static final String MULTIPLY = '*';
    
    /**********
    * @description Auto Populate Divide Operator
    */
    public static final String DIVIDE = '/';
    
    /**********
    * @description Auto Populate Plus Operator Text
    */
    public static final String PLUS_TEXT = 'Add';
    
    /**********
    * @description Auto Populate Minus Operator Text
    */
    public static final String MINUS_TEXT = 'Subtract';
    
    /**********
    * @description Auto Populate Multiply Operator Text
    */
    public static final String MULTIPLY_TEXT = 'Multiply';
    
    /**********
    * @description Auto Populate Divide Operator Text
    */
    public static final String DIVIDE_TEXT = 'Divide';
    
    
    /**********
    * @description Auto Populate Operator List
    */
    public static final List<String> AUTO_PUPULATE_OPERATOR_LIST = new List<String>{PLUS, MINUS, MULTIPLY, DIVIDE};
    
    /**********
    * @description Auto Populate Operator List
    */
    public static final List<String> AUTO_PUPULATE_OPERATOR_TEXT_LIST = new List<String>{PLUS_TEXT, MINUS_TEXT, MULTIPLY_TEXT, DIVIDE_TEXT};
    
    /**********
    * @description Auto Populate Operator Map
    */
    public static final Map<String, String> AUTO_POPULATE_FORMULA_OPERATOR_MAP = new Map<String, String>{PLUS => PLUS_TEXT, MINUS => MINUS_TEXT, MULTIPLY => MULTIPLY_TEXT, DIVIDE => DIVIDE_TEXT};
    
    /******
    * @description Auto populate Constant
    */
    public static final String AUTOPOPULATE = 'AUTOPOPULATE';
    // Auto Populate Field Property #689 Ends
    
    public static final Set<String> SYSTEM_FIELD_TYPES = new set<String>{'NAME'};
     
    /****
    * @description insuffiecient access error message
    */
    public final static string PERMISSION_ERROR_MESSAGE = 'Insufficient Privileges. Please contact your administrator to grant object/field level permissions';
    
    public static final String COMMUNITY_DISABLE_MESSAGE = 'Communities disabled. Please enable community.';
    /**** 
    * @description message for when event package is not installed.
    */
    public static final String EVENT_NOT_SUPPORTED = 'Event Listing Functionality is not supported.';
    
    /****
    * @description Re:PD-2018 DML message substring for Portal Package Loggers
    */
    public static final String LOGGER_OBJECT = 'id';
    
    /****
    * @description Re:PD-1492 Set of packaged objects
    */
    public static final Set<String> PACKAGE_OBJECT_SET = new Set<String>{'Env__c', 'Erx_Page__c', 'Erx_Section__c', 'Homepage_Widget_Layout__c', 'Homepage_Widget__c', 
    													'Portal_Login_Custom_Field__c', 'Portal_Package_Logger__c', 'Portal_Registration_Message__c', 'SiteLoginTemplate__c'};
    
    //Added Re Kavita:PD-2137 DUPLICATE LEAD MESSAGE for Existing Lead without Contact
    public static final String DUPLICATE_LEAD_MESSAGE = 'Email ID already exists, please use different email ID to register.';
    
    //Message to show when registeration message not setup.
    public static final String LEAD_CONFIG_ERROR_MESSAGE = 'Contact your system admin to configure lead configuration.';
    
    public static final Pattern CURLIES_WORD_STRING = Pattern.compile('\\{\\{(.*?)\\}\\}');
    
    public static final String VALUE_FROM_ADD_ANOTHER_MAP_INDEX = 'VALUE_FROM_ADD_ANOTHER_MAP_INDEX';
}