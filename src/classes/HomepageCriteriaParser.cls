public 	class HomepageCriteriaParser {
	public Map<String, String> mapWhereClause;
	public Set<String> customFields;
	public Map<String,List<HomepageCriteria>> Object_CriteriaList;
	public Map<String,Boolean> Order_FilterLogic;

	public sObject contact;//current contact
	public sObject application;//active appliation

	public List<sObject> sobjList;//store processed sobjList
	public List<String> errorMessageList;
	
	public Map<String, Set<String>> referenceObjectFieldMap; 

	public String namespacePrefix;
	public static Map<String, Schema.SobjectType> schemaMap = Schema.getGlobalDescribe();

	public HomepageCriteriaParser(List<sObject> sobjList, sObject contact, sObject application, Map<String, Set<String>> referenceObjectFieldMap) {
		//init variables
		namespacePrefix=ngForceController.namespacePrefix;
		mapWhereClause = new Map<String, String>();
		customFields = new Set<String>();

		this.contact=contact;
		this.application=application;
		this.referenceObjectFieldMap = referenceObjectFieldMap;
		this.sobjList=new List<sObject>();
		this.errorMessageList=new List<String>();

		Object_CriteriaList=new Map<String,List<HomepageCriteria>>();
		Order_FilterLogic=new Map<String,Boolean>();

		system.debug('### sobjList: '+sobjList);
		processsobjList(sobjList);

	}

	private void processsobjList(List<sObject> sobjList){

			system.debug('### processsobjList sobjList: '+sobjList);

		for(sObject sobj:sobjList){
			//add "Default" to sobjList
			if((Boolean)sobj.get(namespacePrefix+'Default__c')){
				this.sobjList.add(sobj);
				continue;
			}

			system.debug('### sobj.get(Name): '+sobj.get('Name'));
			system.debug('### sobj.get(namespacePrefix+Criteria__c): '+sobj.get(namespacePrefix+'Criteria__c'));

			if(!String.isBlank((String)sobj.get(namespacePrefix+'Criteria__c'))){
				list<HomepageCriteria> criteriaList=(list<HomepageCriteria>)JSON.deserialize((String)sobj.get(namespacePrefix+'Criteria__c'),list<HomepageCriteria>.class);
				system.debug('### criteriaList: '+criteriaList);

				Object_CriteriaList.clear();// clear map for each sobj

				//construct  Object_CriteriaList Map
				for(HomepageCriteria c:criteriaList){
					if(!String.isBlank(c.referenceObjectName) && !String.isBlank(c.referenceFieldName)){//referenceObjectName has higher priority

						if((c.objectName == 'EnrollmentrxRx__Enrollment_Opportunity__c' || c.referenceObjectName == 'EnrollmentrxRx__Enrollment_Opportunity__c') && !ngForceController.hasApplicationObject)
						continue;

						String mergedName=c.objectName+c.fieldName+c.referenceObjectName;
						if(!Object_CriteriaList.containsKey(mergedName)){
							Object_CriteriaList.put(mergedName,new List<HomepageCriteria>());
							Object_CriteriaList.get(mergedName).add(c);
						}
						else
						Object_CriteriaList.get(mergedName).add(c);
					}
					else if(!String.isBlank(c.objectName)){
						if(c.objectName == 'EnrollmentrxRx__Enrollment_Opportunity__c' && !ngForceController.hasApplicationObject)
						continue;

						if(!Object_CriteriaList.containsKey(c.objectName)){
							Object_CriteriaList.put(c.objectName,new List<HomepageCriteria>());
							Object_CriteriaList.get(c.objectName).add(c);
						}
						else
						Object_CriteriaList.get(c.objectName).add(c);
					}
				}

				//construct  Order_FilterLogic Map for each sobj
				Order_FilterLogic.clear();// clear map for new each sobj
				Map<String,Boolean> queriedMap = new Map<String,Boolean>();

				system.debug('### Object_CriteriaList '+Object_CriteriaList);

				for(List<HomepageCriteria> cs:Object_CriteriaList.values()){
					for(HomepageCriteria c:cs){
						if(!String.isBlank(c.referenceObjectName) && !String.isBlank(c.referenceFieldName)){
							String mergedName=c.objectName+c.fieldName+c.referenceObjectName;
							if(queriedMap.get(mergedName)==null){
								queriedMap.put(mergedName,true);
								constructOrderFilterLogicMap(c.objectName, c.fieldName, c.referenceObjectName, c.referenceFieldName, sobj);//lookup field
							}
						}
						else if(!String.isBlank(c.objectName)){
							if(queriedMap.get(c.objectName)==null){
								queriedMap.put(c.objectName,true);
								constructOrderFilterLogicMap(c.objectName, null, null, null, null);//none lookup field
							}
						}
					}
				}
				//re-constructed Criteria_Filter_Logic__c field value
				String fieldName=namespacePrefix+'Criteria_Filter_Logic__c';//Criteria_Filter_Logic__c field name

				if(!String.isBlank(String.valueOf(sobj.get(fieldName)))){
					sobj.put(fieldName,String.valueOf(sobj.get(fieldName)).toLowerCase());
					List<String> orderList=getOrderList(String.valueOf(sobj.get(fieldName)));
					for(String order:orderList){
						if(Order_FilterLogic.get(order)!=null)
						sobj.put(fieldName,String.valueOf(sobj.get(fieldName)).replace(order,String.valueOf(Order_FilterLogic.get(order))));
					}
					sobj.put(fieldName,String.valueOf(sobj.get(fieldName)).replace('or','||'));
					sobj.put(fieldName,String.valueOf(sobj.get(fieldName)).replace('and','&&'));
					sobj.put(fieldName,String.valueOf(sobj.get(fieldName)).replace('not','!'));
					this.sobjList.add(sobj);
				}
				else{//default to all AND
					sobj.put(fieldName,'');
					system.debug('### Order_FilterLogic: '+Order_FilterLogic);
					List<String> orderList=new List<String>();
					orderList.addAll(Order_FilterLogic.keySet());
					orderList.sort();

					for(String order:orderList){
						sobj.put(fieldName,String.valueOf(sobj.get(fieldName)) + String.valueOf(Order_FilterLogic.get(order))+' && ');
					}
					if(!String.isBlank(String.valueOf(sobj.get(fieldName))))
					sobj.put(fieldName,String.valueOf(sobj.get(fieldName)).substring(0,String.valueOf(sobj.get(fieldName)).length() - 4));
					this.sobjList.add(sobj);

					system.debug('### String.valueOf(sobj.get(fieldName)): '+ String.valueOf(sobj.get(fieldName)));
				}
			}
		}
	}

	private void constructOrderFilterLogicMap(String objectName,String fieldName, String referenceObjectName, String referenceFieldName, SObject sourceSobj){
		mapWhereClause.clear();
		customFields.clear();
		SObject sobj;

		String sobjectId;//for creating error message

		if(!String.isBlank(referenceObjectName) && !String.isBlank(referenceFieldName)){

			system.debug('### fieldName: '+fieldName);
			//system.debug('### this.contact.get(fieldName): '+this.contact.get(fieldName));

			if(objectName=='Contact'){
				mapWhereClause.put('Id=', (String)this.contact.get(fieldName));
				sobjectId=(String)this.contact.get('Id');
			}
			else if(objectName=='EnrollmentrxRx__Enrollment_Opportunity__c'){
				mapWhereClause.put('Id=', (String)this.application.get(fieldName));
				sobjectId=(String)this.application.get('Id');
			}

			//validate Id for lookupfield,must be not null/''
 			if(!String.isBlank(mapWhereClause.get('Id='))){
 				String mergedName=objectName+fieldName+referenceObjectName;
 				if(this.referenceObjectFieldMap != null && this.referenceObjectFieldMap.containsKey(mergedName)) {
 					sobj=CommunityUtility.getObjectWithFields(referenceObjectName, mapWhereClause, referenceObjectFieldMap.get(mergedName))[0];
 				} else {
					sobj=CommunityUtility.getObjectWithFields(referenceObjectName, mapWhereClause, null,null, customFields, RequiredFieldTypes.ALL_FIELDS_WITH_CUSTOM_FIELDS)[0];
 				}
				
				for(HomepageCriteria c:Object_CriteriaList.get(mergedName)){
					if(!String.isBlank(c.referenceObjectName)) {
						fillOrderFilterLogicMap(c.order,c.referenceFieldName,c.operator,c.value,sobj);
					}
				}
			}
			//else create errorMessage
			else{
				// Case : Lookup field value is blank then treating condition as false. 
				//String errorMessage='Invalid criteria on <span style="color:blue">'+sourceSobj.getSObjectType().getDescribe().getName()+'</span> {Name : '+(String)sourceSobj.get('Name') + ', Id : ' + (String)sourceSobj.get('Id')+'}<br/>Field Missing [<span style="color:red">'+fieldName+'</span>] on : <span style="color:blue">'+ objectName + '</span> ('+sobjectId+')<br/>';
				//errorMessageList.add(errorMessage);
				String mergedName=objectName+fieldName+referenceObjectName;
				for(HomepageCriteria c:Object_CriteriaList.get(mergedName)){
					if(!String.isBlank(c.referenceObjectName)) {
						fillOrderFilterLogicMap(c.order,c.referenceFieldName,c.operator,c.value,null);
					}
				}
			}
		}
		else{
			if(objectName=='Contact')
			sobj=this.contact;
			else if(objectName=='EnrollmentrxRx__Enrollment_Opportunity__c')
			sobj=this.application;

			System.debug('### Object_CriteriaList.get(objectName): '+Object_CriteriaList.get(objectName));
			System.debug('### Object_CriteriaList.get(objectName).size(): '+Object_CriteriaList.get(objectName).size());

			for(HomepageCriteria c:Object_CriteriaList.get(objectName)){
				System.debug('### c: '+c);
				if(!String.isBlank(c.objectName))
				fillOrderFilterLogicMap(c.order,c.fieldName,c.operator,c.value,sobj);
			}
		}
	}

	private void fillOrderFilterLogicMap(String order, String fieldName,String operator,String value,SObject sobj){
		if(sobj == null) {
			Order_FilterLogic.put(order, false);
		} else {
			if(sobj.get(fieldName) == null){
				//changes done for Bug - PD-2192
				// Exception occured due to illegal assignment
				try{
					sobj.put(fieldName,'');	
				}catch(SObjectException sObjectEx){
					// Checking for Formula Field and Merged field (Address)
					if(!sObjectEx.getMessage().contains(fieldName + ' is not editable') && !sObjectEx.getMessage().contains('Invalid field ' + fieldName)) {
						sobj.put(fieldName,null);
					}
				}
			}
			
	
			if(value == null) {
				value='';
			}
	
			if(operator=='equals'){
				// if(String.isBlank(String.valueOf(sobj.get(fieldName)))==true && String.isBlank(value)==true)
				// Order_FilterLogic.put(order,true);
				// else
				// Fix PD-2417
				if(String.isBlank(value)) {
					Order_FilterLogic.put(order,String.isBlank(String.valueOf(sobj.get(fieldName))));
				} else {
					Order_FilterLogic.put(order,String.valueOf(sobj.get(fieldName))==value);
				}
			}
			else if(operator=='not equal to'){
				// if(String.isBlank(String.valueOf(sobj.get(fieldName)))==true && String.isBlank(value)==true)
				// Order_FilterLogic.put(order,false);
				// else
				// Fix PD-2417
				if(String.isBlank(value)) {
					Order_FilterLogic.put(order,String.isNotBlank(String.valueOf(sobj.get(fieldName))));
				} else {
					Order_FilterLogic.put(order,String.valueOf(sobj.get(fieldName))!=value);
				}
			}
			else if(operator=='starts with'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,String.valueOf(sobj.get(fieldName)).startsWith(value));
				}
			}
			else if(operator=='ends with'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,String.valueOf(sobj.get(fieldName)).endsWith(value));
				}
			}
			else if(operator=='contains'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,String.valueOf(sobj.get(fieldName)).contains(value));
				}
			}
			else if(operator=='does not contain'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,!String.valueOf(sobj.get(fieldName)).contains(value));
				}
			}
			//PD-2787 Adding inequality operators for numeric fields
			else if(operator=='greater than'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(String.isNotBlank(value) && sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,Decimal.valueOf(String.valueof(sobj.get(fieldName)))>Decimal.valueof(value));
				}
			}
			else if(operator=='greater than or equal to'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(String.isNotBlank(value) && sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,Decimal.valueOf(String.valueof(sobj.get(fieldName)))>=Decimal.valueof(value));
				}
			}
			else if(operator=='less than'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(String.isNotBlank(value) && sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,Decimal.valueOf(String.valueof(sobj.get(fieldName)))<Decimal.valueof(value));
				}
			}
			else if(operator=='less than or equal to'){
				//PD-2996 Bug-Fix | Null check for exttacted critera fields
				if(String.isNotBlank(value) && sobj.get(fieldName) != null) {
					Order_FilterLogic.put(order,Decimal.valueOf(String.valueof(sobj.get(fieldName)))<=Decimal.valueof(value));
				}
			}
		}
	}

    private List<String> getOrderList(String criteriaFilterLogic){
    	List<String> orderListString=new List<String>();
		List<Integer> orderListInteger=new List<Integer>();
		Pattern p = Pattern.compile('(\\d+)');
		Matcher m = p.matcher(criteriaFilterLogic);
		if(m.find()) {
			do {
				orderListInteger.add(Integer.valueOf(m.group()));
	    		} while(m.find());
		}
		orderListInteger.sort();

		//Desending order to avoid wrong replacement e.g. 1 replaced wrongly in 10
		for(Integer i = orderListInteger.size()-1; i>=0;i--)
		orderListString.add(String.valueOf(orderListInteger[i]));

		return orderListString;
    }
}