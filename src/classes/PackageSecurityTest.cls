/**
* FormBuilderRx
* @company : Copyright ? 2016, Enrollment Rx, LLC
    All rights reserved.
    Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
     - THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ?AS IS? AND ANY EXPRESS OR IMPLIED WARRANTIES,
     - INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
     - DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
     - SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
     - SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
     - IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
     - SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* @website : http://www.enrollmentrx.com/
* @author  Metacube
* @version 1.0
* @date   2016-05-13 
* @description Test class for PackageSecurity
*/
@IsTest(SeeAllData=false)
public with sharing class PackageSecurityTest {
    // This test method test with no record of custom setting existing
	static testMethod void testPackageSecurityInitial() {
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.insertCustomSetting();
			system.assertEquals(false,psObj.checkProceed('Portal_New_Page_Configure'));
			system.assertEquals(null,psObj.convertStringToDate(''));
        	system.assertNotEquals(null,psObj.convertStringToDate('06/06/2018'));
		Test.stopTest();
		psObj.isPaid = false;
		system.assertEquals(true,psObj.checkProceed('Portal_New_Page_Configure'));
		system.assertEquals(true,psObj.checkProceed('TestClassPageNew'));
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	// This test method test with no record of custom setting existing and insert blank custom setting
	static testMethod void testPackageSecurityEmptyCustomSetting() {
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.isPaid = false;
			psObj.insertCustomSetting();
		Test.stopTest();
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	// This test method test with record of custom setting Expiry Date greatter than today
	static testMethod void testPackageSecurityCustomSettingEDGreatter() {
		insertCustomSettingTestClass(Date.Today().addDays(7),true);
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.insertCustomSetting();
		Test.stopTest();
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	// This test method test with record of custom setting Expiry Date Smaller than today
	static testMethod void testPackageSecurityCustomSettingEDSmaller() {
		insertCustomSettingTestClass(Date.Today().addDays(-7),false);
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.insertCustomSetting();
		Test.stopTest();
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	// This test method test with record of custom setting Expiry Date Current than today
	static testMethod void testPackageSecurityCustomSettingEDCurrent() {
		insertCustomSettingTestClass(Date.Today(),true);
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.insertCustomSetting();
		Test.stopTest();
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	// This test method test force update custom setting
	static testMethod void testPackageSecurityForceUpdateCustomSetting() {
		insertCustomSettingTestClass(Date.Today().addDays(4),true);
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.forceUpdateCustomSetting();
		Test.stopTest();
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	// This test method test force update custom setting
	static testMethod void testPackageSecurityForceUpdateCustomSettingNoCustomSetting() {
		Test.startTest();
			PackageSecurity psObj = new PackageSecurity ();
			psObj.forceUpdateCustomSetting();
		Test.stopTest();
		system.assertEquals(true,psObj.pageName == null);
		system.assert(Package_Security__c.getAll().keySet() != null);
	}
	
	static void insertCustomSettingTestClass(Date ed,Boolean isPaidFrmMethod){
		insert new Package_Security__c(Name = '00D61000000cuCs',Expiration_Date__c = ed,isPaid__c = isPaidFrmMethod);
	}
}