/********
FormBuilderRx
@company : Copyright � 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
********/
public with sharing class Portal_RegisterController {

	public Portal_UserWrapper uWrap {get;set;}
	public Portal_Settings setting {get;set;}
	public Contact ct {get;set;}
	public User cu {get;set;}
	public Env__c cEnv {get;set;}
	public String urlParamMapString {get;set;}
	public Boolean isUserField {get;set;}
    private Map<String, String> urlParamMap;
    //PD-2689 | Gourav | to see if login is through social media
    public Boolean isSocialLogin;
    //PD-2689 | Gourav | to see if extra field is required or not
    public Boolean isExtraFieldsRequired;
    //PD-2689 | Gourav | social media button wrapper list
    public SocialMediaConfiguration_Wrapper[] socialWrapperList{get;set;}
    //PD-2689 | Gourav | environmentId of current environment in case of multiple community
    public Id environmentId{get;set;}
    //PD-2689 | Gourav | in case of extra field required it will hide the social buttons
    public Boolean showSocialButtons{get;set;}
    //PD-2689 | Gourav | to show error msg in case of required field during social media registration
    public string infoMsg{get;set;}
    
    //PD-4253 | Shubham
    public String FaviconIcon{
    	get {
    		Env__c cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String imageLink = '';
    		String siteURL = Site.getPathPrefix();
	        if(cEnv != null){
	            if(String.isNotBlank(cEnv.Favicon_Icon__c) && !cEnv.Favicon_Icon__c.contains('null')){
	            	List<String> pathValues = cEnv.Favicon_Icon__c.split('##');
					if(pathValues != null && pathValues.size() > 0){
						if(pathValues.size() == 1){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]);	
						}
						else if(pathValues.size() == 2){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]) + PortalPackageUtility.escapeQuotes(pathValues[1]) ;
						}
					}
	            }
	        }
	        return imageLink;
    	}
    	set;
    }
    
    //PD-4103 | Priyamvada
    public String gtmScript{
    	get {
    		cEnv = ERx_PortalPackUtil.getCurrentEnv();
    		String script = '';
	        if(cEnv != null){
	            script = cEnv.GTM_Account_Id__c;
	        }
	        return script;
    	}
    	set;
    }
    
    /****
    * @description To store PackageName;
    */
    private static String namespacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
    
    /****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
    
	public Portal_RegisterController() {
		if(!Test.isRunningTest()){
			cEnv = ERx_PortalPackUtil.getCurrentEnv();
		if(String.isNotBlank(cEnv.Registration_User_Configure_Text__c) && cEnv.Registration_User_Configure_Text__c.contains('<pre>')){
			
        	cEnv.Registration_User_Configure_Text__c= changeToHTML(cEnv.Registration_User_Configure_Text__c);
        }
        if(String.isNotBlank(cEnv.Registration_User_Configure_Text_Another__c) && cEnv.Registration_User_Configure_Text_Another__c.contains('<pre>')){
        	
        	cEnv.Registration_User_Configure_Text_Another__c= changeToHTML(cEnv.Registration_User_Configure_Text_Another__c);
        }
			
			
		}else{
			if(SecurityEnforceUtility.checkObjectAccess('Env__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
				//PD-2689 | Gourav | for test class
				cEnv = [select Id,Social_Login__c,By_Pass_required_fields__c from Env__c limit 1];
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::Env__c'), 'Portal_RegisterController', 'Portal_RegisterController', null);
				throw new PortalPackageException(permissionErrorMessage);  
				          	
			}
			
		}
		//PD-2689 /Gourav
		isSocialLogin = false;
		isExtraFieldsRequired=false;
		showSocialButtons = true;
		socialWrapperList = new SocialMediaConfiguration_Wrapper[]{};
		
		
		system.debug('22222222222'+ERx_PortalPackUtil.getEnvModeForCurrentUser());
		System.debug('crrrEnv---------------------'+cEnv);
		
		if(cEnv != null){
			uWrap = new Portal_UserWrapper(cEnv.Id);
			setting = new Portal_Settings(cEnv.Id);
			//PD-2689| Gourav | environmnet Id used in handler class to handle cases if multiple community exist
			environmentId = cEnv.Id;
			//Wrapper list to display soccial buttons on the page
			socialWrapperList = ERx_PortalPackUtil.socialMediaButtonConfiguration(cEnv);
		}
		if(ApexPages.currentPage() != null){
			urlParamMap = ApexPages.currentPage().getParameters();
        	urlParamMapString = POrtalPackageHelper.createParamString(urlParamMap);
		}
        // PD-1966 Portal Registration: rearrange order of all fields | Adding portal fields for the first time when an env is created
        isUserField = false;
        if(uWrap.customFields != null){
			for(Portal_CustomField f : uWrap.customFields){
				if(f.isRequired && f.fldType.toUpperCase() == 'STRING' && f.objectName.toUpperCase() == 'USER' 
				&& (f.fieldName == 'firstname' || f.fieldName == 'lastname' 
				|| f.fieldName == 'email' || f.fieldName == 'password' 
				|| f.fieldName == 'confirmpassword')) {
					isUserField = true;
					break;
				}
			}
        }
        //PD-2689 | Gourav | to check if any error occurs during the social registration
        if(ApexPages.currentPage() != null){
            if( Apexpages.currentpage().getparameters().containsKey('ErrorDescription')){
                String errDesc = ApexPages.currentPage().getParameters().get('ErrorDescription');
                errDesc = EncodingUtil.urlDecode(errDesc,'UTF-8');
                //if there are any required fields present on the registration form
                if(errDesc.contains('requiredFields')){
                    
                    String dataRecieved =  errDesc.substringAfter('##@##');
                    String[] userInfo = dataRecieved.split('##@##');
                    uWrap.firstname = userInfo[0];
                    uWrap.lastname = userInfo[1];
                    uWrap.email = userInfo[2];
                    showSocialButtons = false;
                    //to get info msg from the custom settings
                    FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
                    infoMsg='Some more information required';
					if(formBuilderSetting != null && String.isNotBlank(formBuilderSetting.Social_Media_Required_Field_Message__c)) {
						infoMsg = formBuilderSetting.Social_Media_Required_Field_Message__c;
					}
                }
                //missing email in case of facebook and server_error in case of LinkedIn
                else if(errDesc.contains('missingEmail') || errDesc.contains('server_error')  ){
                    String providerName = ApexPages.currentPage().getParameters().get('ProviderType');
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your ' + providerName +' privacy settings do not allow us to access your email address,' 
                                                               											+ 'so this login method is not available'));
                }else{
                	//To handle any other error
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errDesc));
                }
            }
        }
		
	}
	
	
	
	//Function to change string into html for rich text field
    //PD-1812 changes.
    public String changeToHTML(String str){
    	str = str.replace('&quot;','\"').replace('&lt;','<').replace('&gt;','>');
    	return str;
    }
    
	public boolean basicFieldsValidation(){
		if(uWrap!= null){
			if(uWrap.firstName == null || uWrap.firstName==''|| uWrap.lastName == null || uWrap.lastName == ''|| uWrap.email == null || uWrap.email == ''|| uWrap.password == null || uWrap.password == '') return false;	
		}
		return true;
	}
	
	
	public boolean extraFieldsValidation(){
		if(uWrap.customFields != null){
			for(Portal_CustomField f : uWrap.customFields){
				if(f.isRequired && 
					(f.fldType=='Picklist' || 
					f.fldType=='TextArea' || 
					f.fldType=='Phone' || 
					f.fldType=='String' || f.fldType == 'Reference') 
					&& (f.inputText == null || f.inputText == '')
					&& (f.objectName.toLowerCase() != 'user')){
					return false;
				}
				
				// 21-06-18 Added By Shubham Re PD-2202 Validation for checkbox field
				if(f.isRequired && (f.fldType=='Checkbox') 
					&& (f.inputCheckbox == null || f.inputCheckbox == false)
					&& (f.objectName.toLowerCase() != 'user')){
					return false;
				}
				
				// 21-06-18 Added By Shubham Re PD-2202 Validation for number field
				if(f.isRequired && (f.fldType=='Number') 
					&& (f.inputNumber == null || String.valueOf(f.inputNumber).trim() == '')
					&& (f.objectName.toLowerCase() != 'user')){
					return false;
				}
				
				if(f.isRequired && (f.fldType=='Date') &&(f.inputDate==null || f.inputDate=='')) return false;
				
				
				if(f.isRequired &&f.hasChildren &&(f.childrenInputText == null || f.childrenInputText == ''))return false;
				
				
				if(f.isRequired && f.hasGrandChildren && ((f.grandChildrenInputText == null || f.grandChildrenInputText == '')))return false;
				
			}
		}
		return true;
	}
	
	

	//new user registration
	public PageReference register(){
		try {
			if(ApexPages.currentPage() != null){
				urlParamMap = ApexPages.currentPage().getParameters();
        		urlParamMapString = POrtalPackageHelper.createParamString(urlParamMap);
			}
			//userId
			String userId = null;
			Savepoint sp = Database.setSavepoint();
			//field missing.
            if(!isSocialLogin){
                if(!uWrap.isValidInputReg()){
                    System.debug('=============000>>>'+setting.Reg_Field_Missing_Message);
                    PageReference pr1 = Portal_LoginUtil.addErrorMessage(setting.Reg_Field_Missing_Message);
                    return pr1;
                }
            }
	
			//password do not match
			//PD-2689 | Gourav | to check social login
            if(!isSocialLogin){
                if (!uWrap.isValidPassword()) {
                    PageReference pr2 = Portal_LoginUtil.addErrorMessage(setting.Reg_Password_No_Match_Message);
                    return pr2;
                } 
            }
	        
	        //password length validation
	        // Code commented due to site.createPortalUser will handle according to site guest user password policies.
	        /*if(!uWrap.isValidPasswordLength()){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Password must be at least 8 characters.'));
				return null;
			}*/
	        
	        //email format validation
			if(!uWrap.validateEmailFormat()){
				PageReference pr3 = Portal_LoginUtil.addErrorMessage(setting.Reg_Email_Format_Error_Message);
				return pr3;
			}
	
	        //email too long
			if(uWrap.email.length() > 40){
				PageReference pr4 = Portal_LoginUtil.addErrorMessage(setting.Reg_Username_Too_Long_Message);
				return pr4;
			}
			//PD-2689 | Gourav | to check social login
            if(!isSocialLogin){
                if(!basicFieldsValidation()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill required fields.'));
                    return null;
                }
            }
			//PD-2689 | Gourav | to check social login
			if(!extraFieldsValidation()){
				boolean flag = false;
				if(isSocialLogin){
					//To check if to by pass required fields during social registration or not
					if(!cEnv.By_Pass_required_fields__c){
						isExtraFieldsRequired = true;
						return null;	
					}else{
						flag= true;
					}
					
				}
				if(!flag){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill all required fields.'));
					return null;
				}
			}
	
			if(uWrap.existingUser() != null){
				//if user exists but not portal user
				this.cu = uWrap.existingUser();
				if(cu != null && uWrap.existingUser().ProfileId != setting.Student_Portal_User_Profile){
	            	return Portal_LoginUtil.addErrorMessage(setting.Reg_Non_Portal_User_Exists_Message);
				}else{
					//portal user already exisitng
					PageReference  pr5= Portal_LoginUtil.addErrorMessage(setting.Reg_Portal_User_Exists_Message);
					return pr5;
				}
	        }else if((ct = uWrap.existingContact()) != null){
	        	/*05-09-18 Added By Shubham Re PD-2499 check if user exist for contact*/ 
	        	if(uWrap.existingUserForContact(ct.Id) != null){
        			//portal user already exisitng
					return Portal_LoginUtil.addErrorMessage(setting.Reg_Contact_Portal_User_Exists_Message);
	        	}
	        	/*ends PD-2499*/
	        	//if contact exist
		        system.debug('c.email-------'+ct);
		    	uWrap.u.ContactId = ct.Id;
		    	if(setting.Update_Existing_Contact){
		    		uWrap.updateExistingContact();
		    	}
		    	if(setting.Activate_Existing_Contact_Via_Email){ 
		    		uWrap.activateExistingContact();
		    		// 29-1-2018 Added By Shubham Re PD-3197 create user when activate contact via email checked
		    		try{
		    			//PD-2689 | Gourav | to check social login
		    			if(!isSocialLogin){
			    			userId = Site.createPortalUser(uWrap.u, ct.AccountId, uwrap.password);
		    			}else{
	     					//Pd-2689 | Creating user
	        				userId = createCustomPortalUser();
	        			}
			    		if(userId != null){
			    			/*Below extra parameter added by Saurabh to send the update existing application field value #PD-3294 on 15-02-2018*/
			    			uWrap.processingExtraFields(new List<Contact>{ct},setting.Update_Existing_ApplicationField);
			    		}
			    	}catch(Exception e){
			    		Database.rollback(sp);
			    		return Portal_LoginUtil.addErrorMessage(e.getMessage());
			    	}
			    	// ends 3197 
		    		return Portal_LoginUtil.addErrorMessage(setting.Activate_Existing_Contact_Message);
		    	}else{
			    	try{
			    		if(!isSocialLogin){
			    			userId = Site.createPortalUser(uWrap.u, ct.AccountId, uwrap.password);
			    		}else{
	     					//Pd-2689 | Creating user
	        				userId = createCustomPortalUser();
	        			}	
			    		if(userId != null){
			    			/*Below extra parameter added by Saurabh to send the update existing application field value #PD-3294 on 15-02-2018*/
			    			uWrap.processingExtraFields(new List<Contact>{ct},setting.Update_Existing_ApplicationField);
			    		}
			    	}catch(Exception e){
			    		Database.rollback(sp);
			    		return Portal_LoginUtil.addErrorMessage(e.getMessage()+'1');
			    	}
		    	}
	        }else{
	        	try{
	        		//if nothing existing, create brand new user
		        	ct = new Contact();
		        	//front end user info
		        	ct.FirstName = uWrap.u.FirstName;
		        	ct.LastName = uWrap.u.LastName;
		        	ct.Email = uWrap.u.Email;
		        	ct.AccountId = this.retrieveAccountId();
		        	//upsert ct;
		        	/*Below extra parameter added by Saurabh to send the update existing application field value #PD-3294 on 15-02-2018*/
		        	uWrap.processingExtraFields(new List<Contact>{ct},setting.Update_Existing_ApplicationField);
		        	uWrap.u.ContactId = ct.Id;
	        		System.debug('ccccccccccccccccccccccccccccccccccc'+uWrap.u + 'account' + ct.AccountId + 'pass' +uwrap.password);
	        		//PD-2689 | Gourav | 9/5/2018 | To bypass the code when it is called from the social registration handler
	        		if(!isSocialLogin){
	        			userId = Site.createPortalUser(uWrap.u, ct.AccountId, uwrap.password);
	        		}else{
	     				//Pd-2689 | Creating user
	        			userId = createCustomPortalUser();
	        		}
	        	}catch(Exception e){
	        		Database.rollback(sp);
		    		return Portal_LoginUtil.addErrorMessage(e.getMessage());
		    	}
	        }
	        
	        if(userId != null){
	        	PageReference pageRef = null;
	        	try{
	        		//PD-2689 | Gourav | to check social login
	        		if(!isSocialLogin){
	        			pageRef = Site.login(uwrap.email, uwrap.password, '');
	        			List<PermissionSet> psl = [SELECT Id,Label From PermissionSet WHERE Label = 'ERxFB_Community'];
				    	if(psl != null && psl.size() > 0){
				    		PermissionSetAssignment psa = new PermissionSetAssignment
							(PermissionSetId = psl[0].Id, AssigneeId = userId);
							insert psa;
				    	}
	        		}else{
	        			insertPermissionSetAssignment(userId);
	        		}
	        	}
	        	catch(Exception e){
	        		Database.rollback(sp);
	        		return Portal_LoginUtil.addErrorMessage(e.getMessage());
	        	}
	        	//PD-2689 | gourav
	        	if(pageRef !=null){
	            	pageRef.setRedirect(true);
	        	}
	        	
	            return pageRef;
	        }
	        else{
	        	Database.rollback(sp);
	        	//return Portal_LoginUtil.addErrorMessage(setting.Reg_Portal_User_Exists_Message);
	        	return null;
	        }
		} catch(PortalPackageException ex) {
			//Added Re Kavita:PD-2137 Show Message for Existing Lead without Contact
			if(ex.getMessage().trim().equalsIgnoreCase(PortalPackageConstants.DUPLICATE_LEAD_MESSAGE.trim())) {
				return Portal_LoginUtil.addErrorMessage(PortalPackageConstants.DUPLICATE_LEAD_MESSAGE);
			} else {
				return Portal_LoginUtil.addErrorMessage(ex.getMessage());
			}
		}
        //should never reach here
        return Portal_LoginUtil.addErrorMessage('USERID is NULL.');
        
        
	}
	
	/*20-08-18 Added By Shubham Re PD-3729*/
	@future 
	public static void insertPermissionSetAssignment(Id userId){
		List<PermissionSet> psl = [SELECT Id,Label From PermissionSet WHERE Label = 'ERxFB_Community'];
		if(psl != null && psl.size() > 0){
			PermissionSetAssignment psa = new PermissionSetAssignment
			(PermissionSetId = psl[0].Id, AssigneeId = userId);
			insert psa;
		}
	}
	/*ends PD-3729*/
		
	
	/*
	* code to create a custom portal user PD-2689
	*@author :- Gourav 
	*@date :- 9/5/18
	*/
	
	public Id createCustomPortalUser(){
		//PD-2689 | Gourav | site user information storage
    	User siteUserCustom;
		String alias = uWrap.u.firstName + uWrap.u.lastName;
       	 //Alias must be 8 characters or less
       	if(alias.length() > 8) {
       		alias = alias.substring(0, 8);
        }
      	uWrap.u.alias = alias;
      	if(Test.isRunningTest()){
      		siteUserCustom = [ Select name,EmailEncodingKey,LocaleSidKey,LanguageLocaleKey,TimeZoneSidKey from user where Profile.name = 'System Administrator' Limit 1];
      	}else{
      		//PD-2689 | Gourav | site user information storage
			String siteIdtemp = Site.getSiteId();
			if(String.isNotBlank(siteIdtemp)){
				Site siteObj = [ select GuestUserId from site where Id =:siteIdtemp Limit 1 ];
				String uId = siteObj.GuestUserId;
				siteUserCustom = [ Select name,EmailEncodingKey,LocaleSidKey,LanguageLocaleKey,TimeZoneSidKey from user where Id=:uId Limit 1];
			}
      	}
        uWrap.u.languagelocalekey = siteUserCustom.LanguageLocaleKey;
        uWrap.u.localesidkey = siteUserCustom.LocaleSidKey;
        uWrap.u.emailEncodingKey = siteUserCustom.EmailEncodingKey;
        uWrap.u.timeZoneSidKey = siteUserCustom.TimeZoneSidKey;
        // Network networkInstance = [ select Id ,selfRegProfileId  from network where Id =: cEnv.Env_Id__c  Limit 1 ];
        // 23-07-18 Changed By Shubham Re PD-3837 use dynamic soql 
        String envIdVal = cEnv.Env_Id__c;
        sObject networkInstance = Database.query('SELECT Id, Name, selfRegProfileId '+
                                                ' FROM Network '
                                                +' WHERE Id = :envIdVal Limit 1');
       	if(networkInstance != null){
	       	String netInstSelfRegProfileId = String.valueof(networkInstance.get('selfRegProfileId'));
	        if(String.isNotBlank(netInstSelfRegProfileId)){
	        	uWrap.u.profileId = netInstSelfRegProfileId;
	        }
       	}
        System.debug('UserInfo' + uWrap.u);
	    insert uWrap.u;
		return uWrap.u.id;
		
	}
	

	//get account Id
	//account name field removed from setting
	//Now account will be created with Last name + Administrative Account 
	public Id retrieveAccountId(){
		// 29-1-2018 Added by shubham Re PD-3197 Get Account with LastName + Administrative Account 
		if(SecurityEnforceUtility.checkObjectAccess('Account', SecurityTypes.IS_ACCESS, namespacePrefix)){
			Id returnAccId;
			// 18-07-18 Changed by Shubham Re PD-3817 new account created everytime for a a new Portal Register contact
			String accName = uWrap.lastName;
			if(accName.length() > 240){
				accName = accName.subString(0,239);
			}
			accName = accName + ' Administrative Account';
			Account acc = new Account();
			acc.Name = accName;
			if(SecurityEnforceUtility.checkObjectAccess('Account', SecurityTypes.IS_INSERT, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('Account', 'Name', SecurityTypes.IS_INSERT, namespacePrefix)) {
					insert acc;
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + 'Account' +' FieldList :: Name'), 'Portal_RegisterController', 'retrieveAccountId', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Account'), 'Portal_RegisterController', 'retrieveAccountId', null);
				throw new PortalPackageException(permissionErrorMessage);
			}
			returnAccId = acc.Id;
			return returnAccId;
		} else {
			ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Account'), 'Portal_RegisterController', 'retrieveAccountId', null);
			throw new PortalPackageException(permissionErrorMessage); 
		}
		/* 29-1-2018 Commented By Shubham to remove account used from setting
		if(setting.Account_Name == null){
			Account acc = new Account();
			string accName = uWrap.firstName + ' ' + uWrap.lastName;
			if(accName.length() > 240){
				accName = accName.subString(0,239);
			}
			accName = accName+' Portal';
			acc.Name = accName;
			Account acc = new Account();
			//string accName = uWrap.firstName + ' ' + uWrap.lastName;
			string accName = uWrap.lastName;
			if(accName.length() > 240){
				accName = accName.subString(0,239);
			}
			accName = accName+' Administrative Account';
			acc.Name = accName;
			if(SecurityEnforceUtility.checkObjectAccess('Account', SecurityTypes.IS_INSERT, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('Account', 'Name', SecurityTypes.IS_INSERT, namespacePrefix)) {
					insert acc;
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + 'Account' +' FieldList :: Name'), 'Portal_RegisterController', 'retrieveAccountId', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Account'), 'Portal_RegisterController', 'retrieveAccountId', null);
				throw new PortalPackageException(permissionErrorMessage);
			}
			returnMe = acc.Id;
		}else{
			list<Account> accList;
			
			if(SecurityEnforceUtility.checkObjectAccess('Account', SecurityTypes.IS_ACCESS, namespacePrefix)){
				accList = [select Id from Account where Name =: setting.Account_Name];
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::Env__c'), 'Portal_RegisterController', 'retrieveAccountId', null);
				throw new PortalPackageException(permissionErrorMessage); 
			}
			
			if(accList.size() > 0){
				returnMe = accList.get(0).Id;
			}else{ 
				Account acc1 = new Account();
				acc1.Name = setting.Account_Name;
				if(SecurityEnforceUtility.checkObjectAccess('Account', SecurityTypes.IS_INSERT, namespacePrefix)){
					if(SecurityEnforceUtility.checkFieldAccess('Account', 'Name', SecurityTypes.IS_INSERT, namespacePrefix)) {
						insert acc1;
					} else {
						ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + 'Account' +' FieldList :: Name'), 'Portal_RegisterController', 'retrieveAccountId', null);
						throw new PortalPackageException(permissionErrorMessage);
					}
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Account'), 'Portal_RegisterController', 'retrieveAccountId', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
				returnMe = acc1.Id;
			}
		}*/
	}
	
	 /*******************************************************************************************************
    * @description forces DML to insert log messages in database
    */
    public void initPageException() {
        try {
         ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
         
        } catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }

}