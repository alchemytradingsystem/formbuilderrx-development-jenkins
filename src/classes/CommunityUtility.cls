public class CommunityUtility {
    public static String SELECT_CLAUSE = ' SELECT ';
    public static String WHERE_CLAUSE = ' WHERE ';
    public static String FROM_CLAUSE = ' FROM ';
	 /**** 
    * @description To store PackageName;
    */
    private static String namespacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
    
    /****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
    
    public static Id getCurrentContactId() {
        List<User> users = [SELECT user.contactId, Name from User user where user.id = :Userinfo.getUserId() and user.contactId != null];
        if(users.size() > 0){
            return users[0].contactId;
        } else {
            return null;
        }
    }

    public static Id getActiveApplicationId(){
      if(ngForceController.hasApplicationObject){
         Id contactId = CommunityUtility.getCurrentContactId();
         if(contactId != null) {
         	Contact contact;
            String query='select Id, EnrollmentrxRx__Active_Enrollment_Opportunity__c from Contact where Id =\''+ contactId+'\' limit 1';
   		    if(SecurityEnforceUtility.checkObjectAccess('Contact', SecurityTypes.IS_ACCESS, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('Contact', 'EnrollmentrxRx__Active_Enrollment_Opportunity__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
   		   			contact=(Contact)Database.query(query)[0];
				} else {
            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Contact  FieldList :: EnrollmentrxRx__Active_Enrollment_Opportunity__c'), 'CommunityUtility', 'getActiveApplicationId', null);
					throw new PortalPackageException(permissionErrorMessage);
				}
   		    } else {
            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Contact '), 'CommunityUtility', 'getActiveApplicationId', null);
            	throw new PortalPackageException(permissionErrorMessage); 
   		    }
   		   	return (Id)contact.get('EnrollmentrxRx__Active_Enrollment_Opportunity__c');
          }
          return null;
       }
       else
       return null;
   }

     /****************************************************************************************
           This method returns list of all fieldSetMembers for passed object.
          @param objectName    Object Name
          @param fieldSetName    fieldset Name
          @return list of fieldset members
     *****************************************************************************************/
    public static List<sObject> getObjectWithFields(String objectName,Map<String,string> mapWhereClause,String OtherClause, List<schema.fieldsetmember> fieldSetValues, Set<String> customFields, RequiredFieldTypes fType) {
        String query = SELECT_CLAUSE ;
        try{
            Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();
            Schema.SObjectType sObjectTypeObj = globalDescribeMap.get(objectName);

            if(fType == RequiredFieldTypes.FIELDSET_FIELDS_CUSTOM_FIELDS || fType == RequiredFieldTypes.ALL_FIELDS || fType == RequiredFieldTypes.ALL_FIELDS_WITH_CUSTOM_FIELDS || fType == RequiredFieldTypes.ONLY_CUSTOM_FIELDS){
                if(fType == RequiredFieldTypes.ALL_FIELDS || fType == RequiredFieldTypes.ALL_FIELDS_WITH_CUSTOM_FIELDS) {
                    Schema.DescribeSObjectResult describeSObjectResultObj = sObjectTypeObj .getDescribe();
                    Map<String,Schema.SObjectField> fields = describeSObjectResultObj.fields.getMap() ;
                    List<Schema.SObjectField> fieldMapValues = fields.values();
                    for(Schema.SObjectField field : fieldMapValues) {
                        query += field.getDescribe().getName();
                        query += ',';
                    }
                }
                if(fType == RequiredFieldTypes.ALL_FIELDS_WITH_CUSTOM_FIELDS || fType == RequiredFieldTypes.ONLY_CUSTOM_FIELDS || fType == RequiredFieldTypes.FIELDSET_FIELDS_CUSTOM_FIELDS) {
                    if(customFields != null) {
                        for(String customeField : customFields) {
                            query += customeField+',';
                        }
                    }
                }
                /*
                if(fType == RequiredFieldTypes.FIELDSET_FIELDS_CUSTOM_FIELDS) {
                    for(schema.fieldsetmember f : fieldSetValues) {
                        if(!query.contains(f.getFieldPath()+',')) {
                            query += f.getFieldPath() + ',';
                        }
                    }
                }
                */
            }
            /*
            else if(fType == RequiredFieldTypes.ONLY_FIELDSET_FIELDS){
                for(schema.fieldsetmember f : fieldSetValues) {
                    if(!query.contains(f.getFieldPath()+',')) {
                        query += f.getFieldPath() + ',';
                    }
                }
            }
            */
            query = query.subString(0, query.length() - 1);
            query += FROM_CLAUSE + objectName;


            if(mapWhereClause.size() > 0){
                query += WHERE_CLAUSE;
                for(String fieldWithOperator : mapWhereClause.keyset()){
                  if(!String.isBlank(mapWhereClause.get(fieldWithOperator))){
      					if(mapWhereClause.get(fieldWithOperator).toLowerCase()=='true' || mapWhereClause.get(fieldWithOperator).toLowerCase()=='false')
                     query += fieldWithOperator + Boolean.valueOf(mapWhereClause.get(fieldWithOperator)) +' AND ';
                     else{
                        query += fieldWithOperator   + '\'' + mapWhereClause.get(fieldWithOperator) + '\'' +' AND ';
                     }
                  }
   					else{
                     query += fieldWithOperator   + '\'' + mapWhereClause.get(fieldWithOperator) + '\'' +' AND ';
                  }
                }
                query = query.subString(0, query.length() - 4);
            }

            if(!String.isBlank(OtherClause))
            query=query+' '+OtherClause;

			   System.debug('### query: '+query);

            return Database.query(query);
        } catch(Exception e) {
            system.debug(e.getMessage() + '### query: ' + query);
            return null;
        }
    }
    
    /*************************************************************
    * @description : Method that iterate criteria and return field map and reference field name map
    * @param List<HomepageCriteria> criteriaList
    * @param Map<String, Set<String>> objectFieldMap
    * @param Map<String, Set<String>> referencedObjectFieldMap
    * @return List<Map<String, Set<String>>>
    *
    *************************************************************/
    public static List<Map<String, Set<String>>> getObjectFieldsFromCriteria(List<HomepageCriteria> criteriaList, Map<String, Set<String>> objectFieldMap, Map<String, Set<String>> referencedObjectFieldMap) {
    	List<Map<String, Set<String>>> criteriaFieldList = new List<Map<String, Set<String>>>();
    	if(criteriaList != null && criteriaList.size() > 0) {
	    	if(objectFieldMap == null) {
	    		// Case : If null then initialize 
	    		objectFieldMap = new Map<String, Set<String>>();
	    	}
	    	
	    	if(referencedObjectFieldMap == null) {
	    		// Case : If null then initialize 
	    		referencedObjectFieldMap = new Map<String, Set<String>>();
	    	}
	    	for(HomepageCriteria criteria : criteriaList) {
	    		// Reseeting field set value
	    		String objectName = criteria.objectName.toLowerCase();
	    		
	    		if(!objectFieldMap.containsKey(objectName)) {
	    			// Case : Key is present not in map
	    			objectFieldMap.put(objectName, new Set<String>());
	    		}
	    		if(String.isNotBlank(criteria.referenceFieldName)) {
	    			// Case : if reference field is not blank
	    			
	    			// Creating reference field map
	    			String mergedKey = criteria.objectName+criteria.fieldName+criteria.referenceObjectName;
	    			if(!referencedObjectFieldMap.containsKey(mergedKey)) {
	    				referencedObjectFieldMap.put(mergedKey, new Set<String>());
	    			}
	    			referencedObjectFieldMap.get(mergedKey).add(criteria.referenceFieldName);
	    			
	    			// Fetching reference field relation name and adding into field map
	    			objectFieldMap.get(objectName).add(PortalPackageHelper.getReferenceNameField(criteria.fieldName, criteria.referenceFieldName));
	    		} else {
	    			// adding into field map
	    			objectFieldMap.get(objectName).add(criteria.fieldName);
	    		}
	    	}
    	}
    	
    	criteriaFieldList.add(objectFieldMap);
    	criteriaFieldList.add(referencedObjectFieldMap);
    	
    	return criteriaFieldList;
    }
    
    /****************************************************************************************
           This method returns list of all fieldSetMembers for passed object.
          @param objectName    Object Name
          @param mapWhereClause where clause map
          @param customFields    custom fields set
          @return list of fieldset members
     *****************************************************************************************/
    public static List<sObject> getObjectWithFields(String objectName, Map<String,string> mapWhereClause, Set<String> customFields) {
        String query = SELECT_CLAUSE ;
        List<String> securityCheckFields = new List<String>();
        String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
        if(customFields == null) {
        	customFields = new Set<String>();
        }
        customFields.add('Id');
    	customFields.add('Name');
        try{
        	
            if(customFields != null) {
                for(String customeField : customFields) {
                	List<String> tempStringList = customeField.split('\\.');
                    if(tempStringList.size() > 1) {
                    	String referenceFieldName = tempStringList[0];
                    	referenceFieldName = PortalPackageHelper.getFieldFromReferenceMappingName(referenceFieldName);
                    	securityCheckFields.add(referenceFieldName);
                    } else { 
                    	securityCheckFields.add(customeField);
                    }
                    query += customeField+',';
                }
            }
            
            query = query.subString(0, query.length() - 1);
            query += FROM_CLAUSE + objectName;


            if(mapWhereClause.size() > 0){
                query += WHERE_CLAUSE;
				for(String fieldWithOperator : mapWhereClause.keyset()){
                  	if(!String.isBlank(mapWhereClause.get(fieldWithOperator))){
  						if(mapWhereClause.get(fieldWithOperator).toLowerCase() == 'true' || mapWhereClause.get(fieldWithOperator).toLowerCase() == 'false') {
                     		query += fieldWithOperator + Boolean.valueOf(mapWhereClause.get(fieldWithOperator)) + ' AND ';
  						} else{
                        	query += fieldWithOperator + '\'' + mapWhereClause.get(fieldWithOperator) + '\'' + ' AND ';
                     	}
                  	} else{
                     	query += fieldWithOperator + '\'' + mapWhereClause.get(fieldWithOperator) + '\'' + ' AND ';
                  	}
            	}
            	query = query.subString(0, query.length() - 4);
        	}

		   	System.debug('### query: '+query);
			if(SecurityEnforceUtility.checkObjectAccess(objectName, SecurityTypes.IS_ACCESS, namespacePrefix)){
				//System.assert(false, objectName + '----' + JSON.serialize(securityCheckFields));
            	if(SecurityEnforceUtility.checkFieldAccess(objectName, securityCheckFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
            		return Database.query(query);
            	} else {
            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + objectName + ' FieldList :: ' + PortalPackageUtility.convertListBySeperator(securityCheckFields, ', ')), 'CommunityUtility', 'getObjectWithFields', null);
            		throw new PortalPackageException(permissionErrorMessage);
            	}
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + objectName), 'CommunityUtility', 'getObjectWithFields', null);
				throw new PortalPackageException(permissionErrorMessage);
			}
        } catch(Exception e) {
            system.debug(e.getMessage() + '### query: ' + query);
            throw e;
        }
    }
    
    /******************************************************
    * @description : Method to filter widgets by widget number
    * @param List<Homepage_Widget__c> hwList widget list that to be filter
    * @param List<String> widgetNumberList to be fetched widget number list
    * @return List<Homepage_Widget__c> widget list
    ******************************************************/
    public static List<Homepage_Widget__c> findWidgetForLayoutByNumber(List<Homepage_Widget__c> hwList, List<String> widgetNumberList) {
    	List<Homepage_Widget__c> returnList = new List<Homepage_Widget__c>();
    	for(Homepage_Widget__c hw : hwList) {
    		if(PortalPackageUtility.isElementFoundInList(widgetNumberList, hw.Number__c)) {
    			// Case : Widget number is same then adding to list
    			returnList.add(hw);
    		}
    	}
    	return returnList;
    }
    /**
    * @description : Method that returns core configuration setting value
    * @param : status Setting name
    * @return Setting Value
    */
    public static String getCoreConfigurationSettingValueByStatus(String status) {
    	String recievedStatus = '';
    	try {
	    	// Add Condition to handle previous core version. In Core 2.4 Configuration Setting is not available.
    		if(ngForceController.hasConfigurationObject) {
		    	List<String> securityCheckFields = new List<String>{'EnrollmentrxRx__Setting_Value__c', 'EnrollmentrxRx__Setting_Name__c'};
		    	String query = 'SELECT EnrollmentrxRx__Setting_Value__c FROM EnrollmentrxRx__Package_Configuration__c where EnrollmentrxRx__Setting_Name__c =: status';
		    	String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
		    	if(SecurityEnforceUtility.checkObjectAccess('EnrollmentrxRx__Package_Configuration__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
		    		if(SecurityEnforceUtility.checkFieldAccess('EnrollmentrxRx__Package_Configuration__c', securityCheckFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
		    			List<sObject> configurationSettingList = Database.query(query);
		    			if(configurationSettingList != null && configurationSettingList.size() > 0) {
		    				if(configurationSettingList[0].get('EnrollmentrxRx__Setting_Value__c') != null) {
		    					recievedStatus = (String) configurationSettingList[0].get('EnrollmentrxRx__Setting_Value__c');
		    				}
		    			}
		    		} else {
		    			ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: EnrollmentrxRx__Package_Configuration__c ' + ' FieldList :: ' + PortalPackageUtility.convertListBySeperator(securityCheckFields, ', ')), 'CommunityUtility', 'getCoreConfigurationSettingByStatus', null);
		        		throw new PortalPackageException(permissionErrorMessage);
		    		}
		    	} else {
		    		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: EnrollmentrxRx__Package_Configuration__c'), 'CommunityUtility', 'getCoreConfigurationSettingByStatus', null);
		    		throw new PortalPackageException(permissionErrorMessage);
		    	}
    		}
    	} catch (Exception e) {
    		ERx_PortalPackageLoggerHandler.addException(e,'CommunityUtility','getCoreConfigurationSettingByStatus', null);
    		throw e;
    	}
    	
    	return recievedStatus;
    }
}