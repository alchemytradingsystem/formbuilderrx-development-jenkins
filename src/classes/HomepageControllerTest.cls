//jenkins setup
@isTest
private class HomepageControllerTest {
    public static Homepage_Widget_Layout__c hwl;
    public static User user;
    public static Env__c env;
    public static List<Homepage_Widget__c> widgetList;
    public static List<String> widgetNumbers;

    static testMethod void myUnitTest(){
        prepareData();
        System.runAs(user){
            ngForceController ngcon=new ngForceController();
            HomepageController con=new HomepageController(ngcon);

            if(!ngForceController.hasApplicationObject){
                String applicationId=con.applicationId;
                try {
                    CommunityUtility.getActiveApplicationId();
                } catch(Exception e) {
                    
                }
            }
			String val = con.checklistResultJSON;
            val = con.receivedChecklistStatusJSON;
            hwl.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

            if(ngForceController.hasApplicationObject)
            hwl.Field_Reference__c='{{contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name}}';
            else
            hwl.Field_Reference__c='{{contact.Account.Name}}';

            update hwl;
            con=new HomepageController(ngcon);

            prepareData1();
            con=new HomepageController(ngcon);
            System.assertNotEquals(con, null);
            con.initPageException();
        }
    }

    static testMethod void myUnitTestNew(){
        prepareData();
        prepareData1();
        System.runAs(user){
            ngForceController ngcon=new ngForceController();
            HomepageController con=new HomepageController(ngcon);
            System.assertNotEquals(con, null);
        }
    }

    static testMethod void myUnitTest1(){
        prepareData();
		ngForceController ngcon=new ngForceController();
        HomepageController con1=new HomepageController(ngcon);
        Map<String, String> mapWhereClause = new Map<String, String>();
    	Set<String> customFields = new Set<String>();

     	//application
    	mapWhereClause.clear();
    	customFields.clear();
    	customFields.add('Account.Name');
    	mapWhereClause.put('Email=', 'testDante@testDante.com');
    	mapWhereClause.put('HasOptedOutOfEmail =', 'false');

    	sObject contact=(sObject)CommunityUtility.getObjectWithFields('Contact', mapWhereClause, 'Order By Name',null, customFields, RequiredFieldTypes.ALL_FIELDS_WITH_CUSTOM_FIELDS)[0];
		List<Homepage_Widget__c> foundWidgetList = CommunityUtility.findWidgetForLayoutByNumber(widgetList, widgetNumbers);
        System.assertNotEquals(contact, null);
    }

    static testMethod void myUnitTest2(){
        prepareData2();
        System.runAs(user){
            ngForceController ngcon=new ngForceController();
            HomepageController con=new HomepageController(ngcon);
            System.assertNotEquals(con, null);
        }
    }

    public static void prepareData() {
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        env=new Env__c();
        env.Version__c=1;
        env.Env_Site_Id__c='123';
        env.Environment_Status__c = 'Live';
        env.UrlPathPrefix__c = '/test';
        env.Favicon_Icon__c = 'Val1##Val2';
        insert env;

        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact c=new Contact();
        c.firstName='testDante';
        c.LastName='testDante';
        c.Email='testDante@testDante.com';
        c.AccountId=a.Id;
        insert c;

        Profile p=[select id from Profile where Name='Applicant Portal User'];
        //Profile p=[select id from Profile where Name='Student Portal User'];

        user = new User();
        user.Username ='testDante@testDante.com';
        user.LastName = c.LastName;
        user.Email = c.Email;
        user.alias = 'testaa';
        user.TimeZoneSidKey = 'America/Chicago';
        user.LocaleSidKey = 'en_US';
        user.ProfileId=p.id;
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        user.ContactId = c.id;
        //insert user;

            sObject app=null;
            if(ngForceController.hasApplicationObject){
                app=Schema.getGlobalDescribe().get('EnrollmentrxRx__Enrollment_Opportunity__c').getDescribe().getSobjectType().newSObject();
                app.put('EnrollmentrxRx__Applicant__c',c.Id);
                app.put('EnrollmentrxRx__Admissions_Status__c','Inquiry');
                insert app;
            }


        hwl=new Homepage_Widget_Layout__c();
        hwl.Name='testLayoutDante';
        hwl.Displaying_Application_Status__c='Inquiry';
        hwl.Env__c=env.Id;
        hwl.Is_Active__c=true;
        hwl.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

        if(ngForceController.hasApplicationObject)
        hwl.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
        else
        hwl.Field_Reference__c='contact.Name';

        insert hwl;

        if(ngForceController.hasApplicationObject){
            c.put('EnrollmentrxRx__Active_Enrollment_Opportunity__c',app.Id);
            update c;
        }

        Homepage_Widget_Layout__c hwlDefault=new Homepage_Widget_Layout__c();
        hwlDefault.Name='TestDefault';
        hwlDefault.Default__c=true;
        hwlDefault.Displaying_Application_Status__c='Inquiry';
        hwlDefault.Env__c=env.Id;
        hwlDefault.Is_Active__c=true;
        hwlDefault.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

        if(ngForceController.hasApplicationObject)
        hwlDefault.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
        else
        hwlDefault.Field_Reference__c='contact.Name';

        insert hwlDefault;

        Homepage_Widget_Layout__c hwl1=new Homepage_Widget_Layout__c();
        hwl1.Name='Test1';
        hwl1.Displaying_Application_Status__c='Inquiry';
        hwl1.Env__c=env.Id;
        hwl1.Is_Active__c=true;
        hwl1.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

        if(ngForceController.hasApplicationObject){
            hwl1.Criteria_Filter_Logic__c='(1 AND 2 AND 3)';
            hwl1.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
            hwl1.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"equals","value":"Inquiry"},{"order":2,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"FirstName","operator":"contains","value":"Dante","referenceObjectName":"Contact"},{"order":3,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"LastName","operator":"contains","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"not equal to","value":""}]';
        }
        else{
            hwl1.Criteria_Filter_Logic__c='1 AND 2';
            hwl1.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"not equal to","value":"","referenceObjectName":""},{"order":2,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"equals","value":"3121231234","referenceObjectName":""}]';
        }

        insert hwl1;

        Homepage_Widget_Layout__c hwl2=new Homepage_Widget_Layout__c();
        hwl2.Name='Test2';
        hwl2.Displaying_Application_Status__c='Inquiry';
        hwl2.Env__c=env.Id;
        hwl2.Is_Active__c=true;
        hwl2.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

        if(ngForceController.hasApplicationObject){
            hwl2.Field_Reference__c='contact.Name;contact.EnrollmentrxRx__Active_Enrollment_Opportunity__r.Name;application.EnrollmentrxRx__Admissions_Status__c;application.EnrollmentrxRx__Program_of_Interest__r.Name;';
            hwl2.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"starts with","value":"Inquiry"},{"order":2,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"FirstName","operator":"ends with","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Applicant__c","referenceFieldName":"LastName","operator":"does not contain","value":"Dante","referenceObjectName":"Contact"},{"order":4,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Admissions_Status__c","referenceFieldName":"","operator":"not equal to","value":""}]';
        }
        else{
            hwl2.Field_Reference__c='contact.Name';
            hwl2.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"not equal to","value":"","referenceObjectName":""},{"order":2,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"equals","value":"3121231234","referenceObjectName":""}]';
        }

        insert hwl2;
        
        widgetList = new List<Homepage_Widget__c>();
        widgetNumbers = new List<String>();
        
        Homepage_Widget__c hpw1 = new Homepage_Widget__c();
        hpw1.Name = 'Widget W1';
        hpw1.Is_Active__c=true;
        hpw1.Default__c = true;
        hpw1.Env__c = env.Id;
        hpw1.Lookup_Fields__c = 'contact.Account.Name';
        hpw1.Content__c = 'LeadSource : {{contact.LeadSource}} --- {{contact.Account.Name}}';
        hpw1.Criteria__c = '';
        hpw1.Criteria_Filter_Logic__c = '';
        hpw1.Number__c = 'W1';
        
        insert hpw1;
        widgetList.add(hpw1);
        widgetNumbers.add(hpw1.Number__c);
        
        Homepage_Widget__c hpw2 = new Homepage_Widget__c();
        hpw2.Name = 'Widget W2';
        hpw2.Is_Active__c=true;
        hpw2.Default__c = true;
        hpw2.Env__c = env.Id;
        hpw2.Lookup_Fields__c = 'contact.Account.Name';
        hpw2.Content__c = 'LeadSource : {{contact.LeadSource}} --- {{contact.Account.Name}}';
        hpw2.Criteria__c = '';
        hpw2.Criteria_Filter_Logic__c = '';
        hpw2.Number__c = 'W2';
        
        insert hpw2;
        widgetList.add(hpw2);
        widgetNumbers.add(hpw2.Number__c);
        
        Homepage_Widget__c hpw3 = new Homepage_Widget__c();
        hpw3.Name = 'Widget W3';
        hpw3.Is_Active__c=true;
        hpw3.Default__c = true;
        hpw3.Env__c = env.Id;
        hpw3.Lookup_Fields__c = 'contact.Account.Name';
        hpw3.Content__c = 'LeadSource : {{contact.LeadSource}} --- {{contact.Account.Name}}';
        hpw3.Criteria__c = '';
        hpw3.Criteria_Filter_Logic__c = '';
        hpw3.Number__c = 'W3';
        
        insert hpw3;
        widgetList.add(hpw3);
        widgetNumbers.add(hpw3.Number__c);      
    }

    public static void prepareData1(){
        Homepage_Widget_Layout__c hwl3=new Homepage_Widget_Layout__c();
        hwl3.Name='Test3';
        hwl3.Displaying_Application_Status__c='Inquiry';
        hwl3.Env__c=env.Id;
        hwl3.Is_Active__c=true;
        hwl3.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

        if(ngForceController.hasApplicationObject)
        hwl3.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"EnrollmentrxRx__Program_of_Interest__c","referenceFieldName":"Name","operator":"not equal to","value":"","referenceObjectName":"EnrollmentrxRx__Program_Offered__c"}]';
        else
        hwl3.Criteria__c='';

        insert hwl3;

        Homepage_Widget_Layout__c hwl4=new Homepage_Widget_Layout__c();
        hwl4.Name='Test4';
        hwl4.Displaying_Application_Status__c='Inquiry';
        hwl4.Env__c=env.Id;
        hwl4.Is_Active__c=true;
        hwl4.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"OwnerId","referenceFieldName":"Name","operator":"not equal to","value":"","referenceObjectName":"User"}]';
        hwl4.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';
        
        insert hwl4;

        Homepage_Widget_Layout__c hwl5=new Homepage_Widget_Layout__c();
        hwl5.Name='Test5';
        hwl5.Displaying_Application_Status__c='Inquiry';
        hwl5.Env__c=env.Id;
        hwl5.Is_Active__c=true;

        if(ngForceController.hasApplicationObject)
        hwl5.Criteria__c='[{"order":1,"objectName":"EnrollmentrxRx__Enrollment_Opportunity__c","fieldName":"Name","referenceFieldName":"","operator":"not equal to","value":"","referenceObjectName":""}]';
        else
        hwl5.Criteria__c='';
        hwl5.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';

        insert hwl5;

        Homepage_Widget_Layout__c hwl6=new Homepage_Widget_Layout__c();
        hwl6.Name='Test6';
        hwl6.Displaying_Application_Status__c='Inquiry';
        hwl6.Env__c=env.Id;
        hwl6.Is_Active__c=true;
        hwl6.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"not equal to","value":"","referenceObjectName":""},{"order":2,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"equals","value":"3121231234","referenceObjectName":""}]';
        hwl6.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';
        insert hwl6;

        Homepage_Widget_Layout__c hwl7=new Homepage_Widget_Layout__c();
        hwl7.Name='Test7';
        hwl7.Displaying_Application_Status__c='Inquiry';
        hwl7.Env__c=env.Id;
        hwl7.Is_Active__c=true;
        hwl7.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"MobilePhone","referenceFieldName":"","operator":"starts with","value":"3121232222","referenceObjectName":""}]';
        hwl7.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';
        insert hwl7;

        Homepage_Widget_Layout__c hwl8=new Homepage_Widget_Layout__c();
        hwl8.Name='Test8';
        hwl8.Displaying_Application_Status__c='Inquiry';
        hwl8.Env__c=env.Id;
        hwl8.Is_Active__c=true;
        hwl8.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"HomePhone","referenceFieldName":"","operator":"ends with","value":"3121231234","referenceObjectName":""},{"order":2,"objectName":"Contact","fieldName":"FirstName","referenceFieldName":"","operator":"contains","value":"test","referenceObjectName":""},{"order":3,"objectName":"Contact","fieldName":"LastName","referenceFieldName":"","operator":"does not contain","value":"test","referenceObjectName":""}]';
        hwl8.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';
        insert hwl8;

        Homepage_Widget_Layout__c hwl9=new Homepage_Widget_Layout__c();
        hwl9.Name='Test9';
        hwl9.Displaying_Application_Status__c='Inquiry';
        hwl9.Env__c=env.Id;
        hwl9.Is_Active__c=true;
        hwl9.Criteria__c='[{"order":1,"objectName":"Contact","fieldName":"MasterRecordId","referenceFieldName":"Name","operator":"not equal to","value":"","referenceObjectName":"MasterRecord"}]';
        hwl9.Grid_Data__c='[{"x":0,"y":0,"width":12,"height":3,"number":"W1","content":"Test"},{"x":12,"y":0,"width":12,"height":3,"number":"W2","content":"Name : {{contact.Name}}"}]';
        insert hwl9;
    }

    public static void prepareData2() {
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        env=new Env__c();
        env.Version__c=1;
        env.Env_Site_Id__c='123';
        env.Environment_Status__c = 'Live';
        insert env;

        Account a = new Account(Name='Test Account Name');
        insert a;

        Contact c=new Contact();
        c.firstName='testDante';
        c.LastName='testDante';
        c.Email='testDante@testDante.com';
        c.AccountId=a.Id;
        insert c;

        Profile p=[select id from Profile where Name='Applicant Portal User'];
        //Profile p=[select id from Profile where Name='Student Portal User'];

        user = new User();
        user.Username ='testDante@testDante.com';
        user.LastName = c.LastName;
        user.Email = c.Email;
        user.alias = 'testaa';
        user.TimeZoneSidKey = 'America/Chicago';
        user.LocaleSidKey = 'en_US';
        user.ProfileId=p.id;
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        user.ContactId = c.id;
        //insert user;

            sObject app=null;
            if(ngForceController.hasApplicationObject){
                app=Schema.getGlobalDescribe().get('EnrollmentrxRx__Enrollment_Opportunity__c').getDescribe().getSobjectType().newSObject();
                app.put('EnrollmentrxRx__Applicant__c',c.Id);
                app.put('EnrollmentrxRx__Admissions_Status__c','Inquiry');
                insert app;
                c.put('EnrollmentrxRx__Active_Enrollment_Opportunity__c',app.Id);
                update c;
            }
    }
    
    static testMethod void testGetObjectFieldsFromCriteria(){
    	Map<String, Set<String>> objectFieldMap;
    	Map<String, Set<String>> referenceObjectFieldMap;
    	List<HomepageCriteria> criteriaList = (List<HomepageCriteria>) JSON.deserialize('[{"order":1,"objectName":"Contact","fieldName":"AccountId","referenceFieldName":"Name","value":"test","displayValue":"","twoLevelReferenceObjectName":"","fieldType":"string","referenceObjectName":"Account","operator":"equals"},{"order":2,"objectName":"Contact","fieldName":"FirstName","referenceFieldName":"","value":"test","displayValue":"","twoLevelReferenceObjectName":"","fieldType":"string","operator":"equals"}]', List<HomepageCriteria>.class);
    	List<Map<String, Set<String>>> tempList = CommunityUtility.getObjectFieldsFromCriteria(criteriaList, objectFieldMap, referenceObjectFieldMap);
		objectFieldMap = tempList[0];
		referenceObjectFieldMap = tempList[1];
		System.assertEquals(1, objectFieldMap.size());
		System.assertEquals(1, referenceObjectFieldMap.size());
    }
    
    static testMethod void testGetObjectWithFields() {
    	Account a = new Account(Name='Test Account Name');
        insert a;

        Contact c=new Contact();
        c.firstName='testDante';
        c.LastName='testDante';
        c.Email='testDante@testDante.com';
        c.AccountId=a.Id;
        insert c;
        
        Map<String, String> mapWhereClause = new Map<String, String>();
        Set<String> customFields = new Set<String>();

        mapWhereClause.put('Id=', c.Id);
        customFields.add('FirstName');
        customFields.add('LastName');
        customFields.add('AccountId');
        
        sObject contact= (sObject) CommunityUtility.getObjectWithFields('Contact', mapWhereClause, customFields)[0];
        System.assertNotEquals(null, contact);
    }
 }