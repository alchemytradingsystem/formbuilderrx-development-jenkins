@isTest
public with sharing class CreatePortalUserTriggerHandlerTest {
     
    
    @testSetup static void DataPreparation(){
        Account acObj  = new Account(Name = 'Test Acc' );
        insert acObj;
        FormBuilder_Settings__c custtomSetting = new FormBuilder_Settings__c(
            Name='Admin Settings', Portal_Account_Id__c = acObj.id
        );
        insert custtomSetting;
    }
    
    private static testMethod void testConstructor() {
        Test.startTest();
            CreatePortalUserTriggerHandler handlerObj = new CreatePortalUserTriggerHandler(true);
            system.assertEquals(true,handlerObj.istriggercontext);
        Test.stopTest();  
    }
     
    private static testMethod void testAfterInsertNoAccount() {
        Test.startTest();
            //Commented code reqiured where record type present
            //RecordType rt = [SELECT Id,Name FROM RecordType where SObjectType = 'Contact' and isActive=true];
            //Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test@gmail.com',Create_User_with_Profile__c = 'Student Portal User', RecordTypeId = rt.id);
            Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test1@gmail.com',Create_User_with_Profile__c = 'Student Portal User');
            insert conObj;
            system.assertEquals(true,conObj != null);

        Test.stopTest();  
    }
    
    private static testMethod void testAfterInsertWithAccount() { 
        Test.startTest();
        
            Account acObj = [SELECT id from Account LIMIT 1];
            //Commented code reqiured where record type present
            //RecordType rt = [SELECT Id,Name FROM RecordType where SObjectType = 'Contact' and isActive=true];
            //Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test@gmail.com',AccountId = acObj.id,Create_User_with_Profile__c = 'Student Portal User', RecordTypeId = rt.id);
            Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test2@gmail.com',AccountId = acObj.id,Create_User_with_Profile__c = 'Student Portal User');
            insert conObj;
            system.assertEquals(true,conObj != null);
        Test.stopTest();  
    }
    
    private static testMethod void testAfterUpdateNoAccount() {
        //Commented code reqiured where record type present
        //RecordType rt = [SELECT Id,Name FROM RecordType where SObjectType = 'Contact' and isActive=true];
        //Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test@gmail.com', RecordTypeId = rt.id);
        Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test3@gmail.com'); 
        insert conObj;
        Test.startTest();
            conObj.Create_User_with_Profile__c = 'Student Portal User';
            update conObj;
            system.assertEquals(true,conObj != null);
        Test.stopTest();  
    }
    
    private static testMethod void testAfterUpdateWithAccount() {
        Account acObj = [SELECT id from Account LIMIT 1];

		Contact c=new Contact();
		c.firstName='testDante';
		c.LastName='testDante';
		c.Email='testDante@testDante.com';
		c.AccountId=acObj.id;
		insert c;

		Profile p=[select id from Profile where Name='Student Portal User'];

        User user = new User();
	    user.Username ='testDante@testDante.com';
	    user.LastName = c.LastName;
	    user.Email = c.Email;
	    user.alias = 'testaa';
	    user.TimeZoneSidKey = 'America/Chicago';
	    user.LocaleSidKey = 'en_US';
	    user.ProfileId=p.id;
	    user.EmailEncodingKey = 'ISO-8859-1';
	   	user.LanguageLocaleKey = 'en_US';
	    user.ContactId = c.id;
        insert user;
        
        c.firstName='testDante1';
        update c;
        //Commented code reqiured where record type present
        //RecordType rt = [SELECT Id,Name FROM RecordType where SObjectType = 'Contact' and isActive=true];
        //Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test@gmail.com',AccountId = acObj.id, RecordTypeId = rt.id);
        Contact conObj = new Contact(LastName = 'Test Erx', FirstName = 'EnrollmentRx', Email = 'Test4@gmail.com',AccountId = acObj.id);
        insert conObj;
        Test.startTest();
            conObj.Create_User_with_Profile__c = 'Student Portal User';
            update conObj;
            system.assertEquals(true,conObj != null);
        Test.stopTest();  
    }
    
}