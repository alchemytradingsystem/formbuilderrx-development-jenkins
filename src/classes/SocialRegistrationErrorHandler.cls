/********
FormBuilderRx
@company : Copyright � 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author : Gourav Gandhi
@date : 23/5/18
//PD-2689 | Gourav | to hold and process the error during the social media registration
********/
public with sharing class SocialRegistrationErrorHandler {
    private String currentUrl;
    private String errorDesc='';
    private Id currEnvId ;
    private String providerName='';
    private String namespacePrefix ='';
			
    
    public SocialRegistrationErrorHandler(){
    	 currentUrl =  URL.getCurrentRequestUrl().toExternalForm().substringBeforeLast('/');
    	 errorDesc = ApexPages.currentPage().getParameters().get('ErrorDescription');
    	 system.debug('current url : ' + currentUrl + 'error desc : ' + errorDesc);
   		 if(apexpages.currentpage().getparameters().containskey('ProviderId')){
   		 	Id providerId = ApexPages.currentPage().getParameters().get('ProviderId');
   		 	List<AuthProvider> authProviderList = [ select Providertype from AuthProvider where id =: providerId ];
   			if(authProviderList != null && authProviderList.size()>0){
   		 		providerName = authProviderList[0].Providertype;
   		 	}
   		 }
   		 namespacePrefix=PortalPackageUtility.getNameSpacePerfix();
		 if(String.isBlank(namespacePrefix)){
			namespacePrefix ='';
		 }else{
			namespacePrefix=namespacePrefix+'__';
		 }
   		 
        system.debug('!!!!!!!!!!!!!!!' + errorDesc);
        //Check needed as in case of linkedIn in directly comes to this class instead of socialRegistrationHandler 
	    if(errorDesc.contains('##@##')){
	        currEnvId = errorDesc.substringAfterLast('Env');
	        errorDesc = errorDesc.substringBeforeLast('##@##');
	        Env__c originalEnv = [ Select Id,Self_Register_URL__c from Env__c where Id =:currEnvId Limit 1  ];
	   		currentUrl = originalEnv.Self_Register_URL__c;
        }
        redirectToPage();
    }
    
    public PageReference redirectToPage(){
    	String newUrl;
    	if(currentUrl.contains('Portal_Register') || currentUrl.contains('Portal_Login')){
    		newUrl = currentUrl + '?ErrorDescription=' + errorDesc + '&ProviderType=' + providerName;
    	}else{
    		newUrl = currentUrl + '/' + namespacePrefix + 'Portal_register' + '?ErrorDescription=' + errorDesc + '&ProviderType=' + providerName;
    	}
    	PageReference p = new PageReference(newUrl);
    	p.setRedirect(false);
    	return p;
    }
   
}