/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
  	All rights reserved.
  	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
  	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
  	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  	@website : http://www.enrollmentrx.com/
  	@author EnrollmentRx
  	@version 1.28
  	@date 2017-09-14 
  	@Created By Kavita Beniwal
  	@description LC_Portal_RegistrationCriteriaController Class is used to hold Criteria field Condition Wrapper Data
 	**/
public with sharing class LC_Portal_RegistrationCriteriaController {

	/****
	 * Default Constructor
	 */ 
	public LC_Portal_RegistrationCriteriaController() {
		nameSpacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
	}

	/****
	 * @description rendered criteria
	 */
	public LC_RenderCriteriaConditionWrapper criteria {get;set;}

	/****
	 * @description variable to hold the current index changes
	 */
	public Integer changeIndex {get;set;}
	/****
	 * @description variable to hold the selected value
	 */
	public String selectedValue {get;set;}
	/****
	 * @description variable to hold the current index changes
	 */
	public String selectedDisplayValue {get;set;}
	/****
	 * @description variable to hold the namespace of package
	 */
	public String nameSpacePrefix {get;set;}

	public String picklistSelectedValueList {get;set;}

	/****
	 * Description : to hold search result output for reference field
	 */
	public List<ERx_PageEntities.SelectOpWrap> lookupResult {get;set;}
	
	/****
	 * @description variable to hold the current order by index changes
	 */
	public Integer orderByIndex {get;set;}

	/****
	 * @description variable to check is Application Object present or not
	 */
	public static Boolean hasApplicationObject{
		get{
			hasApplicationObject =Schema.getGlobalDescribe().containsKey('EnrollmentrxRx__Enrollment_Opportunity__c');
			//hasApplicationObject=false;
			return hasApplicationObject;
		}
		set;
	}

	/****
	 * @description criteria operator list
	 */
	public static List<SelectOption> operatorList {
		get {
			operatorList = new List<SelectOption>();
			operatorList.add(new SelectOption('', '--None--'));
			for(String op : new List<String>{'equals', 'not equal to', 'starts with', 'ends with', 'contains', 'does not contain'}) {
				operatorList.add(new SelectOption(op, op));
			}
			return operatorList;
		}
		set;
	}

	/****
	 * @description criteria condition object list
	 */
	public static List<String> criteriaObjectNamesAPEX {
		get{
			criteriaObjectNamesAPEX = new List<String>();
			criteriaObjectNamesAPEX.add('Contact');
			if(hasApplicationObject && !Test.isRunningTest()) {
				criteriaObjectNamesAPEX.add('EnrollmentrxRx__Enrollment_Opportunity__c');
			}
			return criteriaObjectNamesAPEX;
		}
		set;
	}


	/****
	 * @description Get all necessary object describe results
	 */
	public static Map<String,Map<String, Object>> describeResultsOutput { 
		get{
			Map<String, Schema.SobjectType> schemaMap = Schema.getGlobalDescribe();
			Map<String,String> objtypeMap = new Map<String,String>();
			List<String>criteriaObjectList = criteriaObjectNamesAPEX;
			//Store name of reference object name
			List<String> lstReferenceObject = new List<String>();
			// creating object map thats field to be fetched
			
			for(String objectName : criteriaObjectList){
				objtypeMap.put(objectName, objectName);
				Map <String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();
				for(Schema.SObjectField sfield : fieldMap.Values()) {
					schema.describeFieldResult dfr = sfield.getDescribe();
					if(String.valueOf(dfr.getType()).toLowerCase() == 'reference'){
						for(Schema.sObjectType sobjType : dfr.getReferenceTo()){
							String refObjectName = sobjType.getDescribe().getName();
							if(refObjectName.toLowerCase() == 'group') {
								continue;
							}
							if(!objtypeMap.containsKey(refObjectName)) {
								objtypeMap.put(refObjectName,refObjectName);
							} 
							//add reference object name
							if(objectName == 'EnrollmentrxRx__Enrollment_Opportunity__c') {
								lstReferenceObject.add(refObjectName);
							}
						}
					}
				}
				
				
				
				//Get 1 level reference object name of reference object - lstReferenceObject 
				for(String refObjectName : lstReferenceObject) {
					Map <String, Schema.SObjectField> refererenceFieldMap = schemaMap.get(refObjectName).getDescribe().fields.getMap();
					for(Schema.SObjectField sfield : refererenceFieldMap.Values()) {
						schema.describeFieldResult dfr = sfield.getDescribe();
						if(String.valueOf(dfr.getType()).toLowerCase() == 'reference'){
							for(Schema.sObjectType sobjType : dfr.getReferenceTo()){
								String referenceObjectName = sobjType.getDescribe().getName();
								if(referenceObjectName.toLowerCase() == 'group') {
									continue;
								}
								if(!objtypeMap.containsKey(referenceObjectName)) {
									objtypeMap.put(referenceObjectName,referenceObjectName);
								}
							}
						}
					}
				}
			}
			describeResultsOutput = LC_Portal_RegistrationCriteriaController.describes(objtypeMap.values());
			return describeResultsOutput;
		}
		set;
	}

	/****
	 * @description Method that describes each object and create necessary map
	 * @param list<String> objtypes : Object Names
	 * @return Map<String,Map<String, Object>> : Key is Object name and Value is Another Map (Key Property Name e.g. fields, selectoption etc.).
	 */
	private static Map<String,Map<String, Object>> describes(list<String> objtypes) {
		Map<String,Map<String, Object>> describeResults = new Map<String,Map<String, Object>>();
		for(String objtype:objtypes){
			// Just enough to make the sample app work!
			Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objtype);
			if (targetType == null) {
				return (Map<String, Map<String, Object>>) json.deserialize('[{"message":"The requested resource does not exist","errorCode":"NOT_FOUND"}]', 
				Map<String, Map<String, Object>>.class);
			}

			Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();

			Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();

			List<Object> fields = new List<Object>();
			for (String key : fieldMap.keySet()) {
				Schema.DescribeFieldResult descField = fieldMap.get(key).getDescribe();
				Map<String, Object> field = new Map<String, Object>();

				field.put('type', descField.getType().name().toLowerCase());
				field.put('name', descField.getName().toLowerCase());
				field.put('label', descField.getLabel());
				field.put('picklistValues', descField.getPicklistValues());
				field.put('controllerName', String.valueOf(descField.getController()));
				List<String> references = new List<String>();
				for (Schema.sObjectType t : descField.getReferenceTo()) {
					if(t.getDescribe().getName().toLowerCase() != 'group') {
						references.add(t.getDescribe().getName());
					}
				}
				if (!references.isEmpty()) {
					field.put('referenceTo', references);
				}

				fields.add(field);
			}

			Map<String, Object> result = new Map<String, Object>();
			result.put('fields', fields);
			List<SelectOption> objectSelectOption = new List<SelectOption>();
			for(Object f : fields) {
				Map<String, Object> fieldInstance = (Map<String, Object>) f;
				objectSelectOption.add(new SelectOption(String.valueOf(fieldInstance.get('name')).toLowerCase(), String.valueOf(fieldInstance.get('label'))));
			}
			result.put('selectoption', objectSelectOption);
			describeResults.put(objtype,result);
		}
		return describeResults;
	}

	/****
	 * Description : Method that create a dummy criteria row
	 */
	public LC_TemplateRenderedConditionWrapper addBlankRow() {
		LC_TemplateRenderedConditionWrapper rrcw = new LC_TemplateRenderedConditionWrapper();
		rrcw.criteriaType = criteria.renderedCondition[0].criteriaType; 
		rrcw.modelName = criteria.renderedCondition[0].modelName;
		rrcw.fieldAPIName = criteria.renderedCondition[0].fieldAPIName; 
		rrcw.fieldDisplayType = criteria.renderedCondition[0].fieldDisplayType;
		rrcw.referencedObjectName = criteria.renderedCondition[0].referencedObjectName;
		rrcw.referencedFieldAPIName = '';
		rrcw.referencedFieldDisplayType = '';
		rrcw.twoLevelReferencedFieldAPIName = '';
		rrcw.twoLevelReferencedFieldObjectName = '';
		rrcw.displayValue = '';
		rrcw.value = '';
		rrcw.fieldOperator = '';
		return rrcw;

	}

	/****
	 * @description Method is used to remove criteria from list when remove button clicked.
	 */
	public void removeCriteria() {
		try {
			if(changeIndex != null) {
				criteria.renderedCondition.remove(changeIndex);
			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
	}

	/****
	 * Description : Method is used to set field property for first level field (Referenced field).
	 */
	public void setReferencedFieldType() {
		try {
			List<Integer> foundLocations = new List<Integer>();
			
			//Store temporary condition in case all condition selected if 2 condition set referencedFieldAPIName to blank
			LC_TemplateRenderedConditionWrapper tempRenderCondition;
			
			// Finding blank Object Name
			if(criteria.renderedCondition != null) {
				
				for(Integer lc = 0; lc < criteria.renderedCondition.size(); lc++) {
					if(String.isBlank(criteria.renderedCondition[lc].referencedFieldAPIName)) {
						foundLocations.add(lc - foundLocations.size());
					}
				}
				
				//Handle case of list size 2 & referencedFieldAPIName for first one selected to -None-
				if(criteria.renderedCondition.size() == 2 && String.isBlank(criteria.renderedCondition[0].referencedFieldAPIName)) {
					tempRenderCondition = addBlankRow();
				}

				// Removing the blank Value from Criteria
				for(Integer location : foundLocations) {
					criteria.renderedCondition.remove(location);
				}
				
				// Adding dummy in Last.
				if(tempRenderCondition == null) {
					criteria.renderedCondition.add(addBlankRow());
				} else {
					criteria.renderedCondition.add(tempRenderCondition);
				}
			}
			
			if(changeIndex != null && String.isNotBlank(criteria.renderedCondition[changeIndex].referencedFieldAPIName) && tempRenderCondition == null) {
				criteria.renderedCondition[changeIndex].referencedFieldDisplayType = '';
				criteria.renderedCondition[changeIndex].displayValue = '';
				criteria.renderedCondition[changeIndex].value = '';
				criteria.renderedCondition[changeIndex].referencedFieldObjectName = '';
				criteria.renderedCondition[changeIndex].lookupSearchString = '';
				criteria.renderedCondition[changeIndex].lookupRecords = null;
				criteria.renderedCondition[changeIndex].fieldOperator = '';
				criteria.renderedCondition[changeIndex].twoLevelReferencedFieldDisplayType = '';
				if(String.isNotBlank(criteria.renderedCondition[changeIndex].referencedFieldAPIName)) {
					if(String.isNotBlank(criteria.renderedCondition[changeIndex].referencedObjectName)) {
						Map<String, Object> fieldMap = describeResultsOutput.get(criteria.renderedCondition[changeIndex].referencedObjectName);
						if(fieldMap != null && fieldMap.size() > 0) {
							List<Object> fieldList = (List<Object>) fieldMap.get('fields');
							if(fieldList != null && fieldList.size() > 0) {
								for(Object f : fieldList) {
									Map<String, Object> fieldObj = (Map<String, Object>) f;
									// Comparing field API Name to find field
									if(criteria.renderedCondition[changeIndex].referencedFieldAPIName == String.valueOf(fieldObj.get('name'))) {
										// Setting field type
										criteria.renderedCondition[changeIndex].referencedFieldDisplayType = String.valueOf(fieldObj.get('type'));
										if(String.valueOf(fieldObj.get('type')) == 'reference') {
											// Case : when field is reference type setting referenced object name
											List<String> referenceToList = (List<String>) fieldObj.get('referenceTo');
											criteria.renderedCondition[changeIndex].referencedFieldObjectName = referenceToList[0];
										}

										break;
									}
								}
							}
						}
					}
				}
			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
		changeIndex = null;
	}

	/****
	 * Description : Method is used to set field property for two level field (Referenced field).
	 */
	public void setTwoLevelReferencedFieldType() {
		try {
			if(changeIndex != null) {
				criteria.renderedCondition[changeIndex].twoLevelReferencedFieldDisplayType = '';
				criteria.renderedCondition[changeIndex].displayValue = '';
				criteria.renderedCondition[changeIndex].value = '';
				criteria.renderedCondition[changeIndex].twoLevelReferencedFieldObjectName = '';
				criteria.renderedCondition[changeIndex].lookupSearchString = '';
				criteria.renderedCondition[changeIndex].lookupRecords = null;
				criteria.renderedCondition[changeIndex].fieldOperator = '';
				if(String.isNotBlank(criteria.renderedCondition[changeIndex].twoLevelReferencedFieldAPIName)) {
					if(String.isNotBlank(criteria.renderedCondition[changeIndex].referencedFieldObjectName)) {
						Map<String, Object> fieldMap = describeResultsOutput.get(criteria.renderedCondition[changeIndex].referencedFieldObjectName);
						if(fieldMap != null && fieldMap.size() > 0) {
							List<Object> fieldList = (List<Object>) fieldMap.get('fields');
							if(fieldList != null && fieldList.size() > 0) {
								for(Object f : fieldList) {
									Map<String, Object> fieldObj = (Map<String, Object>) f;
									// Comparing field API Name to find field
									if(criteria.renderedCondition[changeIndex].twoLevelReferencedFieldAPIName == String.valueOf(fieldObj.get('name'))) {
										// Setting field type
										criteria.renderedCondition[changeIndex].twoLevelReferencedFieldDisplayType = String.valueOf(fieldObj.get('type'));
										if(String.valueOf(fieldObj.get('type')) == 'reference') {
											// Case : when field is reference type setting referenced object name
											List<String> referenceToList = (List<String>) fieldObj.get('referenceTo');
											criteria.renderedCondition[changeIndex].twoLevelReferencedFieldObjectName = referenceToList[0];
										}

										break;
									}
								}
							}
						}
					}
				}
			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
		changeIndex = null;
	}

	public void getRecords() {
		try {
			lookupResult = new List<ERx_PageEntities.SelectOpWrap>();
			if(changeIndex != null) {
				LC_TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
				String searchString = conditionWrapper.lookupSearchString;
				if(String.isNotBlank(searchString)) {
					String objectName;
					if(String.isNotBlank(conditionWrapper.twoLevelReferencedFieldObjectName)) {
						objectName = conditionWrapper.twoLevelReferencedFieldObjectName;
					} else if(String.isNotBlank(conditionWrapper.referencedFieldObjectName)) {
						objectName = conditionWrapper.referencedFieldObjectName;
					} else if(String.isNotBlank(conditionWrapper.referencedObjectName)) {
						objectName = conditionWrapper.referencedObjectName;
					}
					if(String.isNotBlank(objectName)) {
						String sobjectType = '';
						if(objectName.toLowerCase() == 'recordtype'){
							// If Object Name is Record Type then Fetching all the records types assosiate with that sobjectType.
							sobjectType = String.isNotBlank(conditionWrapper.referencedFieldObjectName) ? conditionWrapper.referencedObjectName : conditionWrapper.modelName;
						}

						// Fetching Search result 
						List<sObject> recordList = PortalPackageUtility.getRecordsByObjectName(objectName, sobjectType, searchString);

						// Adding Null in List
						lookupResult.add(new ERx_PageEntities.SelectOpWrap('NULL', 'NULL'));

						if(recordList != null) {
							for(sObject r : recordList) {
								// Creating Id, Name value object
								lookupResult.add(new ERx_PageEntities.SelectOpWrap(ERx_PortalPackUtil.get15DigitId(String.valueOf(r.get('Id'))), String.valueOf(r.get('Name'))));
							}
						}
					} 
				}

				// Assigning result data to wrapper
				criteria.renderedCondition[changeIndex].lookupRecords = lookupResult;
			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
	}

	public void selectReferencedValue() {
		try {
			if(changeIndex != null) {
				LC_TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
				conditionWrapper.value = selectedValue;
				conditionWrapper.displayValue = selectedDisplayValue;
				conditionWrapper.lookupSearchString = '';
				conditionWrapper.lookupRecords = null;

				criteria.renderedCondition[changeIndex] = conditionWrapper;

				selectedValue = null;
				selectedDisplayValue = null;
				changeIndex = null;

			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
	}

	public void removeReferencedValue() {
		try {
			if(changeIndex != null) {
				LC_TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
				conditionWrapper.value = null;
				conditionWrapper.displayValue = '';

				criteria.renderedCondition[changeIndex] = conditionWrapper;

				changeIndex = null;

			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
	}
	
	/****
	 * @description Method is used to remove order by criteria from list when remove button clicked.
	 */
	public void removeOrderByCriteria() {
		try {
			if(orderByIndex != null) {
				criteria.orderByCondition.remove(orderByIndex);
			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
	}
	
	/****
	 * Description : Method that create a dummy order by row
	 */
	public LC_OrderByWrapper addOrderByBlankRow() {
		LC_OrderByWrapper rrcw = new LC_OrderByWrapper();
		rrcw.orderByField = '';
		rrcw.orderByReferenceObject = '';
		rrcw.orderByReferenceField = '';
		rrcw.sortOrder = '';
		return rrcw;

	}
	
	/**
	 *  @KB : For PD-1493 added by Kavita Beniwal
	 *  @description : This method is responsible to set Order ByField Object
	 **/
	public void setOrderByFieldObject() {
        try {
        	if(orderByIndex != null) {
	        	List<Integer> foundLocations = new List<Integer>();
				// Finding blank Object Name
				if(criteria.orderByCondition != null) {
					
					for(Integer lc = 0; lc < criteria.orderByCondition.size(); lc++) {
						if(String.isBlank(criteria.orderByCondition[lc].orderByField)) {
							foundLocations.add(lc - foundLocations.size());
						}
					}
					// Removing the blank Value from Criteria
					for(Integer location : foundLocations) {
						criteria.orderByCondition.remove(location);
					}
					criteria.orderByCondition.add(addOrderByBlankRow());
				}
				
	            DescribeFieldResult fieldDescribe = SchemaCache.getFieldDescribe(criteria.orderByObject, criteria.orderByCondition[orderByIndex].orderByField);
	            //case: selected field is reference type
	            if(String.valueOf(fieldDescribe.getType()).toLowerCase()== 'reference') {
	                for(Schema.sObjectType sobjType : fieldDescribe.getReferenceTo()) {
	                    String refObjectName = sobjType.getDescribe().getName();
	                    if(refObjectName.toLowerCase() == 'group') {
	                        continue;
	                    } else {         
	                        criteria.orderByCondition[orderByIndex].orderByReferenceObject = refObjectName;
	                        break;
	                    }
	                }
	            } else {
	                criteria.orderByCondition[orderByIndex].orderByReferenceField = null;
	                criteria.orderByCondition[orderByIndex].orderByReferenceObject = null;
	            }
	            orderByIndex = null;
        	}               
        } catch(Exception e) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }
       
    }

	public void updatePicklistValue() {
		try {
			
			List<ERx_PageEntities.SelectOpWrap> picklistSelected = null;
			String tempValue = '';
			String tempDisplayValue = '';
			if(String.isNotBlank(picklistSelectedValueList)) {
				picklistSelected = (List<ERx_PageEntities.SelectOpWrap>) JSON.deserialize(picklistSelectedValueList, List<ERx_PageEntities.SelectOpWrap>.class);
				for(ERx_PageEntities.SelectOpWrap sow : picklistSelected) {
					tempValue += sow.id + ',';
					tempDisplayValue += sow.name + ',';
				}

				if(String.isNotBlank(tempValue)) {
					tempValue = tempValue.removeEnd(',');
				}
				if(String.isNotBlank(tempDisplayValue)) {
					tempDisplayValue = tempDisplayValue.removeEnd(',');
				}
			}

			if(changeIndex != null) {
				LC_TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
				conditionWrapper.value = tempValue;
				conditionWrapper.displayValue = tempDisplayValue;

				criteria.renderedCondition[changeIndex] = conditionWrapper;

				changeIndex = null;
				picklistSelectedValueList = null;
			}
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
	}
}