/** 
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
  	All rights reserved.
  	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
  	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
  	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  	@website : http://www.enrollmentrx.com/
  	@author EnrollmentRx
  	@version 1.28
  	@date 2017-09-18 
  	@Created By Kavita Beniwal
  	@description TestLC_Portal_RegistrationCriteria test Class is used to cover test cases for LC_Portal_RegistrationCriteriaController
 	**/
@isTest
private class TestLC_Portal_RegistrationCriteria {

	public static LC_TemplateRenderedConditionWrapper templateConditionWrapper;
	private static Profile testProfile ;
	private static Contact student;
	private static User testUser;
	private static Portal_Login_Custom_Field__c customField;
	
	static testMethod void hasApplicationObjectTest() {
		Boolean hasObj = LC_Portal_RegistrationCriteriaController.hasApplicationObject;
		if(hasObj){
			System.assertEquals(true, hasObj);
		}else{
			System.assertEquals(false, hasObj);
		}

	}

	static testMethod void testCriteriaLists() {
		//Testing operator list
		Account a;
		a = new Account(name = 'TEST ACCOUNT');
		Database.insert(a);
		student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
		insert student;
		testProfile = [select id, name from profile where name='Applicant Portal User'];
		testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
		insert testUser;
		prepareData();
		Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
		sfm.frontFieldId = customField.Id;
		sfm.initPage();
		Map<String, LC_RenderCriteriaConditionWrapper> mapFieldsCriteria = sfm.mapFieldsCriteria;
		System.runAs(testUser){
			
			LC_Portal_RegistrationCriteriaController templateObject = new LC_Portal_RegistrationCriteriaController();
			templateObject.orderByIndex = 0;
			templateObject.setOrderByFieldObject();
			templateObject.changeIndex = 0;
			templateObject.updatePicklistValue();
			templateObject.removeReferencedValue();
			templateObject.selectReferencedValue();
			templateObject.removeCriteria();
			templateObject.setReferencedFieldType();
			templateObject.selectedValue = a.Id;
			templateObject.selectedDisplayValue = 'TEST ACCOUNT';
			
			List<SelectOption> expectedselectOp = new List<SelectOption>();
			expectedselectOp.add(new SelectOption('', '--None--'));
			for(String op : new List<String>{'equals', 'not equal to', 'starts with', 'ends with', 'contains', 'does not contain'}) {
				expectedselectOp.add(new SelectOption(op, op));
			}
			List<SelectOption> resultSelectOp = LC_Portal_RegistrationCriteriaController.operatorList;
			system.assertEquals(expectedselectOp, resultSelectOp);  

			//Testing criteriaObjectNamesAPEX
			List<String> criteriaObjectNamesList = new List<String>();
			criteriaObjectNamesList.add('Contact');
			List<String> resultCriteriaObjectNamesList = LC_Portal_RegistrationCriteriaController.criteriaObjectNamesAPEX;
			system.assertEquals(criteriaObjectNamesList, resultCriteriaObjectNamesList);

			Map<String,Map<String, Object>> templateMap = LC_Portal_RegistrationCriteriaController.describeResultsOutput;
			
			templateObject.criteria = mapFieldsCriteria.get('field');  
			templateObject.setOrderByFieldObject();
			//System.assert(false, templateObject.criteria);
			templateObject.addBlankRow();
			templateObject.criteria.renderedCondition[0].lookupSearchString = 'Test Parent';
			templateObject.changeIndex = 0;
			templateObject.getRecords();
			templateObject.changeIndex = 0;
			templateObject.setReferencedFieldType();
			templateObject.criteria = mapFieldsCriteria.get('field');  
			templateObject.changeIndex = 1;
			templateObject.setTwoLevelReferencedFieldType();
			templateObject.changeIndex = 0;
			templateObject.selectReferencedValue();
			//Testing for exception in generating picklist value
			List<ERx_PageEntities.SelectOpWrap> selectOpWrap = new List<ERx_PageEntities.SelectOpWrap>();
			templateObject.picklistSelectedValueList = 'Test:Test;';
			templateObject.changeIndex = 0;
			templateObject.updatePicklistValue();
			//Testing for generating picklist value
			ERx_PageEntities.SelectOpWrap selectOp1 = new ERx_PageEntities.SelectOpWrap('Test','Test');
			ERx_PageEntities.SelectOpWrap selectOp2 = new ERx_PageEntities.SelectOpWrap('Test1','Test1');
			selectOpWrap.add(selectOp1);
			selectOpWrap.add(selectOp2);
			templateObject.picklistSelectedValueList = JSON.serialize(selectOpWrap);
			templateObject.updatePicklistValue();
			templateObject.changeIndex = 0;
			templateObject.changeIndex = 0;
			templateObject.removeReferencedValue();
			templateObject.changeIndex = 0;
			templateObject.removeCriteria();
			templateObject.criteria = mapFieldsCriteria.get('childfield');  
			templateObject.changeIndex = 0;
			templateObject.setTwoLevelReferencedFieldType();
		}
	}

	private static void createDataToGenerateException() {
		templateConditionWrapper.criteriaType = 'object';
		templateConditionWrapper.modelName = 'Contact';
		templateConditionWrapper.fieldAPIName = 'accountid';
		templateConditionWrapper.fieldOperator = 'equals';
		templateConditionWrapper.lookupSearchString = 'TEST ACCOUNT';
		templateConditionWrapper.referencedFieldAPIName = 'Account';
		templateConditionWrapper.referencedFieldDisplayType = 'reference';
		templateConditionWrapper.referencedObjectName = 'Account';
	}
	
	/**
	 *  @KB : For PD-1493 added by Kavita Beniwal
	 *  @description : Prepare data for custom field to check for Reference field criteria
	 **/
	private static void prepareData() { 
		Test_Erx_Data_Preparation.preparation();
		Account superParentAccount;
		superParentAccount = new Account(name = 'Test Parent');
		insert superParentAccount;
		
		Account parentAccount;
		parentAccount = new Account(ParentId = superParentAccount.Id, name = 'Test1');
		insert parentAccount;
		
		Account account = new Account(ParentId = parentAccount.Id, name = 'Test');
		insert account;

		customField = new Portal_Login_Custom_Field__c(gdChildQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
			+parentAccount.Id+'%\''+' ORDER BY Parent.Id DESC LIMIT 2', Grand_Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
			FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'AccountSource = \'Web\' AND Name != \'Test\' ORDER BY Parent.Id DESC LIMIT 2', 
			Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
			childQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c LIKE \'%Low%\''+')' + ' ORDER BY Parent.Id DESC LIMIT 2', 
			Children_Field_API_Name__c = 'accountid');
		insert customField;
	}


}