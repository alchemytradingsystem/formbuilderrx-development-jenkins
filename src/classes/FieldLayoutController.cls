/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Class for field layout component
*/
public with sharing class FieldLayoutController {
    /****
    * @description component list
    */ 
    public String componentList {get;set;}
    
    // 24-05-18 Added by Shubham Re PD-3336 get appropriate currency symbol
    /****
    * @description currency symbol
    */ 
    public String symbol {get;set;}
    
    public FieldLayoutController(){
    	String iso = UserInfo.getDefaultCurrency();
    	if (iso=='ALL') { 
    		symbol='Lek'; 
    	} 
    	else if (iso=='USD' || iso=='ARS' || iso=='AUD' || iso=='BSD' || iso=='BBD'|| iso=='CAD') { 
    		symbol='$'; 
    	}
    	else if (iso=='AWG') { 
    		symbol='ƒ'; 
    	}
    	else if (iso=='EUR') { 
    		symbol='€'; 
    	}
    	else if (iso=='JPY') { 
    		symbol='¥'; 
    	}
    	else if (iso=='GBP') { 
    		symbol='£'; 
    	}
    	else{
    		symbol = iso;
    	}
    }
}