/********
FormBuilderRx
@company : Copyright � 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
********/
public with sharing class Portal_ErrorMsgConfigureController { 
    public String curEnvId{get;set;}
    public Portal_Registration_Message__c setting{get;set;}
    
    public Integer assignedUserCount {get;set;}
    public Integer userRemaining {get;set;}
    public Boolean showNotification{get;set;}
    public Boolean showButton{get;set;}
    private Set<Id> userIDList;
    private Env__c curEnv;
    private List<PermissionSet> psl;
    private Integer totalCount; 
    private String profileId;
    private Integer limitOfUsers;        
     /****
    * @description To store Permission error message
    */ 
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
    public String namespacePrefix{
        get{
            namespacePrefix=PortalPackageUtility.getNameSpacePerfix();
            if(String.isBlank(namespacePrefix))
            namespacePrefix ='';
            else
            namespacePrefix=namespacePrefix+'__';

            return namespacePrefix;
        }
        set;
    }

    public Portal_ErrorMsgConfigureController(){ 
        // PD-4014
        showNotification = false;
        showButton = true;
        userIDList = new Set<Id>();
        psl = new List<PermissionSet>();
        limitOfUsers = 100;
        
        assignedUserCount = 0;
        curEnvId = ERx_PortalPackUtil.getCurrentEnvForConfiguration().Env__c;
        curEnv = [SELECT Id,Name,Apply_Permission_Set_to_Existing_Users__c FROM Env__c WHERE Id =:curEnvId LIMIT 1];
        //4014 ends
        System.debug('ggggggggggggggggggggggggg::'+curEnvId);
        List<Portal_Registration_Message__c> settingList;
        /*Below Update_Existing_Application__c field added by Saurabh as per #PD-3294 on 15-02-2018*/
        List<String> listFields = new List<String>{'Account_Name__c','Reg_Contact_Portal_User_Exists_Message__c','Activate_Existing_Contact_Message__c','Activate_Existing_Contact_Via_Email__c','CustomSettingURL__c',
            'Env__c','Incorrect_Username_Password_Message__c','Login_Field_Missing_Message__c','Login_Non_Portal_User_Message__c','Password_Field_Missing_Message__c'
            ,'Password_Non_Portal_User_Exists_Message__c','Password_No_Portal_User_Message__c','Password_Reset_Confirm_Message__c','Reg_Email_Format_Error_Message__c',
            'Reg_Field_Missing_Message__c', 'RegistrationURL__c','Reg_Non_Portal_User_Exists_Message__c','Reg_Password_No_Match_Message__c',
            'Reg_Portal_User_Exists_Message__c','Reg_Username_Too_Long_Message__c', 'SiteLoginTemplateName__c','Student_Portal_User_Profile__c',
            'Update_Existing_Contact__c','Update_Existing_Application__c','Convert_Leads__c','Lead_Non_Convert_Message__c','Lead_Converted_Status__c'};
        if(SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
            if(SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
                settingList = [SELECT Id,Name,Account_Name__c, Reg_Contact_Portal_User_Exists_Message__c,
                Activate_Existing_Contact_Message__c,Activate_Existing_Contact_Via_Email__c,CustomSettingURL__c,
                Env__c,Incorrect_Username_Password_Message__c,  Login_Field_Missing_Message__c,
                Login_Non_Portal_User_Message__c,   Password_Field_Missing_Message__c,Password_Non_Portal_User_Exists_Message__c,
                Password_No_Portal_User_Message__c,Password_Reset_Confirm_Message__c,Reg_Email_Format_Error_Message__c,
                Reg_Field_Missing_Message__c,RegistrationURL__c,Reg_Non_Portal_User_Exists_Message__c,
                Reg_Password_No_Match_Message__c,Reg_Portal_User_Exists_Message__c,Reg_Username_Too_Long_Message__c,
                SiteLoginTemplateName__c,Student_Portal_User_Profile__c,Update_Existing_Contact__c,Update_Existing_Application__c,
                Convert_Leads__c,Lead_Non_Convert_Message__c,Lead_Converted_Status__c
                FROM Portal_Registration_Message__c WHERE Env__c =: curEnvId ];
            } else {
                ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'Portal_ErrorMsgConfigureController', 'Portal_ErrorMsgConfigureController', null);
                throw new PortalPackageException(permissionErrorMessage); 
            }
        } else {
            ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  '), 'Portal_ErrorMsgConfigureController', 'Portal_ErrorMsgConfigureController', null);
            throw new PortalPackageException(permissionErrorMessage); 
        }
        setting = new Portal_Registration_Message__c();
        if(settingList != null && settingList.size() > 0){
            setting = settingList[0];
            /*PD-4014*/
            if(curEnv.Apply_Permission_Set_to_Existing_Users__c){
                showButton = false;
            }
            else{
                profileId = setting.Student_Portal_User_Profile__c;
                totalCount = database.countQuery('SELECT COUNT() FROM User WHERE usertype != '+'\'guest\'' +' AND isActive = TRUE AND Profile.Id = :profileId');
                psl = [SELECT Id From PermissionSet WHERE Label = 'ERxFB_Community' LIMIT 1];
                // get all users assigned with permission set ERxFB_Community
                List<PermissionSetAssignment> psaList = [SELECT Id,AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :psl[0].Id
                                                        LIMIT :limits.getLimitQueryRows()];
                for(PermissionSetAssignment psa : psaList){
                    userIDList.add(psa.AssigneeId);
                }
                List<User> userWithProfileAndPermissionSet = [SELECT Id FROM User WHERE usertype != 'guest' AND isActive = TRUE 
                                                                AND Profile.Id = :profileId
                                                                AND Id IN :userIDList LIMIT :limits.getLimitQueryRows()];                                   
                if(userWithProfileAndPermissionSet != null && userWithProfileAndPermissionSet.size() > 0){
                    assignedUserCount = userWithProfileAndPermissionSet.size();
                    userIDList = new Set<Id>();
                    for(User usr : userWithProfileAndPermissionSet){
                    	userIDList.add(usr.Id);
                    }
                }
            }
            FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
			if(adminSettings != null){
				if(adminSettings.PermissionSet_Assignment_User_Limit__c != null){
					limitOfUsers = adminSettings.PermissionSet_Assignment_User_Limit__c.intValue(); 
				}
			}	
            /*PD-4014 ends*/
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Error Msg Record existed, please create one.'));
        }
    }
    
    public PageReference initMessage(){

        PageReference p = null;

        if(setting.Id == null){
            Portal_Registration_Message__c prm = new Portal_Registration_Message__c();
            prm.Env__c = curEnvId;

            if(SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c', SecurityTypes.IS_INSERT, nameSpacePrefix)){
                List<String> fieldsList = new List<String>{'Account_Name__c','Activate_Existing_Contact_Message__c','Activate_Existing_Contact_Via_Email__c','CustomSettingURL__c','Env__c','Incorrect_Username_Password_Message__c','Login_Field_Missing_Message__c','Login_Non_Portal_User_Message__c','Password_Field_Missing_Message__c','Password_Non_Portal_User_Exists_Message__c','Password_No_Portal_User_Message__c','Password_Reset_Confirm_Message__c','Reg_Email_Format_Error_Message__c','Reg_Field_Missing_Message__c','RegistrationURL__c','Reg_Non_Portal_User_Exists_Message__c','Reg_Password_No_Match_Message__c','Reg_Portal_User_Exists_Message__c','Reg_Username_Too_Long_Message__c','SiteLoginTemplateName__c','Student_Portal_User_Profile__c','Update_Existing_Contact__c','Update_Existing_Application__c','Convert_Leads__c','Lead_Non_Convert_Message__c','Lead_Converted_Status__c'};
                if(SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c', fieldsList, SecurityTypes.IS_INSERT, nameSpacePrefix)) {
                    insert prm;
                }
            }

            //p = new PageReference();
            p = Page.Portal_ErrorMsgConfigure;
            p.setRedirect(true);
            return p;

        }
        
        return p;
        
    }
    
    //11-09-18 Added By SHubham RE PD-4014 
    public void updateExistingUsers(){
        try{    
            if(String.isNotBlank(profileId)){
                // get all users  limit 10000 ,other than userIDList who have not been assigned the permission set  
                system.debug('###@@@totalCount '+totalCount);
                List<User> userList = [SELECT Id FROM User WHERE usertype != 'guest' AND isActive = TRUE AND Profile.Id = :profileId 
                                        AND Id NOT IN :userIDList
                                        LIMIT :limitOfUsers];
                List<PermissionSetAssignment> psaToInsert = new List<PermissionSetAssignment>();
                for(User usr : userList){
                    psaToInsert.add(new PermissionSetAssignment(PermissionSetId = psl[0].Id, AssigneeId = usr.Id));
                    userIDList.add(usr.Id);
                }
                // insert permission set to all these 10000 users
                Integer psaListSize = psaToInsert.size();
                if(psaListSize > 0){
                    insert psaToInsert;
                }
                system.debug('###@@@totalCount '+totalCount);
                assignedUserCount += psaListSize;
                system.debug('###@@assignedUserCount '+assignedUserCount);
                userRemaining = totalCount - assignedUserCount;
                system.debug('###@@userRemaining '+userRemaining);
                if(userRemaining == 0){
                    showButton = false;
                    updateEnvSetting(curEnv.Name);
                }
                else{
                    showButton = true;
                }
                showNotification = true;
            }
        }
        catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,+'Portal_ErrorMsgConfigureController','updateExistingUsers', 'Portal_ErrorMsgConfigureController');
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }
    
    @future
    private static void updateEnvSetting(String envName){
        List<Env__c> envList = [SELECT Id, Apply_Permission_Set_to_Existing_Users__c 
                                                            FROM Env__c WHERE Name = :envName];
        for(Env__c env : envList){
            env.Apply_Permission_Set_to_Existing_Users__c = true;
        }
        update envList;                                             
    }
    
    public pageReference save(){
        PageReference pf;
        try{
            if(setting.Id != null){
                //security review changes
                if(SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c', SecurityTypes.IS_UPDATE, nameSpacePrefix)){
                    update setting;
                } else {
                    ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  '), 'Portal_ErrorMsgConfigureController', 'Portal_ErrorMsgConfigureController', null);
                    throw new PortalPackageException(permissionErrorMessage); 
                }
                //security review changes
            }
            pf = new pagereference('/apex/'+nameSpacePrefix+'Portal_ErrorMsgConfigure');
            pf.setRedirect(true);
            }
            catch (Exception e) {
             ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
             ERx_PortalPackageLoggerHandler.saveExceptionLog();
             pf = null;
            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'test'));
    
            }
            return pf;
    }

    public List<SelectOption> getProfileList(){
        list<SelectOption> returnMe = new list<SelectOption>();
        returnMe.add(new SelectOption('','-- None --'));

        list<Profile> pList = new list<Profile>();
        pList = [select Name, Id from Profile where UserType !='Guest' AND UserType !='Standard' order by Name];
        if(pList.size()>0){
            for(Profile p : pList){
                returnMe.add(new SelectOption(p.Id,p.Name));
            }
        }
        
        return returnMe;
    }
    
    //reterive all lead status list for which IsConverted is true and create select option lisgt
    public List<SelectOption> getLeadStatusList(){
        list<SelectOption> leadConvertStatusSelectOptionList = new list<SelectOption>();
        leadConvertStatusSelectOptionList.add(new SelectOption('','-- None --'));
        list<LeadStatus> leadConvertStatusList = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true limit: limits.getLimitQueryRows()];
        if(leadConvertStatusList.size() > 0){
            for(LeadStatus leadConvertStatus : leadConvertStatusList){
                leadConvertStatusSelectOptionList.add(new SelectOption(leadConvertStatus.MasterLabel,leadConvertStatus.MasterLabel));
            }
        }
        return leadConvertStatusSelectOptionList;
    } 
    
     
     /*******************************************************************************************************
    * @description forces DML to insert log messages in database
    */
    public void initPageException() {
        try {
         ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
         
        } catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }
}