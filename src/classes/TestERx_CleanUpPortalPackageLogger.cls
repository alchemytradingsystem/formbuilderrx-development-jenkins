/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-01-31
	@description TestERx_CleanUpPortalPackageLogger to cover test coverage for ERx_CleanUpPortalPackageLogger
*/
@isTest
private class TestERx_CleanUpPortalPackageLogger {

    static testMethod void testLoggerBatch() {
        Test.startTest();
	    	//insert test data
	    	Portal_Package_Logger__c log = new Portal_Package_Logger__c( Message__c = ('Test record for ERx_CleanUpPortalPackageLogger'), TypeName__c = 'Info');
	    	insert log;
	    	
	    	//Records before execution of batch
	    	List<Portal_Package_Logger__c> lgRecList = [SELECT id FROM Portal_Package_Logger__c LIMIT :limits.getLimitQueryRows()];
	        System.assertEquals(1, lgRecList.size());
	        
	    	String query = 'select id,CreatedDate from Portal_Package_Logger__c LIMIT 1';
	        ERx_CleanUpPortalPackageLogger delBatch = new ERx_CleanUpPortalPackageLogger(query);
			ID batchprocessid = Database.ExecuteBatch(delBatch);
        Test.stopTest();
        
        //Records after execution of batch
        List<Portal_Package_Logger__c> logRecords = [SELECT id FROM Portal_Package_Logger__c LIMIT :limits.getLimitQueryRows()];
        System.assertEquals(0, logRecords.size());
        
        //Testing the email case of batch class
	    List<AsyncApexJob> asyncJob = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob where Id =: batchprocessid LIMIT :limits.getLimitQueryRows()];
	    System.assertEquals(1, asyncJob.size());
	    Erx_EmailLoggerCleanUp.sendMail('Record Clean Up Status: ' + asyncJob[0].Status, 'The batch Apex job processed ' + asyncJob[0].TotalJobItems + ' batches with '+ asyncJob[0].NumberOfErrors + ' failures.', asyncJob[0].CreatedBy.Email);
    }
}