/********
FormBuilderRx
@company : Copyright � 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx 
********/

public class Portal_UserWrapper {
    public String firstName {get;set;}
    public String lastName {get; set;}
    public String email {get;set;}
    public String password {get; set;}
    public String confirmPassword {get; set; }
    public String userName {get; set; }
    public String curEnvId  {get; set; }
    /*Below property added by Saurabh due to check whether we need to update the application field or not on 13-02-2018 #PD-3294*/
    private Boolean isUpdateAppFieldReq = true;
    /****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
    public User u {
        get{
        	//PD-2689 |null check
        	if(u!=null){
        		return u;
        	}
            u = new User();
            u.Username = this.email;
            u.Email = this.email;
            u.FirstName = this.firstName;
            u.LastName = this.lastName;
            u.CommunityNickname = ''+this.email;
            return u;
        }
        set;
    }

    public String namespacePrefix{
        get{
            namespacePrefix=PortalPackageUtility.getNameSpacePerfix();
            if(String.isBlank(namespacePrefix))
            namespacePrefix ='';
            else
            namespacePrefix=namespacePrefix+'__';

            return namespacePrefix;
        }
        set;
    }

    public Contact existContact {get;set;}

    //custom fields list for contact, application
    public list<Portal_CustomField> customFields { get; set; }


    public Portal_UserWrapper() {

    }

    public Portal_UserWrapper(Id eId) {
        //Env__c cEnv = ERx_PortalPackUtil.getCurrentEnv();
        //system.debug('!!!!!!!!!!!!'+SecurityEnforceUtility.checkObjectAccess('SiteLoginTemplate__c', SecurityTypes.IS_DELETE, ''));


        //curEnvId = null;
        if(eId != null){
            curEnvId = eId;
        }
        if(curEnvId != null){
            list<Portal_Login_Custom_Field__c> fieldListSetting;
            // PD-1966 Portal Registration: rearrange order of all fields
            List<String> listFields = new List<String>{'Children_Index_Field_API__c','Children_Field_Label__c','Children_Objects_API__c','Display_Order__c','Field_API_Name__c',
            'Is_Field_Required__c','Linked_to_Portal__c','Look_Up_Field__c','Object_Name__c','Grand_Children_Field_API_Name__c','Grand_Children_Field_Label__c','Grand_Children_Index_Field_API__c','Grand_Children_Objects_API__c','Active__c','Children_Field_API_Name__c','queryCriteria__c','childQueryCriteria__c','gdChildQueryCriteria__c','Env__c','Display_Order__c','Active__c'};
            if(SecurityEnforceUtility.checkObjectAccess('Portal_Login_Custom_Field__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
                if(SecurityEnforceUtility.checkFieldAccess('Portal_Login_Custom_Field__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
                    fieldListSetting = [SELECT Name, Children_Index_Field_API__c, Children_Field_Label__c,Children_Objects_API__c,Display_Order__c,Field_API_Name__c,Is_Field_Required__c,Linked_to_Portal__c,Look_Up_Field__c,Object_Name__c,Grand_Children_Field_API_Name__c,Grand_Children_Field_Label__c,Grand_Children_Index_Field_API__c,Grand_Children_Objects_API__c,Active__c,Children_Field_API_Name__c,queryCriteria__c,childQueryCriteria__c,gdChildQueryCriteria__c,Env__c  FROM Portal_Login_Custom_Field__c WHERE Active__c=true AND Env__c=: curEnvId order by Display_Order__c];
                    System.debug('=========------->'+fieldListSetting.size());
                    if(fieldListSetting != null && fieldListSetting.size() > 0){
                        customFields = new list<Portal_CustomField>();
                        for(Portal_Login_Custom_Field__c p :fieldListSetting){
                            System.debug('hhhhhhhhhhhhhhhhhhhh{complete');
                            customFields.add(new Portal_CustomField(p));
                        }
                    }
                } else {
                    ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Login_Custom_Field__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'Portal_UserWrapper', 'Portal_UserWrapper', null);
                    throw new PortalPackageException(permissionErrorMessage); 
                }
            } else {
                ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Login_Custom_Field__c '), 'Portal_UserWrapper', 'Portal_UserWrapper', null);
                throw new PortalPackageException(permissionErrorMessage); 
            }
            
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No environment is selected.'));
        }
    }

    //if password match
    public boolean isValidPassword() {
        return String.valueOf(this.password).equals(String.valueOf(this.confirmPassword));
    }

    //if password length should be > 8
    public boolean isValidPasswordLength() {
        return String.valueOf(this.password).length() >= 8;
    }

    //if all fields are filled
    public boolean isValidInputReg() {
        return (this.firstName != null && this.lastName != null && this.email != null && this.password != null && this.confirmPassword != null && this.firstName != '' && this.lastName != '' && this.email != '' && this.password != '' && this.confirmPassword != '');
    }

    //if all fields are filled for login
    public boolean isValidInputLogin() {
        return (this.email != null && this.password != null && this.email != '' && this.password != '');
    }

    //if all fields are filled for login
    public boolean isValidInputPassword() {
        return (this.email != null && this.email != '');
    }

    //validate email format
    public Boolean validateEmailFormat() {
        Boolean res = true;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);

        if (!MyMatcher.matches())
            res = false;
        return res;
    }

    //check existng user
    public User existingUser(){
        list<User> ul = [SELECT Id, ProfileId,Email 
        					FROM User 
        					WHERE Username = : this.email];
        User existUser;
        if(ul.size() > 0){
            existUser = ul.get(0);
        }
        return existUser;
    }
    
    /*05-09-18 Added By Shubham Re PD-2499*/
    //check existng user for a contact
    public User existingUserForContact(Id conId){
        list<User> ul = [SELECT Id, ContactId FROM User WHERE ContactId = :conId];
        User existUser;
        if(ul.size() > 0){
            existUser = ul.get(0);
        }
        return existUser;
    }

    //check existing Contact
    public Contact existingContact(){
    	existContact = null;
        List<String> listFields = new List<String>{'AccountId','Email'};
        List<Contact> cl;
        if(SecurityEnforceUtility.checkObjectAccess('Contact', SecurityTypes.IS_ACCESS, namespacePrefix)){
            if(SecurityEnforceUtility.checkFieldAccess('Contact', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
                cl = [Select Id,AccountId,Email,FirstName,LastName from Contact where Email = : this.email 
                		and FirstName = : this.firstName and LastName = : this.lastName ];
                system.debug('Anydatatype_msg++++++++'+cl);
            } else {
                ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Contact  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'Portal_UserWrapper', 'existingContact', null);
                throw new PortalPackageException(permissionErrorMessage); 
            }
        } else {
            ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Contact'), 'Portal_UserWrapper', 'existingContact', null);
            throw new PortalPackageException(permissionErrorMessage); 
            
        }
        //Contact existContact;
        if(cl.size() > 0){
            existContact = cl.get(0);
			/*Below line is added by Saurabh due to PD-3294 on 13-02-2018 to set the value false in case of contact exists without portal user*/
			isUpdateAppFieldReq = false;
            return existContact;
        }else if(cl.size() == 0){
        	// 14-06-18 Added By Shubham Re PD-2202
        	// get contact deduplication condition and criteria for current env
        	Env__c curEnv = [SELECT Id,Name,deduplication_criteria__c,deduplication_condition__c FROM Env__c WHERE Id =: curEnvId];
            // get deserialized list of contact condition
            List<ERx_PageEntities.SerializeDeDuplicationDataModel> contactDeduplicationConditionList = new List<ERx_PageEntities.SerializeDeDuplicationDataModel>();
            if(curEnv != null && curEnv.deduplication_condition__c != null){
            	contactDeduplicationConditionList = (List<ERx_PageEntities.SerializeDeDuplicationDataModel>) JSON.deserialize(curEnv.deduplication_condition__c, List<ERx_PageEntities.SerializeDeDuplicationDataModel>.class);
            }
            // get deserialized list of contact criteria
            List<ERx_PageEntities.FilterModel> contactDeduplicationCriteriaList = new List<ERx_PageEntities.FilterModel>();
            if(curEnv != null && curEnv.deduplication_criteria__c != null){
            	contactDeduplicationCriteriaList = (List<ERx_PageEntities.FilterModel>) JSON.deserialize(curEnv.deduplication_criteria__c, List<ERx_PageEntities.FilterModel>.class);
            }
            String queryField ='';
	        List<String> conditionList = PortalPackageHelper.getConditionListDDM(contactDeduplicationConditionList,this.firstName,this.lastName,this.email,customFields);
            for(ERx_PageEntities.SerializeDeDuplicationDataModel condition : contactDeduplicationConditionList){
        		if(condition.fieldType.equalsIgnoreCase('reference')){
            		String fname = PortalPackageHelper.getReferenceNameField(condition.targetValue,condition.lookupField);
            		if(!queryField.containsIgnoreCase(fname)){
            			queryField += fname+',';	
            		}
            	}
            	else{
            		if(!queryField.containsIgnoreCase(condition.targetValue)){
            			queryField += condition.targetValue+',';
            		}
            	}	
	        }
	        // check if contact matches any criteria
            Boolean isMatchFound = false;
            for(ERx_PageEntities.FilterModel criteria : contactDeduplicationCriteriaList){
            	criteria.filter = PortalPackageHelper.getQueryFilterDDM(criteria.filter);
            	String filter = ERx_PortalPackUtil.generateFinalConditionExpression(criteria.filter,conditionList,true);
	            queryField = queryField.removeEnd(',');
	            if(!queryField.containsIgnoreCase('FirstName')){
	            	queryField += ',Firstname';
	            }
	            if(!queryField.containsIgnoreCase('LastName')){
	            	queryField += ',Lastname';
	            }
	            if(!queryField.containsIgnoreCase('Email')){ 
	            	queryField += ',email';
	            }
	            if(!queryField.containsIgnoreCase('AccountId')){
	            	queryField += ',AccountId';
	            }
	            system.debug('Select '+queryField+' from Contact where '+filter+' Limit 1');
	            cl = Database.query('Select '+queryField+' from Contact where '+filter+' Limit 1'); 
	            if(criteria.filterOption.equalsIgnoreCase('message')){
	            	if(cl.size() > 0){
	            		throw new PortalPackageException(criteria.message);
	            	}
	            }
	            else{
	            	if(cl.size() > 0){
	            		//throw new PortalPackageException('Select '+queryField+' from Contact where '+filter);
	            		isMatchFound = true;
	            		break;
	            	}
	            }
	        }
            
            system.debug('cl '+cl+' isMatchFound '+isMatchFound);
            if(isMatchFound){ 
	            existContact = cl.get(0);
				/*Below line is added by Saurabh due to PD-3294 on 13-02-2018 to set the value false in case of contact exists without portal user*/
				isUpdateAppFieldReq = false;
	            return existContact;
	        }
	        else{
	            List<String> listFieldLead = new List<String>{'Email','LastName','FirstName','IsConverted'};
	            List<Lead> ll;
	            if(SecurityEnforceUtility.checkObjectAccess('Lead', SecurityTypes.IS_ACCESS, namespacePrefix)){
	                if(SecurityEnforceUtility.checkFieldAccess('Lead', listFieldLead, SecurityTypes.IS_ACCESS, namespacePrefix)){
	                    //KB: Only fetch those leads which are not converted beacause contact is not found.
	                    ll = [Select Id,FirstName,Email,LastName,IsConverted from Lead where Email = : this.email AND LastName = : this.lastName 
	                    AND FirstName = : this.firstName AND IsConverted = false order by CreatedDate DESC Limit 1];
	                    Lead existLead;
	                    //Lead exists with no contact
	                    if(ll.size() > 0){
	                        existLead = ll.get(0);
	                		List<String> messageFieldList = new List<String>{'Convert_Leads__c','Lead_Converted_Status__c','Lead_Non_Convert_Message__c'};
	                		//apply secruity check
	                		if(SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
								if(SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c', messageFieldList, SecurityTypes.IS_ACCESS, namespacePrefix)){
									List<Portal_Registration_Message__c> currentEnvMsgList = [SELECT Convert_Leads__c,Lead_Converted_Status__c,Lead_Non_Convert_Message__c FROM Portal_Registration_Message__c WHERE Env__c =: curEnvId ];		
									if(currentEnvMsgList.size() > 0){
										//Manish: FB Package -> Lead conversion 
										//if Convert_Leads__c true from admin panel then convert lead to contact on given converted status else show message to user.
										if(currentEnvMsgList[0].Convert_Leads__c){
					                        Database.LeadConvert lc = new database.LeadConvert();
					                        lc.setLeadId(existLead.id);
					                        lc.ConvertedStatus = currentEnvMsgList[0].Lead_Converted_Status__c;
					                        system.debug('lc here '+lc);
					                        Database.LeadConvertResult lcr = Database.convertLead(lc);          
											Account convertedAccount = new Account();
											convertedAccount.id = lcr.getAccountId();
											convertedAccount.Name = existLead.LastName + ' Administrative Account';
											update convertedAccount;
											//update existLead; 
					                        existContact = existingContact();	
										}else{
											ERx_PortalPackageLoggerHandler.addInformation(this.email + ' ' + currentEnvMsgList[0].Lead_Non_Convert_Message__c,'Portal_UserWrapper','existingContact','Portal_Register');
					                        ERx_PortalPackageLoggerHandler.saveExceptionLog();
					                        throw new PortalPackageException(String.isBlank(currentEnvMsgList[0].Lead_Non_Convert_Message__c) ? PortalPackageConstants.LEAD_CONFIG_ERROR_MESSAGE : currentEnvMsgList[0].Lead_Non_Convert_Message__c);
										}
									}else{
										ERx_PortalPackageLoggerHandler.addInformation(this.email + ' ' + PortalPackageConstants.LEAD_CONFIG_ERROR_MESSAGE,'Portal_UserWrapper','existingContact','Portal_Register');
				                        ERx_PortalPackageLoggerHandler.saveExceptionLog();
				                        throw new PortalPackageException(PortalPackageConstants.LEAD_CONFIG_ERROR_MESSAGE);
									}							
								}else {
					    			ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(messageFieldList, ', ')), 'Portal_UserWrapper', 'Portal_UserWrapper', null);
									throw new PortalPackageException(permissionErrorMessage); 
								}
	                		}else {
					    		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  '), 'Portal_UserWrapper', 'Portal_UserWrapper', null);
								throw new PortalPackageException(permissionErrorMessage); 
							}
	                    }
	                    else if(ll.size() == 0){
	                    	// get lead deduplication condition and criteria
	                    	Env__c env = [SELECT Id,Name,lead_deduplication_criteria__c,lead_deduplication_condition__c FROM Env__c WHERE Id =: curEnvId];
				            // get deserailaized lead condition
				            List<ERx_PageEntities.SerializeDeDuplicationDataModel> leadDeduplicationConditionList = new List<ERx_PageEntities.SerializeDeDuplicationDataModel>();
				            if(env != null && env.lead_deduplication_condition__c != null){
				            	leadDeduplicationConditionList = (List<ERx_PageEntities.SerializeDeDuplicationDataModel>) JSON.deserialize(env.lead_deduplication_condition__c, List<ERx_PageEntities.SerializeDeDuplicationDataModel>.class);
				            }
				            // get deserealized lead criteria
				            List<ERx_PageEntities.FilterModel> leadDeduplicationCriteriaList = new List<ERx_PageEntities.FilterModel>();
				            if(env != null && env.lead_deduplication_criteria__c != null){
				            	leadDeduplicationCriteriaList = (List<ERx_PageEntities.FilterModel>) JSON.deserialize(env.lead_deduplication_criteria__c, List<ERx_PageEntities.FilterModel>.class);
				            }
				            
				            queryField ='';
					        conditionList = PortalPackageHelper.getConditionListDDM(leadDeduplicationConditionList,this.firstName,this.lastName,this.email,customFields); 
				            for(ERx_PageEntities.SerializeDeDuplicationDataModel condition : leadDeduplicationConditionList){
				        		if(condition.fieldType.equalsIgnoreCase('reference')){
				            		String fname = PortalPackageHelper.getReferenceNameField(condition.targetValue,condition.lookupField);
				            		if(!queryField.containsIgnoreCase(fname)){
				            			queryField += fname+',';
				            		}
				            	}
				            	else{
				            		if(!queryField.containsIgnoreCase(condition.targetValue)){
				            			queryField += condition.targetValue+',';
				            		}
				            	}	
					        }
					        
				            for(ERx_PageEntities.FilterModel criteria : leadDeduplicationCriteriaList){
					            criteria.filter = PortalPackageHelper.getQueryFilterDDM(criteria.filter);
					            String filter = ERx_PortalPackUtil.generateFinalConditionExpression(criteria.filter,conditionList,true);
					            queryField = queryField.removeEnd(',');
					            if(!queryField.containsIgnoreCase('lastname')){
					            	queryField += ',LastName';
					            }
					            System.debug('#*##Select '+queryField+' from Lead where ( IsConverted = false ) AND ('+filter+')');
					            ll = Database.query('Select '+queryField+' from Lead where ( IsConverted = false ) AND ('+filter+') Limit 1');
					            if(criteria.filterOption.equalsIgnoreCase('message')){
					            	if(ll.size() > 0){
					            		throw new PortalPackageException(criteria.message);
					            	}
					            }
					            else{
					            	System.debug('ll there '+ll);
					            	if(ll.size() > 0){
					            		existLead = ll.get(0);
				                		List<String> messageFieldList = new List<String>{'Convert_Leads__c','Lead_Converted_Status__c','Lead_Non_Convert_Message__c'};
				                		//apply secruity check
				                		if(SecurityEnforceUtility.checkObjectAccess('Portal_Registration_Message__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
											if(SecurityEnforceUtility.checkFieldAccess('Portal_Registration_Message__c', messageFieldList, SecurityTypes.IS_ACCESS, namespacePrefix)){
												List<Portal_Registration_Message__c> currentEnvMsgList = [SELECT Convert_Leads__c,Lead_Converted_Status__c,Lead_Non_Convert_Message__c FROM Portal_Registration_Message__c WHERE Env__c =: curEnvId ];		
												if(currentEnvMsgList.size() > 0){
													if(currentEnvMsgList[0].Convert_Leads__c){
								                        Database.LeadConvert lc = new database.LeadConvert();
								                        lc.setLeadId(existLead.id);
								                        lc.ConvertedStatus = currentEnvMsgList[0].Lead_Converted_Status__c;
								                        system.debug('lc '+lc);
								                        Database.LeadConvertResult lcr = Database.convertLead(lc);         
								                        Account convertedAccount = new Account();
														convertedAccount.id = lcr.getAccountId();
														convertedAccount.Name = existLead.LastName + ' Administrative Account';
														update convertedAccount;
														Id conId = lcr.getContactId();
														List<Contact> conList = [Select Id,FirstName,LastName,Email,AccountId from Contact where Id = :conId Limit 1];
														existContact = conList[0];
														/*to set the value false in case of contact exists without portal user*/
														isUpdateAppFieldReq = false;
														break;	
													}else{
														ERx_PortalPackageLoggerHandler.addInformation(this.email + ' ' + currentEnvMsgList[0].Lead_Non_Convert_Message__c,'Portal_UserWrapper','existingContact','Portal_Register');
								                        ERx_PortalPackageLoggerHandler.saveExceptionLog();
								                        throw new PortalPackageException(String.isBlank(currentEnvMsgList[0].Lead_Non_Convert_Message__c) ? PortalPackageConstants.LEAD_CONFIG_ERROR_MESSAGE : currentEnvMsgList[0].Lead_Non_Convert_Message__c);
													}
												}else{
													ERx_PortalPackageLoggerHandler.addInformation(this.email + ' ' + PortalPackageConstants.LEAD_CONFIG_ERROR_MESSAGE,'Portal_UserWrapper','existingContact','Portal_Register');
							                        ERx_PortalPackageLoggerHandler.saveExceptionLog();
							                        throw new PortalPackageException(PortalPackageConstants.LEAD_CONFIG_ERROR_MESSAGE);
												}							
											}else {
								    			ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(messageFieldList, ', ')), 'Portal_UserWrapper', 'Portal_UserWrapper', null);
												throw new PortalPackageException(permissionErrorMessage); 
											}
				                		}else {
								    		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Registration_Message__c  '), 'Portal_UserWrapper', 'Portal_UserWrapper', null);
											throw new PortalPackageException(permissionErrorMessage); 
										}
					            	}
					            }
					        }
				            system.debug('ll '+ll);
	                    }
	                } else {
	                    ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Lead  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'Portal_UserWrapper', 'existingContact', null);
	                    throw new PortalPackageException(permissionErrorMessage); 
	                }
	            } else {
	                ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Lead'), 'Portal_UserWrapper', 'existingContact', null);
	                throw new PortalPackageException(permissionErrorMessage); 
	            }
	        }
        }
        system.debug('existContact '+existContact);
        return existContact;
    }

    //search for core package lead convert field
    public void convertLeadInCore(Lead coreLead){
        Set<String> objectFields = Schema.SObjectType.Lead.fields.getMap().keySet();
        if(objectFields.contains('EnrollmentrxRx__Convert_Lead__c')){
            coreLead.put('EnrollmentrxRx__Convert_Lead__c',true);
        }
    }

    //update existing contact
    public void updateExistingContact(){
        if(existContact != null){
            existContact.FirstName = this.firstName;
            existContact.LastName = this.lastName;
            existContact.Email = this.email;
            update existContact;
        }
    }

    public void activateExistingContact(){
        if(existContact != null){
            existContact.Create_Portal_User__c = true;
            update existContact;
        }
    }

    /******************************************************************************************
                save registration - processing extra contact/application fields
                function input : extra front-end fields(except five login fields) and contact
                function output: update those extra fields to corresponding sobject records
                Change Function Agument Object to List to Solve Bulkify Apex Methods Using Collections In Methods
    ******************************************************************************************/
    public void processingExtraFields(List<Contact> contactList, Boolean isUpdateExistingAppFields){
        if(contactList != null && contactList.size() > 0) {
            Contact ct = contactList[0];
            // assign values to record
            Set<String> contactFieldSet = new Set<String>{'firstname', 'lastname', 'email', 'accountid'};
            if(ct != null && customFields != null){
                for(Portal_CustomField x : customFields){
                    if(x != null && x.objectName != null && (x.objectName.tolowerCase().equals('contact'))){
                        contactFieldSet.add(x.fieldname.toLowerCase());
                        if (x.fldType == 'String' || x.fldType == 'Picklist'|| x.fldType == 'Phone'){
                         	if(x.inputText != null){   
                            	ct.put(x.fieldname, x.inputText);
                         	}
                        }
                        if (x.fldType == 'Multipicklist'){ 
                            if(x.inputTextList != null){
                            	ct.put(x.fieldname, x.inputTextList);
                            }
                        }
                        if (x.fldType == 'Number'){ 
                            if(x.inputNumber != null){
                            	ct.put(x.fieldname, x.inputNumber);
                            }
                        }
                        if (x.fldType == 'Decimal'){ 
                        	if(x.inputDecimal != null){
                            	ct.put(x.fieldname, x.inputDecimal);
                        	}
                        }
                        if (x.fldType == 'Checkbox') {
                            if(x.inputCheckbox != null){
                            	ct.put(x.fieldname, x.inputCheckbox);
                            }
                        }
                        if (x.fldType == 'Date'){
                            if(x.inputDate != null)
                                ct.put(x.fieldname, Date.parse(x.inputDate.trim()));
                        }

                        System.debug('------------------------------>'+x.fieldName + '<--------------'+x.inputText);
                        if (x.inputText != null && x.fldType == 'reference' ) 
                            ct.put(x.fieldname, x.inputText);
                        // find app.school_c.child__c field, then assign with x.childrenInputText
                        if (x.hasChildren && x.childrenInputText != null && x.fldType == 'reference') {
                            contactFieldSet.add(x.childrenFieldAPI.toLowerCase());
                            ct.put(x.childrenFieldAPI, x.childrenInputText);
                        }
                        // find app.school__c.grandChild__c field, then assign with x.grandCHild
                        if (x.hasGrandChildren && x.grandChildrenInputText != null && x.fldType == 'reference') {
                            contactFieldSet.add(x.grandChildrenFieldAPI.toLowerCase());
                            ct.put(x.grandChildrenFieldAPI,x.grandChildrenInputText);
                        }
                    }
                }

                /**********************************/
                /**********************************/
                /**********************************/
                /**********************************/
                /**********************************/
                /**********************************/
                /**********************************/
                List<Contact> upsertContactList = new List<Contact>();
                upsertContactList.add(ct);
                // Checking only insert permission for contact for site user. Editable is false for site guest user.
                if(SecurityEnforceUtility.checkObjectAccess('Contact', SecurityTypes.IS_INSERT, namespacePrefix)){
                    if(SecurityEnforceUtility.checkFieldAccess('Contact', new list<String>(contactFieldSet), SecurityTypes.IS_INSERT, namespacePrefix)) {
                        upsert upsertContactList;
                    } else {
                        ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::' + 'Contact' +' FieldList :: ' + PortalPackageUtility.convertListBySeperator(new list<String>(contactFieldSet), ', ')), 'Portal_UserWrapper', 'processingExtraFields', null);
                        throw new PortalPackageException(permissionErrorMessage);
                    }
                } else {
                    ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Contact '), 'Portal_UserWrapper', 'processingExtraFields', null);
                    throw new PortalPackageException(permissionErrorMessage);
                }
                
                processingExtraFieldsForApp(upsertContactList, isUpdateExistingAppFields);
            }
        }
    }

    /*************************************************************************
        @parameters: List<contact>
        @function: update related app fields which get from front-end page
        Change Function Agument Object to List to Solve Bulkify Apex Methods Using Collections In Methods
    **************************************************************************/
    public void processingExtraFieldsForApp(List<Contact> contactList, Boolean isUpdateExistingAppFields){
        if(contactList != null && contactList.size() > 0) {
            Contact ct = contactList[0];
            List<Sobject> appList; // for applist related with contact
            String sqlStr = ''; // for app query
            List<Portal_CustomField> appFields = new List<Portal_CustomField>(); // app fields
            Set<String> setFields = new Set<String>();
            //get relate app if it is existed

            for(Portal_CustomField p : customFields){
                if(p.objectName.trim().tolowerCase().equals('enrollmentrxrx__enrollment_opportunity__c')){
                    sqlStr += p.fieldName;
                    sqlStr += ',';
                    appFields.add(p);
                    setFields.add(p.fieldName);
                }
            }
			
            if(appFields != null && appFields.size() > 0 && ct.Id != null){
                sqlStr = sqlStr.subString(0, sqlStr.length() - 1);
                if((String)ct.get('EnrollmentrxRx__Active_Enrollment_Opportunity__c') != null){
                    setFields.add('EnrollmentrxRx__Active_Enrollment_Opportunity__c');
                    string activeAppId = (String)ct.get('EnrollmentrxRx__Active_Enrollment_Opportunity__c');
                    activeAppId = String.escapeSingleQuotes(activeAppId);
                    sqlStr ='SELECT '+sqlStr+ ' FROM EnrollmentrxRx__Enrollment_Opportunity__c' +' WHERE Id = '+ '\''+ activeAppId+'\'';
                    
                }
                else{
                    string ctId = ct.Id;
                    ctId = String.escapeSingleQuotes(ctId);
                    sqlStr ='SELECT '+sqlStr+ ' FROM EnrollmentrxRx__Enrollment_Opportunity__c' +' WHERE EnrollmentrxRx__Applicant__c = '+ '\''+ ctId+'\' LIMIT 1';
                    
                }
                //sqlStr = String.escapeSingleQuotes(sqlStr);
                List<String> listFields = new List<String>();
                listFields.addAll(setFields);
                if(SecurityEnforceUtility.checkObjectAccess('EnrollmentrxRx__Enrollment_Opportunity__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
                    if(SecurityEnforceUtility.checkFieldAccess('EnrollmentrxRx__Enrollment_Opportunity__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
                        appList = Database.query(sqlStr);
                    } else {
                        ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: EnrollmentrxRx__Enrollment_Opportunity__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'TM_TemplateContentController', 'processingExtraFieldsForApp', null);
                        throw new PortalPackageException(permissionErrorMessage); 
                    }
                } else {
                    ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: EnrollmentrxRx__Enrollment_Opportunity__c'), 'TM_TemplateContentController', 'processingExtraFieldsForApp', null);
                    throw new PortalPackageException(permissionErrorMessage); 
                }
            }
            //get exposed app and assign value to record
            setFields = new Set<String>();
            if(appList != null && appList.size() > 0 ){
                for(Sobject app0 : appList){
                    for(Portal_CustomField x : appFields){
                        //standard fields
                        setFields.add(x.fieldname.toLowerCase());
                        if (x.fldType == 'String' || x.fldType == 'Picklist'|| x.fldType == 'Phone')
                        app0.put(x.fieldname, x.inputText);
                        if (x.fldType == 'Multipicklist') app0.put(x.fieldname, x.inputTextList);
                        if (x.fldType == 'Number') app0.put(x.fieldname, x.inputNumber);
                        if (x.fldType == 'Decimal') app0.put(x.fieldname, x.inputDecimal);
                        if (x.fldType == 'Checkbox') app0.put(x.fieldname, x.inputCheckbox);
                        if (x.fldType == 'Date') app0.put(x.fieldname, Date.parse(x.inputDate.trim()));

                        //referece number is not static variable, using loop
                        if (x.inputText != null && x.fldType == 'reference' ) app0.put(x.fieldname, x.inputText);
                        // find app.school_c.child__c field, then assign with x.childrenInputText
                        if (x.hasChildren && x.childrenInputText != null && x.fldType == 'reference') {
                            setFields.add(x.childrenFieldAPI.toLowerCase());
                            app0.put(x.childrenFieldAPI, x.childrenInputText);
                        }
                        // find app.school__c.grandChild__c field, then assign with x.grandCHild
                        if (x.hasGrandChildren && x.grandChildrenInputText != null && x.fldType == 'reference') {
                            setFields.add(x.grandChildrenFieldAPI.toLowerCase());
                            app0.put(x.grandChildrenFieldAPI,x.grandChildrenInputText);
                        }
                    }
                }
                if(SecurityEnforceUtility.checkObjectAccess('EnrollmentrxRx__Enrollment_Opportunity__c', SecurityTypes.IS_UPDATE, nameSpacePrefix)){
                    if(SecurityEnforceUtility.checkFieldAccess('EnrollmentrxRx__Enrollment_Opportunity__c', new list<String>(setFields), SecurityTypes.IS_UPDATE, namespacePrefix)) {
                        /*Below If case added by Saurabh to check if application field is need to update in case of first time contact creation and setting is set to true*/
                        if(isUpdateExistingAppFields || isUpdateAppFieldReq){ 
                        	update appList;	
                        }
                    } else {
                        ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::' + 'EnrollmentrxRx__Enrollment_Opportunity__c' +' FieldList :: ' + PortalPackageUtility.convertListBySeperator(new list<String>(setFields), ', ')), 'Portal_UserWrapper', 'processingExtraFieldsForApp', null);
                        throw new PortalPackageException(permissionErrorMessage);
                    }
                } else {
                    ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: EnrollmentrxRx__Enrollment_Opportunity__c'), 'TM_TemplateContentController', 'processingExtraFieldsForApp', null);
                    throw new PortalPackageException(permissionErrorMessage);
                }
            }
        }
    }
    
     /*******************************************************************************************************
    * @description forces DML to insert log messages in database
    */
    public void initPageException() {
        try {
         ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
         
        } catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,'PageBuildDirector','initPageException', null);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }

}