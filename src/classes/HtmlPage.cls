/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Page Properties
	
*/
public with sharing class HtmlPage {
	/****
    * @description model fields json
    */
    public String jsonModalFields {get;set;}
    /****
    * @description view json
    */
    public String jsonViewlayout {get;set;}
    /****
    * @description page button json
    */
    public String buttonJSON {get;set;}
 	/****
    * @description platform is desktop or not
    */
    public Boolean isDesktop {get;set;}
    /****
    * @description page is authorized or not
    */
    public Boolean isAuthorized{get;set;}
  	/****
    * @description page error message
    */
    public String errorMessage{get;set;} 
    /****
    * @description current page id
    */
    public String currentPageId {get;set;}
    /****
    * @description next page id
    */
    public String nextPageId {get;set;}
    /****
    * @description previous page id
    */
    public String prevPageId {get;set;}
 	/****
    * @description next section id
    */
    public String nextSectionId {get;set;}
    /****
    * @description previous section id
    */
    public String prevSectionId {get;set;}
 	/****
    * @description next section exist or not
    */
    public Boolean isNextSection {get;set;}
    /****
    * @description previous section exist or not
    */
    public Boolean isPrevSection {get;set;}
     /****
    * @description next page exist or not
    */
    public Boolean isNextPage {get;set;}
 	/****
    * @description previous page exist or not
    */
    public Boolean isPrevPage {get;set;}
    
    /****
    * @description attachment map json
    */
    public String attachmentMap {get;set;}
    /****
    * @description param map json
    */
    public String paramMap {get;set;}
    /****
    * @description pagewrapper model list
    */
    public List<PageWrapperModel> pageList {get;set;}
    public String pageListJSON {get;set;}
    //public Map<String, Map<String, List<String>>> mapOfOptionList {get;set;}
    //public String jsonOptionListMap {get;set;}
    
 	/****
    * @description helper icon url
    */
    public String helperIcon {get;set;}
    /****
    * @description model name and object api name map
    */
    public Map<String, String> modelObjectMap {get;set;} //map of model and object
    /****
    * @description page parameter name and parameter value map
    */
    public Map<String, String> modelPageUrlParameters {get;set;} //map of model and object
    public String modelPageUrlParametersJSON {get;set;} 
    /****
    * @description model name and object api name map json
    */
    public String modelObjectMapJSON {get;set;}
    /*
    * @description model condition json 
    */
    public String modelConditionMap {get;set;} 
    
    /*
    * @description model fields json
    */
    public String modelFieldsMap {get;set;}
    
    /****
    * @description page is custom or not
    */
    public Boolean isCustomPage {get;set;}
    /****
    * @description custom page name
    */
    public String customPageName {get;set;}
    /****
    * @description is custom header and footer
    */
    public Boolean isCustomHeaderFooter{get;set;}
 	/****
    * @description env error message
    */
    public String environmentErrorMessage{get; set;}
    
    //Depending upon the model criteria while fetching records, some fields can be autopopulated. for eg. While fetching all test records for current application object, 
    //we need to populate Application__c field on test record object. And a field can be populated if that field is editable and in criteria having equal operator.
    /****
    * @description key => modelName, value = map 
    */
    public Map<String, Map<String, String>> fieldToPopulateMap {get;set;} 
    /****
    * @description key => modelName, value = if add another record is allowed or not. 
    */
    public Map<String, Boolean> isAddAnotherAllowedMap {get;set;} 
 	/****
    * @description json of of field and respective value to be populated.
    */
    public String fieldToPopulateMapJSON {get;set;}
    /****
    * @description json of of dd another record is allowed or not. 
    */
    public String isAddAnotherAllowedMapJSON {get;set;}
    
	/****
    * @description page is disable or not 
    */
    public boolean isPageDisable {get;set;}
    /****
    * PD-2819 @description navigation is hidden or not 
    */
    public boolean isHideNavigation {get;set;}
    /****
    * @description page is accessable or not
    */
    public boolean isUnAutorizedPageAccess {get;set;} 
    
    /****
    * @description use to store type of Env either community or Site.
    */
    public String envType{get; set;}
    
    /****
    * @description use to store Id of Current env.
    */
    public String currentEnvId{get; set;}
    
     /****
    * @description use to store Id of Current env.
    */
    public String currentEnvName{get; set;}
     /****
    * @description use to store status of Current env.
    */
    public String currentEnvStatus{get; set;}
    
    /****
    * @description use to store the Page Complition JSON
    */
    public String pageCompletionJSON{get; set;}
    
    /****
    * @description use to store the boolean value for Page Completion
    */
    public boolean isPageCompletion{get; set;}
    
    /****
    * @description use to store the boolean value for page dependent check.
    */
    public boolean isPageDependent{get; set;}
     /****
    * @description use to store the boolean value for page Redirect check.
    */
    public boolean isPageRedirect{get; set;}
     /****
    * @description use to store the boolean value for page Redirect check.
    */
    public String redirectURL{get; set;}
    
    /****
    * @description use to store the page dependent message.
    */ 
    public String pageDependentMessage{get; set;}
    /****
    * @description use to store is Submit Show.
    */ 
    public boolean isSubmitShow {get;set;}	
    /****
    * @description use current pageid
    */ 
    public String pageId {get;set;}
    /****
    * @description use current page order
    */ 
    public Decimal pageOrder {get;set;}
  	/****
  	 * @description use current pageid
    */ 
    public String pageName {get;set;}
  	/****
    * @description isCheckListShow
    */
    public boolean isCheckListShow {get;set;}
    /****
    * @description sectionErrorMessage
    */
    public String sectionErrorMessage {get;set;}
    /*
    * @description modelName With fields
    */
    public String modelNameFieldList{get;set;}
    /*
    * @description holds component names to be rendered.
    */
    public String usedComponentsOnPage{get;set;}
    /*
    * @description holds page Header value from ADMIN PANEL
    */
    public String pageHeaderAdminPanel {get;set;}
    /*
    * @description holds Env UrlPathPrefixValue
    */
    public String urlPathPrefix {get;set;}
    /*
    * @description holds PageIndexing Type
    */
    public String pageTabIndexing {get;set;}
    /*
    * @description holds Error Message Location # PD-1515
    */
    public String errorMessageLocation {get;set;}
     /*
    * @description holds permissioln to allow force update # PD-1083
    */
    public Boolean allowForceUpdate {get;set;}
    /*
    * @description holds browser tab title for Env # PD-1739
    */
    public String browserTabTitleEnv {get;set;}
    
    /*
    * @description If next page is custom then it contain that page name
    */
    public String nextPageName {get;set;}
    /*
    * @description If pervious page is custom then it contain that page name
    */
    public String previousPageName {get;set;}
     /*
    * @description if redirect condition satisfied then custom page name is set
    */
    public String redirectPageName {get;set;}
    
    public Boolean isSessionExpired {get;set;}
    /*
    * @description contains error message of deleted field
    */
    public String errorMessageFields{get;set;}
    
    public String addAnotherDependentModelsRecordMappingDetails {get;set;}
    
    public String addAnotherDependentModelNamesList {get;set;}
    
    public String allAddAnotherDependentModelNamesList {get;set;}
    
    public String modelCreateSequence {get;set;}
    
    //default constructor
    public HtmlPage(){    	
    	isDesktop = ERx_PortalPackUtil.isDesktop();
    	jsonModalFields = '{}';
    	jsonViewlayout = '[]';  
    	pageCompletionJSON = '{}';
    	modelObjectMapJSON = '{}';
    	buttonJSON = '{}';
        fieldToPopulateMap = new Map<String, Map<String, String>>();
        fieldToPopulateMapJSON = JSON.serialize(fieldToPopulateMap);
        redirectPageName = '';
        isAddAnotherAllowedMap = new Map<String, Boolean>();
        isAddAnotherAllowedMapJSON = JSON.serialize(isAddAnotherAllowedMap);
     	
     	modelPageUrlParameters = new map<string , string>();            
		modelPageUrlParameters = new map<string , string>(ApexPages.currentPage().getParameters());
		modelPageUrlParametersJSON = JSON.serialize(modelPageUrlParameters);	
        
        attachmentMap = JSON.serialize(new Map<String, List<ERx_PageEntities.AttachmentWrapper>>());    
        paramMap = JSON.serialize(new  Map<String, String>()); 
        
        isUnAutorizedPageAccess = false; // user is granted page rights
        
        usedComponentsOnPage = '';
        urlPathPrefix = '';
        isSessionExpired = false;
        addAnotherDependentModelsRecordMappingDetails = '{}';
        modelCreateSequence = JSON.serialize(new List<String>());
        addAnotherDependentModelNamesList = JSON.serialize(new Set<String>());
        allAddAnotherDependentModelNamesList = JSON.serialize(new List<String>());
    }
}