/**
 * Copyright (c) 2012, FinancialForce.com, inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 * - Neither the name of the FinancialForce.com, inc nor the names of its contributors 
 *      may be used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * This is a dummy test class to obtain 100% coverage for the generated WSDL2Apex code, it is not a funcitonal test class
 **/ 
@isTest(SeeAllData=true)
private class MetadataServiceTest {
	private class WebServiceMockImpl implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			
			if(request instanceof MetadataService.updateMetadata_element){
				MetadataService.updateMetadata_element requestUpdateMetadata_element  = 
	            			(MetadataService.updateMetadata_element) request;
	                response.put('response_x', new MetadataService.updateMetadataResponse_element());
			}
	        if(request instanceof MetadataService.readMetadata_element){
	      		MetadataService.readMetadata_element requestReadMetadata_element  = 
	            			(MetadataService.readMetadata_element) request;
	      		// This allows you to generalize the mock response by type of metadata read
	      		if (requestReadMetadata_element.type_x == 'CustomSite') { 
	         		MetadataService.readCustomSiteResponse_element mockRes   = 
	              		new MetadataService.readCustomSiteResponse_element();
	         		mockRes.result = new MetaDataService.ReadCustomSiteResult();
	         		mockRes.result.records = createCustomSites();
	         		response.put('response_x', mockRes);
	      		}
	    
	   		}
		return;
		}
		
		private  MetadataService.CustomSite[] createCustomSites(){
		    MetadataService.CustomSite[] objects = new MetadataService.CustomSite[]{};
		
		    MetadataService.CustomSite obj = new MetadataService.CustomSite();
		    obj.fullName = 'Contactt';
		    
		    objects.add(obj);
		
		    MetadataService.CustomSite obj2 = new MetadataService.CustomSite();
		    obj2.fullName = 'Accountt';
		    
		    objects.add(obj2);
		
		    MetadataService.CustomSite obj3 = new MetadataService.CustomSite();
		    obj3.fullName = 'MUSW__Inspection__cl';
		  
		    objects.add(obj3);
		
		    return objects;

		} 
	} 
	
	@IsTest
	private static void testReadMetaData(){
		Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
		MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
      	MetaDataService.ReadCustomSiteResult result =(MetaDataService.ReadCustomSiteResult)metaDataPort.readMetadata('CustomSite',new String[] {'Contactt'});
      	System.assertEquals('Contactt', result.records[0].fullName);
	}
	
	
	
	@IsTest
    private static void coverGeneratedCodeTypes(){
    	 new MetadataService.CustomSite();
    	 new MetadataService.SiteWebAddress();
    	 new MetadataService.readCustomSiteResponse_element();
    	 new MetadataService.SiteRedirectMapping();
    	 new MetadataService.updateMetadata_element();
    	 new MetadataService.updateMetadataResponse_element();
    	 new MetadataService.SaveResult();
    	 new MetadataService.readMetadataResponse_element();
    	 new MetadataService.ReadResult();
    	 new MetadataService.Error();
    	 new MetadataService.ExtendedErrorDetails();
    	 new MetadataService.readMetadata_element();
    	 new MetadataService.ReadCustomSiteResult();
    	 new MetadataService.SessionHeader_element();
         System.assertEquals('Test', 'Test');
    }
    
    @IsTest
	private static void coverGeneratedCodeCRUDOperations()
	{	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations     
        Test.startTest();    
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
        System.assertEquals(true, metaDataPort != null);
        Test.stopTest();
	}

	
	@IsTest
    private static void coverGeneratedCodeFileBasedOperations()
    {    	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations    
        Test.startTest();     
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
       	System.assertEquals(null, metaDataPort.updateMetadata(null));
        Test.stopTest();
         
    }
    
	
}