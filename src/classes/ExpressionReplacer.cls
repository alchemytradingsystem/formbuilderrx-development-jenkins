/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Class is used for ExpressionReplacer
*/
public class ExpressionReplacer {
 
    public static Boolean evaluateExpression(string expression){
    Boolean result = false;
        string evaluateMe = expression;
        Integer totalOpenBrac = evaluateMe.countMatches('(');
        Integer totalCloseBrac = evaluateMe.countMatches(')');
        if(totalOpenBrac == totalCloseBrac){
            Integer startBracPos = 0;
            Integer endBracPos = 0;
            Integer counter = 0;
            string strPartial = '';
            do{
                endBracPos = evaluateMe.indexOf(')');
                startBracPos = evaluateMe.lastIndexOf('(', endBracPos);
                if(endBracPos > -1){
                    strPartial = evaluateMe.substring(startBracPos, endBracPos+1); 
                             
                    evaluateMe = evaluateMe.replace(strPartial, resolveExpression(strPartial));
                    counter++;
                }
            }while((counter < totalOpenBrac) && (evaluateMe.contains(')')));
            
            //call final method for result
            result = Boolean.valueOf(resolveExpression(evaluateMe).trim());
        }else{
          result = false;
        }
        return result;
  }
  
  public static string resolveExpression(string strPart){
        
      string replacedString = strPart.toLowerCase();
        replacedString = replacedString.replace('(', '');
        replacedString = replacedString.replace(')', '');
        do{            
            if(replacedString.contains('false or false')){
                replacedString = replacedString.replace('false or false', 'false');
            }else if(replacedString.contains('false or true')){
                replacedString = replacedString.replace('false or true', 'true');                
            }else if(replacedString.contains('true or false')){
                replacedString = replacedString.replace('true or false', 'true');
            }else if(replacedString.contains('true or true')){
            replacedString = replacedString.replace('true or true', 'true');
            }else if(replacedString.contains('false and false')){
                replacedString = replacedString.replace('false and false', 'false');
            }else if(replacedString.contains('false and true')){
                replacedString = replacedString.replace('false and true', 'false');
            }else if(replacedString.contains('true and false')){
                replacedString = replacedString.replace('true and false', 'false');                
            }else if(replacedString.contains('true and true')){
                replacedString = replacedString.replace('true and true', 'true');
            }             
        }while(replacedString.contains('and') || replacedString.contains('or'));
        return replacedString;
    }
}