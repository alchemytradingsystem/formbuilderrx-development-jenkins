/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description The PortalPackageUtility used to for common method that can be used in other classes.
 */
public with sharing class PortalPackageUtility {

	/****
	 * @description currrent login user's contact Id
	 */
	public static String namespace;

	/****
	 * @description currrent login user's contact Id
	 */
	public static String finalNamespace;

	/*******************************************************************************************************
	 * @description Method is used to read the field set and returns the field list
	 * @param fieldSetName contain field set name
	 * @param SObjectTypeObj object name on which field set is available
	 * @return return all fields of fielset.
	 */
	public static List<Schema.FieldSetMember> readFieldSetbyFieldSetNameAndObj(String fieldSetName, Schema.SObjectType SObjectTypeObj) {
		Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
		//null check is missing review by arpit.
		//null check is missing. (Reviewed by Avish)
		if(DescribeSObjectResultObj == null) {
			return null;
		}
		Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
		if(fieldSetObj == null) {
			return null;
		}
		return fieldSetObj.getFields();
	}

	/*******************************************************************************************************
	 * @description  Method Returns The Prefix Namae Spaces.
	 * @return return name space of portal package.
	 */
	public static String getNameSpacePerfix(){
		// This query was in loop. To Minimized SOQL
		if(String.isBlank(namespace)) {
			// case 1 : If namespace is blank
			List<ApexClass> apxs = [SELECT NameSpacePrefix 
			                        FROM ApexClass 
			                        WHERE Name = 'PortalPackageUtility' AND NameSpacePrefix != null];
			if(apxs.size() > 0) {
				namespace = apxs[0].NameSpacePrefix;
			} else {
				namespace = '';
			}
		}
		return namespace;

	}

	/*******************************************************************************************************
	 * @description  Method Returns The Prefix Namae Spaces.
	 * @return return name space of portal package.
	 */
	public static String getFinalNameSpacePerfix(){
		// This query was in loop. To Minimized SOQL
		if(String.isBlank(finalNamespace)) {
			// case 1 : If namespace is blank
			List<ApexClass> apxs = [SELECT NameSpacePrefix 
			                        FROM ApexClass 
			                        WHERE Name = 'PortalPackageUtility' AND NameSpacePrefix != null];
			if(apxs.size() > 0) {
				finalNamespace = apxs[0].NameSpacePrefix;
			} else {
				finalNamespace = '';
			}

			if(String.isNotBlank(finalNamespace)) {
				finalNamespace = finalNamespace + '__';
			}
		}

		return finalNamespace;
	}

	/*******************************************************************************************************
	 * @description Method is used to escape quotes in String.
	 * @return return escaped string
	 */
	public static String escapeQuotes(String str) {
		if(!String.IsBlank(str)) {
			str = String.escapeSingleQuotes(str);
		}
		return str;
	}
	
	/*******************************************************************************************************
	 * @description Method is used to escape quotes in String.
	 * @return return escaped string
	 */
	public static List<String> escapeQuotesBulk(List<String> strList) {
		if(strList != null && strList.size()>0) {
			for(String str: strList){
				str = escapeQuotes(str);
			}
		} 
		return strList;
	}
	

	/*******************************************************************************************************
	 * @description Method used to find the element in list.
	 * @param elementList contain list of string
	 * @param element that need to be found in list.
	 * @return isFound element found or not in list.
	 */
	public static boolean isElementFoundInList(List<String> elementList, String element) {
		boolean isFound = false;
		for(String ele : elementList) {
			if(ele == element) {
				isFound = true;
				break;
			}           
		}
		return isFound;
	}

	/*******************************************************************************************************
	 * @description Method used to convert list to string.
	 * @param eleList list of string
	 * @param separator seperator
	 * @return String output String.
	 */
	public static String convertListBySeperator(List<String> eleList, String separator) {
		String returnString = '';
		if(eleList != null && eleList.size() > 0) {
			for(String ele : eleList) {
				returnString += ele + separator;
			}
			returnString = returnString.substring(0, (returnString.length() - separator.length()));
		}
		return returnString;
	}
	
	/*******************************************************************************************************
	 * @description Method used to convert list to string.
	 * @param eleList list of string
	 * @param separator seperator
	 * @return String output String.
	 */
	public static String convertSobjectListBySeperator(List<sObject> eleList, String separator, String fieldValue) {
		String returnString = null;
		if(eleList != null && eleList.size() > 0) {
			returnString = '';
			for(sObject ele : eleList) {
				returnString += ele.get(fieldValue) + separator;
			}
			returnString = returnString.substring(0, (returnString.length() - separator.length()));
		}
		return returnString;
	}
	//arpit code changes 25 Oct'2016

	/**
	 * @description get Permission error message
	 * @return error message for permission.
	 */
	public static String getPermissionErrorMessage() {
		String permissionErrorMessage = '';
		//Getting Permission error message
		FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
		if(formBuilderSetting != null && formBuilderSetting.Permission_Error_Message__c != null) {
			permissionErrorMessage = formBuilderSetting.Permission_Error_Message__c;
		} else {
			permissionErrorMessage = PortalPackageConstants.PERMISSION_ERROR_MESSAGE;
		}
		return permissionErrorMessage;
	}

	/*******************************************************************************************************
	 * Method added by Kavita on 1/3/2017
	 * @description Method used to check if package logger record with type INFO to be inserted 
    			   based on InsertInfotypeLoggers custom setting
	 * @return Boolean output
	 */
	public static Boolean checkInfoLoggerInsertSetting() {
		Boolean isInfoInsert;
		FormBuilder_Settings__c formBuilderSetting = FormBuilder_Settings__c.getInstance('Admin Settings');
		if(formBuilderSetting != null && formBuilderSetting.InsertInfoLoggers__c == true) {
			isInfoInsert = true;
		} else {
			isInfoInsert = false;	
		}
		return isInfoInsert; 	
	} 

	/*******************************************************************************************************
	 * Method added by Arpit
	 * @description Method used create date form string
	 * @return Boolean output
	 */
	public static Date getDateInFormat(String dateString) {
		List<String> newDateList = dateString.split('/');
		Date calculateDate = null;
		if(newDateList.size() > 2){
			calculateDate = Date.newInstance(Integer.valueOf(newDateList[2]),Integer.valueOf(newDateList[0]),Integer.valueOf(newDateList[1]));
		}
		return calculateDate; 	
	}

	/*******************************************************************************************************
	 * Method that return record list by object name and search string value
	 * @param objectName - Name of Object that records to be fetched
	 * @param sobjectType - Name of Object in Case of RecordType object is
	 * @param searchString Value for where clause search
	 * @return List<sObject> Record List
	 */
	public static List<sObject> getRecordsByObjectName(String objectName, String sobjectType, String searchString) {
		//PD-4402 || SOSL and SOQL injection 
		objectName = PortalPackageUtility.escapeQuotes(objectName);
		sobjectType = PortalPackageUtility.escapeQuotes(sobjectType);
		searchString = PortalPackageUtility.escapeQuotes(searchString);
		String query;
		String permissionErrMsg = PortalPackageUtility.getPermissionErrorMessage();
		String nameSpacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
		Integer refereceRecordMaxLimit = ERx_PortalPackUtil.getReferenceAdminMaxLimit();
		if(objectName.toLowerCase() == 'recordtype'){
			// If Object Name is Record Type then Fetching all the records types assosiate with that sobjectType.
			//dynamic limit added from cutom setting
			if(SecurityEnforceUtility.checkObjectAccess(sobjectType, SecurityTypes.IS_ACCESS, nameSpacePrefix)) {
				query = 'SELECT Id, Name '+
						' FROM ' + objectName + getWhereConditionClause(searchString, 'Name')
						+ ' AND (SObjectType=\'' + sobjectType +'\'' + ') LIMIT ' + refereceRecordMaxLimit ;
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + SObjectType), 
						'TM_TemplateCriteriaController', 'getRecords', ERx_PortalPackUtil.getPageIdFromURL());
				throw new PortalPackageException(permissionErrMsg);
			}
		} else{
			//dynamic limit added from cutom settin
			if(SecurityEnforceUtility.checkObjectAccess(objectName, SecurityTypes.IS_ACCESS, nameSpacePrefix)) { 
				query = 'SELECT Id, Name '
						+' FROM ' + objectName + getWhereConditionClause(searchString, 'Name') 
						+ ' LIMIT ' + refereceRecordMaxLimit;
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: ' + objectName), 
						'TM_TemplateCriteriaController', 'getRecords', ERx_PortalPackUtil.getPageIdFromURL());
				throw new PortalPackageException(permissionErrMsg);
			}
		}
		return Database.query(query);
	}

	/**
	 * @description Method that returns where condition by search string.
	 * @param searchString search string that need to be searched
	 * @return Where Condition 
	 */
	private static String getWhereConditionClause(String searchString, String fieldAPIName) {
		String tempStr = '';
		for(String conditionValue : searchString.trim().split(' ')) {
			if(conditionValue != '') {
				tempStr += ' ' + fieldAPIName + ' like ' + '\'%' + PortalPackageUtility.escapeQuotes(conditionValue.trim()) + '%\'' + ' OR ';
			}
		}

		if(tempStr != '') {
			tempStr = tempStr.trim().subString(0,tempStr.length()-4);
		}
		return ' where (' + tempStr + ')';      
	}

	/*
	 * Method that returns clean list - Remove the Deleted Value from the list
	 * @param arrayList - list to be updated
	 * @param deleteValue - value to be cleaned
	 * @return List<String> Cleaned list
	 */
	public static List<String> clean(List<String> arrayList, String deleteValue) {
		if(arrayList != null) {
			for(Integer i=0; i<arrayList.size(); i++) {
				if( arrayList[i] == deleteValue ) {
					arrayList.remove(i);
					i--;
				}
			}
		}
		return arrayList;
	}

	/*
	 * Method that returns Numeric sum of list
	 * @param arrayList - List of Integer numbers
	 * @return sum of list
	 */
	public static Integer sum(List<Integer> arrayList) {
		Integer sum = 0;
		if(arrayList != null) {
			for(Integer v : arrayList) {
				sum += v;
			}
		}
		return sum;
	}
	/*
	 * Method that returns max number from list
	 * @param arrayList - List of Integer numbers
	 * @return Max number
	 */
	public static Integer max(List<Integer> arrayList) {
		Integer max;
		if(arrayList != null) {
			arrayList.sort();
			max = arrayList[arrayList.size() - 1];
		}
		return max;
	}

	/*******************************************************************************************************
	 * @description Method used to find the element in list.
	 * @param elementList contain list of string
	 * @param element that need to be found in list.
	 * @return isFound element found or not in list.
	 */
	public static boolean isElementFoundInList(List<Integer> elementList, Integer element) {
		boolean isFound = false;
		for(Integer ele : elementList) {
			if(ele == element) {
				isFound = true;
				break;
			}           
		}
		return isFound;
	}

	/*******************************************************************************************************
	 * @description Method used to remove key from map and return updated string
	 * @param criteriaString Serialize criteria string
	 * @param key - that to remove from map
	 * @return isFound element found or not in list.
	 */
	public static String updateTemplateCriteriaAssignment(String criteriaString, String key) {
		String returnOutput = null;
		if(String.isNotBlank(criteriaString)) {
			Map<String, RenderCriteriaConditionWrapper> mainTemplateCriteriaMap = 
					(Map<String, RenderCriteriaConditionWrapper>) json.deserialize(criteriaString, Map<String, RenderCriteriaConditionWrapper>.class);
			if(mainTemplateCriteriaMap != null && String.isNotBlank(key) && mainTemplateCriteriaMap.containsKey(key)) {
				mainTemplateCriteriaMap.remove(key);
			}

			returnOutput = JSON.serialize(mainTemplateCriteriaMap);
		}
		return returnOutput;
	}

	/****
	 * @description variable to check is Application Object present or not
	 */
	public static Boolean hasApplicationObject() {
		return Schema.getGlobalDescribe().containsKey('EnrollmentrxRx__Enrollment_Opportunity__c');
	}

	/*
	 * @description Method that returns query string
	 * @param Object name - Name off Object that to be queried
	 * @param fieldSet - List of fields
	 * @whereClause where clause of query
	 * @return String QueryString
	 */
	private static String getQuery(String objectName, Set<String> fieldSet, String whereClause) {
		if(String.isNotBlank(whereClause)) {
			if(!whereClause.trim().startsWithIgnoreCase('where')) {
				whereClause = ' where ' + whereClause;
			}
		} else {
			whereClause = '';
		}

		List<String> fieldList = new List<String>();
		fieldList.addAll(fieldSet);
		return ('SELECT ' + PortalPackageUtility.convertListBySeperator(fieldList, ', ') + ' FROM ' + objectName + whereClause);
	}

	/*
	 * @description Method that returns current user contact record with fieldlist
	 * @param fieldList - List of field names to be fetched
	 * @param permissionCheckFieldList - list of field names for permission check
	 * @return sObject fetched record
	 */

	public static sObject getCurrentContact(Set<String> fieldList, Set<String> permissionCheckFieldList) {
		sObject queriedData = null;
		String nameSpacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
		String permissionErrorMessage = getPermissionErrorMessage();
		List<String> tempPermissionCheck = new List<String>();
		tempPermissionCheck.addAll(permissionCheckFieldList);
		try {
			String queryString = getQuery('Contact', fieldList, 'Id = \'' + ERx_PortalPackUtil.getCurrentContactId() + '\'');
			if(SecurityEnforceUtility.checkObjectAccess('Contact', SecurityTypes.IS_ACCESS, nameSpacePrefix)) {
				if(SecurityEnforceUtility.checkFieldAccess('Contact', tempPermissionCheck, SecurityTypes.IS_ACCESS, nameSpacePrefix)) {
					
					List<sObject> queriedDataList = database.query(queryString);
					if(queriedDataList != null && queriedDataList.size() > 0) {
						queriedData = queriedDataList[0];
					}
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::' + 'Contact' +' FieldList :: ' 
							+ PortalPackageUtility.convertListBySeperator(tempPermissionCheck, ', ')), 'PortalPackageUtility',
							'getCurrentContact', ERx_PortalPackUtil.getPageIdFromURL());
					throw new PortalPackageException(permissionErrorMessage);
				}
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::' + 'Contact'), 'PortalPackageUtility',
						'getCurrentContact', ERx_PortalPackUtil.getPageIdFromURL());
				throw new PortalPackageException(permissionErrorMessage);
			}
		} catch(Exception e) {
			if(!e.getMessage().contains(permissionErrorMessage)) {
				ERx_PortalPackageLoggerHandler.addException(e, 'PortalPackageUtility', 'getCurrentContact', ERx_PortalPackUtil.getPageIdFromURL());
			}
		}

		return queriedData;

	}

	/*
	 * @description Method that returns current user contact's Active Application
	 * @param fieldList - List of field names to be fetched
	 * @param permissionCheckFieldList - list of field names for permission check
	 * @return sObject fetched record
	 */
	public static sObject getActiveApplication(Set<String> fieldList, Set<String> permissionCheckFieldList) {
		sObject queriedData = null;
		String nameSpacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
		String permissionErrorMessage = getPermissionErrorMessage();
		List<String> tempPermissionCheck = new List<String>();
		tempPermissionCheck.addAll(permissionCheckFieldList);
		try {
			if(hasApplicationObject()) {
				sObject contactObject = getCurrentContact(new Set<String>{'EnrollmentrxRx__Active_Enrollment_Opportunity__c'}, 
						new Set<String>{'EnrollmentrxRx__Active_Enrollment_Opportunity__c'});
				if(contactObject != null) {
					String queryString = getQuery('EnrollmentrxRx__Enrollment_Opportunity__c', fieldList, 'Id = \'' 
							+ contactObject.get('EnrollmentrxRx__Active_Enrollment_Opportunity__c') + '\'');
					if(SecurityEnforceUtility.checkObjectAccess('EnrollmentrxRx__Enrollment_Opportunity__c', 
							SecurityTypes.IS_ACCESS, nameSpacePrefix)) {
						if(SecurityEnforceUtility.checkFieldAccess('EnrollmentrxRx__Enrollment_Opportunity__c', 
								tempPermissionCheck, SecurityTypes.IS_ACCESS, nameSpacePrefix)) {
							List<sObject> queriedDataList = database.query(queryString);
							if(queriedDataList != null && queriedDataList.size() > 0) {
								queriedData = queriedDataList[0];
							}
						} else {
							ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::' 
									+ 'EnrollmentrxRx__Enrollment_Opportunity__c' +' FieldList :: ' 
									+ PortalPackageUtility.convertListBySeperator(tempPermissionCheck, ', ')), 'PortalPackageUtility', 'getCurrentActiveApplication', ERx_PortalPackUtil.getPageIdFromURL());
							throw new PortalPackageException(permissionErrorMessage);
						}
					} else {
						ERx_PortalPackageLoggerHandler.addPermissionError(('objectName ::' 
								+ 'EnrollmentrxRx__Enrollment_Opportunity__c'), 'ERx_PortalPackUtil', 'getCurrentActiveApplication', ERx_PortalPackUtil.getPageIdFromURL());
						throw new PortalPackageException(permissionErrorMessage);
					}
				}
			}
		} catch(Exception e) {
			if(!e.getMessage().contains(permissionErrorMessage)) {
				ERx_PortalPackageLoggerHandler.addException(e, 'PortalPackageUtility', 'getCurrentActiveApplication', ERx_PortalPackUtil.getPageIdFromURL());
			}
		}

		return queriedData;
	}

	/*******************************************************************************************************
	 * @description This method converts datetime to gmt forted date
	 * @param dateStr contains date in string from admin panel 
	 * @return Datetime in gmt
	 */ 
	public static Datetime convertDateToGMT(String dateStr){
		list<String> tempValParts = dateStr.split(' ');
		list<String> dateParts = tempValParts[0].split('/');
		Date myDate = Date.newInstance(Integer.valueOf(dateParts[2]),Integer.valueOf(dateParts[0]),Integer.valueOf(dateParts[1]));
		Time myTime;
		list<string> timeparts = tempValParts[1].split(':');
		if(tempValParts[2].containsIgnoreCase('PM')){
			myTime = Time.newInstance(Integer.valueOf(timeparts[0])+12,Integer.valueOf(timeparts[1]),0,0);
		}else{
			myTime = Time.newInstance(Integer.valueOf(timeparts[0]),Integer.valueOf(timeparts[1]),0,0);
		}
		//ERx_PortalPackageLoggerHandler.addInformation((myDate  + '- @@DATE## -' + myTime + '- @@DATE## -' + DateTime.newInstanceGMT(myDate, myTime)), 'DEV4_PageBuilder', 'getConditionOutput',null);
		return DateTime.newInstanceGMT(myDate, myTime);
	}

	/*******************************************************************************************************
	 * @description This method converts GMT to local date
	 * @param dateStr contains date in string
	 * @return Datetime in locale
	 */ 
	public static Datetime convertGMTDateToLocal(String dateStr){
		list<String> tempValParts = dateStr.split(' ');
		list<String> dateParts = tempValParts[0].split('-');
		Date myDate = Date.newInstance(Integer.valueOf(dateParts[0]),Integer.valueOf(dateParts[1]),Integer.valueOf(dateParts[2]));
		Time myTime;
		list<string> timeparts = tempValParts[1].split(':');
		myTime = Time.newInstance(Integer.valueOf(timeparts[0]),Integer.valueOf(timeparts[1]),Integer.valueOf(timeparts[2]),0);
		return DateTime.newInstance(myDate, myTime);
	}

	/********
	 * @description : Method that returns security by pass details
	 * @return Map<String, Security_Check_Bypass_Setting__c> Map of Bypass setting
	 */
	public static Map<String, Security_Check_Bypass_Setting__c> getByPassObjectandFieldDetails() {
		Map<String, Security_Check_Bypass_Setting__c> finalMap = new Map<String, Security_Check_Bypass_Setting__c>();
		Map<String, Security_Check_Bypass_Setting__c> byPassMap = Security_Check_Bypass_Setting__c.getall();
		for(String mapKey : byPassMap.keySet()) {
			if(String.isNotBlank(byPassMap.get(mapKey).field_list__c)) {
				byPassMap.get(mapKey).field_list__c = byPassMap.get(mapKey).field_list__c.toLowerCase();
			}
			finalMap.put(mapKey.toLowerCase(), byPassMap.get(mapKey));   		
		}
		return finalMap;
	}

	/********
	 * @description : Method that returns security by pass Object and field list details
	 * @return Map<String, Set<String>> object field map
	 */
	public static Map<String, Set<String>> getByPassFieldlist() {
		Map<String, Set<String>> finalMap = new Map<String, Set<String>>();
		Map<String, Security_Check_Bypass_Setting__c> byPassMap = Security_Check_Bypass_Setting__c.getall();
		List<String> fieldList;
		for(String mapKey : byPassMap.keySet()) {
			fieldList = new List<String>();
			if(String.isNotBlank(byPassMap.get(mapKey).field_list__c)) {
				fieldList = getTrimedList(byPassMap.get(mapKey).field_list__c.trim().toLowerCase().split('\\,'));
			}
			finalMap.put(mapKey.trim().toLowerCase(), new Set<String>(fieldList));
		}
		return finalMap;
	}

	/********
	 * @description : Method that trim the list content and create a new list
	 * @param List<String> fieldList : field list
	 * @return List<String> field list
	 */
	public static List<String> getTrimedList(List<String> fieldList) {
		List<String> returnList = new List<String>();
		if(fieldList != null) {
			for(String field : fieldList) {
				if(String.isNotBlank(field.trim())) {
					returnList.add(field.trim());
				}
			}
		}
		return returnList;
	}
	
	/********
	 * @description : Method is used to replace &quot; with \",&lt; with  < and &gt; with >
	 */
	public static String changeToHTML(String str){
		str = str.replace('&quot;','\"').replace('&lt;','<').replace('&gt;','>');
		return str;
	}
}