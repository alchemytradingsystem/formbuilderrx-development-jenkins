@isTest 
private class TM_TemplateCriteriaControllerTest {
    public static ERx_PageEntities.TemplateRenderedConditionWrapper templateConditionWrapper;
    private static Profile testProfile ;
    private static Contact student;
    private static User testUser;
    
    static testMethod void hasApplicationObjectTest() {
        Boolean hasObj = TM_TemplateCriteriaController.hasApplicationObject;
        if(hasObj){
        	System.assertEquals(true, hasObj);
        }else{
        	System.assertEquals(false, hasObj);
        }
        
    }
    
    static testMethod void testCriteriaLists() {
        //Testing operator list
        Account a;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        testProfile = [select id, name from profile where name='Applicant Portal User'];
        testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
        System.runAs(testUser){
        TM_TemplateCriteriaController templateObject = new TM_TemplateCriteriaController();
        List<SelectOption> expectedselectOp = new List<SelectOption>();
        expectedselectOp.add(new SelectOption('', '--None--'));
        for(String op : new List<String>{'equals', 'not equal to', 'starts with', 'ends with', 'contains', 'does not contain'}) {
    			expectedselectOp.add(new SelectOption(op, op));
    		}
        List<SelectOption> resultSelectOp = TM_TemplateCriteriaController.operatorList;
        system.assertEquals(expectedselectOp, resultSelectOp);
        
        //Testing criteriaObjectNamesAPEX
        List<String> criteriaObjectNamesList = new List<String>();
        criteriaObjectNamesList.add('Contact');
        //criteriaObjectNamesList.add('EnrollmentrxRx__Enrollment_Opportunity__c');
        List<String> resultCriteriaObjectNamesList = TM_TemplateCriteriaController.criteriaObjectNamesAPEX;
        system.assertEquals(criteriaObjectNamesList, resultCriteriaObjectNamesList);
        
        //Testing criteriaObjectSelectOption
        List<SelectOption> criteriaObjectNamesOption = new List<SelectOption>();
        for(String objName : criteriaObjectNamesList) {
    			criteriaObjectNamesOption.add(new SelectOption(objName, objName));
    	}
        List<SelectOption> resultCriteriaObjectNamesOption = TM_TemplateCriteriaController.criteriaObjectSelectOption;
        Map<String,Map<String, Object>> templateMap = TM_TemplateCriteriaController.describeResultsOutput;
        //Testing for creating exceptions
        templateObject.changeIndex = -1;
        templateObject.removeCriteria();
        templateObject.disableActive();
        templateObject.activeSelected();
        templateObject.updateCriteriaType();
        templateObject.updateCriteriaList();
        templateObject.setFieldType();
        templateObject.removeReferencedValue();
        templateObject.changeIndex = -1;
        templateObject.setReferencedFieldType();
        templateObject.changeIndex = -1;
        templateObject.getRecords();
        templateObject.changeIndex = -1;
        templateObject.removeReferencedValue();
        templateObject.changeIndex = -1;
        templateObject.selectReferencedValue();
        //Testing for 1 record created
        templateObject.criteria = new RenderCriteriaConditionWrapper();
        templateObject.criteria.isDefault = true;
        templateObject.criteria.isActive = true; 
        templateObject.criteria.renderedCondition = null;
        templateObject.activeSelected();
        templateObject.changeIndex = 0;
	    templateConditionWrapper = new ERx_PageEntities.TemplateRenderedConditionWrapper();
        createDataToGenerateException();
        templateObject.criteria.renderedCondition = new List<ERx_PageEntities.TemplateRenderedConditionWrapper>();
        templateObject.criteria.renderedCondition.add(templateConditionWrapper);
        templateObject.disableActive();
        templateObject.activeSelected();
        templateObject.changeIndex = 0;
        templateObject.setFieldType();
        templateObject.changeIndex = 0;
        createDataToGenerateException();
        templateObject.criteria.renderedCondition = new List<ERx_PageEntities.TemplateRenderedConditionWrapper>();
        templateObject.criteria.renderedCondition.add(templateConditionWrapper);
        templateObject.setReferencedFieldType();
        createDataToGenerateException();
        templateObject.criteria.renderedCondition = new List<ERx_PageEntities.TemplateRenderedConditionWrapper>();
        templateObject.criteria.renderedCondition.add(templateConditionWrapper);
        templateObject.changeIndex = 0;
        templateObject.getRecords();
        templateObject.changeIndex = 0;
        templateObject.updateCriteriaType();
        templateObject.changeIndex = 0;
        templateObject.updateCriteriaList();
        templateObject.changeIndex = 0;
        templateObject.selectReferencedValue();
        templateObject.changeIndex = 0;
        templateObject.removeReferencedValue();
        templateObject.changeIndex = 0;
        //Testing for exception in generating picklist value
        List<ERx_PageEntities.SelectOpWrap> selectOpWrap = new List<ERx_PageEntities.SelectOpWrap>();
        templateObject.picklistSelectedValueList = 'Test:Test;';
        templateObject.updatePicklistValue();
        //Testing for generating picklist value
        ERx_PageEntities.SelectOpWrap selectOp1 = new ERx_PageEntities.SelectOpWrap('Test','Test');
        ERx_PageEntities.SelectOpWrap selectOp2 = new ERx_PageEntities.SelectOpWrap('Test1','Test1');
        selectOpWrap.add(selectOp1);
        selectOpWrap.add(selectOp2);
        templateObject.picklistSelectedValueList = JSON.serialize(selectOpWrap);
        templateObject.updatePicklistValue();
        templateObject.changeIndex = 0;
        templateObject.removeCriteria();
        //Testing in case of false conditions
        templateObject.criteria.isDefault = false;
        templateObject.criteria.isActive = false;  
        templateObject.criteria.renderedCondition = null;
        templateObject.changeIndex = 0;
        templateObject.disableActive();
        templateObject.changeIndex = 0;
        templateObject.activeSelected();
        templateObject.changeIndex = 0;
        templateObject.updateCriteriaType();
        templateObject.changeIndex = 0;
        templateObject.updateCriteriaList();
        system.assertEquals(criteriaObjectNamesOption, resultCriteriaObjectNamesOption);
        }
    }
    
    private static void createDataToGenerateException() {
		templateConditionWrapper.criteriaType = 'STATIC';
        templateConditionWrapper.modelName = 'Contact';
        templateConditionWrapper.fieldAPIName = 'FirstName';
        templateConditionWrapper.fieldOperator = 'equals';
        templateConditionWrapper.lookupSearchString = 'test';
        templateConditionWrapper.referencedFieldAPIName = 'Id';
        templateConditionWrapper.referencedFieldDisplayType = 'id';
        templateConditionWrapper.referencedObjectName = 'User';
    }

}