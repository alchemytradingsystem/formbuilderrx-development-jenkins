/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/ 
	@author EnrollmentRx
	@created by : gourav
	@version 1.0
	@date 2018-04-9 
	@description The PackageSecurityVerificationController class is used for calling the rest service which links to the LMA Org.
*/
public with sharing class GettingStartedController{
	
	public String nameSpacePrefix;
	public FormBuilder_Settings__c fbsetting{get;set;}
	
	
	public GettingStartedController(){
		fbsetting = new FormBuilder_Settings__c();
		fbsetting = FormBuilder_Settings__c.getValues('Admin Settings');
		nameSpacePrefix = PortalPackageUtility.getNameSpacePerfix();
        if(!(String.isBlank(nameSpacePrefix))) {
        	nameSpacePrefix = nameSpacePrefix + '__';
        }
        
	}
	
	public void updateGettingStarted(){
		if(SecurityEnforceUtility.checkObjectAccess('FormBuilder_Settings__c', SecurityTypes.IS_UPDATE, nameSpacePrefix)) {
			// updating the package security record.
			if(fbsetting != null) {
				update fbsetting;
			}
	     }
	}
	
	
}