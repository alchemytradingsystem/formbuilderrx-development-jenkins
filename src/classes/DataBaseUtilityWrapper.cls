/********
FormBuilderRx
@company : Copyright Â© 2016, Enrollment Rx, LLC
All rights reserved.
Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@website : http://www.enrollmentrx.com/
@author EnrollmentRx
********/

public with sharing class DataBaseUtilityWrapper {
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.query(query) method
    * @param String contains query
    * @return records as result of Database.query method.
    */
	public SObject DatabaseQuery(String query){
		return Database.query(query);
	}
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.setSavepoint() method
    * @return savepoint for transaction.
    */
	public Savepoint DatabaseSavepoint(){
		return Database.setSavepoint();
	}
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.rollBack() method
    * @param Savepoint contains save point of data base
    * @return savepoint for transaction.
    */
	public void DatabaseRollBack(Savepoint sp){
		 Database.rollBack(sp);
	}
	
	/*******************************************************************************************************
    * @description this method is wrapper for Database.rollBack() method
    * @param Savepoint contains save point of data base
    * @return savepoint for transaction.
    */
	public ID DatabaseExecuteBatch(Database.Batchable<sObject> batchClassObject , Integer Scope){
		 return Database.executeBatch(batchClassObject, Scope);
	}
	
	/*******************************************************************************************************
    * @description this method is wrapper for Database.query(query)getQueryLocator method
    * @param String contains query
    * @return records as result of Database.getQueryLocator method.
    */
	public void DatabaseGetQueryLocator(String query){
		 Database.getQueryLocator(query);
	}
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.emptyRecycleBin() method
    * @param contains List<sObject>
    * @return Database.emptyRecycleBin to empty recyclebin.
    */
	public void DatabaseEmptyRecycleBinObjList(List<sObject> sObjList){
		 Database.emptyRecycleBin(sObjList);
	}
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.emptyRecycleBin() method
    * @param contains sObject
    * @return Database.emptyRecycleBin to empty recyclebin.
    */
	public void DatabaseEmptyRecycleBinObj(sObject sObj){
		 Database.emptyRecycleBin(sObj);
	}
	
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.emptyRecycleBin() method
    * @param contains sObject
    * @return Database.emptyRecycleBin to empty recyclebin.
    */
	public void DatabaseEmptyRecycleBin(ID[] recordIds){
		 Database.emptyRecycleBin(recordIds);
	}
	
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.insert() method
    * @param contains sObject record
    * @return insert record in sobject.
    */
	public void DatabaseInsert(Sobject sObj){
		 Database.insert(sObj);
	}
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.insert() method
    * @param contains sObject record
    * @return insert record in sobject.
    */
	public void DatabaseInsertPartial(Sobject sObj ,Boolean allOrNone){
		 Database.insert(sObj ,allOrNone);
	}
	
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.delete() method
    * @param contains sObject record
    * @return delete record in sobject.
    */
	public List<Database.DeleteResult> DatabaseDeleteObjList(List<sObject> sObj,Boolean allOrNone){
		return Database.Delete(sObj,allOrNone);
	}
	
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.delete() method
    * @param contains sObject record
    * @return delete record in sobject.
    */
	public Database.DeleteResult DatabaseDeleteObj(sObject sObj,Boolean allOrNone){
		return Database.Delete(sObj,allOrNone);
	}
		
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.convertLead(lc)() method
    * @param contains Database.LeadConvert record
    * @return Converts a list of LeadConvert objects into accounts and contacts, as well as (optionally) opportunities.
    */
	public Database.LeadConvertResult DatabaseConvertLead(Database.LeadConvert lc){
		 return Database.convertLead(lc);
	}
	
		
	 /*******************************************************************************************************
    * @description this method is wrapper for Database.convertLead(lc)() method
    * @param contains Database.LeadConvert record
    * @return Converts a list of LeadConvert objects into accounts and contacts, as well as (optionally) opportunities.
    */
	public Database.LeadConvert DatabaseLeadConvert(){
		 return new Database.LeadConvert();
	}
	
	
	
     
}