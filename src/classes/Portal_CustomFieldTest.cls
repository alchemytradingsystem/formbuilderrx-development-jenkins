/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Portal_CustomFieldTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        Portal_CustomField pf = new Portal_CustomField(Test_Erx_Data_Preparation.customFIeldsList[0]);
        pf.inputCheckBox = false;
        pf.inputNumber = 10;
        pf.inputDecimal = 10.0;
        pf.queryCriteria = 'active=true';
        pf.inputDate='10/12/2014';
        pf.getChildrenOptions();
        pf.getGrandChildrenOptions();
		pf.initPageException();
        list<SelectOption> returnOpts = new list<SelectOption>();
        returnOpts = pf.getGrandChildrenOptions();
        returnOpts = pf.getChildrenOptions();
        Account parentAccount;
        Account superParentAccount;
        superParentAccount = new Account(name = 'Test Parent');
        insert superParentAccount;
        parentAccount = new Account(ParentId = superParentAccount.Id, name = 'Test1');
        insert parentAccount;
        Portal_CustomField.addWhereClause('Parent.ParentId = \''+superParentAccount.Id+'\' ORDER BY Parent.Id DESC LIMIT 2');
        System.assertNotEquals(pf, null);
    }

     static testMethod void myUnitTest1() {
        Test_Erx_Data_Preparation.preparation();

        Portal_Login_Custom_Field__c f4 = new Portal_Login_Custom_Field__c();
        f4.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        f4.Active__c = true;
        f4.Object_Name__c = 'Contact';
        f4.Field_API_Name__c = 'OwnerId';
        f4.Name = 'Owner';
        f4.FieldType__c = 'REFERENCE';
        f4.Children_Field_API_Name__c = 'ReportsTo';
        f4.Children_Index_Field_API__c = 'OwnerId';
        f4.Children_Objects_API__c = 'Contact';
        f4.Grand_Children_Field_API_Name__c  = 'ReportsTo';
        f4.Grand_Children_Index_Field_API__c = 'OwnerId';
        f4.Grand_Children_Objects_API__c = 'Contact';
        insert f4;


        Portal_CustomField pf1 = new Portal_CustomField(f4);
        pf1.getChildrenOptions();
        pf1.getGrandChildrenOptions();
		pf1.initPageException();
        System.assertNotEquals(pf1, null);
     }

     static testMethod void myUnitTest2() {
        Test_Erx_Data_Preparation.preparation();
        Portal_CustomField pf2 = new Portal_CustomField(Test_Erx_Data_Preparation.customFIeldsList[2]);
        pf2.inputCheckBox = false;
        pf2.inputNumber = 10;
        pf2.inputDecimal = 10.0;
        pf2.queryCriteria = 'active=true';
        pf2.inputDate='10/12/2014';
        pf2.getChildrenOptions();
        pf2.getGrandChildrenOptions();

        System.assertNotEquals(pf2, null);
     }

     static testMethod void myUnitTest3() {
        Test_Erx_Data_Preparation.preparation();

        Portal_Login_Custom_Field__c f4 = new Portal_Login_Custom_Field__c();
        f4.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        f4.Active__c = true;
        f4.Object_Name__c = 'Contact';
        f4.Field_API_Name__c = 'BirthDate';
        f4.Name = 'BirthDate';
        insert f4;

        Portal_CustomField pf2 = new Portal_CustomField(f4);
        Portal_CustomField pf3 = new Portal_CustomField(Test_Erx_Data_Preparation.customFIeldsList[3]);

        System.assertNotEquals(pf2, null);
     }

     static testMethod void myUnitTest4() {
        Test_Erx_Data_Preparation.preparation();

        Portal_Login_Custom_Field__c f = new Portal_Login_Custom_Field__c();
        f.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        f.Active__c = true;
        f.Object_Name__c = 'Contact';
        f.Field_API_Name__c = 'BirthDate';
        f.Name = 'BirthDate';
        insert f;

        Portal_CustomField pf2 = new Portal_CustomField(f);

        System.assertNotEquals(pf2, null);
     }

     static testMethod void myUnitTest5() {
        Test_Erx_Data_Preparation.preparation();

        Portal_Login_Custom_Field__c f = new Portal_Login_Custom_Field__c();
        f.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        f.Active__c = true;
        f.Object_Name__c = 'Contact';
        f.Field_API_Name__c = 'Department';
        f.Name = 'Department';
        insert f;

        Portal_CustomField pf2 = new Portal_CustomField(f);

        System.assertNotEquals(pf2, null);
     }

     static testMethod void myUnitTest6() {
        Test_Erx_Data_Preparation.preparation();

        Portal_Login_Custom_Field__c f = new Portal_Login_Custom_Field__c();
        f.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        f.Active__c = true;
        f.Object_Name__c = 'Contact';
        f.Field_API_Name__c = 'OtherPhone';
        f.Name = 'OtherPhone';
        insert f;

        Portal_CustomField pf2 = new Portal_CustomField(f);

        System.assertNotEquals(pf2, null);
     }

     static testMethod void myUnitTest7() {
        Test_Erx_Data_Preparation.preparation();

        Portal_Login_Custom_Field__c f = new Portal_Login_Custom_Field__c();
        f.Env__c = Test_Erx_Data_Preparation.listEnv[0].Id;
        f.Active__c = true;
        f.Object_Name__c = 'Contact';
        f.Field_API_Name__c = 'LastCURequestDate';
        f.Name = 'LastCURequestDate';
        insert f;

        Portal_CustomField pf2 = new Portal_CustomField(f);
        list<string> l= pf2.inputTextList;
        boolean va=pf2.hasCriteria;
        boolean va1=pf2.hasChildCriteria;
        string st = pf2.grandChildrenInputText;
        string va3=pf2.hasGdChildCriteria;
        map<string, list<SelectOption>> sda = pf2.grandChildrenMapOptions;

        System.assertNotEquals(pf2, null);
     }
     
     //Added by Kavita PD-1493
     static testMethod void myUnitTestQueryCriteria() {
        Test_Erx_Data_Preparation.preparation();
        
		Account superParentAccount;
        superParentAccount = new Account(name = 'Test Parent');
        insert superParentAccount;
        
        Account parentAccount;
        parentAccount = new Account(ParentId = superParentAccount.Id, name = 'Test1');
        insert parentAccount;
        
        Account account = new Account(ParentId = parentAccount.Id, name = 'Test');
        insert account;
        Portal_Login_Custom_Field__c orderByField = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
        FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'Parent.ParentId = \''+superParentAccount.Id+'\' ORDER BY Parent.Id DESC LIMIT 2', Children_Index_Field_API__c = 'parentid',
        Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
        gdChildQueryCriteria__c = 'Parent.ParentId = \''+superParentAccount.Id+'\' ORDER BY Parent.Id DESC', 
        Grand_Children_Field_API_Name__c = 'accountid');

        Portal_CustomField pf2 = new Portal_CustomField(orderByField);
        list<string> l= pf2.inputTextList;
        boolean va=pf2.hasCriteria;
        boolean va1=pf2.hasChildCriteria;
        string st = pf2.grandChildrenInputText;
        string va3=pf2.hasGdChildCriteria;
        map<string, list<SelectOption>> sda = pf2.grandChildrenMapOptions;

        System.assertNotEquals(pf2, null);
         
     }


}