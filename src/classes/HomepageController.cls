public with sharing class HomepageController{
    //sobject
    public String hwlListJSON {get;set;}
    public String errorMessage {get;set;}

	public String envId{get;set;}
    public String contactId{get;set;}
	public String applicationId{get;set;}
	public String browserTabTitle {get;set;}
	public String sitePathPreFix {get;set;}
	public String currentContact {get;set;}
	public String activeApplication {get;set;}
	public String homepageWidgetDescribesResultJSON {get;set;}
	public String checklistResultJSON {get;set;}
	public String receivedChecklistStatusJSON {get;set;}
	public Boolean isActiveApplicationExist {get;set;}
	public Boolean isApplicationFieldExist{get;set;} 
	
	//PD-4253 | Shubham
    public String FaviconIcon{get;set;}
	
	//PD-4103 | Priyamvada
    public String gtmScript{get;set;}
	
	/****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();

    public HomepageController(ngForceController ngcon){
        try{
        	// Initialize default value
        	currentContact = '{}';
        	activeApplication = '{}';
        	sitePathPreFix = '';
        	isActiveApplicationExist = true;
            Env__c env = ERx_PortalPackUtil.getEnvDetailsForCurrentUser();
            
            /*PD-4103 add gtm and favicon icon*/
            // for gtm
			String script = '';
	        if(env != null){
	            script = env.GTM_Account_Id__c;
	        }
	        gtmScript = script;
	        // for favicon icon
	        String imageLink = '';
    		String siteURL = Site.getPathPrefix();
	        if(env != null){
	            if(String.isNotBlank(env.Favicon_Icon__c) && !env.Favicon_Icon__c.contains('null')){
	            	List<String> pathValues = env.Favicon_Icon__c.split('##');
					if(pathValues != null && pathValues.size() > 0){
						if(pathValues.size() == 1){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]);	
						}
						else if(pathValues.size() == 2){
							imageLink = siteURL + '/resource/'+ PortalPackageUtility.escapeQuotes(pathValues[0]) + PortalPackageUtility.escapeQuotes(pathValues[1]) ;
						}
					}
	            }
	        }
	        FaviconIcon = imageLink;
	        /*ends PD-4103*/
            
            if(String.isNotBlank(env.UrlPathPrefix__c)) {
            	sitePathPreFix = '/' + 	env.UrlPathPrefix__c;
            }
			envId=env.Id;
			//changes done for PD-1739
			browserTabTitle = env.Browser_Tab_Title__c;
            system.debug('### env: '+env);
		
            //Homepage Layout - query all active hwl for current environment
			String namespacePrefix=ngForceController.namespacePrefix;
			String query='select Id,Name,'+namespacePrefix+'Grid_Data__c,'+namespacePrefix+'Grid_Width__c,'+namespacePrefix+'Field_Reference__c,'+namespacePrefix+'Criteria_Filter_Logic__c,'+namespacePrefix+'Criteria__c,'+namespacePrefix+'Default__c from '+namespacePrefix+'Homepage_Widget_Layout__c ';
            query+='where '+namespacePrefix+'Is_Active__c=true and '+namespacePrefix+'Env__c=\'' + env.Id + '\'';
			system.debug('### query: '+query);
			List<Homepage_Widget_Layout__c> hwlList;
			List<String> listFields = new List<String>{'Grid_Data__c','Grid_Width__c','Field_Reference__c','Criteria_Filter_Logic__c','Criteria__c','Default__c','Is_Active__c','Env__c'};
        		if(SecurityEnforceUtility.checkObjectAccess('Homepage_Widget_Layout__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
					if(SecurityEnforceUtility.checkFieldAccess('Homepage_Widget_Layout__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
            			hwlList=(List<Homepage_Widget_Layout__c>)Database.query(query);
					} else {
            			ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Homepage_Widget_Layout__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'HomepageController', 'HomepageController', null);
						throw new PortalPackageException(permissionErrorMessage);
					}
        		} else {
            		ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Homepage_Widget_Layout__c '), 'HomepageController', 'HomepageController', null);
					throw new PortalPackageException(permissionErrorMessage);
        		}
            system.debug('### hwlList: '+hwlList);
            
            
            Map<String, Set<String>> objectFieldMap = new Map<String, Set<String>>();
            Map<String, Set<String>> referenceObjectFieldMap = new Map<String, Set<String>>();
            
            String gridString;
            List<ERx_pageEntities.HomePageWigetGridDataWrapper> gridDataList;
            List<String> totalWidgetNumberList = new List<String>();
            String layoutCriteriaString;
            List<HomepageCriteria> criteriaList;
            if(hwlList != null && hwlList.size() > 0) {
            	for(Homepage_Widget_Layout__c hwlItem : hwlList) {
            		
            		// Fetching field list from Home widget layout criteria
            		if (!hwlItem.Default__c) {
            			// Case : Layout has criteria. Fetch Criteria Fields Details to Queried
            			layoutCriteriaString = hwlItem.Criteria__c;
            			if(String.isNotBlank(layoutCriteriaString)) {
            				criteriaList = (List<HomepageCriteria>) JSON.deserialize(layoutCriteriaString, List<HomepageCriteria>.class);
            				if(criteriaList != null && criteriaList.size() > 0) {
            					List<Map<String, Set<String>>> tempList = CommunityUtility.getObjectFieldsFromCriteria(criteriaList, objectFieldMap, referenceObjectFieldMap);
            					objectFieldMap = tempList[0];
            					referenceObjectFieldMap = tempList[1];
            				}
            			}
            			
            		}
            		
            		// Creating list of widget numbers to be fetched
            		gridString = hwlItem.Grid_Data__c;
            		gridString = gridString.replace('"number"', '"widgetNumber"');
            		gridDataList = (List<ERx_PageEntities.HomePageWigetGridDataWrapper>) JSON.deserialize(gridString, List<ERx_PageEntities.HomePageWigetGridDataWrapper>.class);
            		for(ERx_pageEntities.HomePageWigetGridDataWrapper gridData : gridDataList) {
                		if(String.isNotBlank(gridData.widgetNumber)) { 
                			totalWidgetNumberList.add(gridData.widgetNumber);
                		}
                	}
            	}
            }
            
            // Fetch All the active widgets by widget number
            List<Homepage_Widget__c> hwList = ngForceController.getEnvWidgetByWidgetNumber(String.join(totalWidgetNumberList, ','), envId);
            
            if(hwList != null && hwList.size() > 0) {
            	for(Homepage_Widget__c hw : hwList) {
            		// Fetching fields from content
            		if(String.isNotBlank(hw.Content__c)) {
            			Matcher m = PortalPackageConstants.CURLIES_WORD_STRING.matcher(hw.Content__c);
            			if(m.find()) {
            				do {
            					// Removing curli brakets from field string
        						String fieldName = m.group().replace('{{', '').replace('}}', '');
        						if(String.isNotBlank(fieldName)) {
        							// Condition Added to resolve PD-2804
        							if(fieldName.containsIgnoreCase('|') && (fieldName.containsIgnoreCase('contact.') || fieldName.containsIgnoreCase('application.'))) {
        								fieldName = fieldName.split('\\|')[0].trim();
        							}
	        						// Spliting field on dot operator to find object and field
	            					List<String> objectField = fieldName.split('\\.');
	            					if(objectField.size() > 0) {
	            						String objectName = objectField[0].toLowerCase();
	            						objectField.remove(0);
	            						// Creating field name to be fetched for the object
	            						String fieldToFetch = String.join(objectField, '.');
	            						if(objectName == 'contact') {
	            							// Case : Object is Contact
	            							if(!objectFieldMap.containsKey(objectName)) {
	            								//fieldsSet = objectFieldMap.get(objectName);
	            								objectFieldMap.put(objectName, new Set<String>());
	            							}
	            							// Adding field in map
	            							objectFieldMap.get(objectName).add(fieldToFetch);
	            						} else if(objectName == 'application') {
	            							// Case : Application Object
	            							if(!objectFieldMap.containsKey('enrollmentrxrx__enrollment_opportunity__c')) {
	            								//fieldsSet = objectFieldMap.get(objectName);
	            								objectFieldMap.put('enrollmentrxrx__enrollment_opportunity__c', new Set<String>());
	            							}
	            							// Adding field in map
	            							objectFieldMap.get('enrollmentrxrx__enrollment_opportunity__c').add(fieldToFetch);
	            						}
	            					}
        						}
            				} while(m.find());
            			}
            		}
            		
            		if (!hw.Default__c) {
            			// Case : Layout has criteria. Fetch Criteria Fields Details to Queried
            			layoutCriteriaString = hw.Criteria__c;
            			if(String.isNotBlank(layoutCriteriaString)) {
            				criteriaList = (List<HomepageCriteria>) JSON.deserialize(layoutCriteriaString, List<HomepageCriteria>.class);
            				if(criteriaList != null && criteriaList.size() > 0) {
            					List<Map<String, Set<String>>> tempList = CommunityUtility.getObjectFieldsFromCriteria(criteriaList, objectFieldMap, referenceObjectFieldMap);
            					objectFieldMap = tempList[0];
            					referenceObjectFieldMap = tempList[1];
            				}
            			}
            		}
            	}
            }
            
            // Checking Contact and Application
            Map<String, String> mapWhereClause = new Map<String, String>();
            Set<String> customFields = new Set<String>();

			//contact
            mapWhereClause.clear();
            customFields.clear();
            mapWhereClause.put('Id=', CommunityUtility.getCurrentContactId());
            
			// Security Enforce Missed
            sObject contact=(sObject)CommunityUtility.getObjectWithFields('Contact', mapWhereClause, objectFieldMap.get('contact'))[0];
            currentContact = JSON.serialize(contact);
            contactId=(String)contact.get('Id');
			isApplicationFieldExist = false;
            //application
            sObject application=null;
            if(ngForceController.hasApplicationObject){
                mapWhereClause.clear();
                customFields.clear();
                mapWhereClause.put('Id=', CommunityUtility.getActiveApplicationId());
               // added by Nikhil PD-1732 home page not loaded until the Active Application is not created.   
                if(CommunityUtility.getActiveApplicationId() == null){
	                isActiveApplicationExist = false;
			        return;
                }
                
				// Security Enforce Missed
                application=(sObject)CommunityUtility.getObjectWithFields('EnrollmentrxRx__Enrollment_Opportunity__c', mapWhereClause, objectFieldMap.get('enrollmentrxrx__enrollment_opportunity__c'))[0];
    			List<Id> recordIdList = new List<Id>();
    			applicationId = (String)application.get('Id');
    			Map<String, List<SObject>> ObjectMap = new Map<String, List<SObject>>();
    			ObjectMap = ngForceController.processChecklistCotents(applicationId);
    			if(ObjectMap != null) {
	    			for(List<SObject> record : ObjectMap.values()) {
	    				if(record != null && record.size() > 0 && record.get(0) != null) {
	    					recordIdList.add(record.get(0).Id);
	    				}
	    			}
    			}
    			for(Id recordId : recordIdList) {
	    			Id appId = recordId;
					Schema.SObjectType sobjectType = appId.getSObjectType();
					String sobjectName = sobjectType.getDescribe().getName();
					String packageName = PortalPackageUtility.getFinalNameSpacePerfix();
					Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(sobjectName).getDescribe().fields.getMap();
					for(Schema.SObjectField f : objectFields.values()) {
		            	string fieldName = f.getDescribe().getName();
			            if (fieldName == 'EnrollmentrxRx__Allow_Upload__c' || fieldName == 'EnrollmentrxRx__Instructive_Text__c' || fieldName == 'EnrollmentrxRx__Form_URL__c') {
			            	isApplicationFieldExist = true;
			            	break;
			            }
				 	}
    			}
            }
            
            activeApplication = JSON.serialize(application);
            if(hwlList.size()!=0){
                HomepageCriteriaParser hcp=new HomepageCriteriaParser(hwlList, contact, application, referenceObjectFieldMap);
                if(hcp.sobjList.size()!=0 && hcp.errorMessageList.size()==0){
                    hwlListJSON=String.escapeSingleQuotes(JSON.serialize(hcp.sobjList));
                    hwlListJSON=hwlListJSON.replace('</script>','<\\/script>');//fix the closing </script> tag escape issue
                    
                    // Iterating each layout to find the widget list
                    String criteriaFilterLogic;
                    List<sObject> tempHwlList = hcp.sobjList;
                    sObject hwl;
                    sObject defaultHwl;
                    if(tempHwlList != null && tempHwlList.size() > 0) {
                    	for(sObject item : tempHwlList) {
                    		if (!Boolean.valueOf(item.get('Default__c'))) {
                    			criteriaFilterLogic = String.valueOf(item.get('Criteria_Filter_Logic__c'));
                    			criteriaFilterLogic = String.join(PortalPackageUtility.clean(criteriaFilterLogic.split('\\s'), ''), ' ');
                    			criteriaFilterLogic = criteriaFilterLogic.replace('&&', 'and').replace('||', 'or');
                    			if(ExpressionReplacer.evaluateExpression(criteriaFilterLogic)) {
                    				hwl = item;
                    			}
                    		} else {
                    			if(defaultHwl == null) {
                    				defaultHwl = item;
                    			}
                    		}
                    	}
                    	
                    	// If No Layout Criteria Satisfield then setting default criteria
                    	if(hwl == null) {
                    		hwl = defaultHwl;
                    	}
                    	
                    }
                    
                    if(hwl != null) {
                    	gridString = String.valueOf(hwl.get('Grid_Data__c'));
                    	List<String> widgetNumberList = new List<String>();
                    	gridString = gridString.replace('"number"', '"widgetNumber"');
                    	gridDataList = (List<ERx_PageEntities.HomePageWigetGridDataWrapper>) JSON.deserialize(gridString, List<ERx_PageEntities.HomePageWigetGridDataWrapper>.class);
                    	for(ERx_pageEntities.HomePageWigetGridDataWrapper gridData : gridDataList) {
                    		if(String.isNotBlank(gridData.widgetNumber)) { 
                    			widgetNumberList.add(gridData.widgetNumber);
                    		}
                    	}
                    	
                    	List<Homepage_Widget__c> layoutWidgetList = communityUtility.findWidgetForLayoutByNumber(hwList, widgetNumberList);
                    	
                    	// fetching widget details
                    	List<Object> homepageWidgetDescribesResult = ngForceController.parseWidgetCriteria(layoutWidgetList, contact, application, referenceObjectFieldMap);
                    	homepageWidgetDescribesResultJSON = JSON.serialize(homepageWidgetDescribesResult);
                    	if(homepageWidgetDescribesResult != null && homepageWidgetDescribesResult.size() > 0 && !(homepageWidgetDescribesResult[0] instanceof String)) {
	                    	Boolean isCheckListUsed = false;
	                    	for(Object hpw : homepageWidgetDescribesResult) {
	                    		if(hpw instanceof Homepage_Widget__c) {
	                    			Homepage_Widget__c hpwi = (Homepage_Widget__c) hpw;
		                    		String widgetContent = '';
		                    		if(hpwi.Default__c) {
		                    			widgetContent = hpwi.Content__c;
		                    		} else {
		                    			String logicFilter = String.valueOf(hpwi.Criteria_Filter_Logic__c);
	                    				logicFilter = String.join(PortalPackageUtility.clean(logicFilter.split('\\s'), ''), ' ');
	                    				logicFilter = logicFilter.replace('&&', 'and').replace('||', 'or');
	                    				if(ExpressionReplacer.evaluateExpression(logicFilter)) {
	                    					widgetContent = hpwi.Content__c;
	                    				}
		                    		}
		                    		
		                    		if(String.isNotBlank(widgetContent)) {
		                    			if(widgetContent.containsIgnoreCase('</erx-checklist>') || widgetContent.containsIgnoreCase('<erx-checklist/>')) {
		                    				isCheckListUsed = true;
		                    				break;
		                    			}
		                    		}
	                    		}
	                    	}
	                    	
	                    	if(isCheckListUsed && ngForceController.hasApplicationObject) {
	                    		receivedChecklistStatusJSON = CommunityUtility.getCoreConfigurationSettingValueByStatus('RECEIVED');
	                    		Map<String, List<SObject>> checklistResultData = ngForceController.processChecklistCotents(applicationId);
	                    		checklistResultJSON = JSON.serialize(checklistResultData);
	                    	}
                    	}
                    }
                }
                else if(hcp.errorMessageList.size()!=0){
                    errorMessage='<div style="color:red">Criteria Error:</div><ul>';
                    for(String em : hcp.errorMessageList){
                        errorMessage += '<li style="line-height: 40px;word-break: break-word;">' + em + '</li>';
                    }
                    errorMessage += '</ul>';
                }
            }
            else
            errorMessage='Homepage Layout Not Found<br/>Please make sure layout active and/or meet the display criteria';
        }
        catch(Exception e){
        	//System.assert(false, e);
            ///ApexPages.addMessage(new ApexPages.Message(Apexpages.severity.ERROR,e.getMessage()));
            // PD-3972 || 28-9-2018 || to prevent the portal package loggers for home page when user is not logged in
            if(CommunityUtility.getCurrentContactId() != null ){
            	ERx_PortalPackageLoggerHandler.addException(e,'HomepageControllerrrr','Constructor', null);
            }
            errorMessage='Environment Error For Current User<br/>'+e.getMessage();
        }
    }

     /*******************************************************************************************************
    * @description forces DML to insert log messages in database
    */
    public void initPageException() {
        try {
         ERx_PortalPackageLoggerHandler.logDebugMessage('@@@@@@@@@@@@@@@@@@' + ERx_PortalPackUtil.loggerList);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();

        } catch (Exception e) {
         ERx_PortalPackageLoggerHandler.addException(e,'HomepageController','initPageException', null);
         ERx_PortalPackageLoggerHandler.saveExceptionLog();
        }
    }


}