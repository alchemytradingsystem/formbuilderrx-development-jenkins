/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description SideBarComponent class used for left navigation list
*/
public with sharing class SideBarComponent {
	/****
    * @description list of page that is displayed on left navigation for current env
    */ 
    public List<PageWrapperModel> pages {get;set;}
    /****
    * @description Id of  current env
    */ 
    public String currentEnvId {get;set;}
    /****
    * @description Type of current type either Community or Site.
    */ 
    public String currentEnyType {get;set;}
    /****
    * @description contain package prefix
    */
    public String prefix {get;set;}
    
    /****
    * @description contain package prefix
    */
    public String currentPageId {get;set;}
 	/*******************************************************************************************************
    * @description init portal package prefix.
    */
    
    public boolean debugging {
    	get {
    		FormBuilder_Settings__c fs= FormBuilder_Settings__c.getValues('Admin Settings');
    		Boolean checkJs = false;
	        if(fs!=null){
	            checkJs = fs.Debugging__c;
	        }
	        return checkJs;
    	}
    	set; 
    }
    
    public SideBarComponent(){
    	 prefix = PortalPackageUtility.getNameSpacePerfix();
    	 if(prefix != null && prefix != ''){
    	 	 prefix = prefix + '__';
    	 }
    }

}