public with sharing class CreatePortalUserTriggerHandler {
 	
	// private variables
    private boolean m_isexecuting = false;
    
    //PD-4402
	 /**** 
    * @description To store PackageName;
    */
    private static String namespacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
    //PD-4402
    /****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
    
    // properties
    public boolean istriggercontext{
        get{ return m_isexecuting;}
    } 
    
    // construction
    public CreatePortalUserTriggerHandler(boolean isexecuting){ 
        m_isexecuting = isexecuting;
    }
    
    public void onBeforeInsertOrUpdate(list<Contact> newContactList){
    	//get Account Id
		FormBuilder_Settings__c customSetting =  FormBuilder_Settings__c.getInstance('Admin Settings');
		// if no account for contact exist assign account id from custom setting
		if(customSetting != null){
			for(Contact conObj : newContactList){
				if(String.isBlank(conObj.AccountId) && String.isNotBlank(conObj.Create_User_with_Profile__c)){
					conObj.AccountId = customSetting.Portal_Account_Id__c;
				}
			}			
		}
    }
    
    public void onAfterInsert(list<Contact> newContactList) {
    	map<Contact,String> mapContactProfile = new map<Contact,String>();
    	set<id> contactIds = new set<id>();
    	for(Contact conObj : newContactList){
    		if(String.isNotBlank(conObj.Create_User_with_Profile__c) && String.isNotBlank(conObj.email)){
    			mapContactProfile.put(conObj,PortalPackageUtility.escapeQuotes(conObj.Create_User_with_Profile__c));
    			contactIds.add(conObj.id);
    		}
    	}
    	if(mapContactProfile.size() > 0){
    		createPortalUser(mapContactProfile,false,contactIds);	
    	}
    	
    }
    
    public void onAfterUpdate(list<Contact> oldContactList, list<Contact> newContactList,
		 map<id, Contact> oldmap, map<id, Contact> updatedmap){
		 	map<Contact,String> mapContactProfile = new map<Contact,String>();
		 	set<id> contactIds = new set<id>();
	    	for(Contact conObj : newContactList){
	    		if(String.isNotBlank(conObj.Create_User_with_Profile__c) && String.isNotBlank(conObj.email) && (oldmap.get(conObj.id).Create_User_with_Profile__c != updatedmap.get(conObj.id).Create_User_with_Profile__c)){
	    			mapContactProfile.put(conObj,PortalPackageUtility.escapeQuotes(conObj.Create_User_with_Profile__c));
	    			contactIds.add(conObj.id);
	    		}
	    	}
	    	
	    	if(mapContactProfile.size() > 0){
	    		createPortalUser(mapContactProfile,true,contactIds);	
	    	}
	}
	
	private void createPortalUser(map<Contact,String> mapContactProfile, boolean isUpdate,set <id> contactIds){
		map <String,User> mapContactUser = new map<String,User>();
		map<String,id> mapProfileToContact = new map<String,id>();
		Map<Id,Id> userProfileIds = new Map<Id,Id>();
		
		//get Users Corresponding to the contact
		if(isUpdate){
			for(User userObj : [SELECT id,ProfileId,ContactId FROM USER where ContactId IN: contactIds]){
				mapContactUser.put(userObj.ContactId,userObj);
			}
		}
		
		// create a map for profile and contact
	    for(Profile profileObj : [SELECT Id,Name FROM Profile WHERE Name IN: mapContactProfile.values()]){
	    	mapProfileToContact.put(profileObj.Name,profileObj.id);
	    }
  		
		
		list<User> usersToUpdate = new list<User> ();
		list<User> usersToInsert = new list<User> ();
		for(Contact conObj : mapContactProfile.keySet()){
			User usr;
			// if user exist update it
			if(mapContactUser.containsKey(conObj.id)){
				usr = mapContactUser.get(conObj.id);
				usr.ProfileId = ( mapProfileToContact.containsKey(conObj.Create_User_with_Profile__c) ? (mapProfileToContact.get(conObj.Create_User_with_Profile__c)) : usr.ProfileId);
				usersToUpdate.add(usr);
				userProfileIds.put(usr.Id,usr.ProfileId);
			}else{
				
				usr = new User(LastName = conObj.LastName,
		                     FirstName= conObj.FirstName,
		                     Alias = (String.isBlank(conObj.FirstName) ? 'PortUser' : 
		                     					((conObj.FirstName.length() > 8) ? conObj.FirstName.subString(0,8) : conObj.FirstName)),
		                     Email = conObj.email,
		                     Username = conObj.email,
		                     ProfileId = (mapProfileToContact.get(conObj.Create_User_with_Profile__c)),
		                     emailencodingkey='UTF-8' , languagelocalekey='en_US', 
		                     localesidkey='en_US', timezonesidkey='America/Los_Angeles',
		                     ContactId = conObj.id
                     		);
         		usersToInsert.add(usr);	
			}
		}
		
		//list to insert new users
		if(usersToInsert.size() > 0){
			
			//PD-4402 || FLS Create - Security check added
   		    List<String> listFields = new List<String>{'LastName','FirstName','Alias','Email','Username','ProfileId','emailencodingkey','languagelocalekey','localesidkey','timezonesidkey','ContactId'};
			if(SecurityEnforceUtility.checkObjectAccess('User', SecurityTypes.IS_INSERT, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('User', listFields, SecurityTypes.IS_INSERT, namespacePrefix)){
						insert usersToInsert;
				} else {
	            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: User  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'CreatePortalUserTriggerHandler', 'createPortalUser', null);
					throw new PortalPackageException(permissionErrorMessage); 
				}
			} else {
	        	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: User'), 'CreatePortalUserTriggerHandler', 'createPortalUser', null);
				throw new PortalPackageException(permissionErrorMessage); 
			}
			//PD-3729 assign permission set
			assignPermissionSetToNewUser(usersToInsert[0].Id);	
		}
		
		//list to update existing users
		if(usersToUpdate.size() > 0){
			updateUser(userProfileIds);
		}

	}
	/*05-09-18 Added By Shubham Re PD-3729*/
	@future
	private static void assignPermissionSetToNewUser(Id userId) {
		List<PermissionSet> psl = [SELECT Id,Label From PermissionSet WHERE Label = 'ERxFB_Community'];
    	if(psl != null && psl.size() > 0){
    		PermissionSetAssignment psa = new PermissionSetAssignment
			(PermissionSetId = psl[0].Id, AssigneeId = userId);
			insert psa;
    	}
	}
	
	@future
	private static void updateUser(Map<Id,Id> userProfileIds) {
		list<User> usersToUpdate = new list<User> ();
		User usr;
		for(Id userId:userProfileIds.keySet()) {
			usr = new User(Id=userId, ProfileId=userProfileIds.get(userId));
			usersToUpdate.add(usr);
		}
		//PD-4402 || FLS Create - Security check added
		if(SecurityEnforceUtility.checkObjectAccess('User', SecurityTypes.IS_UPDATE, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('User', 'ProfileId', SecurityTypes.IS_UPDATe, namespacePrefix)){
					update usersToUpdate;
				} else {
	            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: User  FieldList :: ProfileId' ), 'CreatePortalUserTriggerHandler', 'createPortalUser', null);
					throw new PortalPackageException(permissionErrorMessage); 
				}
			} else {
	        	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: User'), 'CreatePortalUserTriggerHandler', 'createPortalUser', null);
				throw new PortalPackageException(permissionErrorMessage); 
			}
		
		}
	
}