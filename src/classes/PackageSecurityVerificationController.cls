/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description The PackageSecurityVerificationController class is used for calling the rest service which links to the LMA Org.
*/
public with sharing class PackageSecurityVerificationController {
    /****
    * @description contains Http Obj to call API
    */ 
    public Http httpObj;
    
    /****
    * @description contains request object to store request to be sent
    */ 
    public HttpRequest reqObj;
    
    /****
    * @description contains URL for the rest API
    */ 
    public String url;
    
    public PackageSecurityVerificationController(){
        // Instantiate a new http object
        httpObj = new Http();
        //url = 'https://justcrafts-developer-edition.ap2.force.com/PackageSite/services/apexrest/PackageSiteVerification';
        url = 'https://enrollmentrxsales.secure.force.com/PackageKey/services/apexrest/PackageSiteVerification';
    }
    
    // Pass in the subscriber id to be used using as param in string url
    public String getCalloutResponseContents(String subsOrgId,String packageName) {
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        reqObj = new HttpRequest();
        String param = '{\"'+'subsOrgId'+'\"'+':'+'\"'+subsOrgId+'\",\"packageName\":\"'+packageName+'\"}';
         //'subsOrgId=' + subsOrgId;
         system.debug('-- INSIDE PackageSecurityVerificationController PARAM -- '+ param);
        reqObj.setEndpoint(url);
        system.debug('--- URL FORMED---' + url);
        reqObj.setMethod('POST');
        reqObj.setHeader('Content-Type', 'application/json');
        reqObj.setBody(param);
        // Send the request, and return a response
        if(!Test.isRunningTest()){
            HttpResponse res = httpObj.send(reqObj);
            //system.assert(false,res.getBody());
            String result;
            if(res.getStatusCode() == 200) {
                result = res.getBody();
            }
            return result;
        }else{
            return '2021-06-30';
        }
        
    }

}