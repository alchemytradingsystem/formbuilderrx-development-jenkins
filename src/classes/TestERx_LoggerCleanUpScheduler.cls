/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-01-31
	@description TestERx_LoggerCleanUpScheduler to cover test coverage for ERx_LoggerCleanUpScheduler
*/
@isTest
private class TestERx_LoggerCleanUpScheduler {	
	
	/*
    	Test Case 1: when no value in DeleteLoggerBeforeDays__c of formBuilderSetting
    */
    static testMethod void testScheduleNoCustomSettingData() {
        Test.startTest();
        
        	//insert test data
        	Portal_Package_Logger__c log = new Portal_Package_Logger__c( Message__c = ('Test record for ERx_CleanUpPortalPackageLogger'), TypeName__c = 'Info');
        	insert log;
        	
        	FormBuilder_Settings__c setting = new FormBuilder_Settings__c();
			setting.Name = 'Admin Settings';
			insert setting;
        	ID processid = ERx_LoggerCleanUpScheduler.scheduleIt('Formbuilder ERx_LoggerCleanUpScheduler', '0 0 0 * * ?');
        	List<CronTrigger> ctList = [SELECT CronExpression, TimesTriggered, NextFireTime, EndTime, PreviousFireTime, StartTime  FROM CronTrigger WHERE Id = :processid LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(1, ctList.size());
		    // Verify the expressions are the same
		    System.assertEquals('0 0 0 * * ?', String.valueOf(ctList[0].CronExpression));
		    // Verify the job has not run
      		System.assertEquals(0, ctList[0].TimesTriggered);
        Test.stopTest();
    }
	
	/*
    	Test Case 2: When no BatchApex job running
    	when some value in DeleteLoggerBeforeDays__c of formBuilderSetting
    */
    static testMethod void testScheduleNoBatchInProgress() {
        Test.startTest();
        
        	//insert test data
        	Portal_Package_Logger__c log = new Portal_Package_Logger__c( Message__c = ('Test record for ERx_CleanUpPortalPackageLogger'), TypeName__c = 'Info');
        	insert log;
        	
        	FormBuilder_Settings__c setting = new FormBuilder_Settings__c();
			setting.Name = 'Admin Settings';
			setting.DeleteLoggerBeforeDays__c = 2;
			insert setting;
        	ID processid = ERx_LoggerCleanUpScheduler.scheduleIt('Formbuilder ERx_LoggerCleanUpScheduler', '0 0 0 * * ?');
        	List<CronTrigger> ctList = [SELECT CronExpression, TimesTriggered, NextFireTime, EndTime, PreviousFireTime, StartTime  FROM CronTrigger WHERE Id = :processid LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(1, ctList.size());
		    // Verify the expressions are the same
		    System.assertEquals('0 0 0 * * ?', String.valueOf(ctList[0].CronExpression));
		    // Verify the job has not run
      		System.assertEquals(0, ctList[0].TimesTriggered);
        Test.stopTest();
    }
    
    /*
    	Test Case 3: When 5 BatchApex job running by setter of runningJobCount
    	when some value in DeleteLoggerBeforeDays__c of formBuilderSetting
    */
    static testMethod void testScheduleWithBatchLimit() {
        Test.startTest();
        
        	//insert test data
        	Portal_Package_Logger__c log = new Portal_Package_Logger__c( Message__c = ('Test record for ERx_CleanUpPortalPackageLogger'), TypeName__c = 'Info');
        	insert log;
        	
        	FormBuilder_Settings__c setting = new FormBuilder_Settings__c();
			setting.Name = 'Admin Settings';
			setting.DeleteLoggerBeforeDays__c = 2;
			insert setting;
			ERx_LoggerCleanUpScheduler.runningJobCount = 5;
        	ID processid = ERx_LoggerCleanUpScheduler.scheduleIt('Test 2 : Formbuilder ERx_LoggerCleanUpScheduler', '0 0 0 * * ?');
        	List<CronTrigger> ctList = [SELECT CronExpression, TimesTriggered, NextFireTime, EndTime, PreviousFireTime, StartTime  FROM CronTrigger WHERE Id = :processid LIMIT :limits.getLimitQueryRows()];
		    System.assertEquals(1, ctList.size());
		    // Verify the expressions are the same
		    System.assertEquals('0 0 0 * * ?', String.valueOf(ctList[0].CronExpression));
		    // Verify the job has not run
      		System.assertEquals(0, ctList[0].TimesTriggered);
        Test.stopTest();
    }
}