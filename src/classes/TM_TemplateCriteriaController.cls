public with sharing class TM_TemplateCriteriaController {
	
	/****
	* Default Constructor
	*/
    public TM_TemplateCriteriaController() {
    	nameSpacePrefix = PortalPackageUtility.getFinalNameSpacePerfix();
	}
	
	/****
    * @description rendered criteria
    */
    public RenderCriteriaConditionWrapper criteria {get;set;}
    
    /****
    * @description variable to hold the current index changes
    */
    public Integer changeIndex {get;set;}
    /****
    * @description variable to hold the selected value
    */
    public String selectedValue {get;set;}
    /****
    * @description variable to hold the current index changes
    */
    public String selectedDisplayValue {get;set;}
    /****
    * @description variable to hold the namespace of package
    */
    public String nameSpacePrefix {get;set;}
    
    public String picklistSelectedValueList {get;set;}
    
    /****
    * Description : to hold search result output for reference field
    */
    public List<ERx_PageEntities.SelectOpWrap> lookupResult {get;set;}
    
    
    
    
    /****
    * @description variable to check is Application Object present or not
    */
    public static Boolean hasApplicationObject{
        get{
            hasApplicationObject =Schema.getGlobalDescribe().containsKey('EnrollmentrxRx__Enrollment_Opportunity__c');
            //hasApplicationObject=false;
            return hasApplicationObject;
        }
        set;
    }
    
    /****
    * @description criteria operator list
    */
    public static List<SelectOption> operatorList {
    	get {
    		operatorList = new List<SelectOption>();
    		operatorList.add(new SelectOption('', '--None--'));
    		for(String op : new List<String>{'equals', 'not equal to', 'starts with', 'ends with', 'contains', 'does not contain'}) {
    			operatorList.add(new SelectOption(op, op));
    		}
    		return operatorList;
    	}
    	set;
    }
    
    /****
    * @description criteria condition object list
    */
    public static List<String> criteriaObjectNamesAPEX {
        get{
            criteriaObjectNamesAPEX = new List<String>();
            criteriaObjectNamesAPEX.add('Contact');
            if(hasApplicationObject && !Test.isRunningTest()) {
            	criteriaObjectNamesAPEX.add('EnrollmentrxRx__Enrollment_Opportunity__c');
            }
            return criteriaObjectNamesAPEX;
        }
        set;
    }
    
    /****
    * @description (object) select option list to display select list
    */
    public static List<SelectOption> criteriaObjectSelectOption {
    	get {
    		criteriaObjectSelectOption = new List<SelectOption>();
    		for(String objName : criteriaObjectNamesAPEX) {
    			criteriaObjectSelectOption.add(new SelectOption(objName, objName));
    		}
    		return criteriaObjectSelectOption;
    	}
    	set;
    }

    /****
    * @description Get all necessary object describe results
    */
    public static Map<String,Map<String, Object>> describeResultsOutput {
        get{
            Map<String, Schema.SobjectType> schemaMap = Schema.getGlobalDescribe();
            Map<String,String> objtypeMap = new Map<String,String>();
            List<String>criteriaObjectList = criteriaObjectNamesAPEX;
            // creating object map thats field to be fetched
            for(String objectName : criteriaObjectList){
                objtypeMap.put(objectName, objectName);
                Map <String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();
                for(Schema.SObjectField sfield : fieldMap.Values()){
                    schema.describeFieldResult dfr = sfield.getDescribe();
                    if(String.valueOf(dfr.getType()).toLowerCase() == 'reference'){
                        for(Schema.sObjectType sobjType : dfr.getReferenceTo()){
                            String refObjectName = sobjType.getDescribe().getName();
                            if(refObjectName.toLowerCase() == 'group') {
                            	continue;
                            }
                            if(!objtypeMap.containsKey(refObjectName)) {
                            	objtypeMap.put(refObjectName,refObjectName);
                            }
                        }
                    }
                }
            }
            describeResultsOutput = TM_TemplateCriteriaController.describes(objtypeMap.values());
            return describeResultsOutput;
        }
        set;
    }
    
    /****
    * @description Method that describes each object and create necessary map
    * @param list<String> objtypes : Object Names
    * @return Map<String,Map<String, Object>> : Key is Object name and Value is Another Map (Key Property Name e.g. fields, selectoption etc.).
    */
    private static Map<String,Map<String, Object>> describes(list<String> objtypes) {
        Map<String,Map<String, Object>> describeResults = new Map<String,Map<String, Object>>();
        for(String objtype:objtypes){
            // Just enough to make the sample app work!
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objtype);
            if (targetType == null) {
                return (Map<String, Map<String, Object>>) json.deserialize('[{"message":"The requested resource does not exist","errorCode":"NOT_FOUND"}]', Map<String, Map<String, Object>>.class);
            }

            Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();

            Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();

            List<Object> fields = new List<Object>();
            for (String key : fieldMap.keySet()) {
                Schema.DescribeFieldResult descField = fieldMap.get(key).getDescribe();
                Map<String, Object> field = new Map<String, Object>();

                field.put('type', descField.getType().name().toLowerCase());
                field.put('name', descField.getName());
                field.put('label', descField.getLabel());
                field.put('picklistValues', descField.getPicklistValues());
                field.put('controllerName', String.valueOf(descField.getController()));
                List<String> references = new List<String>();
                for (Schema.sObjectType t : descField.getReferenceTo()) {
                	if(t.getDescribe().getName().toLowerCase() != 'group') {
                    	references.add(t.getDescribe().getName());
                	}
                }
                if (!references.isEmpty()) {
                    field.put('referenceTo', references);
                }

                fields.add(field);
            }

            Map<String, Object> result = new Map<String, Object>();
            result.put('fields', fields);
            List<SelectOption> objectSelectOption = new List<SelectOption>();
            for(Object f : fields) {
            	Map<String, Object> fieldInstance = (Map<String, Object>) f;
            	objectSelectOption.add(new SelectOption(String.valueOf(fieldInstance.get('name')), String.valueOf(fieldInstance.get('label'))));
            }
            result.put('selectoption', objectSelectOption);
            describeResults.put(objtype,result);
        }
        return describeResults;
    }
    
	/****
	* @desction : Method is used to reset property value for criteria
	*/
    private void resetCriteriaValueOnCriteriaChange() {
    	if(changeIndex != null) {
    		if(String.isNotBlank(criteria.renderedCondition[changeIndex].criteriaType)) {
    			// Case : When Criteria Value Changed to Criteria value then Resetting Property Value.
    			criteria.renderedCondition[changeIndex].modelName = '';
    			criteria.renderedCondition[changeIndex].fieldAPIName = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldAPIName = '';
		    	criteria.renderedCondition[changeIndex].fieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].displayValue = '';
		    	criteria.renderedCondition[changeIndex].value = '';
		    	criteria.renderedCondition[changeIndex].referencedObjectName = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldObjectName = '';
		    	criteria.renderedCondition[changeIndex].lookupSearchString = '';
				criteria.renderedCondition[changeIndex].lookupRecords = null;
    		}
    	}
    	changeIndex = null;
    }
    
    /****
    * Description : Method that create a dummy criteria row
    */
    public static ERx_PageEntities.TemplateRenderedConditionWrapper addBlankRow() {
    	ERx_PageEntities.TemplateRenderedConditionWrapper rrcw = new ERx_PageEntities.TemplateRenderedConditionWrapper();
    	rrcw.criteriaType = '';
    	rrcw.modelName = '';
    	rrcw.fieldAPIName = '';
    	rrcw.referencedFieldAPIName = '';
    	rrcw.fieldDisplayType = '';
    	rrcw.referencedFieldDisplayType = '';
    	rrcw.displayValue = '';
    	rrcw.value = '';
    	rrcw.fieldOperator = '';
    	rrcw.referencedObjectName = '';
    	return rrcw;
    	
    }
    
    /*
    * @description Method is used to set active true if default is checked.
    */
    public void disableActive() {
    	try {
	    	if(criteria.isDefault) {
	    		// Case 1 : When Default is True setting Active true
	    		criteria.isActive = true;
	    	} else {
	    		// Case 2 : If Default False and Active True Adding Blank Row for Render Condition
	    		if(criteria.renderedCondition == null || criteria.renderedCondition.size() == 0) {
	    			criteria.renderedCondition = new List<ERx_PageEntities.TemplateRenderedConditionWrapper>();
	    			criteria.renderedCondition.add(addBlankRow());
	    		}
	    	}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    /****
    * @description Method is used to set dummy when active selected first time
    */
    public void activeSelected() {
    	try {
	    	if(criteria.isActive) {
	    		// Case : When Active Checked.
	    		if(criteria.renderedCondition == null || criteria.renderedCondition.size() == 0) {
	    			// Case : When Criteria is Null then Assigning a dummy row.
	    			criteria.renderedCondition = new List<ERx_PageEntities.TemplateRenderedConditionWrapper>();
	    			criteria.renderedCondition.add(addBlankRow());
	    		}
	    	}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    public void updateCriteriaType() {
    	try {
    		resetCriteriaValueOnCriteriaChange();
	    	List<Integer> foundLocations = new List<Integer>();
	        // Finding blank Object Name
	        if(criteria.renderedCondition != null) { 
		        for(Integer lc = 0; lc < criteria.renderedCondition.size(); lc++) {
		        	if(String.isBlank(criteria.renderedCondition[lc].criteriaType)) {
		        		foundLocations.add(lc - foundLocations.size());
		        	}
		        }
		        
		        // Removing the blank Value from Criteria
		        for(Integer location : foundLocations) {
	        		criteria.renderedCondition.remove(location);
	        	}
	        	
	        	// Adding dummy in Last.
	        	criteria.renderedCondition.add(addBlankRow());
	        }
        } catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    /****
    * @description : Method to Update the Criteria List. Called When Object Field Value Changed.
    */
    public void updateCriteriaList() {
    	try {
	    	if(changeIndex != null) {
	    		// Case : When Object Value Changed to Another Object then Resetting Property Value.
				criteria.renderedCondition[changeIndex].fieldAPIName = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldAPIName = '';
		    	criteria.renderedCondition[changeIndex].fieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].displayValue = '';
		    	criteria.renderedCondition[changeIndex].value = '';
		    	criteria.renderedCondition[changeIndex].referencedObjectName = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldObjectName = '';
		    	criteria.renderedCondition[changeIndex].lookupSearchString = '';
				criteria.renderedCondition[changeIndex].lookupRecords = null;
	    	}
	    	changeIndex = null;
        } catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    /****
    * @description Method is used to remove criteria from list when remove button clicked.
    */
    public void removeCriteria() {
    	try {
    		if(changeIndex != null) {
    			criteria.renderedCondition.remove(changeIndex);
    		}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    /****
    * @description Method is used to set selected field type and reference object property value.
    */
    public void setFieldType() {
    	try {
	    	if(changeIndex != null) {
	    		criteria.renderedCondition[changeIndex].referencedFieldAPIName = '';
		    	criteria.renderedCondition[changeIndex].fieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].displayValue = '';
		    	criteria.renderedCondition[changeIndex].value = '';
		    	criteria.renderedCondition[changeIndex].referencedObjectName = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldObjectName = '';
		    	criteria.renderedCondition[changeIndex].lookupSearchString = '';
				criteria.renderedCondition[changeIndex].lookupRecords = null;
	    		if(String.isNotBlank(criteria.renderedCondition[changeIndex].fieldAPIName)) {
	    			if(String.isNotBlank(criteria.renderedCondition[changeIndex].modelName)) {
	    				Map<String, Object> fieldMap = describeResultsOutput.get(criteria.renderedCondition[changeIndex].modelName);
	    				if(fieldMap != null && fieldMap.size() > 0) {
	    					List<Object> fieldList = (List<Object>) fieldMap.get('fields');
	    					if(fieldList != null && fieldList.size() > 0) {
	    						for(Object f : fieldList) {
	    							Map<String, Object> fieldObj = (Map<String, Object>) f;
	    							// Comparing the field API Name to fiind the field
	    							if(criteria.renderedCondition[changeIndex].fieldAPIName == String.valueOf(fieldObj.get('name'))) {
	    								// Setting and Reseeting field Property
								    	criteria.renderedCondition[changeIndex].fieldDisplayType = String.valueOf(fieldObj.get('type'));
	    								if(String.valueOf(fieldObj.get('type')) == 'reference') {
	    									// Case : When Selected Field is reference type then setting referenced object Name for fetching one level field.
	    									List<String> referenceToList = (List<String>) fieldObj.get('referenceTo');
	    									criteria.renderedCondition[changeIndex].referencedObjectName = referenceToList[0];
	    								}
	    								break;
	    							}
	    						}
	    					}
	    				}
	    			}
	    		}
	    	}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    	changeIndex = null;
    }
    
    /****
    * Description : Method is used to set field property for first level field (Referenced field).
    */
    public void setReferencedFieldType() {
    	try {
    		if(changeIndex != null) {
    			criteria.renderedCondition[changeIndex].referencedFieldDisplayType = '';
		    	criteria.renderedCondition[changeIndex].displayValue = '';
		    	criteria.renderedCondition[changeIndex].value = '';
		    	criteria.renderedCondition[changeIndex].referencedFieldObjectName = '';
		    	criteria.renderedCondition[changeIndex].lookupSearchString = '';
				criteria.renderedCondition[changeIndex].lookupRecords = null;
	    		if(String.isNotBlank(criteria.renderedCondition[changeIndex].referencedFieldAPIName)) {
	    			if(String.isNotBlank(criteria.renderedCondition[changeIndex].referencedObjectName)) {
	    				Map<String, Object> fieldMap = describeResultsOutput.get(criteria.renderedCondition[changeIndex].referencedObjectName);
	    				if(fieldMap != null && fieldMap.size() > 0) {
	    					List<Object> fieldList = (List<Object>) fieldMap.get('fields');
	    					if(fieldList != null && fieldList.size() > 0) {
	    						for(Object f : fieldList) {
	    							Map<String, Object> fieldObj = (Map<String, Object>) f;
	    							// Comparing field API Name to find field
	    							if(criteria.renderedCondition[changeIndex].referencedFieldAPIName == String.valueOf(fieldObj.get('name'))) {
	    								// Setting field type
								    	criteria.renderedCondition[changeIndex].referencedFieldDisplayType = String.valueOf(fieldObj.get('type'));
	    								if(String.valueOf(fieldObj.get('type')) == 'reference') {
	    									// Case : when field is reference type setting referenced object name
	    									List<String> referenceToList = (List<String>) fieldObj.get('referenceTo');
	    									criteria.renderedCondition[changeIndex].referencedFieldObjectName = referenceToList[0];
	    								}
	    								
	    								break;
	    							}
	    						}
	    					}
	    				}
	    			}
	    		}
    		}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    	changeIndex = null;
    }
    
    public void getRecords() {
    	try {
    		lookupResult = new List<ERx_PageEntities.SelectOpWrap>();
	    	if(changeIndex != null) {
	    		ERx_PageEntities.TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
	    		String searchString = conditionWrapper.lookupSearchString;
	    		if(String.isNotBlank(searchString)) {
					String objectName;
					if(String.isNotBlank(conditionWrapper.referencedFieldObjectName)) {
						objectName = conditionWrapper.referencedFieldObjectName;
					} else if(String.isNotBlank(conditionWrapper.referencedObjectName)) {
						objectName = conditionWrapper.referencedObjectName;
					}
	    		
		    		if(String.isNotBlank(objectName)) {
		    			String sobjectType = '';
		    			if(objectName.toLowerCase() == 'recordtype'){
			              	// If Object Name is Record Type then Fetching all the records types assosiate with that sobjectType.
			              	sobjectType = String.isNotBlank(conditionWrapper.referencedFieldObjectName) ? conditionWrapper.referencedObjectName : conditionWrapper.modelName;
			          	}
			          	
			          	// Fetching Search result 
			          	List<sObject> recordList = PortalPackageUtility.getRecordsByObjectName(objectName, sobjectType, searchString);
			          	
			          	// Adding Null in List
			          	lookupResult.add(new ERx_PageEntities.SelectOpWrap(null, 'NULL'));
			          	
			          	if(recordList != null) {
				          	for(sObject r : recordList) {
				          		// Creating Id, Name value object
				          		lookupResult.add(new ERx_PageEntities.SelectOpWrap(String.valueOf(r.get('Id')), String.valueOf(r.get('Name'))));
				          	}
			          	}
		    		} 
	    		}
	    		// Assigning result data to wrapper
	    		criteria.renderedCondition[changeIndex].lookupRecords = lookupResult;
	    	}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    public void selectReferencedValue() {
    	try {
    		if(changeIndex != null) {
    			ERx_PageEntities.TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
    			conditionWrapper.value = selectedValue;
    			conditionWrapper.displayValue = selectedDisplayValue;
    			conditionWrapper.lookupSearchString = '';
    			conditionWrapper.lookupRecords = null;
    			
    			criteria.renderedCondition[changeIndex] = conditionWrapper;
    			
    			selectedValue = null;
    			selectedDisplayValue = null;
    			changeIndex = null;
    			
    		}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    public void removeReferencedValue() {
    	try {
    		if(changeIndex != null) {
    			ERx_PageEntities.TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
    			conditionWrapper.value = null;
    			conditionWrapper.displayValue = '';
    			
    			criteria.renderedCondition[changeIndex] = conditionWrapper;
    			
    			changeIndex = null;
    			
    		}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
    
    public void updatePicklistValue() {
    	try {
    		List<ERx_PageEntities.SelectOpWrap> picklistSelected = null;
	    	String tempValue = '';
	    	String tempDisplayValue = '';
	    	if(String.isNotBlank(picklistSelectedValueList)) {
	    		picklistSelected = (List<ERx_PageEntities.SelectOpWrap>) JSON.deserialize(picklistSelectedValueList, List<ERx_PageEntities.SelectOpWrap>.class);
	    		for(ERx_PageEntities.SelectOpWrap sow : picklistSelected) {
	    			tempValue += sow.id + ',';
	    			tempDisplayValue += sow.name + ',';
	    		}
	    		
	    		if(String.isNotBlank(tempValue)) {
	    			tempValue = tempValue.removeEnd(',');
	    		}
	    		if(String.isNotBlank(tempDisplayValue)) {
	    			tempDisplayValue = tempDisplayValue.removeEnd(',');
	    		}
	    	}
	    	
	    	
	    	if(changeIndex != null) {
	    		ERx_PageEntities.TemplateRenderedConditionWrapper conditionWrapper = criteria.renderedCondition[changeIndex];
				conditionWrapper.value = tempValue;
				conditionWrapper.displayValue = tempDisplayValue;
				
				criteria.renderedCondition[changeIndex] = conditionWrapper;
				
				changeIndex = null;
				picklistSelectedValueList = null;
	    	}
    	} catch(Exception e) {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
        	ApexPages.addMessage(msg);
    	}
    }
}