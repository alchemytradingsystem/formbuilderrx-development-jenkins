@isTest
private class TM_TemplateController_TEST
{
	@isTest
	static void itShould()
	{
		Test_Erx_Data_Preparation.preparation();
		System.runAs(Test_Erx_Data_Preparation.testAdminUser){
			Test_Erx_Data_Preparation.listEnv[0].Environment_Status__c = 'Live';
			Test_Erx_Data_Preparation.listEnv[0].TP_Manage_Active_Template__c = Test_Erx_Data_Preparation.siteTemplate[0].Id;
			Test_Erx_Data_Preparation.listEnv[1].Environment_Status__c = 'Test';
			Test_Erx_Data_Preparation.listEnv[1].TP_Manage_Active_Template__c = Test_Erx_Data_Preparation.siteTemplate[1].Id;
			update Test_Erx_Data_Preparation.listEnv;
			TM_TemplateController tc = new TM_TemplateController();

			Test.setCurrentPage(Page.TM_Template);
        	ApexPages.currentPage().getParameters().put('previewMode', 'tmpReview');
        	ApexPages.currentPage().getParameters().put('tmpId', Test_Erx_Data_Preparation.listEnv[1].TP_Manage_Active_Template__c);
        	tc = new TM_TemplateController();
        	ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        	ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[1].Id);
        	tc = new TM_TemplateController();

        	System.assertNotEquals(tc, null);
		}
		
		User testUser;
		Account a;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        Contact student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', TestClassPicklist__c='Test1', DoNotCall=true, Birthdate = Date.Today(), AssistantName='');
        insert student;
        Profile testProfile = [select id, name from profile where name='Applicant Portal User'];
        testUser = new User(alias = 'uh', email='uh@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='uh@testorg.com',contactId=student.Id);
        insert testUser;
		system.runAs(testUser){
			Test_Erx_Data_Preparation.listEnv[0].Environment_Status__c = 'Live';
			Test_Erx_Data_Preparation.listEnv[0].TP_Manage_Active_Template__c = Test_Erx_Data_Preparation.siteTemplate[0].Id;
			Test_Erx_Data_Preparation.listEnv[1].Environment_Status__c = 'Test';
			Test_Erx_Data_Preparation.listEnv[1].TP_Manage_Active_Template__c = Test_Erx_Data_Preparation.siteTemplate[1].Id;
			update Test_Erx_Data_Preparation.listEnv;
			TM_TemplateController tc = new TM_TemplateController();

			Test.setCurrentPage(Page.TM_Template);
        	ApexPages.currentPage().getParameters().put('previewMode', 'tmpReview');
        	ApexPages.currentPage().getParameters().put('tmpId', Test_Erx_Data_Preparation.listEnv[1].TP_Manage_Active_Template__c);
        	tc = new TM_TemplateController();
        	ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        	ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[1].Id);
        	tc = new TM_TemplateController();

        	System.assertNotEquals(tc, null);
		}

	}
    
    static testmethod void testTemplate(){
        Test_Erx_Data_Preparation.preparation();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
        	TM_TemplateController tm = new TM_TemplateController();
            tm.initPageException();
            String val = tm.entityId;
            String testUserInfo = tm.testUserInfo;
            String testENVID = tm.testENVID;
        }
    }
}