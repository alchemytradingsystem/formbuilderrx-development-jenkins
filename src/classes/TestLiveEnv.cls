/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Test Class for LiveEnv
*/
@isTest
public with sharing class TestLiveEnv {
    private static SiteLoginTemplate__c siteTemplate;
    private static Env__c env;
    @isTest
    static void testEnvTrigger()	{
        try{
            TestLiveEnv.datapreparation();
            env = new Env__c(Description__c = 'Test', Version__c = 1,Environment_Status__c='Live');
            insert env; 
            env.Site_Template_Criteria__c='Community';
            update env;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.TEMPLATE_UPSERTION_DELETION_ERROR_MESSAGE),false);
        }
    }
    @isTest
    static void testSiteTemplateTrigger()	{
        try{
            TestLiveEnv.datapreparation();
            siteTemplate=new SiteLoginTemplate__c(
                SiteTemplateHeader__c = 'Header', 
                SiteTemplateFooter__c = 'Footer',
                Static_Resource_List__c='[{"type":"JavaScript/JQuery","subPath":"","name":"standardjquery","idTime":"1,505,374,585,254","alias":"test"},{"type":"JavaScript/JQuery","subPath":"","name":"standardjquery","idTime":"1,505,374,585,254","alias":""},{"type":"JavaScript","subPath":"/AdlerSiteCss/default.css","name":"AdlerSiteCss","idTime":"1,461,097,925,122","alias":""},{"type":"Open Text","subPath":"/AdlerSiteCss/default.css","name":"AdlerSiteCss","idTime":"1,461,097,925,122","alias":""},{"type":"CSS","subPath":"/AdlerSiteCss/default.css","name":"AdlerSiteCss","idTime":"1,461,097,925,122","alias":""},{"type":"CSS","subPath":"/AdlerSiteCss/other.css","name":"AdlerSiteCss","idTime":"1,461,098,038,695","alias":""},{"type":"Image","subPath":"/AdlerSiteCss/logo.png","name":"AdlerSiteCss","idTime":"1,461,098,107,125","alias":"{{logo}}"},{"type":"CSS","subPath":"/AdlerSiteCss/responsive.css","name":"AdlerSiteCss","idTime":"1,461,098,514,555","alias":""},{"type":"CSS","subPath":"/AdlerSiteCss/login.css","name":"AdlerSiteCss","idTime":"1,461,098,531,794","alias":""},{"type":"Image","subPath":"/AdlerSiteCss/class2.jpg","name":"AdlerSiteCss","idTime":"1,461,098,737,859","alias":"{{backImg}}"},{"type":"Image","subPath":"/AdlerSiteCss/orange_hover.png","name":"AdlerSiteCss","idTime":"1,461,098,909,817","alias":"{{orangeHover}}"},{"type":"Image","subPath":"/AdlerSiteCss/blog.gif","name":"AdlerSiteCss","idTime":"1,461,099,063,493","alias":"{{blog}}"},{"type":"Image","subPath":"/AdlerSiteCss/facebook.gif","name":"AdlerSiteCss","idTime":"1,461,099,532,225","alias":"{{facebook}}"},{"type":"Image","subPath":"/AdlerSiteCss/twitter.gif","name":"AdlerSiteCss","idTime":"1,461,099,596,242","alias":"{{twitter}}"},{"type":"Image","subPath":"/AdlerSiteCss/linkedin.gif","name":"AdlerSiteCss","idTime":"1,461,099,648,044","alias":"{{linkedIn}}"},{"type":"Image","subPath":"/AdlerSiteCss/youtube.gif","name":"AdlerSiteCss","idTime":"1,461,099,723,031","alias":"{{youtube}}"},{"type":"Image","subPath":"/AdlerSiteCss/vimeo.gif","name":"AdlerSiteCss","idTime":"1,461,099,798,808","alias":"{{vimeo}}"},{"type":"Image","subPath":"/AdlerSiteCss/class1.jpg","name":"AdlerSiteCss","idTime":"1,461,099,886,424","alias":"{{class1}}"},{"type":"Image","subPath":"/AdlerSiteCss/overlay.png","name":"AdlerSiteCss","idTime":"1,461,099,973,758","alias":"{{overlay}}"},{"type":"Image","subPath":"/AdlerSiteCss/makeagiftbutton.png","name":"AdlerSiteCss","idTime":"1,461,100,031,191","alias":"{{giftButton}}"},{"type":"Image","subPath":"/AdlerSiteCss/flickr.png","name":"AdlerSiteCss","idTime":"1,461,100,148,167","alias":"{{fliker}}"}]'
                ,Active__c = false,
                Description__c = 'test',
                SiteTemplateName__c = 'test'
            );            
            
            insert siteTemplate;
            siteTemplate.SiteTemplateHeader__c = 'Header test';
            update siteTemplate;
            env = new Env__c(Description__c = 'Test', Version__c = 1,Environment_Status__c='Live',TP_Manage_Active_Template__c = siteTemplate.Id);
            insert env;
            
            env.Environment_Status__c = 'Live';
            update	env;
            delete siteTemplate;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.TEMPLATE_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    } 
    
    @isTest
    static void testportalRegistrationMessageTrigger(){
        try{
            datapreparation();
            Portal_Registration_Message__c prm =  new Portal_Registration_Message__c();
            prm.Env__c = env.id;
            insert prm;
            prm.Lead_Converted_Status__c='test';
            update prm;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.REGISTRATION_MESSAGE_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
    @isTest
    static void testHomepageWidgetLayoutTrigger(){
        try{
            datapreparation();
            Homepage_Widget_Layout__c hwl =  new Homepage_Widget_Layout__c();
            hwl.Env__c = env.id;
            hwl.Name = 'Test Homepage Widget Layout';
            insert hwl;
            hwl.Is_Active__c=true;
            update hwl;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.HOMEPAGE_WIDGET_LAYOUT_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
    
     @isTest
    static void testHomepageWidgetLayoutTriggerDelete(){
        try{
            datapreparation();
            Homepage_Widget_Layout__c hwl =  new Homepage_Widget_Layout__c();
            hwl.Env__c = env.id;
            hwl.Name = 'Test Homepage Widget Layout';
            insert hwl;
            delete hwl;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.HOMEPAGE_WIDGET_LAYOUT_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
    @isTest
    static void testHomeHomepageWidgetTrigger(){
        try{
            datapreparation();
            Homepage_Widget__c hw =  new Homepage_Widget__c();
            hw.Env__c = env.id;
            insert hw;
            hw.Is_Active__c=true;
            update hw;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.HOMEPAGE_WIDGET_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    } @isTest
    static void testHomeHomepageWidgetTriggerDelete(){
        try{
            datapreparation();
            Homepage_Widget__c hw =  new Homepage_Widget__c();
            hw.Env__c = env.id;
            insert hw;
            delete hw;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.HOMEPAGE_WIDGET_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
    @isTest
    static void testErxPageTrigger(){
        try{	
            datapreparation();
            Erx_Page__c page = new Erx_Page__c ( Env__c = env.Id, IsActive__c = true,Page_Header__c = '{"title":"Test"}', Page_Name__c = ('Test Page '), ModelList__c='[{"objectName":"Application","objectAPIName":"EnrollmentrxRx__Enrollment_Opportunity__c","modelName":"Application","field":[{"fieldName":"Admissions Confidence","fieldAPIName":"EnrollmentrxRx__Admissions_Confidence__c"},{"fieldName":"Admissions Status","fieldAPIName":"EnrollmentrxRx__Admissions_Status__c"},{"fieldName":"Record ID","fieldAPIName":"Id"}],"conditionExpression":null,"condition":[{"valueType":"MODEL","value":"EnrollmentrxRx__Active_Enrollment_Opportunity__c","referencedObjectName":"EnrollmentrxRx__Enrollment_Opportunity__c","modelName":"Contact","isFormulaField":false,"fieldOperator":"equals","fieldDisplayType":"ID","fieldAPIName":"Id","displayValue":""}]}]');
            insert page;
            page.IsActive__c=true;
            update page;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
     @isTest
    static void testErxPageTriggerDelete(){
        try{	
            datapreparation();
            Erx_Page__c page = new Erx_Page__c ( Env__c = env.Id, IsActive__c = false,Page_Header__c = '{"title":"Test"}', Page_Name__c = ('Test Page '), ModelList__c='[{"objectName":"Application","objectAPIName":"EnrollmentrxRx__Enrollment_Opportunity__c","modelName":"Application","field":[{"fieldName":"Admissions Confidence","fieldAPIName":"EnrollmentrxRx__Admissions_Confidence__c"},{"fieldName":"Admissions Status","fieldAPIName":"EnrollmentrxRx__Admissions_Status__c"},{"fieldName":"Record ID","fieldAPIName":"Id"}],"conditionExpression":null,"condition":[{"valueType":"MODEL","value":"EnrollmentrxRx__Active_Enrollment_Opportunity__c","referencedObjectName":"EnrollmentrxRx__Enrollment_Opportunity__c","modelName":"Contact","isFormulaField":false,"fieldOperator":"equals","fieldDisplayType":"ID","fieldAPIName":"Id","displayValue":""}]}]');
            insert page;
           	delete page;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.PAGE_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
    @isTest
    static void testPortalLoginCustomFieldTrigger(){
        try{
            datapreparation();
            Portal_Login_Custom_Field__c plcf =  new Portal_Login_Custom_Field__c();
            plcf.Env__c = env.id;
            insert plcf;
            plcf.Name='test';
            update plcf;
            delete plcf;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.PORTAL_LOGIN_CUSTOM_FIELD_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
     @isTest
    static void testPortalLoginCustomFieldTriggerDelete(){
        try{
            datapreparation();
            Portal_Login_Custom_Field__c plcf =  new Portal_Login_Custom_Field__c();
            plcf.Env__c = env.id;
            plcf.Active__c = false;
            insert plcf;
            delete plcf;
        }
        catch(Exception e){
            System.assertEquals(e.getMessage().contains(PortalPackageConstants.PORTAL_LOGIN_CUSTOM_FIELD_UPSERTION_DELETION_ERROR_MESSAGE),true);
        }
    }
    
    private static void  datapreparation(){
        FormBuilder_Settings__c editing = new FormBuilder_Settings__c(name='Admin Settings',allow_editing__c=true);
        insert editing;
        env = new Env__c(Description__c = 'Test', Version__c = 1,Environment_Status__c='Live');
        insert env;
        
    }
}