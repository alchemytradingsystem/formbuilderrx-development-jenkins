//PD-3601 | getting started controller test
@isTest
public class TestGettingStartedController {
    static testMethod void testCoustructor() {
    	FormBuilder_Settings__c fbsetting = new FormBuilder_Settings__c();
        fbsetting.Name = 'Admin Settings';
        insert fbsetting;
        GettingStartedController obj = new GettingStartedController();
    	obj.updateGettingStarted();
        System.assertEquals(true, obj != null);
    }
}