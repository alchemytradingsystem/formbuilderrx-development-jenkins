/** 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Portal_SiteFieldsMainControllerTest { 

    private static User testCommunityUser;
    private static Portal_Login_Custom_Field__c customField;
    private static Portal_Login_Custom_Field__c orderByField; 
    private static Portal_Login_Custom_Field__c limitField;
    private static Portal_Login_Custom_Field__c notInField;
    private static Portal_Login_Custom_Field__c orderByWithlookup; 
    private static Portal_Login_Custom_Field__c orderByWithIncludes;
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        createCommunityUser();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Test.startTest();
            Portal_SiteFieldsMainController controllerInstance = new Portal_SiteFieldsMainController();
            controllerInstance.initPage();
            LC_RenderCriteriaConditionWrapper criteriaInstance = new LC_RenderCriteriaConditionWrapper(new List<LC_TemplateRenderedConditionWrapper>(), '1AND2');
            Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
            sfm.frontFieldId = customField.Id;
            sfm.initPage();
            sfm.getFieldList();
            sfm.deleteField();
            sfm.validate();
            sfm.editField();
          //  sfm.initPage();
            sfm.getSupportFieldType();
            sfm.getSupportFields();
            sfm.getReferenceFields();
            sfm.getAllSobject();
            sfm.getChildSobject();
            sfm.getGrandChildSobject();
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.validateRelationship();
            sfm.isDuplicateFieldExist();
            sfm.validateFields();
            sfm.cancel();
            sfm.getFieldsMap('Contact');
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.EQUALS_TO,'Test',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.NOT_EQUALS_TO,'Test',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.EQUALS_TO,'Test,Test1',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.NOT_EQUALS_TO,'Test,Test1',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.EQUALS_TO,'TestTest1',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.NOT_EQUALS_TO,'TestTest1',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.EQUALS_TO,null,null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.NOT_EQUALS_TO,null,null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.CONTAINS,null,null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.CONTAINS,'a',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.DOES_NOT_CONTAINS,null,null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.DOES_NOT_CONTAINS,'a',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.STARTS_WITH,'a',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.INCLUDES,'a',null);
            Portal_SiteFieldsMainController.constructQueryCondition('TestField',PortalPackageConstants.EXCLUDES,'a',null);
            sfm.addField();
            sfm.hideEdit();
            sfm.updateCurEnv();
            sfm.getTemplatesName();
            sfm.grandChildfieldAPIAction();
            sfm.childFieldAPIAction();
            string f3 = sfm.frontEnvId ;
            List<Portal_Login_Custom_Field__c> f21 = sfm.fieldsList ;
            string f5 = sfm.customSettingURL  ;
            Map<String, Schema.SObjectField> f8 = sfm.appFields;
            String f6 = sfm.fieldType;
            System.assertNotEquals(sfm, null);
            sfm.initPageException();
            sfm.f.Children_Objects_API__c = 'Account';
            sfm.f.Grand_Children_Objects_API__c = 'Account';
            sfm.f.Field_API_Name__c = 'AccountId';
            sfm.f.Children_Field_API_Name__c = 'AccountId';
            sfm.f.Grand_Children_Field_API_Name__c = 'AccountId';
            sfm.f.Object_Name__c = 'Contact';
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.getGrandChildSobject();
            sfm.getChildSobject();
            sfm.f.FieldType__c = 'STRING';
            sfm.getSupportFields();
            sfm.frontEndObjectId  = Test_Erx_Data_Preparation.customFIeldsList[1].Id;
            sfm.clearChildrenInput();
            sfm.clearGrandChildrenInput();
            sfm.fieldAPIAction();
            sfm.f.Name = 'newField';
            sfm.f.FieldType__c = 'null';
            sfm.f.Object_Name__c = 'null' ;
            sfm.f.Display_Order__c = 5 ;
            sfm.f.Linked_to_Portal__c = 'null';
            sfm.validateFields();
            sfm.deleteField();
            sfm.frontFieldId = orderByField.Id;
            sfm.getFieldList();
            sfm.deleteField();
            sfm.validate();
            sfm.editField();
            sfm.initPage();
            sfm.getSupportFieldType();
            sfm.getSupportFields();
            sfm.getReferenceFields();
            sfm.getAllSobject();
            sfm.getChildSobject();
            sfm.getGrandChildSobject();
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.validateRelationship();
            sfm.isDuplicateFieldExist();
            sfm.validateFields();
            sfm.cancel();
            sfm.getFieldsMap('Contact');
            sfm.addField();
            sfm.hideEdit();
            sfm.updateCurEnv();
            sfm.getTemplatesName();
            sfm.grandChildfieldAPIAction();
            sfm.childFieldAPIAction();
            f3 = sfm.frontEnvId ;
            f21 = sfm.fieldsList ;
            f5 = sfm.customSettingURL  ;
            f8 = sfm.appFields;
            f6 = sfm.fieldType;
            System.assertNotEquals(sfm, null);
            sfm.initPageException();
            sfm.f.Children_Objects_API__c = 'Account';
            sfm.f.Grand_Children_Objects_API__c = 'Account';
            sfm.f.Field_API_Name__c = 'AccountId';
            sfm.f.Children_Field_API_Name__c = 'AccountId';
            sfm.f.Grand_Children_Field_API_Name__c = 'AccountId';
            sfm.f.Object_Name__c = 'Contact';
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.getGrandChildSobject();
            sfm.getChildSobject();
            sfm.f.FieldType__c = 'STRING';
            sfm.getSupportFields();
            sfm.frontEndObjectId  = Test_Erx_Data_Preparation.customFIeldsList[1].Id;
            sfm.clearChildrenInput();
            sfm.clearGrandChildrenInput();
            sfm.fieldAPIAction();
            sfm.f.Name = 'newField';
            sfm.f.FieldType__c = 'null';
            sfm.f.Object_Name__c = 'null' ;
            sfm.f.Display_Order__c = 5 ;
            sfm.f.Linked_to_Portal__c = 'null';
            sfm.validateFields();
            sfm.deleteField();
           
            Test.stopTest();
           
        }
    }
     static testMethod void unitTestLimitQueryCriteria() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        createCommunityUser();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Test.startTest();
            Portal_SiteFieldsMainController controllerInstance = new Portal_SiteFieldsMainController();
            controllerInstance.initPage();
            LC_RenderCriteriaConditionWrapper criteriaInstance = new LC_RenderCriteriaConditionWrapper(new List<LC_TemplateRenderedConditionWrapper>(), '1AND2');
            Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
            sfm.frontFieldId = limitField.Id;
            sfm.initPage();
            sfm.getFieldList();
            sfm.deleteField();
            sfm.validate();
            sfm.editField();
            sfm.initPage();
            sfm.getSupportFieldType();
            sfm.getSupportFields();
            sfm.getReferenceFields();
            sfm.getAllSobject();
            sfm.getChildSobject();
            sfm.getGrandChildSobject();
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.validateRelationship();
            sfm.isDuplicateFieldExist();
            sfm.validateFields();
            sfm.cancel();
            sfm.getFieldsMap('Contact');
            sfm.addField();
            sfm.hideEdit();
            sfm.updateCurEnv();
            sfm.getTemplatesName();
            sfm.grandChildfieldAPIAction();
            sfm.childFieldAPIAction();
            string f3 = sfm.frontEnvId ;
            List<Portal_Login_Custom_Field__c> f21 = sfm.fieldsList ;
            string f5 = sfm.customSettingURL  ;
            Map<String, Schema.SObjectField> f8 = sfm.appFields;
            String f6 = sfm.fieldType;
            System.assertNotEquals(sfm, null);
            sfm.initPageException();
            sfm.f.Children_Objects_API__c = 'Account';
            sfm.f.Grand_Children_Objects_API__c = 'Account';
            sfm.f.Field_API_Name__c = 'AccountId';
            sfm.f.Children_Field_API_Name__c = 'AccountId';
            sfm.f.Grand_Children_Field_API_Name__c = 'AccountId';
            sfm.f.Object_Name__c = 'Contact';
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.getGrandChildSobject();
            sfm.getChildSobject();
            sfm.f.FieldType__c = 'STRING';
            sfm.getSupportFields();
            sfm.frontEndObjectId  = Test_Erx_Data_Preparation.customFIeldsList[1].Id;
            sfm.clearChildrenInput();
            sfm.clearGrandChildrenInput();
            sfm.fieldAPIAction();
            sfm.f.Name = 'newField';
            sfm.f.FieldType__c = 'null';
            sfm.f.Object_Name__c = 'null' ;
            sfm.f.Display_Order__c = 5 ;
            sfm.f.Linked_to_Portal__c = 'null';
            sfm.validateFields();
            sfm.deleteField();
            Test.stopTest();
           
        }
    }
    
    
     static testMethod void unitTestNotINQueryCriteria() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        createCommunityUser();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Test.startTest();
            Portal_SiteFieldsMainController controllerInstance = new Portal_SiteFieldsMainController();
            controllerInstance.initPage();
            LC_RenderCriteriaConditionWrapper criteriaInstance = new LC_RenderCriteriaConditionWrapper(new List<LC_TemplateRenderedConditionWrapper>(), '1AND2');
            Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
            sfm.frontFieldId = notInField.Id;
            sfm.initPage();
            sfm.getFieldList();
            sfm.deleteField();
            sfm.validate();
            sfm.editField();
            
            sfm.getSupportFieldType();
            sfm.getSupportFields();
            sfm.getReferenceFields();
            sfm.getAllSobject();
            sfm.getChildSobject();
            sfm.getGrandChildSobject();
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.validateRelationship();
            sfm.isDuplicateFieldExist();
            sfm.validateFields();
            sfm.cancel();
            sfm.getFieldsMap('Contact');
            sfm.addField();
            sfm.hideEdit();
            sfm.updateCurEnv();
            sfm.getTemplatesName();
            sfm.grandChildfieldAPIAction();
            sfm.childFieldAPIAction();
            string f3 = sfm.frontEnvId ;
            List<Portal_Login_Custom_Field__c> f21 = sfm.fieldsList ;
            string f5 = sfm.customSettingURL  ;
            Map<String, Schema.SObjectField> f8 = sfm.appFields;
            String f6 = sfm.fieldType;
            System.assertNotEquals(sfm, null);
            sfm.initPageException();
            sfm.f.Children_Objects_API__c = 'Account';
            sfm.f.Grand_Children_Objects_API__c = 'Account';
            sfm.f.Field_API_Name__c = 'AccountId';
            sfm.f.Children_Field_API_Name__c = 'AccountId';
            sfm.f.Grand_Children_Field_API_Name__c = 'AccountId';
            sfm.f.Object_Name__c = 'Contact';
            sfm.getAllChildrenRelatedAPIName();
            sfm.getAllGrandChildrenRelatedAPIName();
            sfm.getGrandChildSobject();
            sfm.getChildSobject();
            sfm.f.FieldType__c = 'STRING';
            sfm.getSupportFields();
            sfm.frontEndObjectId  = Test_Erx_Data_Preparation.customFIeldsList[1].Id;
            sfm.clearChildrenInput();
            sfm.clearGrandChildrenInput();
            sfm.fieldAPIAction();
            sfm.f.Name = 'newField';
            sfm.f.FieldType__c = 'null';
            sfm.f.Object_Name__c = 'null' ;
            sfm.f.Display_Order__c = 5 ;
            sfm.f.Linked_to_Portal__c = 'null';
            sfm.validateFields();
            sfm.deleteField();
            sfm.clearDependentObjectAction();
            sfm.clearDependentAddChildAction();
            sfm.clearDependentAddGrandChildAction();
            Test.stopTest();
           
        }
    }
    static testMethod void unitTestOrderByWithlookup() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        createCommunityUser();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Test.startTest();
            Portal_SiteFieldsMainController controllerInstance = new Portal_SiteFieldsMainController();
            controllerInstance.initPage();
            LC_RenderCriteriaConditionWrapper criteriaInstance = new LC_RenderCriteriaConditionWrapper(new List<LC_TemplateRenderedConditionWrapper>(), '1AND2');
            Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
               
            sfm.frontFieldId = orderByWithlookup.Id;
            sfm.initPage();
            Test.stopTest();  
        }
    }
    
    static testMethod void unitTestIncludes() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        createCommunityUser();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Test.startTest();
            Portal_SiteFieldsMainController controllerInstance = new Portal_SiteFieldsMainController();
            controllerInstance.initPage();
            LC_RenderCriteriaConditionWrapper criteriaInstance = new LC_RenderCriteriaConditionWrapper(new List<LC_TemplateRenderedConditionWrapper>(), '1AND2');
            Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
               
            sfm.frontFieldId = orderByWithIncludes.Id;
            sfm.initPage();
            Test.stopTest();  
        }
    }
    
    
    
    
    static testMethod void runAsAnotherUser() {
        Test_Erx_Data_Preparation.preparation();
        createUser();
        System.runAs(testCommunityUser){
            try{
                Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
                sfm.getFieldList();
            }Catch(Exception e){
                //system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
            }
        }
    }

    static testMethod void runAsAnotherUserGetFieldList() {
        Test_Erx_Data_Preparation.preparation();
        Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
        sfm.frontFieldId = Test_Erx_Data_Preparation.customFIeldsList[1].Id;
        createUser();
        System.runAs(testCommunityUser){
            try{
                sfm.getFieldList();
            }Catch(Exception e){
                system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
            }
        }
    }

    static testMethod void runAsAnotherUserDeleteField() {
        Test_Erx_Data_Preparation.preparation();
        Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
        sfm.frontFieldId = Test_Erx_Data_Preparation.customFIeldsList[1].Id;
        createUser();
        System.runAs(testCommunityUser){
            try{
                sfm.deleteField();
            }Catch(Exception e){
                system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
            }
        }
    }

    private static void createUser(){
        Contact student;
        Account account = new Account(name = 'Test');
        insert account;
        student = new Contact(AccountId = account.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        testCommunityUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testCommunityUser;
    }

    /**
     *  @KB : For PD-1493 added by Kavita Beniwal
     *  @description : Prepare data for custom field to check for Reference field criteria
     **/
    private static void createCommunityUser() { 
        Account superParentAccount;
        superParentAccount = new Account(name = 'Test Parent');
        insert superParentAccount;
        
        Account parentAccount;
        parentAccount = new Account(ParentId = superParentAccount.Id, name = 'Test1');
        insert parentAccount;
        
        Account account = new Account(ParentId = parentAccount.Id, name = 'Test');
        insert account;
        
        Contact student = new Contact(AccountId = account.Id, firstname='test', lastname='Contact');
        insert student;
        
        profile testProfile = [SELECT id, name 
                                FROM profile 
                                WHERE name='Applicant Portal User'];
        testCommunityUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testCommunityUser;

        customField = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
            +parentAccount.Id+'%\''+'ORDER BY Parent.Id DESC LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
            FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'AccountSource = \'Web\' AND Name != \'Test\' order by AccountSource limit 10 ', 
            Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
            gdChildQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c LIKE \'%Low%\' order by TestClassPicklist__c  limit 10'+')', 
            Grand_Children_Field_API_Name__c = 'accountid');
        
        insert customField;
        
        orderByField = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
            +parentAccount.Id+'%\''+'ORDER BY Parent.Id DESC LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
            FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'AccountSource = \'Web\' AND Name != \'Test\' order by AccountSource', 
            Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
            gdChildQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c LIKE \'%Low%\' order by TestClassPicklist__c'+')', 
            Grand_Children_Field_API_Name__c = 'accountid');
        
        insert orderByField ;
        
         limitField = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
            +parentAccount.Id+'%\''+'ORDER BY Parent.Id DESC LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
            FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'AccountSource = \'Web\' AND Name != \'Test\' limit 10', 
            Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
            gdChildQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c LIKE \'%Low%\' limit 10'+')', 
            Grand_Children_Field_API_Name__c = 'accountid');
        
        insert limitField ;
        
          notInField = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
            +parentAccount.Id+'%\''+'ORDER BY Parent.Id DESC LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
            FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'AccountSource  IN(\'Web\') AND Name != \'Test\' limit 10', 
            Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
            gdChildQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c  IN(\'Low\')  limit 10'+')', 
            Grand_Children_Field_API_Name__c = 'accountid');
        
        insert notInField;
        
          orderByWithlookup = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
            +parentAccount.Id+'%\''+'ORDER BY Parent.Id DESC LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
            FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c  IN(\'Low\') ORDER BY Parent.Id DESC LIMIT 2'+')', 
            Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
            gdChildQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c  IN(\'Low\') ORDER BY Parent.Id DESC LIMIT 2'+')', 
            Grand_Children_Field_API_Name__c = 'accountid');
        
        insert orderByWithlookup ;
        
        orderByWithIncludes = new Portal_Login_Custom_Field__c(Look_Up_Field__c=true,Has_GrandChildren__c=true,childQueryCriteria__c = 'Id LIKE \'%'+account.Id+ '\' OR Parent.Id LIKE \''
            +parentAccount.Id+'%\''+'ORDER BY Parent.Id DESC LIMIT 2', Children_Field_API_Name__c = 'accountid', Children_Objects_API__c = 'Account', Field_API_Name__c = 'accountid', 
            FieldType__c = 'REFERENCE', Object_Name__c = 'Contact', queryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c  IN(\'Low\') ORDER BY Parent.Id DESC LIMIT 2'+')', 
            Env__c = Test_Erx_Data_Preparation.listEnv[0].Id, Active__c = true, 
            gdChildQueryCriteria__c = 'Parent.ParentId LIKE \'%'+superParentAccount.Id+'%\' AND  ( NOT TestClassPicklist__c  INCLUDES(\'Low\') AND EXCLUDES(\'Low\') ORDER BY Parent.Id DESC LIMIT 2'+')', 
            Grand_Children_Field_API_Name__c = 'accountid');
        
        insert orderByWithIncludes;
    }
    
    //PD-2689 | Gourav 
     static testMethod void socialMediaConfigurationtest() {
       
        Test_Erx_Data_Preparation.preparation();
        createCommunityUser();
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Test.startTest();
            Portal_SiteFieldsMainController controllerInstance = new Portal_SiteFieldsMainController();
            controllerInstance.initPage();
            LC_RenderCriteriaConditionWrapper criteriaInstance = new LC_RenderCriteriaConditionWrapper(new List<LC_TemplateRenderedConditionWrapper>(), '1AND2');
            Portal_SiteFieldsMainController sfm = new Portal_SiteFieldsMainController();
           	sfm.showSocial();
           	sfm.cancelSocialConfiguration();
           	sfm.getOptions();
           	sfm.saveSocialConfiguration();
            Test.stopTest();
           
        }
    }
}