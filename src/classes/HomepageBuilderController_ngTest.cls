@isTest
private class HomepageBuilderController_ngTest {
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile p=[select id from Profile where Name='System Administrator'];

		ERx_Portal_EnvPanelController.isFromAdminPanel = true;
        Env__c env=new Env__c();
        env.Version__c=1;
        insert env;

        User user = new User();
        user.Username ='testDante@testDante.com';
        user.LastName = 'testDante';
        user.Email = 'testDante@testDante.com';
        user.alias = 'testaa';
        user.TimeZoneSidKey = 'America/Chicago';
        user.LocaleSidKey = 'en_US';
        user.ProfileId=p.id;
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        insert user;

        Current_Env__c ce=new Current_Env__c();
        ce.Name=user.Id;
        ce.Type__c='Community';
        ce.Env__c=env.Id;
        insert ce;

        Homepage_Widget_Layout__c hwl=new Homepage_Widget_Layout__c();
        hwl.Name='testLayoutDante';
        hwl.Displaying_Application_Status__c='Inquiry';
        hwl.Env__c=env.Id;
        insert hwl;

        System.runAs(user){
            ngForceController ngcon=new ngForceController();
            HomepageBuilderController_ng con=new HomepageBuilderController_ng(ngcon);
            System.assertNotEquals(con, null);
        }
    }
}