/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Portal_UserWrapperTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();

        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
            insert testContact;
            
			Portal_UserWrapper up = new Portal_UserWrapper(null);
            Test.setCurrentPage(Page.Portal_Register);
            ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
            ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);

            up = new Portal_UserWrapper(Test_Erx_Data_Preparation.listEnv[0].Id);
            up.initPageException();
            up.existingUserForContact(testContact.Id);
            up.email = 'test@testuuu.com';
            up.password = 'testPassword123';
            up.confirmPassword = 'testPassword123';
            up.userName = up.email;
            up.validateEmailFormat();
            up.isValidPassword();
            up.isValidInputReg();
            up.isValidInputLogin();
            up.isValidInputPassword();
            up.existingUser();
            up.existingContact();
            up.updateExistingContact();
            up.activateExistingContact();
            up.processingExtraFields(new List<Contact>{testContact},false);
            string nps = up.namespacePrefix;
            up.email = 'z2z3z4@z1z2z3.xcom';
            up.FirstName  = 'z2z3';
            up.LastName  = 'z2z';
            up.existingContact();
            up.updateExistingContact();
            up.isValidPasswordLength();
            User a= up.u;
            up = new Portal_UserWrapper();
          	Lead myLead = new Lead(lastname = 'z2z', company='testing',Email='z2z3z4@z1z2z3.xcom',Status='Open');
            insert myLead;
            up.convertLeadInCore(myLead);

            up.existContact = testContact;
            up.activateExistingContact();
            System.assertNotEquals(up, null);
        }
    }
    
    static testMethod void myUnitTest2() {
        
        // TO DO: implement unit test
        Account a;
        Contact student;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testUser = new User(alias = 'u5', email='u5@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u5@testorg.com',contactId=student.Id);
        insert testUser;
    
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        Portal_UserWrapper up;
        System.runAs(Test_Erx_Data_Preparation.testAdminUser){
            Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
            insert testContact;

            Test.setCurrentPage(Page.Portal_Register);
            ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
            ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
            up = new Portal_UserWrapper(Test_Erx_Data_Preparation.listEnv[0].Id);            
        }
        System.runAs(testUser){
            up.initPageException();
            up.email = 'test@testuuu.com';
            up.password = 'testPassword123';
            up.confirmPassword = 'testPassword123';
            up.userName = up.email;
            up.validateEmailFormat();
            up.isValidPassword();
            up.isValidInputReg();
            Lead myLead = new Lead(lastname = 'test', company='testing',Email='test@testuuu.com',Status='Open');
            insert myLead;
            try{ 
            	up.existingContact();
        	}catch(Exception e){
                system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges'));
            }
        }
    }
    
    static testMethod void myUnitTest1() {
        
        // TO DO: implement unit test
        Account a;
        Contact student;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
    
        // TO DO: implement unit test
        Test_Erx_Data_Preparation.preparation();
        System.runAs(testUser){
            Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
            insert testContact;

            Test.setCurrentPage(Page.Portal_Register);
            ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
            ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);

            Portal_UserWrapper up;
            try{
                up = new Portal_UserWrapper(Test_Erx_Data_Preparation.listEnv[0].Id);
            }catch(Exception e){
                system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges'));
            }
            
            
        }
    }
    
    static testMethod void myUnitTest3() {
        
        // TO DO: implement unit test
        Account a;
        Contact student;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
        Test_Erx_Data_Preparation.preparation();
    	Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
        insert testContact;
        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Portal_UserWrapper up = new Portal_UserWrapper(Test_Erx_Data_Preparation.listEnv[0].Id);
        // TO DO: implement unit test
        System.runAs(testUser){
            try{
                up.existingContact();
            }catch(Exception e){
                system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges'));
            }
        }
    }
    
    static testMethod void myUnitTest4() {
        
        // TO DO: implement unit test
        Account a;
        Contact student;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
        Test_Erx_Data_Preparation.preparation();
    	Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
        insert testContact;
        list<contact> lstContact = new list<contact>();
        lstContact.add(testContact);
        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Portal_UserWrapper up = new Portal_UserWrapper(Test_Erx_Data_Preparation.listEnv[0].Id);
        // TO DO: implement unit test
        System.runAs(testUser){
            try{
                up.processingExtraFields(lstContact,false);
            }catch(Exception e){
                system.assertEquals(true, e != null);
            }
        }
    }
    
    static testMethod void myUnitTest5() {
        
        // TO DO: implement unit test
        Account a;
        Contact student;
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
        Test_Erx_Data_Preparation.preparation();
    	Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
        insert testContact;
        list<contact> lstContact = new list<contact>();
        lstContact.add(testContact);
        Test.setCurrentPage(Page.Portal_Register);
        ApexPages.currentPage().getParameters().put('previewMode', 'siteReview');
        ApexPages.currentPage().getParameters().put('envId', Test_Erx_Data_Preparation.listEnv[0].Id);
        Portal_UserWrapper up = new Portal_UserWrapper(Test_Erx_Data_Preparation.listEnv[0].Id);
        // TO DO: implement unit test
        System.runAs(testUser){
            try{
                up.processingExtraFieldsForApp(lstContact,false);
            }catch(Exception e){
                system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges'));
            }
        }
    }
    
    static testmethod void testupdateExistingContact(){
        Contact ct = new Contact(FirstName='Testing', LastName='Testing123', Email='Testing@123.com');
        insert ct;
        Env__c env = new Env__c(Name='EnvTest',Version__c=1);
        insert env;
        Portal_UserWrapper pu = new Portal_UserWrapper(env.Id);
        pu.existContact = ct;
        pu.firstName = 'test';
        pu.lastName = 'test2';
        pu.email = 'test@b.com';
        pu.updateExistingContact(); 
        system.assertEquals(true, pu.existContact.firstName.equals('test'));
    }
    
    static testMethod void myUnitTest6() {
        Contact testContact = new Contact(firstname='test',lastname='Contact', Email='z2z3z4@z1z2z3.xcom', Birthdate = Date.today());
        insert testContact;
        List<sObject> networkList = Database.query('Select Id,Name from Network');
        Env__c env = new Env__c(Name = 'Test123', Env_Id__c = String.valueOf(networkList[0].get('Id')) ,Description__c = 'Test', Version__c = 1 ,Environment_Status__c = 'Live' );
        env.deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and 2"},{"message":"Yes Contact Match","filterOption":"Message","filter":"1 or 2 "},{"message":"Another Match","filterOption":"Message","filter":"1 and 2"}]';
        env.deduplication_condition__c = '[{"targetValue":"AccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"MyAccount","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"FirstName","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"STRING","compareWith":"Field"},{"targetValue":"ERx_Forms__Gender1__c","sourceField":"ERx_Forms__gender1__c","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"PICKLIST","compareWith":"Field"},{"targetValue":"Birthdate","sourceField":"birthdate","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"DATE","compareWith":"Field"},{"targetValue":"Id","sourceField":"firstname","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"123456789123456","filterFieldName":null,"fieldType":"ID","compareWith":"Value"}]';
        env.lead_deduplication_criteria__c = '[{"message":"","filterOption":"Match","filter":"1 and 2"},{"message":"Yes Lead Match","filterOption":"Message","filter":"1 or 2"}]';
        env.lead_deduplication_condition__c = '[{"targetValue":"PartnerAccountId","sourceField":null,"operatorType":"=","lookupFieldType":"STRING","lookupField":"Name","filterValue":"true","filterFieldName":null,"fieldType":"REFERENCE","compareWith":"Value"},{"targetValue":"Email","sourceField":"email","operatorType":"=","lookupFieldType":null,"lookupField":null,"filterValue":"","filterFieldName":null,"fieldType":"EMAIL","compareWith":"Field"}]';
        insert env;
        
        Portal_UserWrapper up = new Portal_UserWrapper(env.Id);
        
        up.email = 'sshubh@gmail.com';
        up.FirstName  = 'Shubh';
        up.LastName  = 'Sharma';
        
        up.existingContact();
        up.updateExistingContact();
        System.assertNotEquals(up, null);
    }
}