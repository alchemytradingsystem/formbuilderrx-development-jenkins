/**
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description PortalPackagePicklistLookupController class is used to reterive picklist value on admin panel
*/
public with sharing class PortalPackagePicklistLookupController {
	/****
    * @description contain object name of picklist field
    */ 
	private String objectName;
	/****
    * @description contain api name of picklist field
    */ 
	private String fieldName;
	/****
    * @description contain field value
    */ 
	private String value;
	/****
    * @description list of SelectOpWrap which contain label,value,isChecked
    */
	private List<SelectOpWrap> selectOpWrapList;
	/****
    * @description serialize string of selectOpWrapList
    */
	public String selectOpWrapListString {get;set;}
	/****
    * @description index to be updated
    */
	public Integer index {get;set;}
	/****
    * @description type of request to field the value
    */
	public String type {get;set;}
	/****
    * @description to check picklist is multiselect or not
    */
	public boolean isMultiSelect {get;set;}
	/****
    * @description is First Value is blank or not
    */
	public boolean isFirstBlank {get;set;}
	/****
    * @description backend field type multipicklist or picklist
    */
	public String fieldType {get;set;}
	
	/*******************************************************************************************************
    * @description default constructor to retierve url parameter and create wrapper picklist field option
    */
	public PortalPackagePicklistLookupController() {
		try {
			selectOpWrapList = new List<SelectOpWrap>();
			// Getting url parameters values.
			objectName = PortalPackageUtility.escapeQuotes(ApexPages.currentPage().getParameters().get('objectName'));
			fieldName = PortalPackageUtility.escapeQuotes(ApexPages.currentPage().getParameters().get('fieldName'));
			//PD-3757 || Gourav | bug | removed escapeQuotes due to the case of apostrope issue |
			//issue:-Picklist condition value with apostrophe not shown as selected when re-opening value selection
			value =(ApexPages.currentPage().getParameters().get('fieldValue'));
			index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
			type = PortalPackageUtility.escapeQuotes(ApexPages.currentPage().getParameters().get('type'));
			isMultiSelect = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isMultiSelect'));
			isFirstBlank = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isFirstBlank'));
			List<String> valueList = new List<String>();
			
			String namespaceFinal = PortalPackageUtility.getFinalNameSpacePerfix();
			
			Map < String, Schema.SObjectType > GlobalDescribeMap = Schema.getGlobalDescribe();
			Schema.SObjectField objField = GlobalDescribeMap.get(objectName).getDescribe().fields.getMap().get(fieldName.replace(namespaceFinal,''));
			fieldType = '';
			if(objField != null) {
				Schema.DescribeFieldResult fieldResult = objField.getDescribe();
				fieldType = String.valueOf(fieldResult.getType()).toUpperCase();
				if(String.isNotBlank(value)) {
					// Checking field type
					if(fieldType == PortalPackageConstants.MULTIPICKLISTFLD) {
						// case 1 : multipicklist (user can select demiliter)
						if(value.contains(';')) {
							valueList = value.split(';');
						} else {
							valueList = value.split(',');
						}
					} else {
						// case 2 : picklist field or others 
						valueList = value.split(',');
					}
				}
				
				// Getting All Picklist Values
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				if(isFirstBlank){
					selectOpWrapList.add(new SelectOpWrap('--None--', 'NULL', PortalPackageUtility.isElementFoundInList(valueList, 'NULL')));
				}
				/*for( Schema.PicklistEntry f : ple) {
		      		selectOpWrapList.add(new SelectOpWrap(f.getLabel(), f.getValue(), PortalPackageUtility.isElementFoundInList(valueList, f.getValue())));
			   	}*/
			   	/*
				 * ADDED FOR # PD-852
				 * If Case added to handle requirement 
				 * for record Types for different Sobject
				 */
				if(objectName.equalsIgnoreCase('RecordType')){
					list<String> sObjectAPIList = new list<String>();
					for( Schema.PicklistEntry f : ple) {
			      		List<Schema.DescribeSObjectResult> listDescribeSObjectResult = getDescribeSObjectResultList(f.getValue());
			      		if(listDescribeSObjectResult != null && listDescribeSObjectResult.size() > 0){
					        selectOpWrapList.add(new SelectOpWrap(listDescribeSObjectResult[0].getLabel(), listDescribeSObjectResult[0].getName(), PortalPackageUtility.isElementFoundInList(valueList, listDescribeSObjectResult[0].getName())));
					    }
				   	}
				}else{
					for( Schema.PicklistEntry f : ple) {
			      		selectOpWrapList.add(new SelectOpWrap(f.getValue(), f.getValue(), PortalPackageUtility.isElementFoundInList(valueList, f.getValue())));
				   	}	
				}
			}
		} catch(Exception e) {
			selectOpWrapList = new List<SelectOpWrap>();
		}
		
		selectOpWrapListString = JSON.serialize(selectOpWrapList);
	}
	
	/**
	 *FormBuilderRx
	 * @company : EnrollmentRx, LLC. - Copyright 2016 All rights reserved.
	 * @website : http://www.enrollmentrx.com/
	 * @author  Metacube
	 * @description method returns the list created to avoid Insuffiecient Priveleges Error
	 */
	public List<Schema.DescribeSObjectResult> getDescribeSObjectResultList(String ObjName){
	    List<Schema.DescribeSObjectResult> tempGetDescribeSObjectResultList;
	    try{
	        tempGetDescribeSObjectResultList = Schema.describeSObjects(new List<String>{ObjName});
	    }catch(Exception e){
	      tempGetDescribeSObjectResultList = new List<Schema.DescribeSObjectResult>(); 
	    }
	    return tempGetDescribeSObjectResultList;
	}
	
	/**
	* FormBuilderRx
	* @company : EnrollmentRx, LLC. - Copyright 2016 All rights reserved.
	* @website : http://www.enrollmentrx.com/
	* @author  Metacube
	* @version 1.0
	* @date   2016-05-13 
	* @description this SelectOpWrap class is used to hold picklist value
	*/
	public class SelectOpWrap {
		/****
	    * @description contain label of option
	    */ 
		private String label;
		/****
	    * @description contain value of option
	    */ 
		private String value;
		/****
	    * @description is option is selected or not
	    */ 
		private boolean isChecked;
		/*******************************************************************************************************
	    * @description default constructor to init label,value,ischecked
	    */
		public SelectOpWrap(String label, String value, boolean isChecked) {
			this.label = label;
			this.value = value;
			this.isChecked = isChecked;
		}
		
	}
}