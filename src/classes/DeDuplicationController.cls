/**
*DeDuplicationController
*This class creates DeDuplication criteria and conditions
*@author Shubham Sharma
*@since 2018-06-14
*/
public with sharing class DeDuplicationController{
    
    //Public/private Variables
    // error message
    public String errorMessage{get;set;}
    // success message
    public String successMessage{get;set;}
    // criteria list for contact
    public List<ERx_PageEntities.FilterModel> filterLogicList{get;set;}
    // criteria list for lead
    public List<ERx_PageEntities.FilterModel> leadFilterLogicList{get;set;}
    // list of contact condition
    public List<DeDuplicationDataModel> contactFieldList{get;set;}
    // list of lead condition
    public List<DeDuplicationDataModel> leadFieldList{get;set;}
    // list of serializable contactCondiiton
    private List<ERx_PageEntities.SerializeDeDuplicationDataModel> contactSerializableFieldList;
    // list of serializable leadCondiiton
    private List<ERx_PageEntities.SerializeDeDuplicationDataModel> leadSerializableFieldList;
    // selectOptionList of contact
    public List<SelectOption> contactSelectFieldList{get;set;}
    // selectoptionList for lead
    public List<SelectOption> leadSelectFieldList{get;set;}
    // All registration fields list
    public List<Portal_Login_Custom_Field__c> registrationFields{get;set;}
    public Integer selectedIndex{get;set;}
    private Env__c curEnv;
    private String namespacePrefix;
    
    //PD-4402
    /****
    * @description To store Permission error message
    */
    private static String permissionErrorMessage = PortalPackageUtility.getPermissionErrorMessage();
    /*
    Initialize variables and get deserealized data from backend
    */
    public DeDuplicationController(){ 
        errorMessage = ''; 
        successMessage = '';
        namespacePrefix = PortalPackageUtility.getNameSpacePerfix();
		if(String.isBlank(namespacePrefix)){
			namespacePrefix ='';
		}
		else{
			namespacePrefix=namespacePrefix+'__';
		}
        // contact field list
        contactSelectFieldList = new List<SelectOption>();
        contactSelectFieldList.add(new SelectOption('--None--','--None--')); 
        contactSelectFieldList.addAll(PortalPackageHelper.retrieveFields('filterable','Contact'));
        // lead Field List
        leadSelectFieldList = new List<SelectOption>();
        leadSelectFieldList.add(new SelectOption('--None--','--None--'));
        leadSelectFieldList.addAll(PortalPackageHelper.retrieveFields('filterable','Lead'));
        // current Env for usage
        curEnv = new Env__c();
        Current_Env__c env = ERx_PortalPackUtil.getCurrentEnvForConfiguration();
        List<String> listFields = new List<String>{'Env_Status__c','deduplication_criteria__c','deduplication_condition__c','lead_deduplication_condition__c','lead_deduplication_criteria__c'};
     	if(SecurityEnforceUtility.checkObjectAccess('Env__c', SecurityTypes.IS_ACCESS, namespacePrefix) && 
    	SecurityEnforceUtility.checkFieldAccess('Env__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
	        curEnv = [SELECT Id,Name,Env_Status__c,deduplication_criteria__c,deduplication_condition__c,lead_deduplication_criteria__c,lead_deduplication_condition__c FROM Env__c WHERE Id =: env.env__c];
    	}
    	contactSerializableFieldList = new List<ERx_PageEntities.SerializeDeDuplicationDataModel>();
    	contactFieldList = new List<DeDuplicationDataModel>();
    	// serializable contact field condition records from backend
    	if(curEnv != null && curEnv.deduplication_condition__c != null){
     		contactSerializableFieldList = (List<ERx_PageEntities.SerializeDeDuplicationDataModel>) JSON.deserialize(curEnv.deduplication_condition__c, List<ERx_PageEntities.SerializeDeDuplicationDataModel>.class);
    	}
    	
    	leadSerializableFieldList = new List<ERx_PageEntities.SerializeDeDuplicationDataModel>();
    	leadFieldList = new List<DeDuplicationDataModel>();
    	// serializable lead field condition records from backend
    	if(curEnv != null && curEnv.lead_deduplication_condition__c != null){
     		leadSerializableFieldList = (List<ERx_PageEntities.SerializeDeDuplicationDataModel>) JSON.deserialize(curEnv.lead_deduplication_condition__c, List<ERx_PageEntities.SerializeDeDuplicationDataModel>.class);
    	}
    	
    	// get all details from backend into Data maodel used for page for Contacts 
    	for(ERx_PageEntities.SerializeDeDuplicationDataModel ddm : contactSerializableFieldList){
        	DeDuplicationDataModel sddm = new DeDuplicationDataModel();
        	sddm.targetValue = ddm.targetValue;
        	sddm.operatorType = ddm.operatorType;
	        sddm.compareWith = ddm.compareWith;
	        sddm.sourceField = ddm.sourceField;
	        sddm.filterValue = ddm.filterValue;
	        sddm.filterFieldName = ddm.filterFieldName;
	        sddm.lookupField = ddm.lookupField;
	        sddm.fieldType = ddm.fieldType;
	        sddm.lookupFieldType = ddm.lookupFieldType;
	        contactFieldList.add(sddm);
        }
    	
    	// get all details from backend into Data maodel used for page for Leads
    	for(ERx_PageEntities.SerializeDeDuplicationDataModel ddm : leadSerializableFieldList){
        	DeDuplicationDataModel sddm = new DeDuplicationDataModel();
        	sddm.targetValue = ddm.targetValue;
        	sddm.operatorType = ddm.operatorType;
	        sddm.compareWith = ddm.compareWith;
	        sddm.sourceField = ddm.sourceField;
	        sddm.filterValue = ddm.filterValue;
	        sddm.filterFieldName = ddm.filterFieldName;
	        sddm.lookupField = ddm.lookupField;
	        sddm.fieldType = ddm.fieldType;
	        sddm.lookupFieldType = ddm.lookupFieldType;
	        leadFieldList.add(sddm);
        }
    	
    	// get filter Logic from backend for Contact
        filterLogicList = new List<ERx_PageEntities.FilterModel>();
        if(curEnv != null && curEnv.deduplication_criteria__c != null){
        	filterLogicList = (List<ERx_PageEntities.FilterModel>) JSON.deserialize(curEnv.deduplication_criteria__c, List<ERx_PageEntities.FilterModel>.class);
        }
        // get filter Logic from backend for Lead
        leadFilterLogicList = new List<ERx_PageEntities.FilterModel>();
        if(curEnv != null && curEnv.lead_deduplication_criteria__c != null){
        	leadFilterLogicList = (List<ERx_PageEntities.FilterModel>) JSON.deserialize(curEnv.lead_deduplication_criteria__c, List<ERx_PageEntities.FilterModel>.class);
        }
        
        setValueForDDMList('Contact',contactFieldList);
        setValueForDDMList('Lead',leadFieldList);
        
        // add dummy when no data ata backend for criteria contact
        if(filterLogicList != null && filterLogicList.isEmpty()){
        	ERx_PageEntities.FilterModel fm = new ERx_PageEntities.FilterModel();
            fm.filter = '';
        	fm.filterOption = 'Match';
            filterLogicList.add(fm);
        }
        // add dummy when no data ata backend for criteria Lead
        if(leadFilterLogicList != null && leadFilterLogicList.isEmpty()){
        	ERx_PageEntities.FilterModel fm = new ERx_PageEntities.FilterModel();
            fm.filter = '';
        	fm.filterOption = 'Match';
            leadFilterLogicList.add(fm);
        }
        //get Registration fields
        getRegistrationFields(env.env__c);
    }
    
    /*
    * set Value for contact and Lead List for page
    */
    public void setValueForDDMList(String objname,List<DeDuplicationDataModel> fieldList){
    	// add dummy when no data at backend for lead
        if(fieldList != null && fieldList.isEmpty()){
            DeDuplicationDataModel temp = new DeDuplicationDataModel();
            temp.targetValue = '--None--';
            temp.fieldtype = 'None';
            temp.operatorType = '=';
            temp.compareWith = 'Value';
            fieldList.add(temp);
        }
        else{
        	// get lookup Data field values for Lead
        	for(DeDuplicationDataModel tempDDM : fieldList){
        		if(tempDDM.targetValue != '--None--'){
		            tempDDM.fieldType = String.valueOf(schema.getGlobalDescribe().get(objname).getDescribe().fields.getMap().get(tempDDM.targetValue).getDescribe().getType());
		            if(tempDDM.fieldType.equalsIgnoreCase('Reference')){
		                String lookupObjName = String.valueOf(schema.getGlobalDescribe().get(objname).getDescribe().fields.getMap().get(tempDDM.targetValue).getDescribe().getReferenceTo());
		                lookupObjName = lookupObjName.removeStart('(');
		                lookupObjName = lookupObjName.removeEnd(')');
		                tempDDM.lookupObjectFields = PortalPackageHelper.retrieveFields('all', lookupObjName);
		            }
		        }
        	}
        }
    }
    
    
    
   	/*
   	* get All portal Registration Fields for the env
   	*/
    public void getRegistrationFields(Id envId){
    	registrationFields = new List<Portal_Login_Custom_Field__c>();
    	List<String> listFields= new List<String>{'Active__c','Children_Field_API_Name__c','Children_Field_Label__c','Children_Index_Field_API__c',
			'Children_Objects_API__c','Display_Order__c','Field_API_Name__c','Grand_Children_Field_API_Name__c','Grand_Children_Field_Label__c',
			'Grand_Children_Index_Field_API__c','Grand_Children_Objects_API__c','Is_Field_Required__c','Linked_to_Portal__c','Look_Up_Field__c','Object_Name__c','Env__c'};
		if(envId != null){
			if(SecurityEnforceUtility.checkObjectAccess('Portal_Login_Custom_Field__c', SecurityTypes.IS_ACCESS, namespacePrefix)){
				if(SecurityEnforceUtility.checkFieldAccess('Portal_Login_Custom_Field__c', listFields, SecurityTypes.IS_ACCESS, namespacePrefix)){
					//PD-3047 The fields of Login configuration must be displayed orderly
					registrationFields = [SELECT Id,
							            Name, Active__c,Children_Field_API_Name__c,Children_Field_Label__c,Children_Index_Field_API__c, 
							            Children_Objects_API__c,Display_Order__c, Field_API_Name__c, 
							            Grand_Children_Field_API_Name__c,Grand_Children_Field_Label__c,Grand_Children_Index_Field_API__c,Grand_Children_Objects_API__c,  
							            Is_Field_Required__c, 
							            Linked_to_Portal__c,
							            Look_Up_Field__c,   
							            Object_Name__c, 
							            Env__c,queryCriteria__c
							            FROM Portal_Login_Custom_Field__c 
							            WHERE Env__c = : envId AND Active__c=true]; 
				} else {
					ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Login_Custom_Field__c  contactFieldList :: ' + 
							PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'DeDuplicationController', 'getFieldList', null);
					throw new PortalPackageException('permissionErrorMessage'); 
				}
			} else {
				ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Portal_Login_Custom_Field__c '), 'DeDuplicationController', 
						'getFieldList', null);
				throw new PortalPackageException('permissionErrorMessage'); 
			}
		}
	 }
    
    /**
    *This is a getter function to retrieve portal registration fields and show on page
    */
    public List<SelectOption> getSourceList(){
        List<SelectOption> registrationFieldsList = new List<SelectOption>();
		for (Portal_Login_Custom_Field__c fieldName : registrationFields){
			if( !( ((fieldName.Name).equalsIgnoreCase('password') && (fieldName.Object_Name__c).equalsIgnoreCase('user')) ||
			 ( (fieldName.Name).equalsIgnoreCase('confirm password') && (fieldName.Object_Name__c).equalsIgnoreCase('user') ) ) )
			registrationFieldsList.add(new SelectOption(fieldName.Field_API_Name__c,fieldName.Name));
		}
		return registrationFieldsList;
    }
    
    /**
    *This function adds a blank filter logic to the filterLogicList Contact
    */
    public void addLogic(){
        errorMessage = '';
        ERx_PageEntities.FilterModel fm = new ERx_PageEntities.FilterModel();
        fm.filterOption = 'Match';
        filterLogicList.add(fm);
        successMessage = '';
    }
    
    /**
    *This function removes filter logic from the filterLogicList Contact
    */
    public void deleteLogic(){
    	errorMessage = '';
    	successMessage = '';
    	if(selectedIndex > 0 && selectedIndex <= filterLogicList.size()){
        	filterLogicList.remove(selectedIndex-1);
    	}
    }
    
    /*
    *This function removes filter logic message from the filterLogicList Contact
    */
    public void messageBlank(){
    	errorMessage = '';
    	successMessage = '';
    	ERx_PageEntities.FilterModel fm = filterLogicList.get(selectedIndex-1);
        fm.message = '';
    }
    
    
    /**
    *This function adds a blank filter logic to the filterLogicList
    */
    public void addLogicLead(){
        errorMessage = '';
        ERx_PageEntities.FilterModel fm = new ERx_PageEntities.FilterModel();
        fm.filterOption = 'Match';
        leadFilterLogicList.add(fm);
        successMessage = '';
    }
    
    /**
    *This function adds a blank filter logic to the filterLogicList
    */
    public void deleteLogicLead(){
    	errorMessage = '';
    	successMessage = '';
    	if(selectedIndex > 0 && selectedIndex <= leadFilterLogicList.size()){
        	leadFilterLogicList.remove(selectedIndex-1);
    	}
    }
    
    /*
    *This function removes filter logic message from the filterLogicList Lead
    */
    public void messageBlankLead(){
    	errorMessage = '';
    	successMessage = '';
    	ERx_PageEntities.FilterModel fm = leadFilterLogicList.get(selectedIndex-1);
        fm.message = '';
    }
    
    /**
    *This function adds a deduplication criteria to the contactFieldList
    */
    public void addFields(){
        errorMessage  = '';
        DeDuplicationDataModel tempDDM = new DeDuplicationDataModel();
        tempDDM.compareWith = 'Value';
        contactFieldList.add(tempDDM);
        successMessage = '';
    }
    
    /*
    * delete row of deduplication condition applied contactFieldList
    */
    public void deleteRow(){
    	errorMessage = '';
    	successMessage = '';
    	if(selectedIndex > 0 && selectedIndex <= contactFieldList.size()){
    		contactFieldList.remove(selectedIndex - 1);
    	}
    } 
    
    /**
    *This function adds a deduplication criteria to the LeadFieldList
    */
    public void addFieldsLead(){
        errorMessage  = '';
        DeDuplicationDataModel tempDDM = new DeDuplicationDataModel();
        tempDDM.compareWith = 'Value';
        leadFieldList.add(tempDDM);
        successMessage = '';
    }
    
    /*
    * delete row of deduplication condition applied LeadFieldList
    */
    public void deleteRowLead(){
    	errorMessage = '';
    	successMessage = '';
    	if(selectedIndex > 0 && selectedIndex <= leadFieldList.size()){
    		leadFieldList.remove(selectedIndex - 1);
    	}
    }
    
    /**
    *This function fetches DeDuplicationDataModel record from the  selected index and checks if the Target value is of reference type,
     if it is then we find *the lookup field object name and fetch all the fields of that object and fill in lookupObjectFields list 
     of wrapper class to show on page for Contacts.
    */
    public void checkFieldType(){
    	errorMessage='';
    	successMessage='';
    	DeDuplicationDataModel tempDDM = contactFieldList[selectedIndex - 1];
        //System.assert(false,tempDDM);
        if(tempDDM.targetValue != '--None--'){
            tempDDM.fieldType = String.valueOf(schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().get(tempDDM.targetValue).getDescribe().getType());
            if(tempDDM.fieldType.equalsIgnoreCase('Reference')){
                String lookupObjName = String.valueOf(schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().get(tempDDM.targetValue).getDescribe().getReferenceTo());
                lookupObjName = lookupObjName.removeStart('(');
                lookupObjName = lookupObjName.removeEnd(')');
                tempDDM.lookupObjectFields = PortalPackageHelper.retrieveFields('all', lookupObjName);
                //contactLookupSelectFieldList.put(selectedIndex-1,ImportProcessUtility.retrieveFields('all', lookupObjName)); 
                contactFieldList[selectedIndex - 1] = tempDDM;
            }else{
            	//contactLookupSelectFieldList.put(selectedIndex-1,new List<SelectOption>());
                tempDDM.lookupObjectFields.clear();
                contactFieldList[selectedIndex - 1] = tempDDM;
            }
        }
        else{
        	tempDDM.fieldtype = 'None';
        	tempDDM.lookupObjectFields.clear();
            contactFieldList[selectedIndex - 1] = tempDDM;
        }
    }
    
    /**
    *This function fetches DeDuplicationDataModel record from the  selected index and checks if the Target value is of reference type,
     if it is then we find *the lookup field object name and fetch all the fields of that object and fill in lookupObjectFields list 
     of wrapper class to show on page for Lead
    */
    public void checkFieldTypeLead(){
    	errorMessage='';
    	successMessage='';
    	DeDuplicationDataModel tempDDM = leadFieldList[selectedIndex - 1];
        if(tempDDM.targetValue != '--None--'){
            tempDDM.fieldType = String.valueOf(schema.getGlobalDescribe().get('Lead').getDescribe().fields.getMap().get(tempDDM.targetValue).getDescribe().getType());
            if(tempDDM.fieldType.equalsIgnoreCase('Reference')){
                String lookupObjName = String.valueOf(schema.getGlobalDescribe().get('Lead').getDescribe().fields.getMap().get(tempDDM.targetValue).getDescribe().getReferenceTo());
                lookupObjName = lookupObjName.removeStart('(');
                lookupObjName = lookupObjName.removeEnd(')');
                tempDDM.lookupObjectFields = PortalPackageHelper.retrieveFields('all', lookupObjName);
                leadFieldList[selectedIndex - 1] = tempDDM;
            }else{
            	tempDDM.lookupObjectFields.clear();
                leadFieldList[selectedIndex - 1] = tempDDM;
            }
        }
        else{
        	tempDDM.fieldtype = 'None';
        	tempDDM.lookupObjectFields.clear();
            leadFieldList[selectedIndex - 1] = tempDDM;
        }
    }
    
    /*
    * Validate Criteria and conditions for Contact and Lead
    */
    public String validateCriteria(List<ERx_PageEntities.FilterModel> criteriaList,String objName,List<DeDuplicationDataModel> fieldList){
    	FormBuilder_Settings__c adminSettings = FormBuilder_Settings__c.getValues('Admin Settings');
    	if( (curEnv != null && curEnv.Env_Status__c == 'Live' && adminSettings !=null && !adminSettings.Allow_editing__c) ){
    		return 'Duplication Rules can not be updated in a Live Env';	
    	}
    	for(Integer index =0;index < criteriaList.size();index++){ 
    		ERx_PageEntities.FilterModel fm = criteriaList[index];
    		String str = fm.filter;
    		if( (str == null || str.trim().equals('')) && !(fieldList.size() == 1  && fieldList[0].targetValue.equalsIgnoreCase('--None--'))){
    			return objName +' Duplication Criteria '+ (index+1) + ' : '+'is blank or invalid';
    		}
    		//pad all digits with space on both sides, split with space and remove empty strings.
	        //result is an array of this type: [ "(", "1", "and", "2", ")", "or", "3" ]
	        String temp = str;
	        for(Integer i = 0;i < temp.length();i++){
	        	String s = temp.substring(i,i+1);
	        	if(s.isNumeric()){
	        		for(Integer j = i+1; j < temp.length();j++){
		        		String k = temp.substring(j,j+1);
		        		if(k.isNumeric()){
		        			s += temp.substring(j,j+1);
		        			i = j;
		        		}
		        		else{
		        			i = j;break;
		        		}
		        	}
	        		str = str.replace(s,' '+s+' ');
	        	}
	        	else if(s.equals('(')){
	        		str = str.replace(s,' '+s+' ');
	        	}
	        	else if(s.equals(')')){
	        		str = str.replace(s,' '+s+' ');
	        	}
	        }
	        
	        List<String> formattedString = str.split(' ');
	        
	        //init vars
	        Integer parenBalance = 0; //balance of open/close parens
	        Integer openParenPlace = -2; //index of open paren in array
	        Integer closeParenPlace = -2; //index of close paren in array
	        Integer wordPlace = -2; //index of word in array
	        Integer numPlace = -2; //index of num in array
	        List<Integer> listOfNumbers = new List<Integer>(); //array containing numbers that appear in parser
	        List<String> listOfAndOr = new List<String>();
	        Integer wordNum = 0;
	        for (Integer i = 0; i < formattedString.size() ; i++) { //loop through array
	        	if( !formattedString[i].trim().equals('') ){
		            if ( formattedString[i].equals('(') ) { //if this unit is open paren
		                parenBalance++; //add 1 to balance
		                openParenPlace = i; //set open paren index
		                if (closeParenPlace + 1 == i && closeParenPlace > -1) { //checks if there was a close paren directly before
		                    //this means there is an illegal construct in string of the form ")("
		                    return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Empty set of parentheses';
		                } else if (numPlace + 1 == i && numPlace > -1) { //checks if there was number directly before 
		                    //this means there is an illegal construct in string of the form "1 ("
		                    return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Number followed by opening parenthesis';
		                }
		            } else if (formattedString[i].equals(')') ) { //if this unit is close paren
		                parenBalance--; //subtract 1 from balance
		                closeParenPlace = i; //set close paren index
		                if (i == 0) {
		                    return  objName + (index+1) + ' : '+'Invalid syntax: Cannot begin string with closing parenthesis';
		                } else if (openParenPlace + 1 == i) { //checks if there was an open paren directly before
		                    //this means there is an illegal construct in string of the form "()"
		                    return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Empty set of parentheses';
		                } else if (wordPlace + 1 == i) { //checks if there was a word directly before
		                    //this means there is an illegal construct in the string of the form "( 1 or )"
		                    return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Word followed by closing parenthesis';
		                }
		            } else if ( formattedString[i] != null && formattedString[i].isNumeric()) { //if this unit is a number
		            	if(!(listOfNumbers.contains(Integer.valueOf(formattedString[i])))){
		                    listOfNumbers.add(Integer.valueOf(formattedString[i]));
		                }
		                if (closeParenPlace + 1 == i && closeParenPlace > -1) {
		                    //checks if there was a close paren directly before
		                    return  objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Closing parenthesis followed by number';
		                }
		                if (numPlace + 1 == i && numPlace > -1) {
		                    //checks if there was a number directly before  
		                    return  objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Number immediately followed by opening number';
		                }
		                numPlace = i;
		            } else { //not number or paren, so some letter string
		                String word = formattedString[i].toUpperCase();
		                if (word.equalsIgnoreCase('AND') || word.equalsIgnoreCase('OR')) {
		                	wordNum++;
		                    if (wordPlace + 1 == i && wordPlace > -1) {
		                        //checks if there was a word directly before    
		                        return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Word immediately followed by word';
		                    }
		                    if (openParenPlace + 1 == i && openParenPlace > -1) {
		                        //checks if there was an open paren directly before
		                        return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Opening parenthesis followed by word';
		                    }
		                    wordPlace = i;
		                    if (i == 0) { //checks if word index is 0, so beginning of string
		                        return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Cannot begin string with word';
		                    }
		                    if (i == formattedString.size() - 1) { //checks if word is at end of string
		                        return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Cannot end string with word';
		                    }
		                } else { //word is not in language
		                    return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: valid words are AND, OR';
		                }
		            }
	        	}
	        }
	        //checks paren relations
	        if (parenBalance > 0) {
	            return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Unclosed parenthesis.';
	        }
	        if (parenBalance < 0) {
	            return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid syntax: Excess parenthesis';
	        }
	        //number larger than amount of conds
	        for(Integer num : listOfNumbers){
		        if (num > fieldList.size()) {
		        	return objName +' Duplication Criteria '+ (index+1) + ' : '+'Invalid input: Condition index out of range';
		        }
	        }
	        if(wordNum < listOfNumbers.size()-1){
		    	return objName +' Duplication Criteria '+ (index+1) + ' : '+' is invalid'; 
		    }
		    if(fm.filterOption.equalsIgnoreCase('Message')){
		    	if(fm.message ==null || fm.message.trim().equals('')){
		    		return objName +' Duplication Criteria '+ (index+1) + ' : '+' Message is invalid or blank';
		    	}
		    }
    	}
    	// check for conditions and if values are valid
    	for(Integer index = 0;index < fieldList.size();index++){
    		DeDuplicationDataModel ddm = fieldList[index];
    		if(ddm.fieldType.equalsIgnoreCase('reference')){
        		String lookupObjName = String.valueOf(schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap().get(ddm.targetValue).getDescribe().getReferenceTo());
        		lookupObjName = lookupObjName.removeStart('(');
	            lookupObjName = lookupObjName.removeEnd(')');
	            if(ddm.lookupField != null){
        			ddm.lookupFieldType = String.valueOf(Schema.getGlobalDescribe().get(lookupObjName).getDescribe().fields.getMap().get(ddm.lookupField).getDescribe().getType());
	            }
        	}
    		if(ddm.targetValue.equals('--None--') && fieldList.size() != 1){
    			return objName +' Duplication Condition '+ (index+1) + ' : '+'Select a field for criteria';
    		}
    		if(ddm.fieldType.equalsIgnoreCase('ID') || (ddm.lookupFieldType != null && ddm.lookupFieldType.equalsIgnoreCase('ID'))){
    			if( ddm.compareWith.equalsIgnoreCase('Value') && ddm.filterValue != null && !(ddm.filterValue.trim().length()==15 || ddm.filterValue.trim().length()==18) ){
    				return objName +' Duplication Condition '+ (index+1) + ' : '+'Id field should be 15 or 18 charachters';
    			}
    		}
    		if(ddm.fieldType.equalsIgnoreCase('date') || (ddm.lookupFieldType != null && ddm.lookupFieldType.equalsIgnoreCase('date'))){
    			if( ddm.filterValue != null && ddm.compareWith.equalsIgnoreCase('Value') ){
    				String val = ddm.filterValue.replace('\'',''); 
                    String[] dateValue = val.split('/');
                    if(dateValue.size() > 0 && dateValue.size() == 3){
                        if(!(dateValue[2].length()==4 && dateValue[0].length()==2 && dateValue[1].length()==2)){
                    		return objName +' Duplication Condition '+ (index+1) + ' : '+'Date should be in format MM/DD/YYYY';    	
                        }
                    }
                    else{
                    	return objName +' Duplication Condition '+ (index+1) + ' : '+'Date should be in format MM/DD/YYYY';
                    }
    			}
    		}
    		if(ddm.fieldType.equalsIgnoreCase('Boolean') || (ddm.lookupFieldType != null && ddm.lookupFieldType.equalsIgnoreCase('Boolean'))){
    			if( ddm.filterValue != null && ddm.compareWith.equalsIgnoreCase('Value') ){
    				if( !(ddm.filterValue.trim().equalsIgnoreCase('true') || ddm.filterValue.trim().equalsIgnoreCase('false')) ){
    					return objName +' Duplication Condition '+ (index+1) + ' : '+'Invalid Boolean value';
    				}
    			}
    		}
    	}
    	return ''; 
    }
    
    /*
    * Save criteria and conditions for Contact and Lead checking if all are valid
    */
    public PageReference saveFilterLogic(){
    	errorMessage = '';
    	successMessage = '';
    	errorMessage = validateCriteria(filterLogicList,'Contact',contactFieldList);
    	if(errorMessage.equals('')){
    		errorMessage = validateCriteria(leadFilterLogicList,'Lead',leadFieldList);	
    	}
    	// update env only if no error occured
        if(errorMessage.equals('')){
	        contactSerializableFieldList = new List<ERx_PageEntities.SerializeDeDuplicationDataModel>();
	        if( !(contactFieldList.size() == 1 && contactFieldList[0].targetValue == '--None--') ){
	            // update data to serialized List Of Contact
		        for(DeDuplicationDataModel ddm : contactFieldList){
		        	if(!ddm.fieldType.equalsIgnoreCase('reference')){
		        		ddm.lookupField = null;
		        		ddm.lookupFieldType = null;
		        	}
		        	ERx_PageEntities.SerializeDeDuplicationDataModel sddm = new ERx_PageEntities.SerializeDeDuplicationDataModel();
		        	sddm.targetValue = ddm.targetValue;
		        	sddm.operatorType = ddm.operatorType;
			        sddm.compareWith = ddm.compareWith;
			        sddm.sourceField = ddm.sourceField;
			        sddm.filterValue = ddm.filterValue;
			        sddm.filterFieldName = ddm.filterFieldName;
			        sddm.lookupField = ddm.lookupField;
			        sddm.fieldType = ddm.fieldType;
			        sddm.lookupFieldType = ddm.lookupFieldType;
			        contactSerializableFieldList.add(sddm);
		        }
		        curEnv.deduplication_criteria__c = JSON.serialize(filterLogicList);
	        	curEnv.deduplication_condition__c = JSON.serialize(contactSerializableFieldList);
	        }else {
	        	curEnv.deduplication_criteria__c = '';
	        	curEnv.deduplication_condition__c = '';
	        }
	        
	        leadSerializableFieldList = new List<ERx_PageEntities.SerializeDeDuplicationDataModel>();
	        if( !(leadFieldList.size() == 1 && leadFieldList[0].targetValue == '--None--') ){
	            // update data to serialized List Of Lead
		        for(DeDuplicationDataModel ddm : leadFieldList){
		        	if(!ddm.fieldType.equalsIgnoreCase('reference')){
		        		ddm.lookupField = null;
		        		ddm.lookupFieldType = null;
		        	}
		        	ERx_PageEntities.SerializeDeDuplicationDataModel sddm = new ERx_PageEntities.SerializeDeDuplicationDataModel();
		        	sddm.targetValue = ddm.targetValue;
		        	sddm.operatorType = ddm.operatorType;
			        sddm.compareWith = ddm.compareWith;
			        sddm.sourceField = ddm.sourceField;
			        sddm.filterValue = ddm.filterValue;
			        sddm.filterFieldName = ddm.filterFieldName;
			        sddm.lookupField = ddm.lookupField;
			        sddm.fieldType = ddm.fieldType;
			        sddm.lookupFieldType = ddm.lookupFieldType;
			        leadSerializableFieldList.add(sddm);
		        }
		        curEnv.lead_deduplication_criteria__c = JSON.serialize(leadFilterLogicList);
	        	curEnv.lead_deduplication_condition__c = JSON.serialize(leadSerializableFieldList);
	        }else {
	        	curEnv.lead_deduplication_criteria__c = '';
	        	curEnv.lead_deduplication_condition__c = '';
	        }
	        
	        try{
	        	  //PD-4402 || FLS Create - Security check added
		   		    List<String> listFields = new List<String>{'lead_deduplication_criteria__c','lead_deduplication_condition__c','deduplication_criteria__c','deduplication_condition__c'};
					if(SecurityEnforceUtility.checkObjectAccess('Env__c', SecurityTypes.IS_UPDATE, namespacePrefix)){
						if(SecurityEnforceUtility.checkFieldAccess('Env__c', listFields, SecurityTypes.IS_UPDATE, namespacePrefix)){
								update curEnv;
						} else {
							errorMessage = 'Env not retreived for save';
			            	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c  FieldList :: ' + PortalPackageUtility.convertListBySeperator(listFields, ', ')), 'DeDuplicationController', 'saveFilterLogic', null);
							throw new PortalPackageException(permissionErrorMessage); 
						}
					} else {
			        	ERx_PortalPackageLoggerHandler.addPermissionError(('objectName :: Env__c'), 'DeDuplicationController', 'saveFilterLogic', null);
						throw new PortalPackageException(permissionErrorMessage); 
					}
		            errorMessage = '';
		        	successMessage = 'Successfully saved';
		        }
	        catch(Exception e){
	        	errorMessage = 'Permission Error or Env not retreived for save';
	        }
        }
        return null;
    }
    
    /**
    *DeDuplicationDataModel is a wrapper class to store details about the Deduplication criteria and conditions. 
    *It contains targetValue = Value compared
    *operatorType = operator used 
    *comapreWith = value / field
    *sourceField = registration field to be compared with
    *filterValue = static value
    *lookupField = reference field 
    *lookupObjectFields List of Selectoption type  to store field names of lookup field object.
    *List of conditions that is shown on page is stored in the form of DeDuplicationDataModel.
    */
    public class DeDuplicationDataModel{
    	public String targetValue{get;set;} 
        public String operatorType{get;set;}
        public String compareWith{get;set;}
        public String sourceField{get;set;}
        public String filterValue{get;set;}
        public String filterFieldName{get;set;}
        public String lookupField{get;set;}
        public String fieldType{get;set;}
        public String lookupFieldType{get;set;}
        public List<SelectOption> lookupObjectFields{get;set;}
        
        public DeDuplicationDataModel(){
            lookupObjectFields = new List<SelectOption>();
        }
    }
}