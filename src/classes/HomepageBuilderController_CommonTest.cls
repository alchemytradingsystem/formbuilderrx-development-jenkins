@isTest
private class HomepageBuilderController_CommonTest {
	private static Homepage_Widget_Layout__c hwl;
	private static User testUser;
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
		Env__c env=new Env__c();
		env.Version__c=1;
		env.Env_Site_Id__c='123';
		insert env;

        hwl=new Homepage_Widget_Layout__c();
		hwl.Name='testLayoutDante';
		hwl.Displaying_Application_Status__c='Inquiry';
		hwl.Env__c=env.Id;
		insert hwl;

		Homepage_Widget__c hw=new Homepage_Widget__c();
		hw.Name='Test HW';
		hw.Env__c=env.Id;
		insert hw;

        ApexPages.currentPage().getParameters().put('id',hwl.Id);
        ngForceController ngcon=new ngForceController();
        //Pd-1977 test cases 3,6
        HomepageBuilderController_Common con=new HomepageBuilderController_Common(ngcon);
        System.assert(true,con.widgetLibraryDocumentList != null);
        con.initPageException();
		System.assertNotEquals(con, null);
    }
    
     static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        preparetestUser();
        try{
        	system.runAs(testUser){
	        Test.startTest();
		        ERx_Portal_EnvPanelController.isFromAdminPanel = true;
		        ApexPages.currentPage().getParameters().put('id',hwl.Id);
		        ngForceController ngcon=new ngForceController();
		        HomepageBuilderController_Common con=new HomepageBuilderController_Common(ngcon);
				con.initPageException();
				System.assertNotEquals(con, null);
			 Test.stopTest();
	        }
        }catch(Exception e){
        	system.assertEquals(true, e.getMessage().containsIgnoreCase('Insufficient Privileges.'));
        }
        
    }
    
     public static void preparetestUser(){
        Account a;
        Contact student;
        //RecordType rt = [select id,Name from RecordType where SobjectType='Contact' Limit 1];
        a = new Account(name = 'TEST ACCOUNT');
        Database.insert(a);
        //student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact', recordTypeId=rt.id );
        student = new Contact(AccountId = a.Id, firstname='test', lastname='Contact');
        insert student;
        profile testProfile = [select id, name from profile where name='Customer Community Login User'];
        testUser = new User(alias = 'u1', email='u1@testorg.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = testProfile.Id, timezonesidkey='America/Los_Angeles', username='u1@testorg.com',contactId=student.Id);
        insert testUser;
        Env__c env=new Env__c();
		env.Version__c=1;
		env.Env_Site_Id__c='123';
		insert env;
		
        hwl=new Homepage_Widget_Layout__c();
		hwl.Name='testLayoutDante';
		hwl.Displaying_Application_Status__c='Inquiry';
		hwl.Env__c=env.Id;
		insert hwl;
		

		Homepage_Widget__c hw=new Homepage_Widget__c();
		hw.Name='Test HW';
		hw.Env__c=env.Id;
		insert hw;
    }
}