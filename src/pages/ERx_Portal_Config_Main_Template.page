<!-- 
	FormBuilderRx
	@company : Copyright © 2016, Enrollment Rx, LLC
	All rights reserved.
	Redistribution and use in source form, with or without modification, are prohibited without the express written consent of Enrollment Rx, LLC.
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	@website : http://www.enrollmentrx.com/
	@author EnrollmentRx
	@version 1.0
	@date 2016-05-13 
	@description Formbuilder Rx tab page (FormBuilder Admmin Panel)
-->
<apex:page controller="PackageSecurity" action="{!insertCustomSetting}" showHeader="false" standardStylesheets="false" sidebar="false" applyHtmlTag="false" applyBodyTag="false" docType="html-5.0" >
    <head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- Lightning Design System -->
        <!-- <apex:stylesheet value="{!URLFOR($Resource.SLDS0121, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />PD-3814-->
        <apex:stylesheet value="{!URLFOR($Resource.SLDS0122, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
        <!-- Bootstrap SF1 -->
        <!-- 09-08-18 Added by Shubham Re PD-3814 -->
        <apex:outputPanel rendered="{!includeBackwardCompatibilityResource}">
	        <apex:stylesheet value="{!URLFOR($Resource.LDS_BootstrapSF1, 'dist/css/bootstrap.css')}" />
	        <apex:stylesheet value="{!URLFOR($Resource.LDS_BootstrapSF1, 'dist/css/docs.min.css')}" />
        </apex:outputPanel>
        <apex:includeScript value="{!URLFOR($Resource.JQueryANDJQueryUI, 'JQuery/2.0.0-jquery.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.Bootstrap_JS, 'BootstrapJS/3.0.0-bootstrap.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.LDS_BootstrapSF1, 'dist/js/docs.js')}" />
        
        <link href="{!URLFOR($Resource.OpenSansFamilycss, 'OpenSansFamilycss/Open-Sans-Familycss.css')}" rel='stylesheet' type='text/css'/>
        <link rel="stylesheet" href="{!URLFOR($Resource.MainTemplateAsset, 'css/reset.css')}"/> <!-- CSS reset -->
        <link rel="stylesheet" href="{!URLFOR($Resource.MainTemplateAsset, 'css/style.css')}"/> <!-- Resource style -->
    </head>

    <style>
        .section-header {
        padding-left: 5px;
        padding-bottom: 5px;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-top: 10px;
        /*border-bottom: solid 1px #333333;*/
        }


        .section-header h1 {
        font-size: 20px;
        color: #0070D2;
        }

        .components-margin {
        margin: 50px 0;
        }

        .demo--inverse {
        background-color: #16325c;
        padding: 20px;
        }

        .demo-space {
        margin: 20px 0;
        }

        .demo-container .slds-col,
        .demo-container .slds-col--padded,
        .demo-container > div > div,
        .demo-container div, {
        text-align: center;
        line-height: 2rem;
        background-color: #1589ee;
        color: #fff;
        box-shadow: rgba(0,0,0,0.1) 0 0 0 1px inset;
        }

        .demo--alert > div,
        .demo--margin > div {
        margin: 10px 0;
        }

        .headRow{
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-transform: uppercase;
        color: #54698D;
        }

        .slds .section-header h1{
        padding: 1em 0 0.5em 0;
        text-align: left;
        }
        .cd-logo {
        margin: 11px 0 11px 11px;
        }
        .erx-popover {
        background-color: #4f6a92 !important;
        color: white;
        text-align: center;
        }
        .action-button {
        border: none !important;
        height: 2.5rem;
        width: 5.5rem;
        }

        .action-img {
        vertical-align: top !important;
        }
        .action-img {
        vertical-align: top !important;
        }

        .selectedRowCus {
        background-color:#d8dde6;
        }
        .unSelectedRowCus {
        display: none;
        }
        section.erx-border {
        /*min-width: 310px;*/
        background-color: white;
        border-radius: .5rem;
        border: 1px solid rgb(216, 221, 230);
        }
        .main-content-wrapper{
        margin-bottom: 100px;
        margin-left: 20px;
        margin-right: 20px;
        background-color: #f4f6f9;
        }
        .col-Padding{
        padding-right: 2.5rem;
        padding-left: 2.5rem;
        background-color: white;
        }
        .slds .slds-form--horizontal .slds-form-element__control {
        width: calc(83% - (12px * 2));
        }
        .slds .slds-card {
        background-color: #fff;
        }

        .erx-page-messages {
        padding: 1rem;
        color: #FFF;
        background: #C23934 none repeat scroll 0% 0%;
        box-shadow: 2px 2px 5px black;
        border-radius: 3px;
        margin-bottom: 1rem;
        }

        .erx-page-messages img {
        margin-right: 0.75rem;
        }

        .slds-active .slds-tabs--default__link {
        border-color: #0070d2;
        color: #16325c;
        }
        .logo {
        padding-bottom: 2rem;
        }
        
       
    
	

    </style>

    <body>
				
        <!-- navigation menu start -->
        <main class="cd-main-content">
            <div class="main-content-wrapper">

                <div class="slds ">
                    <div class="logo">
                        <a href="/home/home.jsp" class="cd-logo"><img src="{!URLFOR($Resource.MainTemplateAsset, 'img/ERx_Logo.jpg')}" alt="Logo"/></a>
                    </div>
                    
                    <apex:pageMessages id="pgMessage"/>
                    <div style="display:none;" class="noEnvMsg">
                   		<apex:pageMessage summary="You need to select an environment before clicking this tab." severity="error" strength="3" />
                   </div>
                   
                    <!-- <header class="envZone">
<div class="envZone-wrapper" > -->
                    <!-- ATTRIBUTE CHANGE FOR WHETHER USER ISPAID #662 -->
                    <c:ERx_Portal_EnvPanel isPaidUsr="{!isPaid}"/>
                    <!-- </div>
</header>  -->
                    <br/>

                    <!-- <hr/> -->
                    <!-- <c:Portal_Breadcrumbs /> -->
                    
                    <div class="slds-tabs--scoped">
                        <ul class="slds-tabs--scoped__nav" role="tablist">
                        <!-- PD-3601| Gourav | added a new tab -->
                        	<li class="navItem7 slds-tabs--scoped__item slds-text-heading--label" title="GettingStarted" role="presentation">
                            	<!-- change href to window.open to work in lightning -->
                            	<!-- PD-4402 || Reflected XSS changes -->
                            	<a class="slds-tabs--scoped__link" onclick="window.open('GettingStarted?mode={!JSENCODE($CurrentPage.URL)}','_self');" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-scoped-2" id="tab-scoped-2__item">Getting Started </a>
                            </li>
                            <li class="navItem3 slds-tabs--scoped__item slds-text-heading--label" title="Template" role="presentation">
                            	<!-- change href to window.open to work in lightning -->
                            	<a class="slds-tabs--scoped__link" onclick="showLoadingImage();checkToRedirect('Template');" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-scoped-3" id="tab-scoped-2__item">Template Builder</a>
                            </li>
                            <li style="display:none;" class="navItem4 slds-tabs--scoped__item slds-text-heading--label" title="Registration" role="presentation">
                                <!-- change href to window.open to work in lightning -->
                                <a class="slds-tabs--scoped__link" onclick="showLoadingImage();checkToRedirect('Registration');"  role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-scoped-4" id="tab-scoped-2__item">Login Configuration</a>
                            </li>
                            <li style="display:none;" class="navItem6 slds-tabs--scoped__item slds-text-heading--label" title="Homepage" role="presentation">
                                <!-- change href to window.open to work in lightning -->
                                <a class="slds-tabs--scoped__link" onclick="showLoadingImage();checkToRedirect('Homepage');"  role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-scoped-6" id="tab-scoped-3__item">Homepage Builder</a>
                            </li>
                            <li class="navItem5 slds-tabs--scoped__item slds-text-heading--label" title="Page Builder" role="presentation">
                                <!-- change href to window.open to work in lightning -->
                                <a class="slds-tabs--scoped__link"  onclick="showLoadingImage();checkToRedirect('PageBuilder');" role="tab" tabindex="-1" aria-selected="false" aria-controls="tab-scoped-5" id="tab-scoped-2__item">Page Builder</a>
                            </li> 
                        </ul>
                        <div id="tab-scoped-1" class="slds-tabs--scoped__content slds-show" role="tabpanel" aria-labelledby="tab-scoped-1__item">
                            <c:Portal_Breadcrumbs />
                            <apex:outputpanel id="mainBody" layout="block" style="max-height: 500px; overflow-y: auto;">
                                <apex:insert name="body"/>
                            </apex:outputpanel>
                        </div>

                    </div>

                </div>
            </div>
        </main>
        <!-- navigation menu end -->
    </body>
    <script type="text/javascript">

    $(document).ready(function() {
    
        //set active menu based on current page
        if({! CONTAINS($CurrentPage.Name,'GettingStarted')}){
            $(".navItem7").addClass("slds-active");
        }else if({! CONTAINS($CurrentPage.Name,'TM_TemplateContent')}){
            $(".navItem3").addClass("slds-active");
        }else if({!CONTAINS($CurrentPage.Name,'Portal_RegistrationSetting')}){
            $(".navItem4").addClass("slds-active");
        }else if({!CONTAINS($CurrentPage.Name,'Portal_Page_Configure')}){
            $(".navItem5").addClass("slds-active");
        }else if({!CONTAINS($CurrentPage.Name,'HomepageBuilder_ng')}){
            $(".navItem6").addClass("slds-active");
        }else if({!CONTAINS($CurrentPage.Name,'Portal_Home')}){
            $("#bcHeader").hide();
        }

        if($(".slds-no-flex .checkIsCommunity").text().trim().indexOf('Community') >= 0){
            $(".navItem4").css("display","block");
            $(".navItem6").css("display","block");
        }
    });

    $( window ).load(function() {
        if($(".slds-no-flex .checkIsCommunity").text().trim().indexOf('Community') >= 0){
            $(".navItem4").css("display","block");
            $(".navItem6").css("display","block");
        }
        
    });
	
	// 15-09-18 Added By Gourav PD-4060 
	function checkToRedirect(redirectPageName){
		var isRedirect = {!isPageRedirect}; 
		if(isRedirect){
			if(redirectPageName =='Template'){
				window.open('TM_TemplateContent','_self');
			} else if(redirectPageName =='Registration'){
				window.open('Portal_RegistrationSetting','_self');
			} else if(redirectPageName =='Homepage'){
				window.open('HomepageBuilder_ng','_self');
			} else if(redirectPageName =='PageBuilder'){
				window.open('Portal_Page_Configure','_self');
			}
			
			return true;
		} else{
			$(".noEnvMsg").css('display','block');
			hideLoadingImage();
			return false;
		}
		
	}

    </script>

    <style type="text/css">
        .envZone {
        /*position: absolute;*/
        z-index: 2;
        top: 0;
        left: 0;
        /* height: 45px; */
        width: 100%;
        background: transparent;
        box-shadow: 0 1px 0px rgba(0, 0, 0, 0.2);
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;

        }
        .envZone .envZone-wrapper{

        margin-left: 0px;
        padding-top: 0px;
        }
        @media only screen and (min-width: 768px){
        .envZone {
        /*position: fixed;*/
        /* height: 55px; */
        /*margin-left: 110px;*/
        }
        }

        @media only screen and (min-width: 1170px){
        .envZone {
        /*margin-left: 200px;*/
        }
        }
        .cd-side-nav{
        padding-top: 10px !important;
        }
        .content-wrapper{
        padding-top: 10px !important;
        }
        body {
        background-color: #f4f6f9;
        }
    </style>
</apex:page>